!> \file
!> \brief Chemical parameters required for in-cloud oxidation of S(IV).
!        This subroutine provides Henry's law constant and reaction rates.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine equip (ilg,ilev,neqp,ilevi,il1,il2,achpa,atemp)
  !
  use fpdef, only : r8
  !
  implicit none
  !
  real, dimension(ilg,ilev) :: atemp !<
  real(r8), dimension(ilg,ilev,neqp) :: achpa !<
  !
  real(r8) :: temp !<
  integer :: ilg !<
  integer :: ilev !<
  integer :: neqp !<
  integer :: ilevi !<
  integer :: il1 !<
  integer :: il2 !<
  integer :: l !<
  integer :: il !<
  !-----------------------------------------------------------------------
  !
  do l=ilevi,ilev
    do il=il1,il2
      temp=atemp(il,l)
      !
      achpa(il,l,1) = temp*1.0093e-01_r8* &! proc-depend
                        exp(3120._r8*(1._r8/temp-1._r8/298._r8))
      achpa(il,l,2) = temp*1.7158e-03_r8* &! proc-depend
                        exp(5210._r8*(1._r8/temp-1._r8/298._r8))
      achpa(il,l,3) = temp*1.0295e-10_r8* &! proc-depend
                        exp(6330._r8*(1._r8/temp-1._r8/298._r8))
      achpa(il,l,4) = temp*2.5520e-03_r8* &! proc-depend
                        exp(2423._r8*(1._r8/temp-1._r8/298._r8))
      achpa(il,l,5) = temp*1.0973e-09_r8* &! proc-depend
                        exp(1510._r8*(1._r8/temp-1._r8/298._r8))
      achpa(il,l,6) = temp*7.9595e+03_r8* &! proc-depend
                        exp(6600._r8*(1._r8/temp-1._r8/298._r8))
      achpa(il,l,7) = temp*9.4366e-04_r8* &! proc-depend
                        exp(2560._r8*(1._r8/temp-1._r8/298._r8))
      achpa(il,l,8) = temp*206.032e+03_r8* &! proc-depend
                        exp(29.1657_r8*(298._r8/temp-1._r8) &
                        + 16.8322_r8*(1._r8+ &! proc-depend
                        log(298._r8/temp)-298._r8/temp))
      achpa(il,l,9) = temp*8.44851e+09_r8* &! proc-depend
                        exp(34.8536_r8*(298._r8/temp-1._r8) &
                        -5.3930_r8*(1._r8+ &! proc-depend
                        log(298._r8/temp)-298._r8/temp))
      achpa(il,l,10) = temp*161.675e+03_r8* &! proc-depend
                         exp(30.2355_r8*(298._r8/temp-1._r8) &
                         + 19.9083_r8*(1._r8+ &! proc-depend
                         log(298._r8/temp)-298._r8/temp))
      achpa(il,l,11) = 4.4e+11_r8* &! proc-depend
                         exp(-4131._r8/temp)
      achpa(il,l,12) = 2.6e+03_r8* &! proc-depend
                         exp(-966._r8 /temp)
      achpa(il,l,13) = 8.0e+04_r8* &! proc-depend
                         exp(-3650._r8*(1._r8/temp-1._r8/298._r8))
    end do
  end do
  !
end subroutine equip
