!> \file
!> \brief Extraction of interpolation parameters from tabulated growth data.
!>       This will provide the tabulated data in the necessary format for
!>       subsequent calculations.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine cnintp (ibtt,ibtm,ird,yndef)
  !
  use cnparm,  only : idef,idpd,ilarge,ireg,rgpn,spp,tpder,tppnt,xpder,xppnt,ylarge
  use cnparmt, only : itmp,rgpno
  !
  implicit none
  !
  integer, dimension(ireg) :: ibtt !<
  integer, dimension(ireg) :: ibtm !<
  integer :: ird !<
  real :: yndef !<
  !
  !     internal work variables
  !
  integer :: ir1 !<
  integer :: ir2 !<
  integer :: ir !<
  integer :: irat !<
  integer :: ib !<
  integer :: ibe !<
  integer :: ibs !<
  integer :: ibm !<
  integer :: in !<
  integer :: ins !<
  integer :: id !<
  integer :: ir1t !<
  integer :: ir2t !<
  integer :: irg !<
  integer :: imina !<
  integer :: iminb !<
  integer :: irx2m !<
  integer :: idbt !<
  integer :: idrt !<
  integer :: idpdt !<
  integer :: ipnlt !<
  integer :: irx1 !<
  integer :: irx2 !<
  real :: xpdifft !<
  real :: xpdiff !<
  real :: xmax !<
  !
  !-----------------------------------------------------------------------
  !     * allocate work arrays.
  !
  allocate (itmp(ireg) )
  do irg=1,ireg
    allocate (itmp(irg)%ipl(ibtt(irg),ird))
    allocate (itmp(irg)%ipu(ibtt(irg),ird))
    allocate (itmp(irg)%ipd(ibtt(irg),ird))
    allocate (itmp(irg)%ipm(ibtm(irg),ird))
    allocate (itmp(irg)%ipnl(ibtm(irg),ird))
    allocate (itmp(irg)%iplnki(ibtm(irg),ird))
    allocate (itmp(irg)%iplnka(2*rgpno(irg)%irt,ibtm(irg),ird))
    allocate (itmp(irg)%iplnkb(2*rgpno(irg)%irt,ibtm(irg),ird))
    allocate (itmp(irg)%islnka(2*rgpno(irg)%irt,ibtm(irg),ird) )
    allocate (itmp(irg)%islnkb(2*rgpno(irg)%irt,ibtm(irg),ird) )
    allocate (itmp(irg)%ichkb(rgpno(irg)%irt,ibtm(irg),ird))
  end do
  do irg=1,ireg
    !
    !       * initialize work arrays.
    !
    itmp(irg)%ipl(:,:)=idef
    itmp(irg)%ipu(:,:)=idef
    itmp(irg)%ipd(:,:)=idef
    itmp(irg)%ipm(:,:)=idef
    itmp(irg)%ipnl(:,:)=0
    itmp(irg)%iplnki(:,:)=idef
    itmp(irg)%iplnka(:,:,:)=idef
    itmp(irg)%iplnkb(:,:,:)=idef
    itmp(irg)%ichkb(:,:,:)=.false.
    !
    !       * determine index of points on lower boundary.
    !
    do irat=1,ird
      do ib=1,ibtt(irg)
        do ir=1,rgpno(irg)%irt
          if (nint(rgpno(irg)%tp(ir,ib,irat)-yndef) /= 0 &
              .and. itmp(irg)%ipl(ib,irat) == idef) then
            itmp(irg)%ipl(ib,irat)=ir
          end if
        end do
      end do
    end do
    !
    !       * determine index of points on upper boundary.
    !
    do irat=1,ird
      do ib=1,ibtt(irg)
        do ir=rgpno(irg)%irt,1,-1
          if (nint(rgpno(irg)%tp(ir,ib,irat)-yndef) /= 0 &
              .and. itmp(irg)%ipu(ib,irat) == idef) then
            itmp(irg)%ipu(ib,irat)=ir
          end if
        end do
      end do
    end do
    !
    !       * number of valid points.
    !
    do irat=1,ird
      do ib=1,ibtt(irg)
        if (itmp(irg)%ipl(ib,irat) /= idef &
            .and. itmp(irg)%ipu(ib,irat) /= idef) then
          itmp(irg)%ipd(ib,irat)=itmp(irg)%ipu(ib,irat) &
                                  -itmp(irg)%ipl(ib,irat)+1
        end if
      end do
    end do
    !
    !       * number of valid points for interpolation between 2 adjacent rows.
    !
    do irat=1,ird
      do ib=1,ibtm(irg)
        if (itmp(irg)%ipd(ib,irat) /= idef &
            .and. itmp(irg)%ipd(ib+1,irat) /= idef) then
          if (itmp(irg)%ipd(ib,irat) &
              >= itmp(irg)%ipd(ib+1,irat) ) then
            itmp(irg)%iplnki(ib,irat)=0
            itmp(irg)%ipm(ib,irat)=itmp(irg)%ipd(ib,irat)
          else
            itmp(irg)%iplnki(ib,irat)=1
            itmp(irg)%ipm(ib,irat)=itmp(irg)%ipd(ib+1,irat)
          end if
        end if
      end do
    end do
    !
    !       * establish link between nearest points for adjacent rows.
    !       * these links start at points that are part of the row that
    !       * has the largest total number of points and connect to the
    !       * points in the adjacent row so that the distance between
    !       * the points will be at its minimum.
    !
    do irat=1,ird
      do ib=1,ibtm(irg)
        in=itmp(irg)%iplnki(ib,irat)
        if (in /= idef) then
          ibs=ib+in
          ibe=ib+1-in
          do ir1=itmp(irg)%ipl(ibs,irat),itmp(irg)%ipu(ibs,irat)
            xpdiff=ylarge
            do ir2=itmp(irg)%ipl(ibe,irat),itmp(irg)%ipu(ibe,irat)
              xpdifft=abs(rgpno(irg)%xp(ir1,ibs,irat) &
                                          -rgpno(irg)%xp(ir2,ibe,irat))
              if (xpdifft < xpdiff) then
                ir1t=ir1
                ir2t=ir2
                xpdiff=xpdifft
              end if
            end do
            itmp(irg)%ipnl(ib,irat)=itmp(irg)%ipnl(ib,irat)+1
            ipnlt=itmp(irg)%ipnl(ib,irat)
            itmp(irg)%iplnka(ipnlt,ib,irat)=ir1t
            itmp(irg)%iplnkb(ipnlt,ib,irat)=ir2t
            itmp(irg)%ichkb(ir2t,ib,irat)=.true.
          end do
        end if
      end do
    end do
    !
    !       * check if there are any points that have not yet been connected
    !       * in the row that contains fewer points. add new links for these
    !       * points to the array that contains the previous links.
    !
    do irat=1,ird
      do ib=1,ibtm(irg)
        in=itmp(irg)%iplnki(ib,irat)
        if (in /= idef) then
          ibs=ib+in
          ibe=ib+1-in
          do ir2=itmp(irg)%ipl(ibe,irat),itmp(irg)%ipu(ibe,irat)
            if ( .not.itmp(irg)%ichkb(ir2,ib,irat) ) then
              xpdiff=ylarge
              do ir1=itmp(irg)%ipl(ibs,irat),itmp(irg)%ipu(ibs,irat)
                xpdifft=abs(rgpno(irg)%xp(ir1,ibs,irat) &
                                          -rgpno(irg)%xp(ir2,ibe,irat))
                if (xpdifft < xpdiff) then
                  ir1t=ir1
                  ir2t=ir2
                  xpdiff=xpdifft
                end if
              end do
              itmp(irg)%ipnl(ib,irat)=itmp(irg)%ipnl(ib,irat)+1
              ipnlt=itmp(irg)%ipnl(ib,irat)
              itmp(irg)%iplnka(ipnlt,ib,irat)=ir1t
              itmp(irg)%iplnkb(ipnlt,ib,irat)=ir2t
            end if
          end do
        end if
      end do
    end do
    !
    !       * sort links in ascending order for grid point indices.
    !
    do irat=1,ird
      do ib=1,ibtm(irg)
        in=itmp(irg)%iplnki(ib,irat)
        if (in /= idef) then
          do irx1=1,itmp(irg)%ipnl(ib,irat)
            imina=ilarge
            iminb=ilarge
            do irx2=1,itmp(irg)%ipnl(ib,irat)
              if (itmp(irg)%iplnka(irx2,ib,irat) <= imina &
                  .and. itmp(irg)%iplnkb(irx2,ib,irat)  < iminb) then
                imina=itmp(irg)%iplnka(irx2,ib,irat)
                iminb=itmp(irg)%iplnkb(irx2,ib,irat)
                irx2m=irx2
              end if
            end do
            itmp(irg)%islnka(irx1,ib,irat)= &
                                              itmp(irg)%iplnka(irx2m,ib,irat)
            itmp(irg)%islnkb(irx1,ib,irat)= &
                                              itmp(irg)%iplnkb(irx2m,ib,irat)
            itmp(irg)%iplnka(irx2m,ib,irat)=ilarge
            itmp(irg)%iplnkb(irx2m,ib,irat)=ilarge
          end do
        end if
      end do
    end do
  end do
  !
  !     * allocate output arrays.
  !
  allocate (rgpn(ireg) )
  do irg=1,ireg
    allocate (rgpn(irg)%kopen(ibtm(irg),ird) )
    allocate (rgpn(irg)%ipnts(ibtm(irg),ird) )
    allocate (rgpn(irg)%xp(ibtm(irg),ird) )
    allocate (rgpn(irg)%tp(ibtm(irg),ird) )
    do irat=1,ird
      do ib=1,ibtm(irg)
        in=itmp(irg)%ipm(ib,irat)
        if (in /= idef) then
          ins=itmp(irg)%ipnl(ib,irat)
          allocate (rgpn(irg)%xp(ib,irat)%pnt(ins))
          allocate (rgpn(irg)%xp(ib,irat)%der(ins))
          allocate (rgpn(irg)%tp(ib,irat)%pnt(ins))
          allocate (rgpn(irg)%tp(ib,irat)%der(ins))
        end if
      end do
    end do
  end do

  !
  !     * initialize flag that indicates whether upper boundary of
  !     * growth domain (at large particle sizes) is an open or closed
  !     * boundary. the default value is for a closed boundary.
  !
  do irg=1,ireg
    do irat=1,ird
      do ib=1,ibtm(irg)
        rgpn(irg)%kopen(ib,irat)=.false.
      end do
    end do
  end do
  !
  !     * specify boundary at large particle sizes for growth domains
  !     * 1 and 3 to be open. for domain 1, only the region corresponding
  !     * to sufficiently large supersaturation (i.e. greater than the
  !     * critical supersaturation) is considered.
  !
  do irat=1,ird
    ibm=spp(irat)%ibs
    if (ibm /= idef) then
      !
      !         * for supersaturation greater than the critical value,
      !         * domain 1 has an open boundary.
      !
      do ib=1,ibm-1
        rgpn(1)%kopen(ib,irat)=.true.
      end do
      !
      !         * domain 3 is always open at its upper boundary for the
      !         * scaled particle size.
      !
      do ib=1,ibtm(3)
        rgpn(3)%kopen(ib,irat)=.true.
      end do
    end if
    !
    !       * domain 1 could also have an open boundary if the boundary
    !       * for the largest particle sizes is for a size that is smaller
    !       * than the critical size and if the supersaturation is greater
    !       * than the equilibrium supersaturation. in that case, the values
    !       * for xp will all be maximal.
    !
    xmax=-ylarge
    do ib=1,ibtm(1)
      xmax=max(rgpno(1)%xp(rgpno(1)%irt,ib,irat),xmax)
    end do
    do ib=1,ibtm(1)
      if (rgpno(1)%xp(rgpno(1)%irt,ib,irat) == xmax &
          .and. nint(rgpno(1)%tp(rgpno(1)%irt,ib  ,irat)-yndef) /= 0 &
          .and. nint(rgpno(1)%tp(rgpno(1)%irt,ib+1,irat)-yndef) /= 0 &
          ) then
        rgpn(1)%kopen(ib,irat)=.true.
      end if
    end do
  end do
  idbt=0
  idrt=0
  idpdt=0
  do irg=1,ireg
    idbt=max(idbt,ibtm(irg))
    !
    !       * initialize output arrays for scaled particle size and growth
    !       * times.
    !
    do irat=1,ird
      do ib=1,ibtm(irg)
        in=itmp(irg)%ipm(ib,irat)
        if (in /= idef) then
          ins=itmp(irg)%ipnl(ib,irat)
          rgpn(irg)%xp(ib,irat)%pnt(1:ins)=yndef
          rgpn(irg)%tp(ib,irat)%pnt(1:ins)=yndef
          rgpn(irg)%xp(ib,irat)%der(1:ins)=yndef
          rgpn(irg)%tp(ib,irat)%der(1:ins)=yndef
        end if
      end do
    end do
    !
    !       * save number of valid points to corresponding output arrays.
    !
    do irat=1,ird
      do ib=1,ibtm(irg)
        in=itmp(irg)%ipm(ib,irat)
        if (in /= idef) then
          ins=itmp(irg)%ipnl(ib,irat)
          rgpn(irg)%ipnts(ib,irat)=ins
        else
          rgpn(irg)%ipnts(ib,irat)=0
        end if
      end do
    end do
    rgpn(irg)%ibt=ibtm(irg)
    !
    !       * extract points xp and tp.
    !
    do irat=1,ird
      do ib=1,ibtm(irg)
        in=itmp(irg)%iplnki(ib,irat)
        if (in /= idef) then
          ibs=ib+in
          ins=itmp(irg)%ipnl(ib,irat)
          do ir=1,ins
            ir1=itmp(irg)%islnka(ir,ib,irat)
            rgpn(irg)%xp(ib,irat)%pnt(ir)=rgpno(irg)%xp(ir1,ibs,irat)
            rgpn(irg)%tp(ib,irat)%pnt(ir)=rgpno(irg)%tp(ir1,ibs,irat)
          end do
        end if
      end do
    end do
    !
    !       * determine derivatives (d xp/d bs and d tp/d bs).
    !
    do irat=1,ird
      do ib=1,ibtm(irg)
        in=itmp(irg)%iplnki(ib,irat)
        if (in /= idef) then
          ibs=ib+in
          ibe=ib+1-in
          ins=itmp(irg)%ipnl(ib,irat)
          do ir=1,ins
            ir1=itmp(irg)%islnka(ir,ib,irat)
            ir2=itmp(irg)%islnkb(ir,ib,irat)
            rgpn(irg)%xp(ib,irat)%der(ir)= &
                                             (rgpno(irg)%xp(ir2,ibe,irat)-rgpno(irg)%xp(ir1,ibs,irat)) &
                                             /(rgpno(irg)%bs(ibe,irat)-rgpno(irg)%bs(ibs,irat))
            rgpn(irg)%tp(ib,irat)%der(ir)= &
                                             (rgpno(irg)%tp(ir2,ibe,irat)-rgpno(irg)%tp(ir1,ibs,irat)) &
                                             /(rgpno(irg)%bs(ibe,irat)-rgpno(irg)%bs(ibs,irat))
            idpdt=idpdt+1
          end do
          idrt=max(idrt,ins)
        end if
      end do
    end do
  end do
  !
  !     * overwrite results for region 1 to account for critical size.
  !     * this is necessary because the interpolation performed previously
  !     * does not provide the necessary step-wise change in bs-direction.
  !     * however, this can be achieved by moving all points in xp-direction
  !     * to the ciritical size at the bs-coordinate that corresponds to
  !     * the critical supersaturation. the interpolation is then done
  !     * starting at these points to the next row of points which have
  !     * a supersaturation below the critical value.
  !
  do irat=1,ird
    ibm=spp(irat)%ibs
    if (ibm /= idef) then
      in=itmp(1)%iplnki(ibm,irat)
      if (in /= idef) then
        ibs=ibm+in
        ibe=ib+1-in
        ins=itmp(1)%ipnl(ibm,irat)
        do ir=1,ins
          if (rgpn(1)%xp(ibm,irat)%pnt(ir) > spp(irat)%xp) then
            ir2=itmp(1)%islnkb(ir,ibm,irat)
            rgpn(1)%xp(ibm,irat)%pnt(ir)=spp(irat)%xp
            rgpn(1)%tp(ibm,irat)%pnt(ir)=spp(irat)%tp
            rgpn(1)%xp(ibm,irat)%der(ir)= &
                                              (rgpno(1)%xp(ir2,ibe,irat)-spp(irat)%xp) &
                                              /(rgpno(1)%bs(ibe,irat)-rgpno(1)%bs(ibs,irat))
            rgpn(1)%tp(ibm,irat)%der(ir)= &
                                              (rgpno(1)%tp(ir2,ibe,irat)-spp(irat)%tp) &
                                              /(rgpno(1)%bs(ibe,irat)-rgpno(1)%bs(ibs,irat))
          end if
        end do
      end if
    end if
  end do
  do irg=1,ireg
    !
    !       * reorder terms to produce simple linear expressions for
    !       * extrapolation.
    !
    do irat=1,ird
      do ib=1,ibtm(irg)
        in=itmp(irg)%iplnki(ib,irat)
        if (in /= idef) then
          ibs=ib+in
          ins=itmp(irg)%ipnl(ib,irat)
          do ir=1,ins
            rgpn(irg)%xp(ib,irat)%pnt(ir)= &
                                             rgpn(irg)%xp(ib,irat)%pnt(ir) &
                                             -rgpn(irg)%xp(ib,irat)%der(ir)*rgpno(irg)%bs(ibs,irat)
            rgpn(irg)%tp(ib,irat)%pnt(ir)= &
                                             rgpn(irg)%tp(ib,irat)%pnt(ir) &
                                             -rgpn(irg)%tp(ib,irat)%der(ir)*rgpno(irg)%bs(ibs,irat)
          end do
        end if
      end do
    end do
  end do
  !
  !     * copy results for reference point (pnt) and derivative (der)
  !     * into holding arrays. the use of holding arrays will speed
  !     * up the look-up of the results from memory in subsequent
  !     * calculations.
  !
  if (idpdt < 1 .or. idbt < 1 .or. idrt < 1) &
      call xit('CNINTP',-1)
  allocate(xppnt(idpdt))
  allocate(xpder(idpdt))
  allocate(tppnt(idpdt))
  allocate(tpder(idpdt))
  allocate(idpd(ireg,ird,idbt,idrt))
  idpd=idef
  id=0
  do irg=1,ireg
    do irat=1,ird
      do ib=1,ibtm(irg)
        in=itmp(irg)%iplnki(ib,irat)
        if (in /= idef) then
          ins=itmp(irg)%ipnl(ib,irat)
          do ir=1,ins
            id=id+1
            xppnt(id)=rgpn(irg)%xp(ib,irat)%pnt(ir)
            xpder(id)=rgpn(irg)%xp(ib,irat)%der(ir)
            tppnt(id)=rgpn(irg)%tp(ib,irat)%pnt(ir)
            tpder(id)=rgpn(irg)%tp(ib,irat)%der(ir)
            idpd(irg,irat,ib,ir)=id
          end do
        end if
      end do
    end do
  end do
  !
  !     * deallocate temporary arrays.
  !
  deallocate(itmp)
  !
end subroutine cnintp
