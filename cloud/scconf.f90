!> \file
!> \brief Initialization of basic parameters for aerosol calculations with
! i>      pla method for activation calculations.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine scconf
  !
  use scparm, only : cedryr,cedryrb,cedryrc,ceis,ceisvl,ceisvr,cekx,cephi, &
                         cidryr,cidryrb,cidryrc,ciis,ciisvl,ciisvr,ciphi, &
                         isesc,isext,isextb,isint,isintb
  use sdparm, only : aextf,aintf,iswext,iswint,kwext,kwint,r0,sextf,sintf
  !
  implicit none
  !
  integer :: is !<
  integer :: isc !<
  integer :: ist !<
  integer :: isi !<
  integer :: isx !<
  integer :: kx !<
  integer :: kxp !<
  real :: dphic !<
  real :: dphi !<
  !
  !-----------------------------------------------------------------------
  !
  !     * total number of aerosol sections for evaporation/condensation
  !     * calculations.
  !
  isext=isesc*iswext
  isextb=isext+kwext
  isint=isesc*iswint
  isintb=isint+kwint
  !
  !     * allocate arrays that contain information about the section
  !     * boundaries, sizes, and densities for wettable aerosols for
  !     * grid-cell invariant information.
  !
  if (isext > 0) then
    if (allocated(cephi) ) deallocate(cephi)
    allocate(cephi  (isext))
    if (allocated(cedryr) ) deallocate(cedryr)
    allocate(cedryr (isext))
    if (allocated(cedryrc) ) deallocate(cedryrc)
    allocate(cedryrc(isext))
    if (allocated(ceisvl) ) deallocate(ceisvl)
    allocate(ceisvl (isext))
    if (allocated(ceisvr) ) deallocate(ceisvr)
    allocate(ceisvr (isext))
  end if
  if (isextb > 0) then
    if (allocated(cedryrb) ) deallocate(cedryrb)
    allocate(cedryrb(isextb))
    if (allocated(cekx) ) deallocate(cekx)
    allocate(cekx   (isextb))
    if (allocated(ceis) ) deallocate(ceis)
    allocate(ceis   (isextb))
  end if
  if (isint > 0) then
    if (allocated(ciphi) ) deallocate(ciphi)
    allocate(ciphi  (isint))
    if (allocated(cidryr) ) deallocate(cidryr)
    allocate(cidryr (isint))
    if (allocated(cidryrc) ) deallocate(cidryrc)
    allocate(cidryrc(isint))
    if (allocated(ciisvl) ) deallocate(ciisvl)
    allocate(ciisvl (isint))
    if (allocated(ciisvr) ) deallocate(ciisvr)
    allocate(ciisvr (isint))
  end if
  if (isintb > 0) then
    if (allocated(cidryrb) ) deallocate(cidryrb)
    allocate(cidryrb(isintb))
    if (allocated(ciis) ) deallocate(ciis)
    allocate(ciis   (isintb))
  end if
  !
  !     * calculate sizes for dry size distribution for externally mixed
  !     * aerosol types.
  !
  isc=0
  do is=1,iswext
    isi=sextf%iswet(is)%isi
    kx=sextf%iswet(is)%ityp
    dphi=aextf%tp(kx)%dpstar
    dphic=dphi/real(isesc)
    isc=isc+1
    cephi(isc)%vl=aextf%tp(kx)%phi(isi)%vl
    cephi(isc)%vr=aextf%tp(kx)%phi(isi)%vl+dphic
    do ist=2,isesc
      isc=isc+1
      cephi(isc)%vl=cephi(isc-1)%vr
      cephi(isc)%vr=cephi(isc-1)%vr+dphic
    end do
  end do
  !
  !     * calculate sizes for dry size distribution for internally mixed
  !     * aerosol types.
  !
  isc=0
  do is=1,iswint
    isi=sintf%iswet(is)%isi
    dphi=aintf%dpstar
    dphic=dphi/real(isesc)
    isc=isc+1
    ciphi(isc)%vl=aintf%phi(isi)%vl
    ciphi(isc)%vr=aintf%phi(isi)%vl+dphic
    do ist=2,isesc
      isc=isc+1
      ciphi(isc)%vl=ciphi(isc-1)%vr
      ciphi(isc)%vr=ciphi(isc-1)%vr+dphic
    end do
  end do
  !
  !     * calculate radii for dry size distribution for externally mixed
  !     * aerosol types. the first group of particle sizes (dryr, dryrc)
  !     * are for each section while the second group (dryrb) is for
  !     * section boundaries.
  !
  do isc=1,isext
    cedryr(isc)%vl=r0*exp(cephi(isc)%vl)
    cedryr(isc)%vr=r0*exp(cephi(isc)%vr)
  end do
  do isc=1,isext
    dphic=cephi(isc)%vr-cephi(isc)%vl
    cedryrc(isc)=r0*exp(cephi(isc)%vl+.5*dphic)
  end do
  if (isextb > 0) then
    isc=0
    isx=0
    do is=1,iswext-1
      kx=sextf%iswet(is)%ityp
      kxp=sextf%iswet(is+1)%ityp
      do ist=1,isesc
        isc=isc+1
        isx=isx+1
        cedryrb(isc)=cedryr(isx)%vl
      end do
      if (kxp /= kx) then
        isc=isc+1
        cedryrb(isc)=cedryr(isx)%vr
      end if
    end do
    do ist=1,isesc
      isc=isc+1
      isx=isx+1
      cedryrb(isc)=cedryr(isx)%vl
    end do
    isc=isc+1
    cedryrb(isc)=cedryr(isx)%vr
  end if
  !
  !     * calculate radii for dry size distribution for internally mixed
  !     * aerosol types.
  !
  do isc=1,isint
    cidryr(isc)%vl=r0*exp(ciphi(isc)%vl)
    cidryr(isc)%vr=r0*exp(ciphi(isc)%vr)
  end do
  do isc=1,isint
    dphic=ciphi(isc)%vr-ciphi(isc)%vl
    cidryrc(isc)=r0*exp(ciphi(isc)%vl+.5*dphic)
  end do
  if (isintb > 0) then
    isc=0
    isx=0
    do is=1,iswint-1
      do ist=1,isesc
        isc=isc+1
        isx=isx+1
        cidryrb(isc)=cidryr(isx)%vl
      end do
    end do
    do ist=1,isesc
      isc=isc+1
      isx=isx+1
      cidryrb(isc)=cidryr(isx)%vl
    end do
    isc=isc+1
    cidryrb(isc)=cidryr(isx)%vr
  end if
  !
  !     * aerosol type index (cekx), corresponding section index (ceis),
  !     * lower section boundary index (ceisvl), and upper section boundary
  !     * index (ceisvr) for section boundaries for externally mixed types
  !     * of aerosol.
  !
  if (isextb > 0) then
    isc=0
    isx=0
    do is=1,iswext-1
      kx=sextf%iswet(is)%ityp
      kxp=sextf%iswet(is+1)%ityp
      do ist=1,isesc
        isc=isc+1
        cekx(isc)=kx
        ceis(isc)=is
        isx=isx+1
        ceisvl(isx)=isc
        ceisvr(isx)=isc+1
      end do
      if (kxp /= kx) then
        isc=isc+1
        cekx(isc)=kx
        ceis(isc)=is
      end if
    end do
    is=iswext
    kx=sextf%iswet(is)%ityp
    do ist=1,isesc
      isc=isc+1
      cekx(isc)=kx
      ceis(isc)=is
      isx=isx+1
      ceisvl(isx)=isc
      ceisvr(isx)=isc+1
    end do
    isc=isc+1
    cekx(isc)=kx
    ceis(isc)=is
  end if
  !
  !     * section index (ciis), lower section boundary index (ciisvl),
  !     * and upper section boundary index (ciisvr) for section
  !     * boundaries for internally mixed types of aerosol.
  !
  if (isintb > 0) then
    isc=0
    do is=1,iswint
      do ist=1,isesc
        isc=isc+1
        ciis(isc)=is
        ciisvl(isc)=isc
        ciisvr(isc)=isc+1
      end do
    end do
    is=iswint
    isc=isc+1
    ciis(isc)=is
  end if
  !
end subroutine scconf
!> \file
!! \subsection ssec_details Details
!! This subroutine needs to be called after SDCONF.
