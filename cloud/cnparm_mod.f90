!> \file
!> \brief Parameters for CCN calculations, relevant to tabulated growth
!>       data.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
module cnparm
  !
  use cnparmb, only : pntdp,reg
  !
  implicit none
  !
  integer :: irdp !<
  integer :: ibtmax !<
  integer :: ina !<
  real, allocatable, dimension(:) :: ratp !<
  real, allocatable, dimension(:) :: bscp !<
  real, allocatable, dimension(:) :: bsminp !<
  real, allocatable, dimension(:) :: bsmaxp !<
  real, allocatable, dimension(:) :: bsminn !<
  real, allocatable, dimension(:) :: bsmaxn !<
  real, allocatable, dimension(:) :: xpmin !<
  real, allocatable, dimension(:) :: xpmax !<
  real, allocatable, dimension(:) :: rpmin !<
  real, allocatable, dimension(:) :: rpmax !<
  type(pntdp), allocatable, dimension(:) :: rgb
  type(reg), allocatable, dimension(:) :: rgpn
  type spclp
    integer :: ibs !<
    real :: xp !<
    real :: tp !<
    real :: bs !<
  end type spclp
  type(spclp), allocatable, dimension(:) :: spp
  real, allocatable, dimension(:) :: xppnt !<
  real, allocatable, dimension(:) :: xpder !<
  real, allocatable, dimension(:) :: tppnt !<
  real, allocatable, dimension(:) :: tpder !<
  real, allocatable, dimension(:) :: xppntl !<
  real, allocatable, dimension(:) :: xppntu !<
  real, allocatable, dimension(:) :: xpderl !<
  real, allocatable, dimension(:) :: xpderu !<
  real, allocatable, dimension(:) :: rbsb !<
  integer, allocatable, dimension(:,:,:) :: idpu !<
  integer, allocatable, dimension(:,:,:) :: idpl !<
  integer, allocatable, dimension(:,:,:) :: idbsb !<
  integer, allocatable, dimension(:,:,:,:) :: idpd !<
  !
  real, parameter :: ypi = 3.141592653589793 !<
  real, parameter :: ysmall=9.e-30 !<
  real, parameter :: sqrt2=1.414213562373095 !<
  real, parameter :: sqrt2p3=sqrt2**3 !<
  real, parameter :: yna=-99999. !<
  integer, parameter :: ilarge= 9999999 !<
  integer, parameter :: ismall=-9999999 !<
  integer, parameter :: ireg=5 !<
  integer, parameter :: idef=-1 !<
  !
  real :: ylarge !<
  !
end module cnparm
