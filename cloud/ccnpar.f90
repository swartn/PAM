!> \file
!> \brief Critical particle sizes and supersaturation of CCN's from
!>       Koehler theory.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine ccnpar(cerc,cesc,circ,cisc,temp,ceddnsl,ceddnis, &
                        cenuio,cekappa,cemolw,ciddnsl,ciddnis,cinuio, &
                        cikappa,cimolw,ciepsm,sv,ilga,leva, &
                        iexkap,iinkap)
  !
  use scparm, only : cedryrb,cidryrb,isextb,isintb
  use cnparm, only : yna,ypi,ysmall
  use sdphys, only : rgasm,rhoh2o,wh2o
  !
  implicit none
  !
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  real, intent(in), dimension(ilga,leva,isextb) :: ceddnsl !<
  real, intent(in), dimension(ilga,leva,isextb) :: ceddnis !<
  real, intent(in), dimension(ilga,leva,isextb) :: cenuio !<
  real, intent(in), dimension(ilga,leva,isextb) :: cekappa !<
  real, intent(in), dimension(ilga,leva,isextb) :: cemolw !<
  real, intent(in), dimension(ilga,leva,isintb) :: ciddnsl !<
  real, intent(in), dimension(ilga,leva,isintb) :: ciddnis !<
  real, intent(in), dimension(ilga,leva,isintb) :: cinuio !<
  real, intent(in), dimension(ilga,leva,isintb) :: cikappa !<
  real, intent(in), dimension(ilga,leva,isintb) :: cimolw !<
  real, intent(in), dimension(ilga,leva,isintb) :: ciepsm !<
  real, intent(in), dimension(ilga,leva) :: temp !<
  real, intent(in), dimension(ilga,leva) :: sv !<
  real, intent(out), dimension(ilga,leva,isextb) :: cerc !<
  real, intent(out), dimension(ilga,leva,isextb) :: cesc !<
  real, intent(out), dimension(ilga,leva,isintb) :: circ !<
  real, intent(out), dimension(ilga,leva,isintb) :: cisc !<
  real, dimension(ilga,leva) :: sfctw !<
  real, dimension(ilga,leva) :: ak !<
  real, dimension(ilga,leva) :: svt !<
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:,:) :: cibk !<
  real, allocatable, dimension(:,:,:) :: cebk !<
  real, allocatable, dimension(:,:,:) :: citmp !<
  real, allocatable, dimension(:,:,:) :: cetmp !<
  integer :: il !<
  integer :: is !<
  integer :: l !<
  integer :: iinkap !<
  integer :: iexkap !<
  real :: arat !<
  real :: amsl !<
  real :: ans !<
  real :: coru !<
  !
  !-----------------------------------------------------------------------
  !
  !     * allocate temporary arrays.
  !
  if (isextb > 0) then
    allocate(cebk (ilga,leva,isextb))
    allocate(cetmp(ilga,leva,isextb))
  end if
  if (isintb > 0) then
    allocate(cibk (ilga,leva,isintb))
    allocate(citmp(ilga,leva,isintb))
  end if
  !
  !-----------------------------------------------------------------------
  !     * surface tension of water.
  !
  sfctw=0.0761-1.55e-04*(temp-273.)
  !
  !     * a-term in koehler equation (for curvature effect).
  !
  ak=2.*wh2o*sfctw/(rgasm*temp*rhoh2o)
  !
  !     * b-term in koehler equation (for solute effect).
  !
  if (isextb > 0) then
    cebk=yna
    if (iexkap /= 1) then
      coru=1.
      do is=1,isextb
        do l=1,leva
          do il=1,ilga
            !
            !           * number of moles of solute.
            !
            amsl=(4.*ypi/3.)*ceddnsl(il,l,is)*cedryrb(is)**3
            ans=amsl*coru*cenuio(il,l,is)/cemolw(il,l,is)
            !
            !           * b-term in koehler equation (for solute effect).
            !
            cebk(il,l,is)=0.75*ans*wh2o/(ypi*rhoh2o)
          end do
        end do
      end do
    else
      do is=1,isextb
        do l=1,leva
          do il=1,ilga
            !
            !           * b-term in koehler equation (for solute effect).
            !
            cebk(il,l,is)=cekappa(il,l,is)*cedryrb(is)**3
          end do
        end do
      end do
    end if
  end if
  if (isintb > 0) then
    cibk=yna
    if (iinkap /= 1) then
      do is=1,isintb
        do l=1,leva
          do il=1,ilga
            if (abs(ciepsm(il,l,is)-yna) > ysmall &
                .and. ciepsm(il,l,is) > ysmall) then
              arat=(1.-ciepsm(il,l,is))/ciepsm(il,l,is)
              if (arat > ysmall) then
                coru=1./(1.+(ciddnsl(il,l,is)/ciddnis(il,l,is))*arat)
              else
                coru=1.
              end if
              !
              !             * number of moles of solute.
              !
              amsl=(4.*ypi/3.)*ciddnsl(il,l,is)*cidryrb(is)**3
              ans=amsl*coru*cinuio(il,l,is)/cimolw(il,l,is)
              !
              !             * b-term in koehler equation (for solute effect).
              !
              cibk(il,l,is)=0.75*ans*wh2o/(ypi*rhoh2o)
            end if
          end do
        end do
      end do
    else
      do is=1,isintb
        do l=1,leva
          do il=1,ilga
            if (abs(cikappa(il,l,is)-yna) > ysmall &
                .and. cikappa(il,l,is) > ysmall &
                .and. abs(ciepsm(il,l,is)-yna) > ysmall &
                .and. ciepsm(il,l,is) > ysmall) then
              arat=(1.-ciepsm(il,l,is))/ciepsm(il,l,is)
              if (arat > ysmall) then
                coru=1./(1.+(ciddnsl(il,l,is)/ciddnis(il,l,is))*arat)
              else
                coru=1.
              end if
              !
              !             * b-term in koehler equation (for solute effect).
              !
              cibk(il,l,is)=cikappa(il,l,is)*coru*cidryrb(is)**3
            end if
          end do
        end do
      end do
    end if
  end if
  !
  !-----------------------------------------------------------------------
  !     * critical particle radius and supersaturation.
  !
  if (isextb > 0) then
    do is=1,isextb
      where (abs(cebk(:,:,is)-yna) > ysmall)
        cetmp(:,:,is)=3.*cebk(:,:,is)/ak(:,:)
      else where
        cetmp(:,:,is)=0.
      end where
    end do
    cerc=sqrt(cetmp)
    where (cerc <= ysmall)
      cebk=yna
    end where
    do is=1,isextb
      where (abs(cebk(:,:,is)-yna) > ysmall)
        cesc(:,:,is)=2.*ak(:,:)/(3.*cerc(:,:,is))
      else where
        cerc(:,:,is)=yna
        cesc(:,:,is)=yna
      end where
    end do
  end if
  if (isintb > 0) then
    do is=1,isintb
      where (abs(cibk(:,:,is)-yna) > ysmall)
        citmp(:,:,is)=3.*cibk(:,:,is)/ak(:,:)
      else where
        citmp(:,:,is)=0.
      end where
    end do
    circ=sqrt(citmp)
    where (circ <= ysmall)
      cibk=yna
    end where
    do is=1,isintb
      where (abs(cibk(:,:,is)-yna) > ysmall)
        cisc(:,:,is)=2.*ak(:,:)/(3.*circ(:,:,is))
      else where
        circ(:,:,is)=yna
        cisc(:,:,is)=yna
      end where
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * deallocate temporary arrays.
  !
  if (isextb > 0) then
    deallocate(cebk)
    deallocate(cetmp)
  end if
  if (isintb > 0) then
    deallocate(cibk)
    deallocate(citmp)
  end if
  !
end subroutine ccnpar
