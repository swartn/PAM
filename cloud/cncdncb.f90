!> \file
!> \brief Diagnosis of cloud droplet concentration (CDNC) based on wet size
!>       distribution and threshold droplet size. The CDNC is calculated
!>       as the cumulative droplet concentration for all droplets bigger
!>       than the threshold size.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine cncdncb(cecdnc,cicdnc,cewetrb,ciwetrb,pen0,pephi0, &
                         pepsi,pephis0,pedphi0,pin0,piphi0,pipsi, &
                         piphis0,pidphi0,ilga,leva)
  !
  use sdparm, only : aextf,ina,isaext,isaint,iswext,iswint, &
                         kext,kint,r0,sextf,sintf,ylarge,yna,ytiny
  use sdcode, only : sdint0
  use scparm, only : cedryrb,ceis,cekx,cidryrb,ciis,isextb,isintb
  !
  implicit none
  !
  real, parameter :: rcldd=1.0e-06 ! droplet size threshold
  real, parameter :: alpha=2. ! shape parameter for
  ! wet size distribution
  ! near activation size
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in), dimension(ilga,leva,isextb) :: cewetrb !< Radius for wet externally mixed aerosol particles
  real, intent(in), dimension(ilga,leva,isintb) :: ciwetrb !< Radius for wet internally mixed aerosol particles
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,amplitude) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,width), ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pephis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for externally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaext) :: pedphi0 !< Dry particle radius in the centres of the size
  !< sections for externally mixed aerosol \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$, amplitude) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$, width), int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for internally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaint) :: pidphi0 !< Dry particle radius in the centres of the size
  !< sections for internally mixed aerosol \f$[m]\f$
  real, intent(out), dimension(ilga,leva) :: cicdnc !< Cloud droplet number concentration from integration of number
  !< size distribution. For internally mixed aerosols
  real, intent(out), dimension(ilga,leva,kext) :: cecdnc !< Cloud droplet number concentration from integration of number
  !< size distribution. For externally mixed aerosols.
  !
  !     internal work variables
  !
  logical, allocatable, dimension(:,:) :: kfrst !<
  logical, allocatable, dimension(:,:,:) :: tiact !<
  logical, allocatable, dimension(:,:,:) :: teact !<
  integer, allocatable, dimension(:,:) :: ciisi !<
  integer, allocatable, dimension(:,:,:) :: ceisi !<
  real, allocatable, dimension(:,:) :: circi !<
  real, allocatable, dimension(:,:,:) :: tidphis !<
  real, allocatable, dimension(:,:,:) :: tiphiss !<
  real, allocatable, dimension(:,:,:) :: tisdf !<
  real, allocatable, dimension(:,:,:) :: tinum !<
  real, allocatable, dimension(:,:,:) :: tipn0 !<
  real, allocatable, dimension(:,:,:) :: tiphi0 !<
  real, allocatable, dimension(:,:,:) :: tipsi !<
  real, allocatable, dimension(:,:,:) :: cerci !<
  real, allocatable, dimension(:,:,:) :: tedphis !<
  real, allocatable, dimension(:,:,:) :: tephiss !<
  real, allocatable, dimension(:,:,:) :: tesdf !<
  real, allocatable, dimension(:,:,:) :: tenum !<
  real, allocatable, dimension(:,:,:) :: tepn0 !<
  real, allocatable, dimension(:,:,:) :: tephi0 !<
  real, allocatable, dimension(:,:,:) :: tepsi !<
  integer :: il !<
  integer :: is !<
  integer :: isi !<
  integer :: ism !<
  integer :: isx !<
  integer :: isc !<
  integer :: l !<
  integer :: kx !<
  real :: phisrt !<
  real :: phisst !<
  real :: dphist !<
  real :: apx !<

  !
  !-----------------------------------------------------------------------
  !     * allocate work arrays.
  !
  if (kext > 0) then
    allocate(kfrst(ilga,leva))
    allocate(cerci(ilga,leva,kext))
    allocate(ceisi(ilga,leva,kext))
  end if
  if (iswext > 0) then
    allocate(tepn0  (ilga,leva,iswext))
    allocate(tephi0 (ilga,leva,iswext))
    allocate(tepsi  (ilga,leva,iswext))
    allocate(tedphis(ilga,leva,iswext))
    allocate(tephiss(ilga,leva,iswext))
    allocate(tesdf  (ilga,leva,iswext))
    allocate(tenum  (ilga,leva,iswext))
    allocate(teact  (ilga,leva,iswext))
  end if
  if (kint > 0) then
    allocate(circi (ilga,leva))
    allocate(ciisi (ilga,leva))
  end if
  if (iswint > 0) then
    allocate(tipn0  (ilga,leva,iswint))
    allocate(tiphi0 (ilga,leva,iswint))
    allocate(tipsi  (ilga,leva,iswint))
    allocate(tidphis(ilga,leva,iswint))
    allocate(tiphiss(ilga,leva,iswint))
    allocate(tisdf  (ilga,leva,iswint))
    allocate(tinum  (ilga,leva,iswint))
    allocate(tiact  (ilga,leva,iswint))
  end if
  !
  !-----------------------------------------------------------------------
  !     * match particle size with specified size cutoff to determine lower
  !     * bound of cloud droplet size spectrum for each externally mixed
  !     * aerosol type.
  !
  if (isextb > 0) then
    cerci=ylarge
    ceisi=ina
    kfrst=.true.
    teact=.false.
    do is=1,isextb-1
      if (cekx(is) == cekx(is+1) ) then
        do l=1,leva
          do il=1,ilga
            if (kfrst(il,l) ) then
              if (cewetrb(il,l,is) > rcldd) then
                ceisi(il,l,cekx(is))=ceis(is)
                cerci(il,l,cekx(is))=cedryrb(is)
              end if
              kfrst(il,l)=.false.
            end if
            if (rcldd>=cewetrb(il,l,is) &
                .and. rcldd<=cewetrb(il,l,is+1) &
                .and. ceisi(il,l,cekx(is))==ina) then
              ceisi(il,l,cekx(is))=ceis(is)
              apx=min(max((rcldd-cewetrb(il,l,is)) &
                   /(cewetrb(il,l,is+1)-cewetrb(il,l,is)),0.),1. &
                                                          )**(1./alpha)
              cerci(il,l,cekx(is))=apx*cedryrb(is+1) &
                                                  +(1.-apx)*cedryrb(is)
            end if
            !
            !             * check if condensate present.
            !
            if (cewetrb(il,l,is)>cedryrb(is) &
                .or. cewetrb(il,l,is+1)>cedryrb(is+1)         ) then
              teact(il,l,ceis(is))=.true.
            end if
          end do
        end do
      else
        kfrst(:,:)=.true.
      end if
    end do
  end if
  !-----------------------------------------------------------------------
  !     * match particle size with specified size cutoff to determine lower
  !     * bound of cloud droplet size spectrum for each internally mixed
  !     * aerosol type.
  !
  if (isintb > 0) then
    circi=ylarge
    ciisi=ina
    !
    !       * set activation size to size of smallest particle
    !       * if supersaturation large enough to activate these
    !       * particles.
    !
    is=1
    do l=1,leva
      do il=1,ilga
        if (ciwetrb(il,l,is) > rcldd .and. ciisi(il,l)==ina) then
          ciisi(il,l)=ciis(is)
          circi(il,l)=cidryrb(is)
        end if
      end do
    end do
    tiact=.false.
    do is=1,isintb-1
      do l=1,leva
        do il=1,ilga
          if (rcldd>=ciwetrb(il,l,is) &
              .and. rcldd<=ciwetrb(il,l,is+1) &
              .and. ciisi(il,l)==ina) then
            ciisi(il,l)=ciis(is)
            apx=min(max((rcldd-ciwetrb(il,l,is)) &
               /(ciwetrb(il,l,is+1)-ciwetrb(il,l,is)),0.),1. &
                                                          )**(1./alpha)
            circi(il,l)=apx*cidryrb(is+1)+(1.-apx)*cidryrb(is)
          end if
          !
          !         * check if condensate present.
          !
          if (ciwetrb(il,l,is)>cidryrb(is) &
              .or. ciwetrb(il,l,is+1)>cidryrb(is+1)             ) then
            tiact(il,l,ciis(is))=.true.
          end if
        end do
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * pla parameters for externally mixed aerosol types.
  !
  do is=1,iswext
    ism=sextf%iswet(is)%ism
    tepn0  (:,:,is)=pen0   (:,:,ism)
    tephi0 (:,:,is)=pephi0 (:,:,ism)
    tepsi  (:,:,is)=pepsi  (:,:,ism)
    tedphis(:,:,is)=pedphi0(:,:,ism)
    tephiss(:,:,is)=pephis0(:,:,ism)
  end do
  !
  !     * pla parameters for internally mixed aerosol types.
  !
  do is=1,iswint
    ism=sintf%iswet(is)%isi
    tipn0  (:,:,is)=pin0   (:,:,ism)
    tiphi0 (:,:,is)=piphi0 (:,:,ism)
    tipsi  (:,:,is)=pipsi  (:,:,ism)
    tidphis(:,:,is)=pidphi0(:,:,ism)
    tiphiss(:,:,is)=piphis0(:,:,ism)
  end do
  !
  !     * copy size information for activation size into size information
  !     * arrays for externally mixed aerosol.
  !
  if (iswext > 0) then
    do kx=1,kext
      do l=1,leva
        do il=1,ilga
          is=ceisi(il,l,kx)
          if (is /= ina) then
            phisrt=tephiss(il,l,is)+tedphis(il,l,is)
            phisst=min(max(log(cerci(il,l,kx)/r0),tephiss(il,l,is)), &
                                                                phisrt)
            dphist=phisrt-phisst
            tephiss(il,l,is)=phisst
            tedphis(il,l,is)=dphist
          end if
        end do
      end do
    end do
  end if
  !
  !     * copy size information for activation size into size information
  !     * arrays for internally mixed aerosol.
  !
  if (iswint > 0) then
    do l=1,leva
      do il=1,ilga
        is=ciisi(il,l)
        if (is /= ina) then
          phisrt=tiphiss(il,l,is)+tidphis(il,l,is)
          phisst=min(max(log(circi(il,l)/r0),tiphiss(il,l,is)), &
                                                                phisrt)
          dphist=phisrt-phisst
          tiphiss(il,l,is)=phisst
          tidphis(il,l,is)=dphist
        end if
      end do
    end do
  end if
  !
  !     * obtain number concentrations from integration of size disribution
  !     * for externally mixed aerosols.
  !
  if (iswext > 0) then
    tesdf=sdint0(tephi0,tepsi,tephiss,tedphis,ilga,leva,iswext)
    where (abs(tepsi-yna) > ytiny)
      tenum=tepn0*tesdf
    else where
      tenum=0.
    end where
  end if
  !
  !     * obtain number concentrations from integration of size disribution
  !     * for internally mixed aerosols.
  !
  if (iswint > 0) then
    tisdf=sdint0(tiphi0,tipsi,tiphiss,tidphis,ilga,leva,iswint)
    where (abs(tipsi-yna) > ytiny)
      tinum=tipn0*tisdf
    else where
      tinum=0.
    end where
  end if
  !
  !-----------------------------------------------------------------------
  !     * cloud droplet number concentration from integration of number
  !     * size distribution from critical size to size of largest particle
  !     * in the size distribution. for externally mixed aerosols.
  !
  if (kext > 0) then
    cecdnc=0.
  end if
  isi=0
  do kx=1,kext
    if (     (abs(aextf%tp(kx)%nuio -yna) > ytiny &
        .and. aextf%tp(kx)%nuio  > ytiny) &
        .or. (abs(aextf%tp(kx)%kappa-yna) > ytiny &
        .and. aextf%tp(kx)%kappa > ytiny) ) then
      do is=1,aextf%tp(kx)%isec
        isi=isi+1
        do l=1,leva
          do il=1,ilga
            isx=ceisi(il,l,kx)
            if (isx /= ina) then
              isc=sextf%iswet(isx)%isi
              if (is >= isc .and. teact(il,l,isi) ) then
                cecdnc(il,l,kx)=cecdnc(il,l,kx)+tenum(il,l,isi)
              end if
            end if
          end do
        end do
      end do
    end if
  end do
  !
  !     * cloud droplet number concentration from integration of number
  !     * size distribution. for internally mixed aerosols.
  !
  if (kint > 0) then
    cicdnc=0.
  end if
  do is=1,iswint
    do l=1,leva
      do il=1,ilga
        isx=ciisi(il,l)
        if (isx /= ina) then
          if (is >= isx .and. tiact(il,l,is) ) then
            cicdnc(il,l)=cicdnc(il,l)+tinum(il,l,is)
          end if
        end if
      end do
    end do
  end do
  !-----------------------------------------------------------------------
  !     * deallocate work arrays.
  !
  if (kext > 0) then
    deallocate(kfrst)
    deallocate(cerci)
    deallocate(ceisi)
  end if
  if (iswext > 0) then
    deallocate(tepn0)
    deallocate(tephi0)
    deallocate(tepsi)
    deallocate(tedphis)
    deallocate(tephiss)
    deallocate(tesdf)
    deallocate(tenum)
    deallocate(teact)
  end if
  if (kint > 0) then
    deallocate(circi)
    deallocate(ciisi)
  end if
  if (iswint > 0) then
    deallocate(tipn0)
    deallocate(tiphi0)
    deallocate(tipsi)
    deallocate(tidphis)
    deallocate(tiphiss)
    deallocate(tisdf)
    deallocate(tinum)
    deallocate(tiact)
  end if
  !
end subroutine cncdncb
