!> \file
!> \brief Cloud processes: wet removal and sulphate in-cloud production.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine scldsp(pedndt,pedmdt,pidndt,pidmdt,pidfdt,dsivdt, &
                        dhpdt,dersn,dersm,dirsmn,dirsn,dirsms,dsicp, &
                        dwr1n,dwr2n,dwr3n,dwr1m,dwr2m,dwr3m,dws1n, &
                        dws2n,dws3n,dws1m,dws2m,dws3m,pehenr,pihenr, &
                        pemas,penum,pephis0,pedphi0,pepsi,peddn,penfrc, &
                        pemfrc,pewetrc,cerci,pimas,pinum,pifrc,piphis0, &
                        pidphi0,pipsi,pinfrc,pimfrc,piwetrc,o3frc, &
                        h2o2frc,circi,xsivfrc,hpfrc,xbc,ta,rhoa,dpa, &
                        pmrep,zmlwc,pfsnow,pfrain,zclf,clrfr,clrfs, &
                        pfevap,pfsubl,xsiv,xhp,xo3,xna,xam,co2_ppm,dt, &
                        zcthr,ilga,leva,ierr)
  !
  use sdparm, only : aextf,aintf, &
                         iexso4,iinso4,ina,isaext,isaint,isextso4,isintso4, &
                         kext,kextbc,kextso4,kextss, &
                         kint,kintbc,kintso4,kintss, &
                         pedphis,pedryrc,pephiss, &
                         pidphis,pidryrc,piphiss, &
                         r0,sextf,sintf,wamsul,ws,ycnst,yna,ysmall,ytiny
  use sdcode, only : sdintb, sdintb0
  !
  implicit none
  !
  real, parameter :: asrphob = 0. !<
  real, parameter :: asrphil = 1. !<
  real, parameter :: drcrit0= 0.05e-06 !<
  real, parameter :: cldimx = 0.9 ! limit non-homogeneity in concentration
  ! (1, max. inhom., 0, no inhom.)
  integer, parameter :: isx=1 !<
  integer, parameter :: neqp=13 !<
  !
  logical, parameter :: ksccn=.false. !<
  !
  integer, intent(in)  :: ilga !<
  integer, intent(in)  :: leva !<
  integer, intent(out) :: ierr !<
  real, intent(in) :: zcthr !<
  real, intent(in) :: co2_ppm !<
  real, intent(in) :: dt !<
  real, intent(out), dimension(ilga,leva) :: xbc !<
  real, intent(out), dimension(ilga,leva) :: dsivdt !<
  real, intent(out), dimension(ilga,leva) :: dhpdt !<
  real, intent(out), dimension(ilga,leva) :: dersn !<
  real, intent(out), dimension(ilga,leva) :: dersm !<
  real, intent(out), dimension(ilga,leva) :: dirsmn !<
  real, intent(out), dimension(ilga,leva) :: dirsn !<
  real, intent(out), dimension(ilga,leva) :: dirsms !<
  real, intent(out), dimension(ilga,leva) :: dsicp !<
  real, intent(out), dimension(ilga,leva) :: dwr1n !<
  real, intent(out), dimension(ilga,leva) :: dwr2n !<
  real, intent(out), dimension(ilga,leva) :: dwr3n !<
  real, intent(out), dimension(ilga,leva) :: dwr1m !<
  real, intent(out), dimension(ilga,leva) :: dwr2m !<
  real, intent(out), dimension(ilga,leva) :: dwr3m !<
  real, intent(out), dimension(ilga,leva) :: dws1n !<
  real, intent(out), dimension(ilga,leva) :: dws2n !<
  real, intent(out), dimension(ilga,leva) :: dws3n !<
  real, intent(out), dimension(ilga,leva) :: dws1m !<
  real, intent(out), dimension(ilga,leva) :: dws2m !<
  real, intent(out), dimension(ilga,leva) :: dws3m !<
  real, intent(out), dimension(ilga,leva) :: pihenr !<
  real, intent(out), dimension(ilga,leva) :: pehenr !<
  real, intent(out), dimension(ilga,leva) :: o3frc !<
  real, intent(out), dimension(ilga,leva) :: h2o2frc !<
  real, intent(out), dimension(ilga,leva,isaext) :: pedndt !<
  real, intent(out), dimension(ilga,leva,isaext) :: pedmdt !<
  real, intent(out), dimension(ilga,leva,isaint) :: pidndt !<
  real, intent(out), dimension(ilga,leva,isaint) :: pidmdt !<
  real, intent(out), dimension(ilga,leva,isaint,kint) :: pidfdt !<
  real, intent(in), dimension(ilga,leva,isaext) :: penum !<
  real, intent(in), dimension(ilga,leva,isaext) :: pemas !<
  real, intent(in), dimension(ilga,leva,isaext) :: peddn !<
  real, intent(in), dimension(ilga,leva,isaext) :: pephis0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pedphi0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !<
  real, intent(in), dimension(ilga,leva,isaext) :: pewetrc !<
  real, intent(in), dimension(ilga,leva,isaint) :: pinum !<
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !<
  real, intent(in), dimension(ilga,leva,isaint) :: piphis0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: pidphi0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !<
  real, intent(in), dimension(ilga,leva,isaint) :: piwetrc !<
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !<
  real, intent(in), dimension(ilga,leva) :: ta !<
  real, intent(in), dimension(ilga,leva) :: rhoa !<
  real, intent(in), dimension(ilga,leva) :: dpa !<
  real, intent(in), dimension(ilga,leva) :: pmrep !<
  real, intent(in), dimension(ilga,leva) :: zmlwc !<
  real, intent(in), dimension(ilga,leva) :: pfsnow !<
  real, intent(in), dimension(ilga,leva) :: pfrain !<
  real, intent(in), dimension(ilga,leva) :: zclf !<
  real, intent(in), dimension(ilga,leva) :: clrfr !<
  real, intent(in), dimension(ilga,leva) :: clrfs !<
  real, intent(in), dimension(ilga,leva) :: pfevap !<
  real, intent(in), dimension(ilga,leva) :: pfsubl !<
  real, intent(in), dimension(ilga,leva) :: xsiv !<
  real, intent(in), dimension(ilga,leva) :: xhp !<
  real, intent(in), dimension(ilga,leva) :: xo3 !<
  real, intent(in), dimension(ilga,leva) :: xna !<
  real, intent(in), dimension(ilga,leva) :: xam !<
  real, intent(in), dimension(ilga,leva,kext) :: cerci !<
  real, intent(in), dimension(ilga,leva) :: circi !<
  real, intent(inout), dimension(ilga,leva) :: xsivfrc !<
  real, intent(inout), dimension(ilga,leva) :: hpfrc !<
  real, intent(inout), dimension(ilga,leva,isaext) :: penfrc !<
  real, intent(inout), dimension(ilga,leva,isaext) :: pemfrc !<
  real, intent(inout), dimension(ilga,leva,isaint) :: pinfrc !<
  real, intent(inout), dimension(ilga,leva,isaint,kint) :: pimfrc !<
  real, dimension(ilga,leva) :: pcesmas !<
  real, dimension(ilga,leva) :: pcismas !<
  real, dimension(ilga,leva) :: aterm !<
  real, dimension(ilga,leva) :: aterms !<
  real, dimension(ilga,leva) :: sdsm !<
  real, dimension(ilga,leva) :: dxsiv !<
  real, dimension(ilga,leva) :: dxsvi !<
  real, dimension(ilga,leva) :: dxhp !<
  real, dimension(ilga,leva) :: zhenry !<
  real, dimension(ilga,leva) :: xsvi !<
  real, dimension(ilga,leva) :: xss !<
  real, dimension(ilga,leva) :: xcsiv !<
  real, dimension(ilga,leva) :: xasiv !<
  real, dimension(ilga,leva) :: xchp !<
  real, dimension(ilga,leva) :: xahp !<
  real, dimension(ilga,leva) :: tmpscl !<
  real, dimension(ilga,leva) :: texsvi !<
  real, dimension(ilga,leva) :: texsvip !<
  real, dimension(ilga,leva) :: texsvia !<
  real, dimension(ilga,leva) :: tixsvi !<
  real, dimension(ilga,leva) :: timass !<
  real, dimension(ilga,leva) :: tiden !<
  real, dimension(ilga,leva) :: timassp !<
  real, dimension(ilga,leva) :: timassa !<
  real, dimension(ilga,leva,kext) :: pcerci !<
  real, dimension(ilga,leva,kext) :: pcepci !<
  real, dimension(ilga,leva) :: pcirci !<
  real, dimension(ilga,leva) :: pcipci !<
  real, dimension(ilga,leva,isx) :: pxdmdt !<
  real, dimension(ilga,leva,isx) :: daxmas !<
  real, dimension(ilga,leva,isx) :: dcxmas !<
  real, dimension(ilga,leva,isx) :: dxwr1m !<
  real, dimension(ilga,leva,isx) :: dxwr2m !<
  real, dimension(ilga,leva,isx) :: dxwr3m !<
  real, dimension(ilga,leva,isx) :: dxws1m !<
  real, dimension(ilga,leva,isx) :: dxws2m !<
  real, dimension(ilga,leva,isx) :: dxws3m !<
  real, dimension(ilga,leva,isx) :: paxmas !<
  real, dimension(ilga,leva,isx) :: pcxmas !<
  real, dimension(ilga,leva,isx) :: pxhenm !<
  real, dimension(ilga,leva,isx) :: pxmcef !<
  real, allocatable, dimension(:,:,:) :: pcedndt !<
  real, allocatable, dimension(:,:,:) :: pcedmdt !<
  real, allocatable, dimension(:,:,:) :: pcidndt !<
  real, allocatable, dimension(:,:,:) :: pcidmdt !<
  real, allocatable, dimension(:,:,:,:) :: pcidfdt !<
  real, allocatable, dimension(:,:,:,:) :: pimscl !<
  real, allocatable, dimension(:,:,:) :: pcenum !<
  real, allocatable, dimension(:,:,:) :: pcemas !<
  real, allocatable, dimension(:,:,:) :: pcen0 !<
  real, allocatable, dimension(:,:,:) :: pcephi0 !<
  real, allocatable, dimension(:,:,:) :: pcepsi !<
  real, allocatable, dimension(:,:,:) :: pceddn !<
  real, allocatable, dimension(:,:,:) :: paenum !<
  real, allocatable, dimension(:,:,:) :: paemas !<
  real, allocatable, dimension(:,:,:) :: paen0 !<
  real, allocatable, dimension(:,:,:) :: paephi0 !<
  real, allocatable, dimension(:,:,:) :: paepsi !<
  real, allocatable, dimension(:,:,:) :: paeddn !<
  real, allocatable, dimension(:,:,:) :: pehenn !<
  real, allocatable, dimension(:,:,:) :: pehenm !<
  real, allocatable, dimension(:,:,:) :: daenum !<
  real, allocatable, dimension(:,:,:) :: dcenum !<
  real, allocatable, dimension(:,:,:) :: daemas !<
  real, allocatable, dimension(:,:,:) :: dcemas !<
  real, allocatable, dimension(:,:,:) :: pencef !<
  real, allocatable, dimension(:,:,:) :: pemcef !<
  real, allocatable, dimension(:,:,:) :: penscl !<
  real, allocatable, dimension(:,:,:) :: pemscl !<
  real, allocatable, dimension(:,:,:) :: pcinum !<
  real, allocatable, dimension(:,:,:) :: pcimas !<
  real, allocatable, dimension(:,:,:) :: pcin0 !<
  real, allocatable, dimension(:,:,:) :: pciphi0 !<
  real, allocatable, dimension(:,:,:) :: pcipsi !<
  real, allocatable, dimension(:,:,:) :: pciddn !<
  real, allocatable, dimension(:,:,:) :: painum !<
  real, allocatable, dimension(:,:,:) :: paimas !<
  real, allocatable, dimension(:,:,:) :: pain0 !<
  real, allocatable, dimension(:,:,:) :: paiphi0 !<
  real, allocatable, dimension(:,:,:) :: paipsi !<
  real, allocatable, dimension(:,:,:) :: paiddn !<
  real, allocatable, dimension(:,:,:) :: pihenn !<
  real, allocatable, dimension(:,:,:) :: pihenm !<
  real, allocatable, dimension(:,:,:) :: dainum !<
  real, allocatable, dimension(:,:,:) :: dcinum !<
  real, allocatable, dimension(:,:,:) :: daimas !<
  real, allocatable, dimension(:,:,:) :: dcimas !<
  real, allocatable, dimension(:,:,:) :: pincef !<
  real, allocatable, dimension(:,:,:) :: pimcef !<
  real, allocatable, dimension(:,:,:) :: pinscl !<
  real, allocatable, dimension(:,:,:) :: psimas !<
  real, allocatable, dimension(:,:,:) :: pshenm !<
  real, allocatable, dimension(:,:,:) :: pasmas !<
  real, allocatable, dimension(:,:,:) :: pcsmas !<
  real, allocatable, dimension(:,:,:) :: psdmdt !<
  real, allocatable, dimension(:,:,:) :: dasmas !<
  real, allocatable, dimension(:,:,:) :: dcsmas !<
  real, allocatable, dimension(:,:,:) :: psmcef !<
  real, allocatable, dimension(:,:,:,:) :: pcifrc !<
  real, allocatable, dimension(:,:,:,:) :: paifrc !<
  real, allocatable, dimension(:,:,:,:) :: psifrc !<
  real, allocatable, dimension(:,:,:,:) :: paitmp !<
  real, allocatable, dimension(:,:,:,:) :: pcitmp !<
  real, allocatable, dimension(:,:,:,:) :: pitmpc !<
  real, allocatable, dimension(:,:,:,:) :: pimasi !<
  real, allocatable, dimension(:,:,:) :: pimast !<
  real, allocatable, dimension(:,:,:) :: tedphis !<
  real, allocatable, dimension(:,:,:) :: tephiss !<
  real, allocatable, dimension(:,:,:) :: tidphis !<
  real, allocatable, dimension(:,:,:) :: tiphiss !<
  real, allocatable, dimension(:,:,:) :: tetrum !<
  real, allocatable, dimension(:,:) :: titrum !<
  real, allocatable, dimension(:,:,:) :: dewr1n !<
  real, allocatable, dimension(:,:,:) :: dewr2n !<
  real, allocatable, dimension(:,:,:) :: dewr3n !<
  real, allocatable, dimension(:,:,:) :: dewr1m !<
  real, allocatable, dimension(:,:,:) :: dewr2m !<
  real, allocatable, dimension(:,:,:) :: dewr3m !<
  real, allocatable, dimension(:,:,:) :: dews1n !<
  real, allocatable, dimension(:,:,:) :: dews2n !<
  real, allocatable, dimension(:,:,:) :: dews3n !<
  real, allocatable, dimension(:,:,:) :: dews1m !<
  real, allocatable, dimension(:,:,:) :: dews2m !<
  real, allocatable, dimension(:,:,:) :: dews3m !<
  real, allocatable, dimension(:,:,:) :: diwr1n !<
  real, allocatable, dimension(:,:,:) :: diwr2n !<
  real, allocatable, dimension(:,:,:) :: diwr3n !<
  real, allocatable, dimension(:,:,:) :: diwr1m !<
  real, allocatable, dimension(:,:,:) :: diwr2m !<
  real, allocatable, dimension(:,:,:) :: diwr3m !<
  real, allocatable, dimension(:,:,:) :: diws1n !<
  real, allocatable, dimension(:,:,:) :: diws2n !<
  real, allocatable, dimension(:,:,:) :: diws3n !<
  real, allocatable, dimension(:,:,:) :: diws1m !<
  real, allocatable, dimension(:,:,:) :: diws2m !<
  real, allocatable, dimension(:,:,:) :: diws3m !<
  integer, allocatable, dimension(:,:,:) :: peicrit !<
  integer, allocatable, dimension(:,:) :: piicrit !<
  real, parameter :: ysmall2 = 1.e-20 !<
  integer :: kx !<
  integer :: is !<
  integer :: ist !<
  integer :: it !<
  integer :: l !<
  integer :: il !<
  integer :: isl !<
  integer :: ish !<
  integer :: isi !<
  integer :: islp !<
  real :: rmom !<
  real :: atmp2 !<
  real :: aextsn !<
  real :: aextsm !<
  real :: wgtn !<
  real :: wgtm !<
  real :: aintsn !<
  real :: aintsm !<
  real :: frcn !<
  !-----------------------------------------------------------------------
  !     * allocation.
  !
  if (isaext > 0) then
    allocate(pcenum  (ilga,leva,isaext))
    allocate(pcemas  (ilga,leva,isaext))
    allocate(dcenum  (ilga,leva,isaext))
    allocate(dcemas  (ilga,leva,isaext))
    allocate(pcen0   (ilga,leva,isaext))
    allocate(pcephi0 (ilga,leva,isaext))
    allocate(pcepsi  (ilga,leva,isaext))
    allocate(pceddn  (ilga,leva,isaext))
    allocate(pcedndt (ilga,leva,isaext))
    allocate(pcedmdt (ilga,leva,isaext))
    allocate(paenum  (ilga,leva,isaext))
    allocate(paemas  (ilga,leva,isaext))
    allocate(daenum  (ilga,leva,isaext))
    allocate(daemas  (ilga,leva,isaext))
    allocate(paen0   (ilga,leva,isaext))
    allocate(paephi0 (ilga,leva,isaext))
    allocate(paepsi  (ilga,leva,isaext))
    allocate(paeddn  (ilga,leva,isaext))
    allocate(pehenn  (ilga,leva,isaext))
    allocate(pehenm  (ilga,leva,isaext))
    allocate(pencef  (ilga,leva,isaext))
    allocate(pemcef  (ilga,leva,isaext))
    allocate(penscl  (ilga,leva,isaext))
    allocate(pemscl  (ilga,leva,isaext))
    allocate(peicrit (ilga,leva,kext))
    allocate(dewr1n  (ilga,leva,isaext))
    allocate(dewr2n  (ilga,leva,isaext))
    allocate(dewr3n  (ilga,leva,isaext))
    allocate(dews1n  (ilga,leva,isaext))
    allocate(dews2n  (ilga,leva,isaext))
    allocate(dews3n  (ilga,leva,isaext))
    allocate(dewr1m  (ilga,leva,isaext))
    allocate(dewr2m  (ilga,leva,isaext))
    allocate(dewr3m  (ilga,leva,isaext))
    allocate(dews1m  (ilga,leva,isaext))
    allocate(dews2m  (ilga,leva,isaext))
    allocate(dews3m  (ilga,leva,isaext))
    allocate(tedphis (ilga,leva,isaext))
    allocate(tephiss (ilga,leva,isaext))
    allocate(tetrum  (ilga,leva,kext))
  end if
  if (isaint > 0) then
    allocate(pcinum  (ilga,leva,isaint))
    allocate(pcimas  (ilga,leva,isaint))
    allocate(dcinum  (ilga,leva,isaint))
    allocate(dcimas  (ilga,leva,isaint))
    allocate(pcin0   (ilga,leva,isaint))
    allocate(pciphi0 (ilga,leva,isaint))
    allocate(pcipsi  (ilga,leva,isaint))
    allocate(pciddn  (ilga,leva,isaint))
    allocate(pcidndt (ilga,leva,isaint))
    allocate(pcidmdt (ilga,leva,isaint))
    allocate(pcifrc  (ilga,leva,isaint,kint))
    allocate(pcidfdt (ilga,leva,isaint,kint))
    allocate(paitmp  (ilga,leva,isaint,kint))
    allocate(pcitmp  (ilga,leva,isaint,kint))
    allocate(pitmpc  (ilga,leva,isaint,kint))
    allocate(pimasi  (ilga,leva,isaint,kint))
    allocate(painum  (ilga,leva,isaint))
    allocate(paimas  (ilga,leva,isaint))
    allocate(dainum  (ilga,leva,isaint))
    allocate(daimas  (ilga,leva,isaint))
    allocate(pain0   (ilga,leva,isaint))
    allocate(paiphi0 (ilga,leva,isaint))
    allocate(paipsi  (ilga,leva,isaint))
    allocate(paiddn  (ilga,leva,isaint))
    allocate(paifrc  (ilga,leva,isaint,kint))
    allocate(pimast  (ilga,leva,isaint))
    allocate(pihenn  (ilga,leva,isaint))
    allocate(pihenm  (ilga,leva,isaint))
    allocate(psimas  (ilga,leva,isaint))
    allocate(pshenm  (ilga,leva,isx))
    allocate(pasmas  (ilga,leva,isx))
    allocate(pcsmas  (ilga,leva,isx))
    allocate(psdmdt  (ilga,leva,isx))
    allocate(dasmas  (ilga,leva,isx))
    allocate(dcsmas  (ilga,leva,isx))
    allocate(psmcef  (ilga,leva,isx))
    allocate(psifrc  (ilga,leva,isaint,kint))
    allocate(pincef  (ilga,leva,isaint))
    allocate(pimcef  (ilga,leva,isaint))
    allocate(pinscl  (ilga,leva,isaint))
    allocate(pimscl  (ilga,leva,isaint,kint))
    allocate(piicrit (ilga,leva))
    allocate(diwr1n  (ilga,leva,isaint))
    allocate(diwr2n  (ilga,leva,isaint))
    allocate(diwr3n  (ilga,leva,isaint))
    allocate(diws1n  (ilga,leva,isaint))
    allocate(diws2n  (ilga,leva,isaint))
    allocate(diws3n  (ilga,leva,isaint))
    allocate(diwr1m  (ilga,leva,isx))
    allocate(diwr2m  (ilga,leva,isx))
    allocate(diwr3m  (ilga,leva,isx))
    allocate(diws1m  (ilga,leva,isx))
    allocate(diws2m  (ilga,leva,isx))
    allocate(diws3m  (ilga,leva,isx))
    allocate(tidphis (ilga,leva,isaint))
    allocate(tiphiss (ilga,leva,isaint))
    allocate(titrum  (ilga,leva))
  end if
  !
  !     initializations.
  !
  xbc=0.
  dersn =0.
  dersm =0.
  dirsmn=0.
  dirsn =0.
  dirsms=0.
  dwr1n =0.
  dwr2n =0.
  dwr3n =0.
  dwr1m =0.
  dwr2m =0.
  dwr3m =0.
  dws1n =0.
  dws2n =0.
  dws3n =0.
  dws1m =0.
  dws2m =0.
  dws3m =0.
  !
  !-----------------------------------------------------------------------
  !     * critical particle radius (dry) for cloud processing.
  !
  if (ksccn) then
    !
    !       * use specified critical radius.
    !
    if (isaext > 0) then
      pcerci=drcrit0
    end if
    if (isaint > 0) then
      pcirci=drcrit0
    end if
  else
    if (isaext > 0) then
      pcerci=cerci
    end if
    if (isaint > 0) then
      pcirci=circi
    end if
  end if
  !
  !     * restrict critical sizes to values within the simulated
  !     * particle size range.
  !
  if (isaext > 0) then
    do kx=1,kext
      pcerci(:,:,kx)=max(pcerci(:,:,kx), &
                         aextf%tp(kx)%dryr(1)%vl &
                +0.01*(aextf%tp(kx)%dryr(1)%vr-aextf%tp(kx)%dryr(1)%vl))
      pcerci(:,:,kx)=min(pcerci(:,:,kx), &
                         aextf%tp(kx)%dryr(aextf%tp(kx)%isec)%vr &
                -0.01*(aextf%tp(kx)%dryr(aextf%tp(kx)%isec)%vr &
                -aextf%tp(kx)%dryr(aextf%tp(kx)%isec)%vl))
    end do
  end if
  if (isaint > 0) then
    do kx=1,kint
      pcirci(:,:)=max(pcirci(:,:),aintf%dryr(1)%vl &
                    +0.01*(aintf%dryr(1)%vr-aintf%dryr(1)%vl))
      pcirci(:,:)=min(pcirci(:,:),aintf%dryr(aintf%isec)%vr &
                    -0.01*(aintf%dryr(aintf%isec)%vr &
                    -aintf%dryr(aintf%isec)%vl))
    end do
  end if
  !
  !     * corresponding pla size parameter.
  !
  if (isaext > 0) then
    do kx=1,kext
      aterm=pcerci(:,:,kx)/r0
      aterms=log(aterm)
      pcepci(:,:,kx)=aterms
    end do
  end if
  if (isaint > 0) then
    aterm=pcirci/r0
    pcipci=log(aterm)
  end if
  !
  !     * copy grid-cell mean results into cloud (pc..) and clear-sky (pa..)
  !     * arrays. concentrations differences between cloudy and clear-sky
  !     * are given by penfrc and pemfrc for number resp. mass. for
  !     * externally mixed aerosol and pinfrc and pimfrc for number resp.
  !     * mass. for internally mixed aerosol (from previous time step).
  !     * these differences are added to the grid-cell mean value to obtain
  !     * values for clear- and cloudy-sky.
  !
  if (isaext > 0) then
    !
    !       * first, limit clear/cloud concentration differences to ensure
    !       * positive concentrations in the entire grid cell.
    !
    penscl=1.
    pemscl=1.
    do is=1,isaext
      where (abs(pepsi(:,:,is)-yna) > ytiny)
        where (zclf(:,:) < zcthr .and. (1.-zclf(:,:)) < zcthr)
          where (penfrc(:,:,is) < -ysmall)
            penscl(:,:,is)=-penum(:,:,is)*(1.-zclf(:,:)) &
                                                 /penfrc(:,:,is)*cldimx
          else where (penfrc(:,:,is) > ysmall)
            penscl(:,:,is)=penum(:,:,is)*zclf(:,:)/penfrc(:,:,is) &
                                                                *cldimx
          else where
            penscl(:,:,is)=0.
          end where
          where (pemfrc(:,:,is) < -ysmall)
            pemscl(:,:,is)=-pemas(:,:,is)*(1.-zclf(:,:)) &
                                                 /pemfrc(:,:,is)*cldimx
          else where (pemfrc(:,:,is) > ysmall)
            pemscl(:,:,is)=pemas(:,:,is)*zclf(:,:)/pemfrc(:,:,is) &
                                                                *cldimx
          else where
            pemscl(:,:,is)=0.
          end where
        end where
      end where
    end do
    penscl=max(min(penscl,1.),0.)
    pemscl=max(min(pemscl,1.),0.)
    penscl=min(penscl,pemscl)
    pemscl=penscl
    penfrc=penscl*penfrc
    pemfrc=pemscl*pemfrc
    !
    !       * number and mass for externally mixed aerosol.
    !
    paenum=0.
    paemas=0.
    pcenum=0.
    pcemas=0.
    do is=1,isaext
      where (abs(pepsi(:,:,is)-yna) > ytiny)
        paenum(:,:,is) = penum(:,:,is)
        paemas(:,:,is) = pemas(:,:,is)
        pcenum(:,:,is) = penum(:,:,is)
        pcemas(:,:,is) = pemas(:,:,is)
        where (zclf(:,:) < zcthr .and. (1.-zclf(:,:)) < zcthr)
          paenum(:,:,is) = paenum(:,:,is)+penfrc(:,:,is)/(1.-zclf(:,:))
          paemas(:,:,is) = paemas(:,:,is)+pemfrc(:,:,is)/(1.-zclf(:,:))
          pcenum(:,:,is) = pcenum(:,:,is)-penfrc(:,:,is)/zclf(:,:)
          pcemas(:,:,is) = pcemas(:,:,is)-pemfrc(:,:,is)/zclf(:,:)
        end where
      end where
    end do
    !
    !       * density.
    !
    paeddn=peddn
    pceddn=peddn
    !
    !       * tendencies and other fields.
    !
    pedndt =0.
    pedmdt =0.
    pcedmdt=0.
    pcedndt=0.
    daenum =0.
    dcenum =0.
    daemas =0.
    dcemas =0.
  end if
  if (isaint > 0) then
    !
    !       * first, limit clear/cloud concentration differences to ensure
    !       * positive concentrations in the entire grid cell.
    !
    pinscl=1.
    pimscl=1.
    do is=1,isaint
      where (abs(pipsi(:,:,is)-yna) > ytiny)
        where (zclf(:,:) < zcthr .and. (1.-zclf(:,:)) < zcthr)
          where (pinfrc(:,:,is) < -ysmall)
            pinscl(:,:,is)=-pinum(:,:,is)*(1.-zclf(:,:)) &
                                                 /pinfrc(:,:,is)*cldimx
          else where (pinfrc(:,:,is) > ysmall)
            pinscl(:,:,is)=pinum(:,:,is)*zclf(:,:)/pinfrc(:,:,is) &
                                                                *cldimx
          else where
            pinscl(:,:,is)=0.
          end where
        end where
      end where
    end do
    do kx=1,kint
      do is=1,isaint
        where (abs(pipsi(:,:,is)-yna) > ytiny)
          where (zclf(:,:) < zcthr .and. (1.-zclf(:,:)) < zcthr)
            where (pimfrc(:,:,is,kx) < -ysmall)
              pimscl(:,:,is,kx)=-pifrc(:,:,is,kx)*pimas(:,:,is) &
                               *(1.-zclf(:,:))/pimfrc(:,:,is,kx)*cldimx
            else where (pimfrc(:,:,is,kx) > ysmall)
              pimscl(:,:,is,kx)=pifrc(:,:,is,kx)*pimas(:,:,is) &
                                           *zclf(:,:)/pimfrc(:,:,is,kx) &
                                                                *cldimx
            else where
              pimscl(:,:,is,kx)=0.
            end where
          end where
        end where
      end do
    end do
    pinscl=max(min(pinscl,1.),0.)
    pimscl=max(min(pimscl,1.),0.)
    do kx=1,kint
      pinscl=min(pinscl,pimscl(:,:,:,kx))
      pimscl(:,:,:,kx)=pinscl(:,:,:)
    end do
    pinfrc=pinscl*pinfrc
    pimfrc=pimscl*pimfrc
    !
    !       * number, mass, and mass fractions for internally mixed aerosol
    !       * for clear-sky.
    !
    painum=0.
    paimas=0.
    pitmpc=0.
    pimasi=0.
    do is=1,isaint
      where (abs(pipsi(:,:,is)-yna) > ytiny)
        painum(:,:,is) = pinum(:,:,is)
        where (zclf(:,:) < zcthr .and. (1.-zclf(:,:)) < zcthr)
          painum(:,:,is) = painum(:,:,is) &
                               +pinfrc(:,:,is)/(1.-zclf(:,:))
        end where
      end where
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        where (abs(pipsi(:,:,is)-yna) > ytiny)
          pitmpc(:,:,is,kx)=pifrc(:,:,is,kx)*pimas(:,:,is)
          pimasi(:,:,is,kx)=pitmpc(:,:,is,kx)
          where (zclf(:,:) < zcthr .and. (1.-zclf(:,:)) < zcthr)
            pitmpc(:,:,is,kx) = pitmpc(:,:,is,kx) &
                                    +pimfrc(:,:,is,kx)/(1.-zclf(:,:))
          end where
          paimas(:,:,is)=paimas(:,:,is)+pitmpc(:,:,is,kx)
        end where
      end do
    end do
    do it=1,kint
      where (2+abs(exponent(pitmpc(:,:,:,it)) - exponent(paimas)) &
          < maxexponent(pitmpc(:,:,:,it)) .and. paimas/=0. )
        paifrc(:,:,:,it)=max(min(pitmpc(:,:,:,it)/paimas,1.),0.)
      else where
        paifrc(:,:,:,it)=1./real(kint)
      end where
      paitmp(:,:,:,it)=pitmpc(:,:,:,it)
    end do
    !
    !       * number, mass, and mass fractions for internally mixed aerosol
    !       * for cloudy-sky.
    !
    pcinum=0.
    pcimas=0.
    pitmpc=0.
    do is=1,isaint
      where (abs(pipsi(:,:,is)-yna) > ytiny)
        pcinum(:,:,is) = pinum(:,:,is)
        where (zclf(:,:) < zcthr .and. (1.-zclf(:,:)) < zcthr)
          pcinum(:,:,is) = pcinum(:,:,is)-pinfrc(:,:,is)/zclf(:,:)
        end where
      end where
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        where (abs(pipsi(:,:,is)-yna) > ytiny)
          pitmpc(:,:,is,kx)=pifrc(:,:,is,kx)*pimas(:,:,is)
          where (zclf(:,:) > ysmall &
              .and. (1.-zclf(:,:)) > ysmall)
            pitmpc(:,:,is,kx) = pitmpc(:,:,is,kx) &
                                    -pimfrc(:,:,is,kx)/zclf(:,:)
          end where
          pcimas(:,:,is)=pcimas(:,:,is)+pitmpc(:,:,is,kx)
        end where
      end do
    end do
    do it=1,kint
      where (2+abs(exponent(pitmpc(:,:,:,it)) - exponent(pcimas)) &
          < maxexponent(pitmpc(:,:,:,it)) .and. pcimas/=0. )
        pcifrc(:,:,:,it)=max(min(pitmpc(:,:,:,it)/pcimas,1.),0.)
      else where
        pcifrc(:,:,:,it)=1./real(kint)
      end where
      pcitmp(:,:,:,it)=pitmpc(:,:,:,it)
    end do
    !
    !       * tendencies and other fields.
    !
    pidndt =0.
    pidmdt =0.
    pidfdt =0.
    pcidndt=0.
    pcidmdt=0.
    pcidfdt=0.
    dainum =0.
    dcinum =0.
    daimas =0.
    dcimas =0.
  end if
  if (kextso4>0 .or. kintso4>0) then
    pxdmdt=0.
    daxmas=0.
    dcxmas=0.
  end if
  !
  !     * clear-sky and cloudy-sky concentrations for sulphur dioxide.
  !
  tmpscl=1.
  where (zclf < zcthr .and. (1.-zclf) < zcthr)
    where (xsivfrc < -ysmall)
      tmpscl=-xsiv*(1.-zclf)/xsivfrc
    else where (xsivfrc > ysmall)
      tmpscl=xsiv*zclf/xsivfrc
    else where
      tmpscl=0.
    end where
  end where
  tmpscl=max(min(tmpscl,1.),0.)
  xsivfrc=tmpscl*xsivfrc
  xasiv=xsiv
  xcsiv=xsiv
  where (zclf < zcthr .and. (1.-zclf) < zcthr)
    xasiv=xasiv+xsivfrc/(1.-zclf)
    xcsiv=xcsiv-xsivfrc/zclf
  end where
  !
  !     * clear-sky and cloudy-sky concentrations for hydrogen peroxide.
  !
  tmpscl=1.
  where (zclf < zcthr .and. (1.-zclf) < zcthr)
    where (hpfrc < -ysmall)
      tmpscl=-xhp*(1.-zclf)/hpfrc
    else where (hpfrc > ysmall)
      tmpscl=xhp*zclf/hpfrc
    else where
      tmpscl=0.
    end where
  end where
  tmpscl=max(min(tmpscl,1.),0.)
  hpfrc=tmpscl*hpfrc
  xahp=xhp
  xchp=xhp
  where (zclf < zcthr .and. (1.-zclf) < zcthr)
    xahp=xahp+hpfrc/(1.-zclf)
    xchp=xchp-hpfrc/zclf
  end where
  !
  !-----------------------------------------------------------------------
  !     * update density (internal mixture) and pla parameters for
  !     * clear-sky and cloudy-sky.
  !
  call nm2par (paen0,paephi0,paepsi,pephis0,pedphi0,pain0,paiphi0, &
                   paipsi,piphis0,pidphi0,paiddn,paenum,paemas,paeddn, &
                   painum,paimas,paifrc,ilga,leva)
  call nm2par (pcen0,pcephi0,pcepsi,pephis0,pedphi0,pcin0,pciphi0, &
                   pcipsi,piphis0,pidphi0,pciddn,pcenum,pcemas,pceddn, &
                   pcinum,pcimas,pcifrc,ilga,leva)
  if (isaint > 0) then
    psimas=pcimas
    psifrc=pcifrc
  end if
  !
  !-----------------------------------------------------------------------
  !     * collection efficiency for below-cloud scavenging.
  !
  call colleff (pencef,pemcef,paenum,paemas,paen0,paephi0, &
                    paepsi,pephis0,pedphi0,pewetrc,pedryrc,paeddn, &
                    ilga,leva,isaext)
  call colleff (pincef,pimcef,painum,paimas,pain0,paiphi0, &
                    paipsi,piphis0,pidphi0,piwetrc,pidryrc,paiddn, &
                    ilga,leva,isaint)
  !
  !-----------------------------------------------------------------------
  !     * temporary aerosol fields for sulphate aerosol for dry
  !     * and wet particle sizes for externally mixed aerosol type.
  !
  if (kext > 0) then
    do is=1,isaext
      tedphis(:,:,is)=pedphis(is)
      tephiss(:,:,is)=pephiss(is)
    end do
  end if
  !
  !     * temporary aerosol fields for sulphate aerosol for dry
  !     * and wet particle sizes for internally mixed aerosol type.
  !
  if (kintso4 > 0) then
    do is=1,isaint
      tidphis(:,:,is)=pidphis(is)
      tiphiss(:,:,is)=piphiss(is)
    end do
  end if
  !
  !     * overwrite section boundaries to account for critical
  !     * particle radius in the cloud. in-cloud processing is
  !     * only considered for particle radii that are equal or greater
  !     * than the critical radius. the numerical approach is to first find
  !     * the section that includes the critical radius. the lower section
  !     * boundary of that section is then redifined so that it matches
  !     * the critical size. subsequent calculations of in-cloud processes
  !     * are only applied to that and following sections.
  !
  if (kext > 0) then
    peicrit=ina
    do is=1,isaext
      kx=sextf%isaer(is)%ityp
      do l=1,leva
        do il=1,ilga
          if (pcepci(il,l,kx) >= tephiss(il,l,is) &
              .and. pcepci(il,l,kx) <  tephiss(il,l,is) &
              +tedphis(il,l,is)    ) then
            tedphis(il,l,is)=max(tephiss(il,l,is)+tedphis(il,l,is) &
                                  -pcepci(il,l,kx),0.)
            tephiss(il,l,is)=pcepci(il,l,kx)
            peicrit(il,l,kx)=is
          end if
        end do
      end do
    end do
  end if
  if (kint > 0) then
    piicrit=ina
    do is=1,isaint
      do l=1,leva
        do il=1,ilga
          if (pcipci(il,l) >= tiphiss(il,l,is) &
              .and. pcipci(il,l) <  tiphiss(il,l,is) &
              +tidphis(il,l,is)             ) then
            tidphis(il,l,is)=max(tiphiss(il,l,is)+tidphis(il,l,is) &
                                -pcipci(il,l),0.)
            tiphiss(il,l,is)=pcipci(il,l)
            piicrit(il,l)=is
          end if
        end do
      end do
    end do
  end if
  !
  !     * calculate non-activated aerosol concentration for the
  !     * truncated section that contains the critical particle size.
  !
  rmom=3.
  if (kext > 0) then
    tetrum=0.
    do kx=1,kext
      do l=1,leva
        do il=1,ilga
          is=peicrit(il,l,kx)
          if (is /= ina) then
            if (abs(pcepsi(il,l,is)-yna) > ytiny) then
              atmp2=pceddn(il,l,is)*ycnst*pcen0(il,l,is) &
                        *sdintb(pcephi0(il,l,is),pcepsi(il,l,is),rmom, &
                                tephiss(il,l,is),tedphis(il,l,is))
              tetrum(il,l,kx)=max(pcemas(il,l,is)-atmp2,0.)
            end if
          end if
        end do
      end do
    end do
  end if
  if (kint > 0) then
    titrum=0.
    do l=1,leva
      do il=1,ilga
        is=piicrit(il,l)
        if (is /= ina) then
          if (abs(pcipsi(il,l,is)-yna) > ytiny) then
            atmp2=pciddn(il,l,is)*ycnst*pcin0(il,l,is) &
                      *sdintb(pciphi0(il,l,is),pcipsi(il,l,is),rmom, &
                              tiphiss(il,l,is),tidphis(il,l,is))
            titrum(il,l)=max(pcimas(il,l,is)-atmp2,0.)
          end if
        end if
      end do
    end do
  end if
  !
  !     * initialize fields.
  !
  dsivdt=0.
  dhpdt=0.
  o3frc=0.
  h2o2frc=0.
  texsvi=0.
  texsvip=0.
  texsvia=0.
  tixsvi=0.
  xss=0.
  xbc=0.
  timass=0.
  timassp=0.
  timassa=0.
  tiden=0.
  !
  !     * cloudy-sky sulphate aerosol mass concentration
  !     * for activated particles.
  !
  if (kextso4 > 0) then
    kx=kextso4
    do l=1,leva
      do il=1,ilga
        isl=peicrit(il,l,kx)
        ish=aextf%tp(kx)%iso(aextf%tp(kx)%isec)
        if (isl /= ina) then
          do is=isl,ish
            texsvi(il,l)=texsvi(il,l)+pcemas(il,l,is)
          end do
          texsvi(il,l)=max(texsvi(il,l)-tetrum(il,l,kx),0.)
          texsvia(il,l)=pcemas(il,l,isl)
          texsvip(il,l)=max(texsvia(il,l)-tetrum(il,l,kx),0.)
        end if
      end do
    end do
  end if
  if (kintso4 > 0) then
    do l=1,leva
      do il=1,ilga
        isl=piicrit(il,l)
        if (isl /= ina) then
          do is=isl,isaint
            tixsvi(il,l)=tixsvi(il,l) &
                        +pcimas(il,l,is)*pcifrc(il,l,is,kintso4)
          end do
          tixsvi(il,l)=max(tixsvi(il,l)-titrum(il,l) &
                                   *pcifrc(il,l,isl,kintso4),0.)
        end if
      end do
    end do
  end if
  xsvi=(texsvi+tixsvi)*ws/wamsul
  !
  !     * cloudy-sky sea salt aerosol mass concentration
  !     * for activated particles.
  !
  if (kextss > 0) then
    kx=kextss
    do l=1,leva
      do il=1,ilga
        isl=peicrit(il,l,kx)
        ish=aextf%tp(kx)%iso(aextf%tp(kx)%isec)
        if (isl /= ina) then
          do is=isl,ish
            xss(il,l)=xss(il,l)+pcemas(il,l,is)
          end do
          xss(il,l)=max(xss(il,l)-tetrum(il,l,kx),0.)
        end if
      end do
    end do
  end if
  if (kintss > 0) then
    do l=1,leva
      do il=1,ilga
        isl=piicrit(il,l)
        if (isl /= ina) then
          do is=isl,isaint
            xss(il,l)=xss(il,l) &
                        +pcimas(il,l,is)*pcifrc(il,l,is,kintss)
          end do
          xss(il,l)=max(xss(il,l)-titrum(il,l) &
                                   *pcifrc(il,l,isl,kintss),0.)
        end if
      end do
    end do
  end if
  !
  !     * cloudy-sky black carbon aerosol mass concentration
  !     * for activated particles.
  !
  if (kextbc > 0) then
    kx=kextbc
    do l=1,leva
      do il=1,ilga
        isl=peicrit(il,l,kx)
        ish=aextf%tp(kx)%iso(aextf%tp(kx)%isec)
        if (isl /= ina) then
          do is=isl,ish
            xbc(il,l)=xbc(il,l)+pcemas(il,l,is)
          end do
          xbc(il,l)=max(xbc(il,l)-tetrum(il,l,kx),0.)
        end if
      end do
    end do
  end if
  if (kintbc > 0) then
    do l=1,leva
      do il=1,ilga
        isl=piicrit(il,l)
        if (isl /= ina) then
          do is=isl,isaint
            xbc(il,l)=xbc(il,l) &
                        +pcimas(il,l,is)*pcifrc(il,l,is,kintbc)
          end do
          xbc(il,l)=max(xbc(il,l)-titrum(il,l) &
                                   *pcifrc(il,l,isl,kintbc),0.)
        end if
      end do
    end do
  end if
  !
  !     * cloudy-sky total aerosol mass concentration
  !     * for activated particles.
  !
  if (kint > 0) then
    do l=1,leva
      do il=1,ilga
        isl=piicrit(il,l)
        if (isl /= ina) then
          do is=isl,isaint
            timass(il,l)=timass(il,l)+pcimas(il,l,is)
            tiden(il,l)=pcimas(il,l,is)/pciddn(il,l,is)
          end do
          timass(il,l)=max(timass(il,l)-titrum(il,l),0.)
          timassa(il,l)=pcimas(il,l,isl)
          timassp(il,l)=max(timassa(il,l)-titrum(il,l),0.)
          tiden(il,l)=max(tiden(il,l)-titrum(il,l) &
                                       /pciddn(il,l,isl),0.)
        end if
      end do
    end do
    where (2+abs(exponent(timass) - exponent(tiden)) &
        < maxexponent(timass) .and. tiden/=0. )
      tiden=timass/tiden
    else where
      tiden=1000.
    end where
  end if
  if (kextso4 > 0 .or. kintso4 > 0) then
    !
    !       * changes in so2, so4, and h2o2 concentrations from in-cloud
    !       * oxidation.
    !
    call suloxi (dxsiv,dxsvi,dxhp,zhenry,o3frc,h2o2frc,xsiv, &
                     xsvi,xo3,xchp,xna,xam,xss,zclf,zmlwc,ta,rhoa, &
                     co2_ppm,dt,ilga,leva,neqp)
    sdsm=dxsvi*wamsul/ws
    o3frc=-o3frc*wamsul/ws
    h2o2frc=-h2o2frc*wamsul/ws
    !
    !       * change in total in-cloud aerosol mass after oxidation.
    !
    pcesmas=0.
    pcismas=0.
    if (kext > 0) then
      pcesmas=sdsm
    end if
    if (kint > 0) then
      pcismas=sdsm
    end if
    !
    !       * account for changes in size distributions from in-cloud
    !       * production of sulphate. the additional mass is distributed
    !       * according to the assumption of volume-controlled growth
    !       * of droplets from in-cloud oxidation.
    !
    call sicprd (pcedndt,pcedmdt,pcidndt,pcidmdt,pcidfdt, &
                     dersn,dersm,dirsmn,dirsn,dirsms, &
                     texsvi,texsvip,texsvia,timass,timassp, &
                     timassa,tiden,peicrit,piicrit,pcesmas, &
                     pcemas,pcenum,pcen0,pcephi0,pcepsi,pceddn, &
                     pcismas,pcimas,pcinum,pcin0,pciphi0,pcipsi, &
                     pciddn,pcifrc,dt,ilga,leva,ierr)
    !
    !       * update residual terms for diagnostic purposes.
    !
    dersn =zclf*dersn
    dersm =zclf*dersm
    dirsmn=zclf*dirsmn
    dirsn =zclf*dirsn
    dirsms=zclf*dirsms
    !
    !       * update aerosol number and mass for in-cloud species to
    !       * account for in-cloud chemical production.
    !
    if (isaext > 0) then
      pcenum=max(pcenum+dt*pcedndt,0.)
      pcemas=max(pcemas+dt*pcedmdt,0.)
    end if
    if (isaint > 0) then
      pcinum=max(pcinum+dt*pcidndt,0.)
      pcimas=max(pcimas+dt*pcidmdt,0.)
      pcifrc=max(min(pcifrc+dt*pcidfdt,1.),0.)
      do is=1,isaint
        do ist=1,sintf%isaer(is)%itypt
          kx=sintf%isaer(is)%ityp(ist)
          pimasi(:,:,is,kx)=pimasi(:,:,is,kx) &
                          +zclf(:,:)*(pcifrc(:,:,is,kx)*pcimas(:,:,is) &
                                     -psifrc(:,:,is,kx)*psimas(:,:,is))
          pcitmp(:,:,is,kx)=pcifrc(:,:,is,kx)*pcimas(:,:,is)
        end do
      end do
    end if
    xcsiv=xcsiv+dxsiv
    xchp=xchp+dxhp
    !
    !-----------------------------------------------------------------------
    !       * update density (internal mixture) and pla parameters for
    !       * cloudy-sky.
    !
    call nm2par (pcen0,pcephi0,pcepsi,pephis0,pedphi0,pcin0,pciphi0, &
                     pcipsi,piphis0,pidphi0,pciddn,pcenum,pcemas,pceddn, &
                     pcinum,pcimas,pcifrc,ilga,leva)
  end if
  !
  !     * diagnose in-cloud production rate for sulphate.
  !
  dsicp=0.
  if (kextso4 > 0) then
    do is=1,isextso4
      dsicp(:,:)=dsicp(:,:)+pcedmdt(:,:,iexso4(is))
    end do
  end if
  if (kintso4 > 0) then
    do is=1,isintso4
      dsicp(:,:)=dsicp(:,:)+zclf(:,:) &
            *(pcifrc(:,:,iinso4(is),kintso4)*pcimas(:,:,iinso4(is)) &
             -psifrc(:,:,iinso4(is),kintso4)*psimas(:,:,iinso4(is)))/dt
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * wet removal.
  !
  if (isaext > 0) then
    !
    !       * scavenging efficiency. completely efficient in-cloud removal
    !       * is assumed for paricles containing soluble material with
    !       * radii that exceed the critical particle size for in-cloud
    !       * processing. no in-cloud removal for completely insoluble
    !       * aerosol.
    !
    do kx=1,kext
      do l=1,leva
        do il=1,ilga
          isi=aextf%tp(kx)%iso(1)
          isl=peicrit(il,l,kx)
          ish=aextf%tp(kx)%iso(aextf%tp(kx)%isec)
          islp=min(isl+1,ish)
          if (isl /= ina) then
            pehenn(il,l,isi:isl)=asrphob
            pehenm(il,l,isi:isl)=asrphob
            pehenn(il,l,islp:ish)=asrphil
            pehenm(il,l,islp:ish)=asrphil
          end if
        end do
      end do
    end do
    !
    !       * overwrite results for section containing critical paricle
    !       * size to account for partial removal in that section. the
    !       * removal efficiencies are determined as the fraction of
    !       * the aerosol whose particle sizes are greater than the
    !       * critical size relative to the total aerosol in the section.
    !
    rmom=3.
    do kx=1,kext
      do l=1,leva
        do il=1,ilga
          is=peicrit(il,l,kx)
          if (is /= ina) then
            if (abs(pcepsi(il,l,is)-yna) > ytiny) then
              aextsn=pcen0(il,l,is) &
                    *sdintb0(pcephi0(il,l,is),pcepsi(il,l,is), &
                           tephiss(il,l,is),tedphis(il,l,is))
              aextsm=pceddn(il,l,is)*ycnst*pcen0(il,l,is) &
                    *sdintb(pcephi0(il,l,is),pcepsi(il,l,is),rmom, &
                          tephiss(il,l,is),tedphis(il,l,is))
              if (pcenum(il,l,is) > ysmall &
                  .and. pcemas(il,l,is) > ysmall &
                  .and. abs(pcepsi(il,l,is)-yna) > ytiny) then
                wgtn=min(max(1.-aextsn/pcenum(il,l,is),0.),1.)
                pehenn(il,l,is)=asrphil*(1.-wgtn)+wgtn*asrphob
                wgtm=min(max(1.-aextsm/pcemas(il,l,is),0.),1.)
                pehenm(il,l,is)=asrphil*(1.-wgtm)+wgtm*asrphob
              end if
            end if
          end if
        end do
      end do
    end do
    !
    !       * mean removal efficiency for sulphate for diagnostic
    !       * purposes.
    !
    if (kextso4 > 0) then
      aterm=0.
      aterms=0.
      do is=1,isextso4
        aterm (:,:)=aterm (:,:)+pcemas(:,:,iexso4(is))
        aterms(:,:)=aterms(:,:)+pcemas(:,:,iexso4(is)) &
                                   *pehenm(:,:,iexso4(is))
      end do
      where (zclf > ytiny .and. 2+abs(exponent(aterms) - exponent(aterm)) &
          < maxexponent(aterms) .and. aterm/=0. )
        pehenr=aterms/aterm
      else where
        pehenr=0.
      end where
    end if
    !
    !       * calculate tendencies for wet removal for number.
    !
    call swetdep(pedndt,daenum,dcenum,dewr1n,dewr2n,dewr3n, &
                     dews1n,dews2n,dews3n,paenum,pcenum,pehenn, &
                     pencef,rhoa,dpa,pmrep,zmlwc,pfsnow,pfrain, &
                     zclf,clrfr,clrfs,pfevap,pfsubl,zcthr,dt, &
                     ilga,leva,isaext)
    !
    !       * diagnose wet removal tendencies for sulphate.
    !
    if (kextso4 > 0) then
      do is=1,isextso4
        dwr1n(:,:)=dwr1n(:,:)+dewr1n(:,:,iexso4(is))
        dwr2n(:,:)=dwr2n(:,:)+dewr2n(:,:,iexso4(is))
        dwr3n(:,:)=dwr3n(:,:)+dewr3n(:,:,iexso4(is))
        dws1n(:,:)=dws1n(:,:)+dews1n(:,:,iexso4(is))
        dws2n(:,:)=dws2n(:,:)+dews2n(:,:,iexso4(is))
        dws3n(:,:)=dws3n(:,:)+dews3n(:,:,iexso4(is))
      end do
    end if
    !
    !       * calculate tendencies for wet removal for mass.
    !
    call swetdep(pedmdt,daemas,dcemas,dewr1m,dewr2m,dewr3m, &
                     dews1m,dews2m,dews3m,paemas,pcemas,pehenm, &
                     pemcef,rhoa,dpa,pmrep,zmlwc,pfsnow,pfrain, &
                     zclf,clrfr,clrfs,pfevap,pfsubl,zcthr,dt, &
                     ilga,leva,isaext)
    !
    !       * diagnose wet removal tendencies for sulphate.
    !
    if (kextso4 > 0) then
      do is=1,isextso4
        dwr1m(:,:)=dwr1m(:,:)+dewr1m(:,:,iexso4(is))
        dwr2m(:,:)=dwr2m(:,:)+dewr2m(:,:,iexso4(is))
        dwr3m(:,:)=dwr3m(:,:)+dewr3m(:,:,iexso4(is))
        dws1m(:,:)=dws1m(:,:)+dews1m(:,:,iexso4(is))
        dws2m(:,:)=dws2m(:,:)+dews2m(:,:,iexso4(is))
        dws3m(:,:)=dws3m(:,:)+dews3m(:,:,iexso4(is))
      end do
    end if
  end if
  if (isaint > 0) then
    !
    !       * scavenging efficiency.
    !
    do l=1,leva
      do il=1,ilga
        isi=1
        isl=piicrit(il,l)
        ish=isaint
        islp=min(isl+1,ish)
        if (isl /= ina) then
          pihenn(il,l,isi:isl)=asrphob
          pihenm(il,l,isi:isl)=asrphob
          pihenn(il,l,islp:ish)=asrphil
          pihenm(il,l,islp:ish)=asrphil
        end if
      end do
    end do
    !
    !       * overwrite results for section containing critical paricle
    !       * size to account for partial removal in that section.
    !
    rmom=3.
    do l=1,leva
      do il=1,ilga
        is=piicrit(il,l)
        if (is /= ina) then
          if (abs(pcipsi(il,l,is)-yna) > ytiny) then
            aintsn=pcin0(il,l,is) &
                  *sdintb0(pciphi0(il,l,is),pcipsi(il,l,is), &
                           tiphiss(il,l,is),tidphis(il,l,is))
            aintsm=pciddn(il,l,is)*ycnst*pcin0(il,l,is) &
                  *sdintb(pciphi0(il,l,is),pcipsi(il,l,is),rmom, &
                          tiphiss(il,l,is),tidphis(il,l,is))
            if (pcinum(il,l,is) > ysmall &
                .and. pcimas(il,l,is) > ysmall) then
              wgtn=min(max(1.-aintsn/pcinum(il,l,is),0.),1.)
              pihenn(il,l,is)=asrphil*(1.-wgtn)+wgtn*asrphob
              wgtm=min(max(1.-aintsm/pcimas(il,l,is),0.),1.)
              pihenm(il,l,is)=asrphil*(1.-wgtm)+wgtm*asrphob
            end if
          end if
        end if
      end do
    end do
    !
    !       * mean removal efficiency for diagnostic purposes.
    !
    aterm=sum(pcimas,dim=3)
    aterms=sum(pcimas*pihenm,dim=3)
    where (zclf > ytiny .and. 2+abs(exponent(aterms) - exponent(aterm)) &
        < maxexponent(aterms) .and. aterm/=0. )
      pihenr=aterms/aterm
    else where
      pihenr=0.
    end where
    !
    !       * calculate tendencies for wet removal for number.
    !
    call swetdep(pidndt,dainum,dcinum,diwr1n,diwr2n,diwr3n, &
                     diws1n,diws2n,diws3n,painum,pcinum,pihenn, &
                     pincef,rhoa,dpa,pmrep,zmlwc,pfsnow,pfrain, &
                     zclf,clrfr,clrfs,pfevap,pfsubl,zcthr,dt, &
                     ilga,leva,isaint)
    !
    !       * diagnose wet removal tendencies for sulphate.
    !
    if (kintso4 > 0) then
      do is=1,isintso4
        dwr1n(:,:)=dwr1n(:,:)+diwr1n(:,:,iinso4(is))
        dwr2n(:,:)=dwr2n(:,:)+diwr2n(:,:,iinso4(is))
        dwr3n(:,:)=dwr3n(:,:)+diwr3n(:,:,iinso4(is))
        dws1n(:,:)=dws1n(:,:)+diws1n(:,:,iinso4(is))
        dws2n(:,:)=dws2n(:,:)+diws2n(:,:,iinso4(is))
        dws3n(:,:)=dws3n(:,:)+diws3n(:,:,iinso4(is))
      end do
    end if
    do is=1,isaint
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        !
        !         * save results for each individual chemical species and
        !         * each section.
        !
        pshenm(:,:,isx)=pihenm(:,:,is)
        pasmas(:,:,isx)=paimas(:,:,is)*paifrc(:,:,is,kx)
        pcsmas(:,:,isx)=pcimas(:,:,is)*pcifrc(:,:,is,kx)
        psmcef(:,:,isx)=pimcef(:,:,is)
        !
        !         * calculate tendencies for wet removal for mass.
        !
        call swetdep(psdmdt,dasmas,dcsmas,diwr1m,diwr2m,diwr3m, &
                       diws1m,diws2m,diws3m,pasmas,pcsmas,pshenm, &
                       psmcef,rhoa,dpa,pmrep,zmlwc,pfsnow,pfrain, &
                       zclf,clrfr,clrfs,pfevap,pfsubl,zcthr,dt, &
                       ilga,leva,isx)
        !
        !         * calculate total tendencies for all species in each section.
        !
        pidmdt(:,:,is)=pidmdt(:,:,is)+psdmdt(:,:,isx)
        dcimas(:,:,is)=dcimas(:,:,is)+dcsmas(:,:,isx)
        daimas(:,:,is)=daimas(:,:,is)+dasmas(:,:,isx)
        !
        !         * calculate new masses for each species after in-cloud production
        !         * and wet removal. for all-sky, clear-sky, and cloudy sky.
        !
        pimasi(:,:,is,kx)=pimasi(:,:,is,kx)+dt*psdmdt(:,:,isx)
        paitmp(:,:,is,kx)=pasmas(:,:,isx)-dasmas(:,:,isx)
        pcitmp(:,:,is,kx)=pcsmas(:,:,isx)-dcsmas(:,:,isx)
        !
        !         * diagnose wet removal tendencies for sulphate.
        !
        if (kx == kintso4) then
          dwr1m(:,:)=dwr1m(:,:)+diwr1m(:,:,isx)
          dwr2m(:,:)=dwr2m(:,:)+diwr2m(:,:,isx)
          dwr3m(:,:)=dwr3m(:,:)+diwr3m(:,:,isx)
          dws1m(:,:)=dws1m(:,:)+diws1m(:,:,isx)
          dws2m(:,:)=dws2m(:,:)+diws2m(:,:,isx)
          dws3m(:,:)=dws3m(:,:)+diws3m(:,:,isx)
        end if
      end do
    end do
  end if
  if (kextso4>0 .or. kintso4>0) then
    !
    !       * wet removal for sulphur dioxide. no below-cloud scavenging.
    !
    paxmas(:,:,isx)=xasiv
    pcxmas(:,:,isx)=xcsiv
    pxhenm(:,:,isx)=zhenry
    pxmcef(:,:,isx)=0.
    call swetdep(pxdmdt,daxmas,dcxmas,dxwr1m,dxwr2m,dxwr3m, &
                     dxws1m,dxws2m,dxws3m,paxmas,pcxmas,pxhenm, &
                     pxmcef,rhoa,dpa,pmrep,zmlwc,pfsnow,pfrain, &
                     zclf,clrfr,clrfs,pfevap,pfsubl,zcthr,dt, &
                     ilga,leva,isx)
  end if
  !
  !-----------------------------------------------------------------------
  !     * update aerosol number and mass for clear-sky and in-cloud
  !     * aerosol species to account for effects of wet removal. for
  !     * internally mixed species, the mass fractions may change
  !     * due to wet removal so that the corresponding results for
  !     * mass will be calculated later.
  !
  if (isaext > 0) then
    pcenum=max(pcenum-dcenum,0.)
    pcemas=max(pcemas-dcemas,0.)
    paenum=max(paenum-daenum,0.)
    paemas=max(paemas-daemas,0.)
  end if
  if (isaint > 0) then
    pcinum=max(pcinum-dcinum,0.)
    painum=max(painum-dainum,0.)
  end if
  if (kextso4>0 .or. kintso4>0) then
    xcsiv=max(xcsiv-dcxmas(:,:,1),0.)
    xasiv=max(xasiv-daxmas(:,:,1),0.)
  end if
  !
  !     * update number and mass tendencies for externally mixed aerosol
  !     * for each externally mixed species after in-cloud production
  !     * and wet removal. for grid-cell mean results.
  !
  if (isaext > 0) then
    do is=1,isaext
      pedndt(:,:,is)=pedndt(:,:,is)+zclf(:,:)*pcedndt(:,:,is)
      pedmdt(:,:,is)=pedmdt(:,:,is)+zclf(:,:)*pcedmdt(:,:,is)
    end do
  end if
  if (isaint > 0) then
    !
    !       * total mass for all species.
    !
    pimast=0.
    do is=1,isaint
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        pimast(:,:,is)=pimast(:,:,is)+pimasi(:,:,is,kx)
      end do
    end do
    !
    !       * change in mass fraction for each species.
    !
    do is=1,isaint
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        do l=1,leva
          do il=1,ilga
            if (2+abs(exponent(pimasi(il,l,is,kx)) - exponent(pimast(il,l,is))) &
                < maxexponent(pimasi(il,l,is,kx)) .and. pimast(il,l,is)/=0. &
                .and. abs(pipsi(il,l,is)-yna) > ytiny) then
              frcn=max(min(pimasi(il,l,is,kx)/pimast(il,l,is),1.),0.)
              pidfdt(il,l,is,kx)=(frcn-pifrc(il,l,is,kx))/dt
            else
              pidfdt(il,l,is,kx)=0.
            end if
          end do
        end do
      end do
    end do
    !
    !       * update number and mass tendencies for internally mixed aerosol.
    !
    do is=1,isaint
      pidndt(:,:,is)=pidndt(:,:,is)+zclf(:,:)*pcidndt(:,:,is)
      pidmdt(:,:,is)=pidmdt(:,:,is)+zclf(:,:)*pcidmdt(:,:,is)
    end do
  end if
  !
  !       * update tendencies for gases.
  !
  if (kextso4>0 .or. kintso4>0) then
    dsivdt=pxdmdt(:,:,1)+zclf*dxsiv/dt
    dhpdt=zclf*dxhp/dt
  end if
  !
  !-----------------------------------------------------------------------
  !     * update clear/cloud concentration differences.
  !
  if (isaext > 0) then
    penfrc=0.
    pemfrc=0.
    do is=1,isaext
      where (abs(pepsi(:,:,is)-yna) > ytiny)
        where (zclf(:,:) < zcthr .and. (1.-zclf(:,:)) < zcthr)
          penfrc(:,:,is)=zclf(:,:)*(1.-zclf(:,:)) &
                                       *(paenum(:,:,is)-pcenum(:,:,is))
          pemfrc(:,:,is)=zclf(:,:)*(1.-zclf(:,:)) &
                                       *(paemas(:,:,is)-pcemas(:,:,is))
        end where
      end where
    end do
  end if
  if (isaint > 0) then
    pinfrc=0.
    do is=1,isaint
      where (abs(pipsi(:,:,is)-yna) > ytiny)
        where (zclf(:,:) < zcthr .and. (1.-zclf(:,:)) < zcthr)
          pinfrc(:,:,is)=zclf(:,:)*(1.-zclf(:,:)) &
                                       *(painum(:,:,is)-pcinum(:,:,is))
        end where
      end where
    end do
    pimfrc=0.
    do it=1,kint
      do is=1,isaint
        where (abs(pipsi(:,:,is)-yna) > ytiny)
          where (zclf(:,:) < zcthr .and. (1.-zclf(:,:)) < zcthr)
            pimfrc(:,:,is,it)=zclf(:,:)*(1.-zclf(:,:)) &
                                 *(paitmp(:,:,is,it)-pcitmp(:,:,is,it))
          end where
        end where
      end do
    end do
  end if
  if (kextso4>0 .or. kintso4>0) then
    xsivfrc=0.
    where (zclf < zcthr .and. (1.-zclf) < zcthr)
      xsivfrc=zclf*(1.-zclf)*(xasiv-xcsiv)
    end where
    hpfrc=0.
    where (zclf < zcthr .and. (1.-zclf) < zcthr)
      hpfrc=zclf*(1.-zclf)*(xahp-xchp)
    end where
  end if
  !
  !     * reset if necessary.
  !
  if (isaext > 0) then
    where (abs(pepsi-yna) <= ytiny)
      pedndt=0.
      pedmdt=0.
    end where
  end if
  if (isaint > 0) then
    where (abs(pipsi-yna) <= ytiny)
      pidndt=0.
      pidmdt=0.
    end where
  end if
  !
  !-----------------------------------------------------------------------
  !     * deallocation.
  !
  if (isaext > 0) then
    deallocate(pcenum)
    deallocate(pcemas)
    deallocate(dcenum)
    deallocate(dcemas)
    deallocate(pcen0)
    deallocate(pcephi0)
    deallocate(pcepsi)
    deallocate(pceddn)
    deallocate(pcedndt)
    deallocate(pcedmdt)
    deallocate(paenum)
    deallocate(paemas)
    deallocate(daenum)
    deallocate(daemas)
    deallocate(paen0)
    deallocate(paephi0)
    deallocate(paepsi)
    deallocate(paeddn)
    deallocate(pehenn)
    deallocate(pehenm)
    deallocate(pencef)
    deallocate(pemcef)
    deallocate(penscl)
    deallocate(pemscl)
    deallocate(peicrit)
    deallocate(dewr1n)
    deallocate(dewr2n)
    deallocate(dewr3n)
    deallocate(dews1n)
    deallocate(dews2n)
    deallocate(dews3n)
    deallocate(dewr1m)
    deallocate(dewr2m)
    deallocate(dewr3m)
    deallocate(dews1m)
    deallocate(dews2m)
    deallocate(dews3m)
    deallocate(tedphis)
    deallocate(tephiss)
    deallocate(tetrum)
  end if
  if (isaint > 0) then
    deallocate(pcinum)
    deallocate(pcimas)
    deallocate(dcinum)
    deallocate(dcimas)
    deallocate(pcin0)
    deallocate(pciphi0)
    deallocate(pcipsi)
    deallocate(pciddn)
    deallocate(pcidndt)
    deallocate(pcidmdt)
    deallocate(pcifrc)
    deallocate(pcidfdt)
    deallocate(paitmp)
    deallocate(pcitmp)
    deallocate(pitmpc)
    deallocate(pimasi)
    deallocate(painum)
    deallocate(paimas)
    deallocate(dainum)
    deallocate(daimas)
    deallocate(pain0)
    deallocate(paiphi0)
    deallocate(paipsi)
    deallocate(paiddn)
    deallocate(paifrc)
    deallocate(pimast)
    deallocate(pihenn)
    deallocate(pihenm)
    deallocate(psimas)
    deallocate(pshenm)
    deallocate(pasmas)
    deallocate(pcsmas)
    deallocate(psdmdt)
    deallocate(dasmas)
    deallocate(dcsmas)
    deallocate(psmcef)
    deallocate(psifrc)
    deallocate(pincef)
    deallocate(pimcef)
    deallocate(pinscl)
    deallocate(pimscl)
    deallocate(piicrit)
    deallocate(diwr1n)
    deallocate(diwr2n)
    deallocate(diwr3n)
    deallocate(diws1n)
    deallocate(diws2n)
    deallocate(diws3n)
    deallocate(diwr1m)
    deallocate(diwr2m)
    deallocate(diwr3m)
    deallocate(diws1m)
    deallocate(diws2m)
    deallocate(diws3m)
    deallocate(tidphis)
    deallocate(tiphiss)
    deallocate(titrum)
  end if
  !
end subroutine scldsp
