!> \file
!> \brief Basic parameters for CCN calculations, relevant to tabulated
!>       growth data.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
module cnparmb
  !
  implicit none
  !
  type pntdp
    integer :: ibt !<
    logical, allocatable, dimension(:) :: flg !<
    real, allocatable, dimension(:) :: bsbmin !<
    real, allocatable, dimension(:) :: bsbmax !<
    real, allocatable, dimension(:,:) :: bsb !<
    real, allocatable, dimension(:,:) :: pntl !<
    real, allocatable, dimension(:,:) :: derl !<
    real, allocatable, dimension(:,:) :: pntu !<
    real, allocatable, dimension(:,:) :: deru !<
  end type pntdp
  type pntds
    real, allocatable, dimension(:)  :: pnt !<
    real, allocatable, dimension(:)  :: der !<
  end type pntds
  type reg
    integer :: ibt !<
    logical, allocatable, dimension(:,:) :: kopen !<
    integer, allocatable, dimension(:,:) :: ipnts !<
    type(pntds), allocatable, dimension(:,:) :: xp,tp
  end type reg
  !
end module cnparmb
