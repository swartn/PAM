!> \file
!> \brief Calculation of dry aerosol densities, number of dissolved ions,
!>       their molecular weight, and mass fraction of soluble material
!>       for internally mixed aerosol for purpose of aerosol activation
!>       calculations. Only sub-sections with potentially at least one
!>       soluble species are considered.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine scmicp(ciddnc,ciddnsl,ciddnis,cinuio,cikappa,ciepsm, &
                       cimolw,ciwetrb,cewetrb,pifrc,pewetrc,piwetrc, &
                       ilga,leva)
  !
  use scparm, only : cedryrb,cidryrb,isesc,isextb,isint,isintb
  use sdparm, only : aintf,isaext,isaint,iswext,iswint,kint, &
                         pedryrc,pidryrc,sextf,sintf,yna,ytiny
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !<
  real, intent(in), dimension(ilga,leva,isaext) :: pewetrc !<
  real, intent(in), dimension(ilga,leva,isaint) :: piwetrc !<
  real, intent(out), dimension(ilga,leva,isint) :: ciddnc !<
  real, intent(out), dimension(ilga,leva,isintb) :: ciddnsl !<
  real, intent(out), dimension(ilga,leva,isintb) :: ciddnis !<
  real, intent(out), dimension(ilga,leva,isintb) :: cinuio !<
  real, intent(out), dimension(ilga,leva,isintb) :: cikappa !<
  real, intent(out), dimension(ilga,leva,isintb) :: ciepsm !<
  real, intent(out), dimension(ilga,leva,isintb) :: cimolw !<
  real, intent(out), dimension(ilga,leva,isintb) :: ciwetrb !<
  real, intent(out), dimension(ilga,leva,isextb) :: cewetrb !<
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:) :: amsl !<
  real, allocatable, dimension(:,:) :: avsl !<
  real, allocatable, dimension(:,:) :: amis !<
  real, allocatable, dimension(:,:) :: avis !<
  real, allocatable, dimension(:,:) :: anuio !<
  real, allocatable, dimension(:,:) :: kappa !<
  real, allocatable, dimension(:,:) :: amolw !<
  real, allocatable, dimension(:,:) :: dvol !<
  real, allocatable, dimension(:,:,:) :: dm !<
  real, allocatable, dimension(:,:,:) :: dv !<
  real, allocatable, dimension(:,:,:) :: dmsl0 !<
  real, allocatable, dimension(:,:,:) :: dvsl0 !<
  real, allocatable, dimension(:,:,:) :: dmis0 !<
  real, allocatable, dimension(:,:,:) :: dvis0 !<
  real, allocatable, dimension(:,:,:) :: nuio0 !<
  real, allocatable, dimension(:,:,:) :: kappa0 !<
  real, allocatable, dimension(:,:,:) :: molw0 !<
  real, dimension(ilga,leva) :: grwthf !<
  integer :: il !<
  integer :: is !<
  integer :: isc !<
  integer :: l !<
  integer :: ist !<
  integer :: kxp !<
  integer :: kx !<
  integer :: kxt !<
  integer :: isi !<
  integer :: ism !<
  real :: afac !<
  real :: dprat !<
  !
  !-----------------------------------------------------------------------
  !
  if (isintb > 0) then
    !
    !       * allocate work arrays.
    !
    allocate(dm    (ilga,leva,iswint))
    allocate(dv    (ilga,leva,iswint))
    allocate(dmsl0 (ilga,leva,iswint))
    allocate(dmis0 (ilga,leva,iswint))
    allocate(dvsl0 (ilga,leva,iswint))
    allocate(dvis0 (ilga,leva,iswint))
    allocate(nuio0 (ilga,leva,iswint))
    allocate(kappa0(ilga,leva,iswint))
    allocate(molw0 (ilga,leva,iswint))
    allocate(amsl  (ilga,leva))
    allocate(avsl  (ilga,leva))
    allocate(amis  (ilga,leva))
    allocate(avis  (ilga,leva))
    allocate(anuio (ilga,leva))
    allocate(kappa (ilga,leva))
    allocate(amolw (ilga,leva))
    allocate(dvol  (ilga,leva))
    !
    !       * masses and volumes for soluble (sl) and insoluble (is)
    !       * aerosol fractions, mean number of dissociated ions, and their
    !       * molecular weight in each section. weighting by volume for
    !       * microphysical parameters (zsr rule).
    !
    dmsl0 =0.
    dmis0 =0.
    dvsl0 =0.
    dvis0 =0.
    nuio0 =0.
    kappa0=0.
    molw0 =0.
    do is=1,iswint
      do kxt=1,sintf%iswet(is)%itypt
        kx=sintf%iswet(is)%ityp(kxt)
        isi=sintf%iswet(is)%isi
        dvol=pifrc(:,:,isi,kx)/aintf%tp(kx)%dens
        if (     (abs(aintf%tp(kx)%nuio-yna) > ytiny &
            .and. aintf%tp(kx)%nuio  > ytiny) &
            .or. (abs(aintf%tp(kx)%kappa-yna) > ytiny &
            .and. aintf%tp(kx)%kappa > ytiny) ) then
          dmsl0 (:,:,is)=dmsl0 (:,:,is)+pifrc(:,:,isi,kx)
          dvsl0 (:,:,is)=dvsl0 (:,:,is)+dvol(:,:)
          nuio0 (:,:,is)=nuio0 (:,:,is)+dvol(:,:)*aintf%tp(kx)%nuio
          kappa0(:,:,is)=kappa0(:,:,is)+dvol(:,:)*aintf%tp(kx)%kappa
          molw0 (:,:,is)=molw0 (:,:,is)+dvol(:,:)*aintf%tp(kx)%molw
        else
          dmis0 (:,:,is)=dmis0 (:,:,is)+pifrc(:,:,isi,kx)
          dvis0 (:,:,is)=dvis0 (:,:,is)+dvol(:,:)
        end if
      end do
    end do
    dm=dmsl0+dmis0
    dv=dvsl0+dvis0
    !
    !       * density of total dry aerosol in the centre of sub-sections.
    !       * for each sub-section, the density is the same as in the
    !       * density for the sections in which in the sub-section resides.
    !
    ciddnc=yna
    isc=0
    do is=1,iswint
      do ist=1,isesc
        isc=isc+1
        do l=1,leva
          do il=1,ilga
            if (2+abs(exponent(dm(il,l,is)) - exponent(dv(il,l,is))) &
                < maxexponent(dm(il,l,is)) .and. dv(il,l,is)/=0. ) then
              ciddnc(il,l,isc)=dm(il,l,is)/dv(il,l,is)
            end if
          end do
        end do
      end do
    end do
    !
    !       * densities for soluble (dnsl) and insoluble (dnis) aerosol
    !       * fractions at boundaries of sub-sections from linear
    !       * interpolation of paricle masses and volumes between the
    !       * mid-points of the pla size sections. the same approach
    !       * is used to calculate the mean number of ions in solution and
    !       * the corresponding molecular weight.
    !
    ciddnsl=yna
    ciddnis=yna
    cinuio =yna
    cikappa=yna
    ciepsm =yna
    cimolw =yna
    dprat=1./real(isesc)
    is=1
    do isc=1,isesc/2
      do l=1,leva
        do il=1,ilga
          if (dvsl0(il,l,is) > ytiny) then
            ciddnsl(il,l,isc)=dmsl0 (il,l,is)/dvsl0(il,l,is)
            cinuio (il,l,isc)=nuio0 (il,l,is)/dvsl0(il,l,is)
            cikappa(il,l,isc)=kappa0(il,l,is)/dvsl0(il,l,is)
            cimolw (il,l,isc)=molw0 (il,l,is)/dvsl0(il,l,is)
          end if
          if (dvis0(il,l,is) > ytiny) then
            ciddnis(il,l,isc)=dmis0(il,l,is)/dvis0(il,l,is)
          end if
          if (dm(il,l,is) > ytiny) then
            ciepsm (il,l,isc)=dmsl0(il,l,is)/dm(il,l,is)
          end if
        end do
      end do
    end do
    isc=isesc/2
    do is=1,iswint-1
      do ist=1,isesc
        isc=isc+1
        afac=real(ist-1)*dprat
        amsl (:,:)=dmsl0 (:,:,is)*(1.-afac)+dmsl0 (:,:,is+1)*afac
        avsl (:,:)=dvsl0 (:,:,is)*(1.-afac)+dvsl0 (:,:,is+1)*afac
        amis (:,:)=dmis0 (:,:,is)*(1.-afac)+dmis0 (:,:,is+1)*afac
        avis (:,:)=dvis0 (:,:,is)*(1.-afac)+dvis0 (:,:,is+1)*afac
        anuio(:,:)=nuio0 (:,:,is)*(1.-afac)+nuio0 (:,:,is+1)*afac
        kappa(:,:)=kappa0(:,:,is)*(1.-afac)+kappa0(:,:,is+1)*afac
        amolw(:,:)=molw0 (:,:,is)*(1.-afac)+molw0 (:,:,is+1)*afac
        do l=1,leva
          do il=1,ilga
            if (avsl(il,l) > ytiny) then
              ciddnsl(il,l,isc)=amsl (il,l)/avsl(il,l)
              cinuio (il,l,isc)=anuio(il,l)/avsl(il,l)
              cikappa(il,l,isc)=kappa(il,l)/avsl(il,l)
              cimolw (il,l,isc)=amolw(il,l)/avsl(il,l)
            end if
            if (avis(il,l) > ytiny) then
              ciddnis(il,l,isc)=amis(il,l)/avis(il,l)
            end if
            if ( (amsl(il,l)+amis(il,l)) > ytiny) then
              ciepsm (il,l,isc)=amsl(il,l)/(amsl(il,l)+amis(il,l))
            end if
          end do
        end do
      end do
    end do
    is=iswint
    do isc=isintb-isesc/2,isintb
      do l=1,leva
        do il=1,ilga
          if (dvsl0(il,l,is) > ytiny) then
            ciddnsl(il,l,isc)=dmsl0 (il,l,is)/dvsl0(il,l,is)
            cinuio (il,l,isc)=nuio0 (il,l,is)/dvsl0(il,l,is)
            cikappa(il,l,isc)=kappa0(il,l,is)/dvsl0(il,l,is)
            cimolw (il,l,isc)=molw0 (il,l,is)/dvsl0(il,l,is)
          end if
          if (dvis0(il,l,is) > ytiny) then
            ciddnis(il,l,isc)=dmis0(il,l,is)/dvis0(il,l,is)
          end if
          if (dm(il,l,is) > ytiny) then
            ciepsm (il,l,isc)=dmsl0(il,l,is)/dm(il,l,is)
          end if
        end do
      end do
    end do
    !
    !       * deallocate work arrays.
    !
    deallocate(dm)
    deallocate(dv)
    deallocate(dmsl0)
    deallocate(dmis0)
    deallocate(dvsl0)
    deallocate(dvis0)
    deallocate(nuio0)
    deallocate(kappa0)
    deallocate(molw0)
    deallocate(amsl)
    deallocate(avsl)
    deallocate(amis)
    deallocate(avis)
    deallocate(anuio)
    deallocate(kappa)
    deallocate(amolw)
    deallocate(dvol)
  end if
  !
  !     * supersaturation.
  !
  !      ysmall2=1.e-06
  !      where ( abs(sv) >= ysmall2)
  !        svt=sv
  !      else where
  !        svt=-ysmall2
  !      end where
  !
  !     * equilibrium particle sizes for given superaturation.
  !
  !      if ( isextb > 0) then
  !        call equis(cewetrb,cedryrb,cebk,ak,svt,ilga,leva,isextb)
  !      end if
  !      if ( isintb > 0) then
  !        call equis(ciwetrb,cidryrb,cebk,ak,svt,ilga,leva,isintb)
  !      end if
  !      if ( isextb > 0) then
  !        do is=1,isextb
  !          cewetrb(:,:,is)=2.*cedryrb(is)
  !        end do
  !      end if
  !      if ( isintb > 0) then
  !        do is=1,isintb
  !          ciwetrb(:,:,is)=2.*cidryrb(is)
  !        end do
  !      end if
  !
  !     * equilibrium particle sizes for undersaturated conditions.
  !     * simply apply the same growth factor within each pla section
  !     * in calculation of particle sizes at boundaries of smaller
  !     * sections.
  !
  if (isextb > 0) then
    isc=0
    do is=1,iswext-1
      ism=sextf%iswet(is)%ism
      kx =sextf%iswet(is)%ityp
      kxp=sextf%iswet(is+1)%ityp
      grwthf=pewetrc(:,:,ism)/pedryrc(ism)
      do ist=1,isesc
        isc=isc+1
        cewetrb(:,:,isc)=grwthf*cedryrb(isc)
      end do
      if (kxp /= kx) then
        isc=isc+1
        cewetrb(:,:,isc)=grwthf*cedryrb(isc)
      end if
    end do
    is=iswext
    ism=sextf%iswet(is)%ism
    grwthf=pewetrc(:,:,ism)/pedryrc(ism)
    do ist=1,isesc
      isc=isc+1
      cewetrb(:,:,isc)=grwthf*cedryrb(isc)
    end do
    isc=isc+1
    cewetrb(:,:,isc)=grwthf*cedryrb(isc)
  end if
  if (isintb > 0) then
    isc=0
    do is=1,iswint
      grwthf=piwetrc(:,:,is)/pidryrc(is)
      do ist=1,isesc
        isc=isc+1
        ciwetrb(:,:,isc)=grwthf*cidryrb(isc)
      end do
    end do
    is=iswint
    grwthf=piwetrc(:,:,is)/pidryrc(is)
    isc=isc+1
    ciwetrb(:,:,isc)=grwthf*cidryrb(isc)
  end if
  !
end subroutine scmicp
