!> \file
!> \brief Calculation of the amount of water in the aerosol or cloud
!>       droplet. The necessary moments from the input are calculated
!>       in SCMOM. Make sure that the moments are consistent with the
!>       wet aerosol sizes that are used in this in this subroutine.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine scwatm(qrc,cewmass,ciwmass,cen0,cepsi,cewetrb,cemom2, &
                        cemom3,cemom4,cemom5,cemom6,cemom7,cemom8, &
                        cin0,cipsi,ciwetrb,cimom2,cimom3,cimom4,cimom5, &
                        cimom6,cimom7,cimom8,ilga,leva)
  !
  use scparm, only : cedryr,cedryrb,ceisvl,ceisvr, &
                         cidryr,cidryrb,ciisvl,ciisvr, &
                         isext,isextb,isint,isintb
  use sdparm, only : ycnst,yna,ytiny
  use sdphys, only : rhoh2o
  use fpdef,  only : r8
  !
  implicit none
  !
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  real, intent(in), dimension(ilga,leva,isextb) :: cewetrb !<
  real, intent(in), dimension(ilga,leva,isintb) :: ciwetrb !<
  real, intent(in), dimension(ilga,leva,isext) :: cen0 !<
  real, intent(in), dimension(ilga,leva,isext) :: cepsi !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom2 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom3 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom4 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom5 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom6 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom7 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom8 !<
  real, intent(in), dimension(ilga,leva,isint) :: cin0 !<
  real, intent(in), dimension(ilga,leva,isint) :: cipsi !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom2 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom3 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom4 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom5 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom6 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom7 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom8 !<
  real, intent(out), dimension(ilga,leva) :: qrc !<
  real, intent(out), dimension(ilga,leva,isext) :: cewmass !<
  real, intent(out), dimension(ilga,leva,isint) :: ciwmass !<
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:,:) :: cetmpb !<
  real(r8), allocatable, dimension(:,:,:) :: cea1 !<
  real(r8), allocatable, dimension(:,:,:) :: cea2 !<
  real(r8), allocatable, dimension(:,:,:) :: ceaa !<
  real(r8), allocatable, dimension(:,:,:) :: ceab !<
  real(r8), allocatable, dimension(:,:,:) :: ceac !<
  real(r8), allocatable, dimension(:,:,:) :: cead !<
  real(r8), allocatable, dimension(:,:,:) :: cewt !<
  real(r8), allocatable, dimension(:,:,:) :: cewlow !<
  real(r8), allocatable, dimension(:,:,:) :: cewhig !<
  real, allocatable, dimension(:,:,:) :: citmpb !<
  real(r8), allocatable, dimension(:,:,:) :: cia1 !<
  real(r8), allocatable, dimension(:,:,:) :: cia2 !<
  real(r8), allocatable, dimension(:,:,:) :: ciaa !<
  real(r8), allocatable, dimension(:,:,:) :: ciab !<
  real(r8), allocatable, dimension(:,:,:) :: ciac !<
  real(r8), allocatable, dimension(:,:,:) :: ciad !<
  real(r8), allocatable, dimension(:,:,:) :: ciwt !<
  real(r8), allocatable, dimension(:,:,:) :: ciwlow !<
  real(r8), allocatable, dimension(:,:,:) :: ciwhig !<
  integer :: is !<
  integer :: il !<
  integer :: l !<
  real :: afact !<
  real :: aflow !<
  real :: afhig !<
  !
  !-----------------------------------------------------------------------
  !     * allocate work arrays.
  !
  if (isext > 0) then
    allocate(cea1  (ilga,leva,isext))
    allocate(cea2  (ilga,leva,isext))
    allocate(ceaa  (ilga,leva,isext))
    allocate(ceab  (ilga,leva,isext))
    allocate(ceac  (ilga,leva,isext))
    allocate(cead  (ilga,leva,isext))
    allocate(cewlow(ilga,leva,isext))
    allocate(cewhig(ilga,leva,isext))
    allocate(cewt  (ilga,leva,isext))
  end if
  if (isextb > 0) then
    allocate(cetmpb(ilga,leva,isextb))
  end if
  if (isint > 0) then
    allocate(cia1  (ilga,leva,isint))
    allocate(cia2  (ilga,leva,isint))
    allocate(ciaa  (ilga,leva,isint))
    allocate(ciab  (ilga,leva,isint))
    allocate(ciac  (ilga,leva,isint))
    allocate(ciad  (ilga,leva,isint))
    allocate(ciwlow(ilga,leva,isint))
    allocate(ciwhig(ilga,leva,isint))
    allocate(ciwt  (ilga,leva,isint))
  end if
  if (isintb > 0) then
    allocate(citmpb(ilga,leva,isintb))
  end if
  !
  !     * growth factor for externally mixed aerosol types.
  !
  do is=1,isextb
    cetmpb(:,:,is)=max(cewetrb(:,:,is)/cedryrb(is)-1.,0.)
  end do
  !
  !     * growth factor for internally mixed aerosol types.
  !
  do is=1,isintb
    citmpb(:,:,is)=max(ciwetrb(:,:,is)/cidryrb(is)-1.,0.)
  end do
  !
  !     * parameters for linear interpolation of growth factors
  !     * for externally mixed aerosol types.
  !
  do is=1,isext
    do l=1,leva
      do il=1,ilga
        cea2(il,l,is)=(cetmpb(il,l,ceisvr(is))-cetmpb(il,l,ceisvl(is))) &
                     /(cedryr(is)%vr          -cedryr(is)%vl)
        cea1(il,l,is)= &
                       cetmpb(il,l,ceisvl(is))-cea2(il,l,is)*cedryr(is)%vl
        afact=ycnst*rhoh2o*cen0(il,l,is)*cemom3(il,l,is)
        aflow=afact*max((min(cetmpb(il,l,ceisvr(is)), &
                                 cetmpb(il,l,ceisvl(is))+1.)**3-1.),0.)
        cewlow(il,l,is)=aflow
        afhig=afact*max((max(cetmpb(il,l,ceisvr(is)), &
                                 cetmpb(il,l,ceisvl(is))+1.)**3-1.),0.)
        cewhig(il,l,is)=afhig
      end do
    end do
  end do
  if (isext > 0) then
    ceaa=3._r8*cea1+3._r8*cea1**2+cea1**3
    ceab=3._r8*cea2*(1._r8+cea1)**2
    ceac=3._r8*(1._r8+cea1)*cea2**2
    cead=cea2**3
  end if
  !
  !     * parameters for linear interpolation of growth factors
  !     * for internally mixed aerosol types.
  !
  do is=1,isint
    do l=1,leva
      do il=1,ilga
        cia2(il,l,is)=(citmpb(il,l,ciisvr(is))-citmpb(il,l,ciisvl(is))) &
                     /(cidryr(is)%vr          -cidryr(is)%vl)
        cia1(il,l,is)= &
                       citmpb(il,l,ciisvl(is))-cia2(il,l,is)*cidryr(is)%vl
        afact=ycnst*rhoh2o*cin0(il,l,is)*cimom3(il,l,is)
        aflow=afact*max((min(citmpb(il,l,ciisvr(is)), &
                                 citmpb(il,l,ciisvl(is))+1.)**3-1.),0.)
        ciwlow(il,l,is)=aflow
        afhig=afact*max((max(citmpb(il,l,ciisvr(is)), &
                                 citmpb(il,l,ciisvl(is))+1.)**3-1.),0.)
        ciwhig(il,l,is)=afhig
      end do
    end do
  end do
  if (isint > 0) then
    ciaa=3._r8*cia1+3._r8*cia1**2+cia1**3
    ciab=3._r8*cia2*(1._r8+cia1)**2
    ciac=3._r8*(1._r8+cia1)*cia2**2
    ciad=cia2**3
  end if
  !
  !     * water content for externally mixed aerosols.
  !
  if (isext > 0) then
    cewt=yna
    where (abs(cepsi-yna) > ytiny)
      cewt=ycnst*rhoh2o*cen0 &
                     *(cemom3*ceaa+cemom4*ceab+cemom5*ceac+cemom6*cead)
    end where
    where (abs(cewt-yna) > ytiny)
      where (cewlow > cewt)
        cewt=cewlow
      else where (cewhig < cewt)
        cewt=cewhig
      end where
    end where
    cewmass=cewt
  end if
  !
  !     * water content for internally mixed aerosols.
  !
  if (isint > 0) then
    ciwt=yna
    where (abs(cipsi-yna) > ytiny)
      ciwt=ycnst*rhoh2o*cin0 &
                     *(cimom3*ciaa+cimom4*ciab+cimom5*ciac+cimom6*ciad)
    end where
    where (abs(ciwt-yna) > ytiny)
      where (ciwlow > ciwt)
        ciwt=ciwlow
      else where (ciwhig < ciwt)
        ciwt=ciwhig
      end where
    end where
    ciwmass=ciwt
  end if
  !
  !     * sum up condensed water.
  !
  qrc=0.
  if (isextb > 0) then
    do is=1,isext
      where (abs(cewmass(:,:,is)-yna) > ytiny)
        qrc=qrc+cewmass(:,:,is)
      end where
    end do
  end if
  if (isintb > 0) then
    do is=1,isint
      where (abs(ciwmass(:,:,is)-yna) > ytiny)
        qrc=qrc+ciwmass(:,:,is)
      end where
    end do
  end if
  !
  !     * deallocate work arrays.
  !
  if (isext > 0) then
    deallocate(cea1)
    deallocate(cea2)
    deallocate(ceaa)
    deallocate(ceab)
    deallocate(ceac)
    deallocate(cead)
    deallocate(cewt)
  end if
  if (isextb > 0) then
    deallocate(cetmpb)
    deallocate(cewlow)
    deallocate(cewhig)
  end if
  if (isint > 0) then
    deallocate(cia1)
    deallocate(cia2)
    deallocate(ciaa)
    deallocate(ciab)
    deallocate(ciac)
    deallocate(ciad)
    deallocate(ciwt)
  end if
  if (isintb > 0) then
    deallocate(citmpb)
    deallocate(ciwlow)
    deallocate(ciwhig)
  end if
  !
end subroutine scwatm
