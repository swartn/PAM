!> \file
!> \brief Extraction of internal boundaries for different growth regimes
!>       from tabulated growth data. This will provide the tabulated in
!>       the necessary format for subsequent calculations.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine cnbnds (ibtm,ird,yndef)
  !
  use cnparm,  only : bsmaxn,bsmaxp,bsminn,bsminp, &
                          ibtmax,idbsb,idef,idpl,idpu,ireg, &
                          rbsb,rgb,rpmax,rpmin, &
                          spp,xpderl,xpderu,xpmax,xpmin,xppntl,xppntu
  use cnparmt, only : rgbt,rgpno
  !
  implicit none
  !
  integer, dimension(ireg) :: ibtm !<
  integer :: ird !<
  real :: yndef !<
  !
  !     internal work variables
  !
  integer :: irg !<
  integer :: irat !<
  integer :: idbsbt !<
  integer :: ib !<
  integer :: idl !<
  integer :: idu !<
  integer :: idplt !<
  integer :: idput !<
  integer :: id !<
  integer :: ir !<
  integer :: ibm !<
  real :: bsmin !<
  real :: bsmax !<
  !
  !     * allocate and initialize temporary arrays.
  !
  allocate (rgbt(ireg))
  do irg=1,ireg
    allocate (rgbt(irg)%pntl(ird))
    allocate (rgbt(irg)%pntu(ird))
    allocate (rgbt(irg)%derl(ird))
    allocate (rgbt(irg)%deru(ird))
  end do
  do irg=1,ireg
    rgbt(irg)%pntl(:)=yndef
    rgbt(irg)%pntu(:)=yndef
    rgbt(irg)%derl(:)=yndef
    rgbt(irg)%deru(:)=yndef
  end do
  !
  !     * allocate and initialize output arrays.
  !
  allocate (bsminp(ird))
  allocate (bsmaxp(ird))
  allocate (bsminn(ird))
  allocate (bsmaxn(ird))
  allocate (xpmin(ird))
  allocate (xpmax(ird))
  allocate (rpmin(ird))
  allocate (rpmax(ird))
  bsminp(:)=yndef
  bsmaxp(:)=yndef
  bsminn(:)=yndef
  bsmaxn(:)=yndef
  xpmin(:)=yndef
  xpmax(:)=yndef
  rpmin(:)=yndef
  rpmax(:)=yndef
  allocate (rgb(ireg))
  do irg=1,ireg
    allocate (rgb(irg)%flg(ird))
    allocate (rgb(irg)%bsbmin(ird))
    allocate (rgb(irg)%bsbmax(ird))
    allocate (rgb(irg)%bsb(ibtm(irg),ird))
    allocate (rgb(irg)%pntl(ibtm(irg),ird))
    allocate (rgb(irg)%pntu(ibtm(irg),ird))
    allocate (rgb(irg)%derl(ibtm(irg),ird))
    allocate (rgb(irg)%deru(ibtm(irg),ird))
  end do
  idbsbt=0
  ibtmax=0
  do irg=1,ireg
    rgb(irg)%flg(1:ird)=.false.
    rgb(irg)%bsbmin(1:ird)=yndef
    rgb(irg)%bsbmax(1:ird)=yndef
    rgb(irg)%bsb(1:ibtm(irg),1:ird)=yndef
    rgb(irg)%pntl(1:ibtm(irg),1:ird)=yndef
    rgb(irg)%pntu(1:ibtm(irg),1:ird)=yndef
    rgb(irg)%derl(1:ibtm(irg),1:ird)=yndef
    rgb(irg)%deru(1:ibtm(irg),1:ird)=yndef
    rgb(irg)%ibt=ibtm(irg)
    ibtmax=max(ibtmax,ibtm(irg))
    idbsbt=idbsbt+ibtm(irg)
  end do
  idbsbt=idbsbt*ird
  !
  !     * determine bs range.
  !
  do irat=1,ird
    bsmin=min(rgpno(1)%bs(1,irat),rgpno(2)%bs(1,irat), &
                                                   rgpno(3)%bs(1,irat))
    bsmax=max(rgpno(1)%bs(ibtm(1)+1,irat), &
               rgpno(2)%bs(ibtm(2)+1,irat),rgpno(3)%bs(ibtm(3)+1,irat))
    if (bsmin > bsmax) call xit ('CNBNDS',-1)
    bsminp(irat)=bsmin
    bsmaxp(irat)=bsmax
  end do
  do irat=1,ird
    bsmin=min(rgpno(4)%bs(1,irat),rgpno(5)%bs(1,irat))
    bsmax=max(rgpno(4)%bs(ibtm(4)+1,irat), &
                                           rgpno(5)%bs(ibtm(5)+1,irat))
    if (bsmin > bsmax) call xit ('CNBNDS',-2)
    bsminn(irat)=bsmin
    bsmaxn(irat)=bsmax
  end do
  !
  !     * determine particle size range.
  !
  do irat=1,ird
    if (rgpno(1)%xp(1,1,irat) &
        > rgpno(3)%xp(rgpno(3)%irt,1,irat) ) then
      call xit ('CNBNDS',-3)
    end if
    xpmin(irat)=rgpno(1)%xp(1,1,irat)
    xpmax(irat)=rgpno(3)%xp(rgpno(3)%irt,1,irat)
    rpmin(irat)=sqrt(2.*xpmin(irat))
    rpmax(irat)=sqrt(2.*xpmax(irat))
  end do
  if (idbsbt < 1) call xit('CNBNDS',-4)
  allocate(rbsb(idbsbt))
  allocate(idbsb(ireg,ird,ibtmax))
  id=0
  do irg=1,ireg
    !
    !       * copy original data into output arrays.
    !
    do irat=1,ird
      do ib=1,ibtm(irg)
        rgb(irg)%bsb(ib,irat)=rgpno(irg)%bs(ib,irat)
        id=id+1
        rbsb(id)=rgpno(irg)%bs(ib,irat)
        idbsb(irg,irat,ib)=id
      end do
    end do
    !
    !       * extract points xp at the lower boundary of the domain.
    !
    do irat=1,ird
      do ib=1,ibtm(irg)
        do ir=1,rgpno(irg)%irt
          if (nint(rgpno(irg)%tp(ir,ib,irat)-yndef) /= 0 &
              .and. nint(rgb(irg)%pntl(ib,irat)-yndef) == 0) then
            rgb(irg)%pntl(ib,irat)=rgpno(irg)%xp(ir,ib,irat)
          end if
        end do
      end do
      ib=ibtm(irg)+1
      do ir=1,rgpno(irg)%irt
        if (nint(rgpno(irg)%tp(ir,ib,irat)-yndef) /= 0 &
            .and. nint(rgbt(irg)%pntl(irat)-yndef) == 0) then
          rgbt(irg)%pntl(irat)=rgpno(irg)%xp(ir,ib,irat)
        end if
      end do
    end do
    !
    !       * extract points xp at the upper boundary of the domain.
    !
    do irat=1,ird
      do ib=1,ibtm(irg)
        do ir=rgpno(irg)%irt,1,-1
          if (nint(rgpno(irg)%tp(ir,ib,irat)-yndef) /= 0 &
              .and. nint(rgb(irg)%pntu(ib,irat)-yndef) == 0) then
            rgb(irg)%pntu(ib,irat)=rgpno(irg)%xp(ir,ib,irat)
          end if
        end do
      end do
      ib=ibtm(irg)+1
      do ir=rgpno(irg)%irt,1,-1
        if (nint(rgpno(irg)%tp(ir,ib,irat)-yndef) /= 0 &
            .and. nint(rgbt(irg)%pntu(irat)-yndef) == 0) then
          rgbt(irg)%pntu(irat)=rgpno(irg)%xp(ir,ib,irat)
        end if
      end do
    end do
    !
    !       * determine derivatives (d xp/d bs) for lower boundary.
    !
    do irat=1,ird
      do ib=1,ibtm(irg)-1
        if (nint(rgb(irg)%pntl(ib,irat)-yndef) /= 0 &
            .and. nint(rgb(irg)%pntl(ib+1,irat)-yndef) /= 0) then
          rgb(irg)%derl(ib,irat)= &
                                      (rgb(irg)%pntl(ib+1,irat)-rgb(irg)%pntl(ib,irat)) &
                                      /(rgpno(irg)%bs(ib+1,irat)-rgpno(irg)%bs(ib,irat))
        end if
      end do
      ib=ibtm(irg)
      if (nint(rgb(irg)%pntl(ib,irat)-yndef) /= 0 &
          .and. nint(rgbt(irg)%pntl(irat)-yndef) /= 0) then
        rgb(irg)%derl(ib,irat)= &
                                    (rgbt(irg)%pntl(irat)-rgb(irg)%pntl(ib,irat)) &
                                    /(rgpno(irg)%bs(ib+1,irat)-rgpno(irg)%bs(ib,irat))
      end if
    end do
    !
    !       * determine derivatives (d xp/d bs) for upper boundary.
    !
    do irat=1,ird
      do ib=1,ibtm(irg)-1
        if (nint(rgb(irg)%pntu(ib,irat)-yndef) /= 0 &
            .and. nint(rgb(irg)%pntu(ib+1,irat)-yndef) /= 0) then
          rgb(irg)%deru(ib,irat)= &
                                      (rgb(irg)%pntu(ib+1,irat)-rgb(irg)%pntu(ib,irat)) &
                                      /(rgpno(irg)%bs(ib+1,irat)-rgpno(irg)%bs(ib,irat))
        end if
      end do
      ib=ibtm(irg)
      if (nint(rgb(irg)%pntu(ib,irat)-yndef) /= 0 &
          .and. nint(rgbt(irg)%pntu(irat)-yndef) /= 0) then
        rgb(irg)%deru(ib,irat)= &
                                    (rgbt(irg)%pntu(irat)-rgb(irg)%pntu(ib,irat)) &
                                    /(rgpno(irg)%bs(ib+1,irat)-rgpno(irg)%bs(ib,irat))
      end if
    end do
  end do
  !
  !     * overwrite results for region 1 to account for critical size.
  !
  do irat=1,ird
    ibm=spp(irat)%ibs
    if (ibm /= idef) then
      if (ibm <= ibtm(1) ) then
        rgb(1)%pntu(ibm,irat)=spp(irat)%xp
      else
        rgbt(1)%pntu(irat)=spp(irat)%xp
      end if
    end if
  end do
  do irat=1,ird
    ibm=spp(irat)%ibs
    if (ibm /= idef) then
      if (ibm <= (ibtm(1)-1) ) then
        if (nint(rgb(1)%pntu(ibm+1,irat)-yndef) /= 0) then
          rgb(1)%deru(ibm,irat)= &
                                     (rgb(1)%pntu(ibm+1,irat)-spp(irat)%xp) &
                                     /(rgpno(1)%bs(ibm+1,irat)-rgpno(1)%bs(ibm,irat))
        end if
      else
        if (nint(rgbt(1)%pntu(irat)-yndef) /= 0) then
          rgb(1)%deru(ibm,irat)= &
                                     (rgbt(1)%pntu(irat)-spp(irat)%xp) &
                                     /(rgpno(1)%bs(ibm+1,irat)-rgpno(1)%bs(ibm,irat))
        end if
      end if
    end if
  end do
  !
  !     * determine bs range for each growth regime.
  !
  do irg=1,ireg
    do irat=1,ird
      if (rgpno(irg)%bs(1,irat) > rgpno(irg)%bs(ibtm(irg)+1,irat) &
          ) then
        call xit ('CNBNDS',-5)
      end if
      do ib=1,ibtm(irg)
        if (nint(rgb(irg)%pntl(ib,irat)-yndef) /= 0 &
            .and. nint(rgb(irg)%pntu(ib,irat)-yndef) /= 0 &
            .and. nint(rgb(irg)%bsbmin(irat)-yndef) == 0) then
          rgb(irg)%bsbmin(irat)=rgpno(irg)%bs(ib,irat)
        end if
      end do
      do ib=ibtm(irg),1,-1
        if (nint(rgb(irg)%pntl(ib,irat)-yndef) /= 0 &
            .and. nint(rgb(irg)%pntu(ib,irat)-yndef) /= 0 &
            .and. nint(rgb(irg)%bsbmax(irat)-yndef) == 0) then
          rgb(irg)%bsbmax(irat)=rgpno(irg)%bs(ib+1,irat)
        end if
      end do
      if (nint(rgb(irg)%bsbmin(irat)-yndef) /= 0 &
          .and. nint(rgb(irg)%bsbmax(irat)-yndef) /= 0) then
        rgb(irg)%flg(irat)=.true.
      end if
    end do
  end do
  idplt=0
  idput=0
  do irg=1,ireg
    !
    !       * reorder terms to produce a simple linear expression for
    !       * extrapolation for lower boundary.
    !
    do irat=1,ird
      do ib=1,ibtm(irg)
        if (nint(rgb(irg)%pntl(ib,irat)-yndef) /= 0) then
          rgb(irg)%pntl(ib,irat)=rgb(irg)%pntl(ib,irat) &
                         -rgb(irg)%derl(ib,irat)*rgpno(irg)%bs(ib,irat)
          idplt=idplt+1
        end if
      end do
    end do
    !
    !       * reorder terms to produce a simple linear expression for
    !       * extrapolation for upper boundary.
    !
    do irat=1,ird
      do ib=1,ibtm(irg)
        if (nint(rgb(irg)%pntu(ib,irat)-yndef) /= 0) then
          rgb(irg)%pntu(ib,irat)=rgb(irg)%pntu(ib,irat) &
                         -rgb(irg)%deru(ib,irat)*rgpno(irg)%bs(ib,irat)
          idput=idput+1
        end if
      end do
    end do
  end do
  !
  !     * copy results for reference point (pntu/l) and derivative (deru/l)
  !     * into holding arrays. the use of holding arrays will speed
  !     * up the look-up of the results from memory in subsequent
  !     * calculations.
  !
  if (idplt < 1 .or. idput < 1 .or. ibtmax < 1) &
      call xit('CNBNDS',-6)
  allocate(xppntl(idplt))
  allocate(xppntu(idput))
  allocate(xpderl(idplt))
  allocate(xpderu(idput))
  allocate(idpl(ireg,ird,ibtmax))
  allocate(idpu(ireg,ird,ibtmax))
  idpl=idef
  idpu=idef
  idl=0
  idu=0
  do irg=1,ireg
    do irat=1,ird
      do ib=1,ibtm(irg)
        if (nint(rgb(irg)%pntl(ib,irat)-yndef) /= 0) then
          idl=idl+1
          xppntl(idl)=rgb(irg)%pntl(ib,irat)
          xpderl(idl)=rgb(irg)%derl(ib,irat)
          idpl(irg,irat,ib)=idl
        end if
        if (nint(rgb(irg)%pntu(ib,irat)-yndef) /= 0) then
          idu=idu+1
          xppntu(idu)=rgb(irg)%pntu(ib,irat)
          xpderu(idu)=rgb(irg)%deru(ib,irat)
          idpu(irg,irat,ib)=idu
        end if
      end do
    end do
  end do
  !
  !     * deallocate temporary array.
  !
  deallocate(rgbt)
  !
end subroutine cnbnds
