!> \file
!> \brief Scaled particle size from non-dimensionalized particle growth
!>       curves after scaled time interval.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine cnrcal (xpn,xp,du,xps,tps,kop,kgr,irs,irsmax,ilga, &
                         leva,isec,irstmax)
  !
  use cnparm, only : idef,ilarge,ina,ismall,yna,ysmall
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isec !< Number of separate aerosol tracers
  integer, intent(in) :: irstmax !< Number of points in tabulated data
  real, intent(in), dimension(ilga,leva,isec,irstmax) :: xps !< Scaled particle sizes
  real, intent(in), dimension(ilga,leva,isec,irstmax) :: tps !< Scaled particle growth times
  real, intent(in), dimension(ilga,leva,isec) :: xp !< Particle sizes
  real, intent(in), dimension(ilga,leva,isec) :: du !< Normalized time interval for paricle growth
  logical, intent(in), dimension(ilga,leva,isec) :: kop !< Flag to indicate whether the upper boundary of the
  !< domain is open to further particle growth
  logical, intent(in), dimension(ilga,leva,isec) :: kgr !< Flag for positive or negative growth
  integer, intent(in), dimension(ilga,leva,isec) :: irs !< Indices of Koehler parameter ratio in tabulated data
  integer, intent(in), dimension(leva,isec) :: irsmax !<
  real, intent(out), dimension(ilga,leva,isec) :: xpn !< New scaled particle size
  !
  !     internal work variables
  !
  logical, dimension(ilga) :: kgrl !<
  integer, dimension(ilga) :: irxl !<
  integer, dimension(ilga) :: irxu !<
  integer, dimension(ilga) :: irsl !<
  real, dimension(ilga) :: dut !<
  real, dimension(ilga) :: upn !<
  real, dimension(ilga) :: xpl !<
  real, dimension(ilga) :: dul !<
  integer :: l !<
  integer :: is !<
  integer :: il !<
  integer :: ir !<
  integer :: irxmin !<
  integer :: irxmax !<
  integer :: irst !<
  integer :: irxlt !<
  integer :: irt !<
  real :: up !<
  real :: dtim !<
  real :: xpnn !<
  !
  !-----------------------------------------------------------------------
  !
  do is=1,isec
    do l=1,leva
      !
      !-----------------------------------------------------------------------
      !       * find discrete scaled particle size on growth curve that is next
      !       * to the initial scaled particle size.
      !
      xpl(:)=xp(:,l,is)
      irxl=idef
      do ir=1,irsmax(l,is)-1
        do il=1,ilga
          if (ir <= irs(il,l,is)-1) then
            if (xpl(il) >= xps(il,l,is,ir) &
                .and. xpl(il)  < xps(il,l,is,ir+1)              ) then
              irxl(il)=ir
            end if
          end if
        end do
      end do
      !
      irxmin=ilarge
      irxmax=ismall
      upn=yna
      dut=yna
      irxu=ina
      do il=1,ilga
        irst=irs(il,l,is)
        if (irst > 0) then
          irxlt=irxl(il)
          !
          !           * normalized start time at beginning of time step.
          !
          if (irxlt /= idef) then
            irxmin=min(irxmin,irxlt)
            irxmax=max(irxmax,irxlt)
            !
            !             * the initial particle size is within the range of the arrays
            !             * for the growth data.
            !
            up=tps(il,l,is,irxlt) &
                +(xp(il,l,is)-xps(il,l,is,irxlt)) &
                  /(xps(il,l,is,irxlt+1)-xps(il,l,is,irxlt)) &
                  *(tps(il,l,is,irxlt+1)-tps(il,l,is,irxlt))
          else
            !
            !             * the initial particle is bigger than the biggest size
            !             * according to the growth data. this is based on a simple
            !             * extrapolation of the growth data under the assumption
            !             * of a vanishing surface vpour concentration, which means
            !             * that the difference in normalized time equals the
            !             * difference in normalized particle size.
            !
            if (kgr(il,l,is) ) then
              up=tps(il,l,is,irst)+(xp(il,l,is)-xps(il,l,is,irst))
            else
              up=tps(il,l,is,irst)-(xp(il,l,is)-xps(il,l,is,irst))
            end if
          end if
          !
          !           * normalized time at end of time step.
          !
          upn(il)=up+du(il,l,is)
          !
          !           * determine first scaled time increment. distinguish between
          !           * situations in which the particle will be limited to
          !           * be within the boundaries of the growth data and otherwise.
          !
          if (kgr(il,l,is) ) then
            if (upn(il) >= tps(il,l,is,irst) ) then
              !
              !               * particles could grow to sizes bigger than the maximum size
              !               * from the growth data because the time interval for
              !               * paricle growth is sufficiently long. limit growth to
              !               * maximum size in case the boundary at that size is not
              !               * open.
              !
              if (kop(il,l,is) ) then
                dut(il)=upn(il)-tps(il,l,is,irst)
              else
                dut(il)=0.
              end if
              irxu(il)=irst
            else
              !
              !               * if final particle sizes are smaller than maximum size,
              !               * let particle grow to next available size.
              !
              dut(il)=tps(il,l,is,irxlt+1)-up
              irxu(il)=irxlt
            end if
          else
            if (upn(il) <= tps(il,l,is,irst) ) then
              !
              !               * particles will shrink to sizes that are still bigger
              !               * than the maximum size according to the growth data.
              !
              dut(il)=du(il,l,is)
              irxu(il)=irst+1
            else
              if (up < tps(il,l,is,irst) ) then
                !
                !                 * the particle will shrink at least to the maximum
                !                 * size according to the growth data. save the time
                !                 * it takes the paricles to shrink to that size.
                !
                dut(il)=tps(il,l,is,irst)-up
                irxu(il)=irst
              else
                !
                !                 * the particles are initially smaller than the maximum
                !                 * size and will continue to shrink. save the first time
                !                 * interval.
                !
                if ( irxlt<1 ) then                                              ! ar: to avoid irxlt=-1
                  print '("warning: irxlt(",i4,") out of range: [1...",i4,"]. is,l,il=",3i4)', irxlt,irstmax,is,l,il
                  irxlt = 1
                else if ( irxlt>irstmax ) then                                   ! ar: to avoid irxlt>irstmax
                  print '("warning: irxlt(",i4,") out of range: [1...",i4,"]. is,l,il=",3i4)', irxlt,irstmax,is,l,il
                  irxlt = irstmax
                end if
                dut(il)=tps(il,l,is,irxlt)-up
                irxu(il)=irxlt
              end if
            end if
          end if
        end if
      end do
      !
      !-----------------------------------------------------------------------
      !       * increment scaled time until it is almost reaches the total
      !       * scaled time for positive particle growth.
      !
      kgrl(:)=kgr(:,l,is)
      irsl(:)=irs(:,l,is)
      dul(:)=du(:,l,is)
      do ir=irxmin+1,irsmax(l,is)-1
        do il=1,ilga
          if (irsl(il) > 0 .and. kgrl(il) &
              .and. ir >= irxu(il)+1 .and. ir <= irsl(il)-1 &
              .and. dut(il) < dul(il)                           ) then
            dut(il)=dut(il) &
                   +(tps(il,l,is,ir+1)-tps(il,l,is,ir))
            irxu(il)=ir
          end if
        end do
      end do
      !
      !       * increment scaled time until it is almost reaches the total
      !       * scaled time for negative particle growth.
      !
      !        do ir=irxmax,2,-1
      do ir=irxmax,1,-1
        do il=1,ilga
          if (irsl(il) > 0 .and. .not.kgrl(il) &
              .and. ir <= irxu(il)-1 .and. ir <= irsl(il)-1 &
              .and. dut(il) < dul(il)                           ) then
            dut(il)=dut(il)+(tps(il,l,is,ir)-tps(il,l,is,ir+1))
            irxu(il)=ir
          end if
        end do
      end do
      !
      !-----------------------------------------------------------------------
      !       * final scaled particle size.
      !
      do il=1,ilga
        irst=irs(il,l,is)
        if (irst > 0) then
          if (irxu(il) < irst) then
            !
            !             * new scaled particle size from interpolation for positive
            !             * particle growth.
            !
            irt=irxu(il)
            if (irt<1 .or. irt>irstmax-1) then                                              ! ar: to avoid irt=-1
              print '("WARNING: IRT(",i4,") out of range: [1...",i4,"]. IS,L,IL=",3i4)', IRT,IRSTMAX-1,IS,L,IL
              irt = 1
            end if
            dtim=tps(il,l,is,irt+1)-tps(il,l,is,irt)
            if (abs(dtim) > ysmall) then
              xpnn=xps(il,l,is,irt) &
                  +(upn(il)-tps(il,l,is,irt)) &
                     /(tps(il,l,is,irt+1)-tps(il,l,is,irt)) &
                     *(xps(il,l,is,irt+1)-xps(il,l,is,irt))
            else
              if (kgr(il,l,is) ) then
                xpnn=xps(il,l,is,irt+1)
              else
                xpnn=xps(il,l,is,irt)
              end if
            end if
            xpn(il,l,is)=max(xpnn,xps(il,l,is,1))
          else
            if (kgr(il,l,is) ) then
              !
              !               * continue particle growth under assumption of vanishing particle surface supersaturation.
              !
              xpn(il,l,is)=xps(il,l,is,irst)+dut(il)
            else
              !
              !               * particle growth under assumption of vanishing particle surface concentration.
              !
              xpn(il,l,is)=xp(il,l,is)-dut(il)
            end if
          end if
        else
          !
          !           * particle size is near equilibrium size.
          !
          xpn(il,l,is)=xp(il,l,is)
        end if
      end do
    end do
  end do
  !
end subroutine cnrcal
