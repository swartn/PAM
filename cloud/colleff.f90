!> \file
!> \brief Collection efficiencies for number and mass.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------

subroutine colleff (pncef,pmcef,anum,amas,pn0,phi0, &
                          psi,phis0,dphi0,wetrc,dryrc,drydn, &
                          ilga,leva,isec)
  use sdparm, only : dnh2o,r0,yna,ytiny
  use sdcode, only : sdint,sdint0,sdintb,sdintb0
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isec !< Number of separate aerosol tracers
  real, intent(in), dimension(ilga,leva,isec) :: anum !< Number
  real, intent(in), dimension(ilga,leva,isec) :: amas !< Mass
  real, intent(in), dimension(ilga,leva,isec) :: pn0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$)
  real, intent(in), dimension(ilga,leva,isec) :: phi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$)
  real, intent(in), dimension(ilga,leva,isec) :: psi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$)
  real, intent(in), dimension(ilga,leva,isec) :: wetrc !< Radius for total (wet), externally mixed aerosol particles \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isec) :: drydn !< Density of dry aerosol particle \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isec) :: phis0 !< Dry particle size ln(Rp /R0) at the boundaries of the size sections
  real, intent(in), dimension(ilga,leva,isec) :: dphi0 !< Section width
  real, intent(in), dimension(isec) :: dryrc !< Dry particle radius in the centres of the size sections \f$[m]\f$
  real, intent(out), dimension(ilga,leva,isec) :: pncef !< Collection efficiencies for number
  real, intent(out), dimension(ilga,leva,isec) :: pmcef !< Collection efficiencies for mass.
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:,:) :: am0 !<
  real, allocatable, dimension(:,:,:) :: am3 !<
  real, allocatable, dimension(:,:,:) :: am0n !<
  real, allocatable, dimension(:,:,:) :: am3n !<
  real, allocatable, dimension(:,:,:) :: phi0w !<
  real, allocatable, dimension(:,:,:) :: phis0w !<
  real, allocatable, dimension(:,:,:) :: wetdn !<
  real :: phis !<
  real :: phie !<
  real :: psit !<
  real :: phi0t !<
  real :: phili !<
  real :: phihi !<
  real :: rcutl !<
  real :: am0t1 !<
  real :: am0t2 !<
  real :: am0t3 !<
  real :: am3t1 !<
  real :: am3t2 !<
  real :: am3t3 !<
  real :: dphit1 !<
  real :: dphit2 !<
  real :: dphit3 !<
  real :: tmom !<
  real :: rmom !<
  real :: rcuth !<
  real :: phil !<
  real :: phih !<
  real :: fgr !<
  real :: alfgr !<
  real :: fgr3i !<
  real :: slope !<
  real :: offs !<
  integer :: il !<
  integer :: l !<
  integer :: is !<
  !
  !---------------------------------------------------------------------
  !     * allocate memory.
  !
  allocate(phi0w (ilga,leva,isec))
  allocate(phis0w(ilga,leva,isec))
  allocate(wetdn (ilga,leva,isec))
  allocate(am0   (ilga,leva,isec))
  allocate(am3   (ilga,leva,isec))
  allocate(am0n  (ilga,leva,isec))
  allocate(am3n  (ilga,leva,isec))
  !
  !---------------------------------------------------------------------
  !     * hygroscopic growth effects on radius and particle density.
  !
  phi0w =phi0
  phis0w=phis0
  wetdn=drydn
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        if (anum(il,l,is) > ytiny .and. amas(il,l,is) > ytiny) then
          fgr=wetrc(il,l,is)/dryrc(is)
          alfgr=log(fgr)
          phi0w (il,l,is)=phi0w (il,l,is)+alfgr
          phis0w(il,l,is)=phis0w(il,l,is)+alfgr
          fgr3i=1./fgr**3
          wetdn(il,l,is)=dnh2o*(1.-fgr3i)+drydn(il,l,is)*fgr3i
        end if
      end do
    end do
  end do
  !
  !---------------------------------------------------------------------
  !     * size-dependent collection efficiency. for radius smaller than
  !     * the low cutoff (rcutl) and larger than the high cutoff (rcuth),
  !     * a collection efficiency of unity is assumed. between the cutoffs,
  !     * a power law is applied (offs*r**slope).
  !
  slope=-1.
  offs=5.e-11
  rcutl=1./offs**(1./slope)
  rcuth=2.e-06
  phil=log(rcutl/r0)
  phih=log(rcuth/r0)
  !
  !---------------------------------------------------------------------
  !     * integration over wet size distribution.
  !
  am0=sdint0(phi0w,psi,phis0w,dphi0,ilga,leva,isec)
  rmom=3.
  am3=sdint (phi0w,psi,rmom,phis0w,dphi0,ilga,leva,isec)
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        if (anum(il,l,is) > ytiny .and. amas(il,l,is) > ytiny) then
          phis=phis0w(il,l,is)
          phie=phis+dphi0(il,l,is)
          psit=psi(il,l,is)
          phi0t=phi0w(il,l,is)
          phili=max(phis,min(phil,phie))
          phihi=min(phie,max(phih,phis))
          dphit1=phili-phis
          am0t1=sdintb0(phi0t,psit,phis,dphit1)
          am3t1=sdintb (phi0t,psit,rmom,phis,dphit1)
          dphit2=phihi-phili
          am0t2=offs*sdintb(phi0t,psit,slope,phili,dphit2)
          tmom=rmom+slope
          am3t2=offs*sdintb(phi0t,psit,tmom,phili,dphit2)
          dphit3=phie-phihi
          am0t3=sdintb0(phi0t,psit,phihi,dphit3)
          am3t3=sdintb (phi0t,psit,rmom,phihi,dphit3)
          am0n(il,l,is)=am0t1+am0t2+am0t3
          am3n(il,l,is)=am3t1+am3t2+am3t3
        else
          am0n(il,l,is)=0.
          am3n(il,l,is)=0.
        end if
      end do
    end do
  end do
  !
  !     * collection efficiencies for number and mass.
  !
  where (am0 > ytiny .and. am3 > ytiny .and. abs(psi-yna) > ytiny)
    pncef=am0n/am0
    pmcef=am3n/am3
  else where
    pncef=1.
    pmcef=1.
  end where
  !
  !---------------------------------------------------------------------
  !     * deallocate memory.
  !
  deallocate(phis0w)
  deallocate(phi0w)
  deallocate(wetdn)
  deallocate(am0)
  deallocate(am3)
  deallocate(am0n)
  deallocate(am3n)
  !
end subroutine colleff
