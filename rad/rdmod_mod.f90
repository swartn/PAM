!>    \file
!>    \brief Radiative properties
!!
!!    @authors K. von Salzen and J. Li
!
!-----------------------------------------------------------------------
module rdmod
  !
  implicit none
  !
  integer, parameter  :: nbs = 4 !<
  integer, parameter  :: nbl = 9 !<
  integer, parameter  :: nh = 7 !<
  integer, parameter  :: nr = 11 !<
  integer, parameter  :: nv = 5 !<
  integer, parameter  :: nf1 = 11 !<
  integer, parameter  :: nf2 = 13 !<
  integer, parameter  :: nhs = 11 !<
  integer, parameter  :: nrs = 6 !<
  integer, parameter  :: nvs = 3 !<
  integer, parameter  :: idim2=nhs*nrs*nvs !<
  !
  real, dimension(idim2,5) :: sgt2 !<
  real, dimension(idim2,5) :: somgt2 !<
  real, dimension(idim2,6) :: sextt2 !<
  real, dimension(idim2,9) :: sabst2 !<
  !
end module rdmod
