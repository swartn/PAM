!>    \file
!>    \brief Diagnosis of aerosol number concentration based on dry size
!>     distribution and threshold droplet size. The CN concentration is
!>     calculated as the cumulative droplet concentration for all
!>     droplets bigger than the threshold size.
!!
!!    @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine cndiag (cnt,cn20,cn50,cn100,cn200,peradc,piradc, &
                         pen0,pephi0,pepsi,pephis0,pedphi0, &
                         pin0,piphi0,pipsi,piphis0,pidphi0, &
                         peinds,peinde,ncat,ilga,leva)
  !
  use sdparm, only : ina,isaext,isaint,kext,kint,pedryr,pidryr,r0,ylarge,yna,ytiny
  use sdcode, only : sdint0
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: ncat !< Number of diagnosed CN categories
  integer, intent(in), dimension(kext) :: peinds !< For externally mixed aerosol, size section range for each individual type
  integer, intent(in), dimension(kext) :: peinde !< For externally mixed aerosol, size section range for each individual type
  real, intent(in), dimension(ilga,leva,kext,ncat) :: peradc !< Externally mixed aerosol number concentration for dry droplet size thresholds
  real, intent(in), dimension(ilga,leva,ncat) :: piradc !< Internally mixed aerosol number concentration for dry droplet size thresholds
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,amplitude) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,width), ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pephis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for externally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaext) :: pedphi0 !< Dry particle radius in the centres of the size
  !< sections for externally mixed aerosol \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$, amplitude) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$, width), int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for internally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaint) :: pidphi0 !< Dry particle radius in the centres of the size
  !< sections for internally mixed aerosol \f$[m]\f$
  real, intent(out), dimension(ilga,leva) :: cnt !< Total aerosol number concentration
  real, intent(out), dimension(ilga,leva) :: cn20 !< Aerosol number concentration, particle diameter > 20nm
  real, intent(out), dimension(ilga,leva) :: cn50 !< Aerosol number concentration, particle diameter > 50nm
  real, intent(out), dimension(ilga,leva) :: cn100 !< Aerosol number concentration, particle diameter > 100nm
  real, intent(out), dimension(ilga,leva) :: cn200 !< Aerosol number concentration, particle diameter > 200nm
  !
  !     internal work variables
  !
  integer, allocatable, dimension(:,:) :: ciisi !<
  integer, allocatable, dimension(:,:,:) :: ceisi !<
  real, allocatable, dimension(:,:) :: circi !<
  real, allocatable, dimension(:,:,:) :: tidphis !<
  real, allocatable, dimension(:,:,:) :: tiphiss !<
  real, allocatable, dimension(:,:,:) :: tisdf !<
  real, allocatable, dimension(:,:,:) :: tinum !<
  real, allocatable, dimension(:,:,:) :: tipn0 !<
  real, allocatable, dimension(:,:,:) :: tiphi0 !<
  real, allocatable, dimension(:,:,:) :: tipsi !<
  real, allocatable, dimension(:,:,:) :: cerci !<
  real, allocatable, dimension(:,:,:) :: tedphis !<
  real, allocatable, dimension(:,:,:) :: tephiss !<
  real, allocatable, dimension(:,:,:) :: tesdf !<
  real, allocatable, dimension(:,:,:) :: tenum !<
  real, allocatable, dimension(:,:,:) :: tepn0 !<
  real, allocatable, dimension(:,:,:) :: tephi0 !<
  real, allocatable, dimension(:,:,:) :: tepsi !<
  integer :: il !<
  integer :: is !<
  integer :: l !<
  integer :: kx !<
  integer :: nc !<
  integer :: isx !<
  real :: apx !<
  real :: dphist !<
  real :: phisrt !<
  real :: phisst !<
  !
  !-----------------------------------------------------------------------
  !     * allocate work arrays.
  !
  if (kext > 0) then
    allocate(cerci(ilga,leva,kext))
    allocate(ceisi(ilga,leva,kext))
  end if
  if (isaext > 0) then
    allocate(tepn0  (ilga,leva,isaext))
    allocate(tephi0 (ilga,leva,isaext))
    allocate(tepsi  (ilga,leva,isaext))
    allocate(tedphis(ilga,leva,isaext))
    allocate(tephiss(ilga,leva,isaext))
    allocate(tesdf  (ilga,leva,isaext))
    allocate(tenum  (ilga,leva,isaext))
  end if
  if (kint > 0) then
    allocate(circi (ilga,leva))
    allocate(ciisi (ilga,leva))
  end if
  if (isaint > 0) then
    allocate(tipn0  (ilga,leva,isaint))
    allocate(tiphi0 (ilga,leva,isaint))
    allocate(tipsi  (ilga,leva,isaint))
    allocate(tidphis(ilga,leva,isaint))
    allocate(tiphiss(ilga,leva,isaint))
    allocate(tisdf  (ilga,leva,isaint))
    allocate(tinum  (ilga,leva,isaint))
  end if
  !
  !     * initialization of output fields.
  !
  cnt=0.
  cn20=0.
  cn50=0.
  cn100=0.
  cn200=0.
  !
  !-----------------------------------------------------------------------
  !     * match particle size with specified size cutoff to determine lower
  !     * bound of size spectrum for each externally mixed aerosol type.
  !
  do nc=1,ncat
    if (isaext > 0) then
      cerci=ylarge
      ceisi=ina
      do kx=1,kext
        is=peinds(kx)
        do l=1,leva
          do il=1,ilga
            if (pedryr(is)%vl > peradc(il,l,kx,nc)  ) then
              ceisi(il,l,kx)=is
              cerci(il,l,kx)=pedryr(is)%vl
            end if
          end do
        end do
        do is=peinds(kx),peinde(kx)
          do l=1,leva
            do il=1,ilga
              if (peradc(il,l,kx,nc)>=pedryr(is)%vl &
                  .and. peradc(il,l,kx,nc)<=pedryr(is)%vr &
                  .and. ceisi(il,l,kx)==ina) then
                apx=(peradc(il,l,kx,nc)-pedryr(is)%vl) &
                   /(pedryr(is)%vr-pedryr(is)%vl)
                ceisi(il,l,kx)=is
                cerci(il,l,kx)=apx*pedryr(is)%vr &
                              +(1.-apx)*pedryr(is)%vl
              end if
            end do
          end do
        end do
      end do
    end if
    !
    !       * match particle size with specified size cutoff to determine lower
    !       * bound of size spectrum for each internally mixed aerosol type.
    !
    if (isaint > 0) then
      circi=ylarge
      ciisi=ina
      is=1
      do l=1,leva
        do il=1,ilga
          if (pidryr(is)%vl > piradc(il,l,nc) .and. ciisi(il,l)==ina &
              ) then
            ciisi(il,l)=is
            circi(il,l)=pidryr(is)%vl
          end if
        end do
      end do
      do is=1,isaint
        do l=1,leva
          do il=1,ilga
            if (piradc(il,l,nc)>=pidryr(is)%vl &
                .and. piradc(il,l,nc)<=pidryr(is)%vr &
                .and. ciisi(il,l)==ina) then
              apx=(piradc(il,l,nc)-pidryr(is)%vl) &
                 /(pidryr(is)%vr-pidryr(is)%vl)
              ciisi(il,l)=is
              circi(il,l)=apx*pidryr(is)%vr+(1.-apx)*pidryr(is)%vl
            end if
          end do
        end do
      end do
    end if
    !
    !-----------------------------------------------------------------------
    !       * pla parameters for externally mixed aerosol types.
    !
    do is=1,isaext
      tepn0  (:,:,is)=pen0   (:,:,is)
      tephi0 (:,:,is)=pephi0 (:,:,is)
      tepsi  (:,:,is)=pepsi  (:,:,is)
      tedphis(:,:,is)=pedphi0(:,:,is)
      tephiss(:,:,is)=pephis0(:,:,is)
    end do
    !
    !       * pla parameters for internally mixed aerosol types.
    !
    do is=1,isaint
      tipn0  (:,:,is)=pin0   (:,:,is)
      tiphi0 (:,:,is)=piphi0 (:,:,is)
      tipsi  (:,:,is)=pipsi  (:,:,is)
      tidphis(:,:,is)=pidphi0(:,:,is)
      tiphiss(:,:,is)=piphis0(:,:,is)
    end do
    !
    !       * copy size information for activation size into size information
    !       * arrays for externally mixed aerosol.
    !
    if (isaext > 0) then
      do kx=1,kext
        do l=1,leva
          do il=1,ilga
            is=ceisi(il,l,kx)
            if (is /= ina) then
              phisrt=tephiss(il,l,is)+tedphis(il,l,is)
              phisst=min(max(log(cerci(il,l,kx)/r0),tephiss(il,l,is)), &
                                                                phisrt)
              dphist=phisrt-phisst
              tephiss(il,l,is)=phisst
              tedphis(il,l,is)=dphist
            end if
          end do
        end do
      end do
    end if
    !
    !       * copy size information for activation size into size information
    !       * arrays for internally mixed aerosol.
    !
    if (isaint > 0) then
      do l=1,leva
        do il=1,ilga
          is=ciisi(il,l)
          if (is /= ina) then
            phisrt=tiphiss(il,l,is)+tidphis(il,l,is)
            phisst=min(max(log(circi(il,l)/r0),tiphiss(il,l,is)), &
                                                                phisrt)
            dphist=phisrt-phisst
            tiphiss(il,l,is)=phisst
            tidphis(il,l,is)=dphist
          end if
        end do
      end do
    end if
    !
    !       * obtain number concentrations from integration of size disribution
    !       * for externally mixed aerosols.
    !
    if (isaext > 0) then
      tesdf=sdint0(tephi0,tepsi,tephiss,tedphis,ilga,leva,isaext)
      where (abs(tepsi-yna) > ytiny)
        tenum=tepn0*tesdf
      else where
        tenum=0.
      end where
    end if
    !
    !       * obtain number concentrations from integration of size disribution
    !       * for internally mixed aerosols.
    !
    if (isaint > 0) then
      tisdf=sdint0(tiphi0,tipsi,tiphiss,tidphis,ilga,leva,isaint)
      where (abs(tipsi-yna) > ytiny)
        tinum=tipn0*tisdf
      else where
        tinum=0.
      end where
    end if
    !
    !-----------------------------------------------------------------------
    !       * cloud droplet number concentration from integration of number
    !       * size distribution from critical size to size of largest particle
    !       * in the size distribution. for externally mixed aerosols.
    !
    do kx=1,kext
      do l=1,leva
        do il=1,ilga
          isx=ceisi(il,l,kx)
          if (isx /= ina) then
            do is=peinds(kx),peinde(kx)
              if (is >= isx) then
                if (nc == 1) then
                  cnt(il,l)=cnt(il,l)+tenum(il,l,is)
                else if (nc == 2) then
                  cn20(il,l)=cn20(il,l)+tenum(il,l,is)
                else if (nc == 3) then
                  cn50(il,l)=cn50(il,l)+tenum(il,l,is)
                else if (nc == 4) then
                  cn100(il,l)=cn100(il,l)+tenum(il,l,is)
                else if (nc == 5) then
                  cn200(il,l)=cn200(il,l)+tenum(il,l,is)
                end if
              end if
            end do
          end if
        end do
      end do
    end do
    !
    !       * cloud droplet number concentration from integration of number
    !       * size distribution. for internally mixed aerosols.
    !
    do is=1,isaint
      do l=1,leva
        do il=1,ilga
          isx=ciisi(il,l)
          if (isx /= ina) then
            if (is >= isx) then
              if (nc == 1) then
                cnt(il,l)=cnt(il,l)+tinum(il,l,is)
              else if (nc == 2) then
                cn20(il,l)=cn20(il,l)+tinum(il,l,is)
              else if (nc == 3) then
                cn50(il,l)=cn50(il,l)+tinum(il,l,is)
              else if (nc == 4) then
                cn100(il,l)=cn100(il,l)+tinum(il,l,is)
              else if (nc == 5) then
                cn200(il,l)=cn200(il,l)+tinum(il,l,is)
              end if
            end if
          end if
        end do
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * deallocate work arrays.
  !
  if (kext > 0) then
    deallocate(cerci)
    deallocate(ceisi)
  end if
  if (isaext > 0) then
    deallocate(tepn0)
    deallocate(tephi0)
    deallocate(tepsi)
    deallocate(tedphis)
    deallocate(tephiss)
    deallocate(tesdf)
    deallocate(tenum)
  end if
  if (kint > 0) then
    deallocate(circi)
    deallocate(ciisi)
  end if
  if (isaint > 0) then
    deallocate(tipn0)
    deallocate(tiphi0)
    deallocate(tipsi)
    deallocate(tidphis)
    deallocate(tiphiss)
    deallocate(tisdf)
    deallocate(tinum)
  end if
  !
end subroutine cndiag
