!> \file
!> \brief PM2.5 and PM10 concentrations.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine intsdpm(fmass,fmassd,pnum,pmas,pn0,phi0,psi, &
                         phis0,dphi0,pfrc,drydn,wetrc,dryrc,radc, &
                         rhoa,isn,ilga,leva,isec,ispc)
  !
  use sdparm, only : dnh2o,r0,ycnst,yna,ytiny
  use sdcode, only : sdintb
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isec !< Number of separate aerosol tracers
  integer, intent(in) :: isn !< Number of particle cutoff sizes
  integer, intent(in) :: ispc !< Number of chemical species
  real, intent(in), dimension(ilga,leva,isec) :: phis0 !< Dry particle size ln(Rp /R0) at the boundaries of the size sections
  real, intent(in), dimension(ilga,leva,isec) :: dphi0 !< Section width
  real, intent(in), dimension(ilga,leva,isec) :: pnum !< Aerosol number concentration \f$[1/kg]\f$
  real, intent(in), dimension(ilga,leva,isec) :: pmas !< Aerosol (dry) mass concentration \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isec) :: pn0 !< First PLA size distribution parameter (\f$n_{0,i}\f$, amplitude)
  real, intent(in), dimension(ilga,leva,isec) :: psi !< Second PLA size distribution parameter (\f$\psi_{i}\f$, mode size)
  real, intent(in), dimension(ilga,leva,isec) :: phi0 !< Third PLA size distribution parameter (\f$\phi_{0,i}\f$, width)
  real, intent(in), dimension(ilga,leva,isec) :: wetrc !< Radius for total (wet), externally mixed aerosol particles \f$[m]\f$
  real, intent(in), dimension(isec)           :: dryrc !< Dry particle radius in the centres of the size sections \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isec) :: drydn !< Density of dry aerosol particle \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva)      :: rhoa !< Air density \f$[kg/m^3]\f$
  real, intent(in), dimension(isn)            :: radc !< Cutoff radius
  real, intent(in), dimension(ilga,leva,isec,ispc) :: pfrc !< Aerosol mass fraction for each aerosol type
  real, intent(out), dimension(ilga,leva,ispc,isn) :: fmass !< Total aerosol mass concentration with size below the cutoff
  real, intent(out), dimension(ilga,leva,ispc,isn) :: fmassd !< Dry aerosol mass concentration with size below the cutoff
  !
  !     internal work variables
  !
  real, dimension(ilga,leva,ispc,isn) :: fmom3 !<
  real, dimension(ilga,leva,ispc,isn) :: fmom3d !<
  real, dimension(:,:,:), allocatable :: tphis0 !<
  real, dimension(:,:,:), allocatable :: tphi0 !<
  real, dimension(:,:,:), allocatable :: wetdn !<
  real, dimension(:,:,:), allocatable :: alfgr !<
  real, dimension(isn) :: tphic !<
  integer :: il !<
  integer :: is !<
  integer :: isd !<
  integer :: kx !<
  integer :: l !<
  real :: fdphi !<
  real :: fdphid !<
  real :: fphie !<
  real :: fphied !<
  real :: fphisd !<
  real :: fphis !<
  real :: tcutd !<
  real :: rmom3 !<
  real :: fgr !<
  real :: fgr3i !<
  !
  !-----------------------------------------------------------------------
  !     * integrate pla size distribution over given sub-sections.
  !
  tphic=log(radc/r0)
  rmom3=3.
  fmass=0.
  fmassd=0.
  fmom3=0.
  fmom3d=0.
  if (isec > 0) then
    allocate(tphis0(ilga,leva,isec))
    allocate(tphi0(ilga,leva,isec))
    allocate(wetdn(ilga,leva,isec))
    allocate(alfgr(ilga,leva,isec))
    !
    !       * section boundaries and density for wet size distribution.
    !
    do is=1,isec
      do l=1,leva
        do il=1,ilga
          tphis0(il,l,is)=phis0(il,l,is)
          tphi0(il,l,is)=phi0(il,l,is)
          wetdn(il,l,is)=drydn(il,l,is)
          alfgr(il,l,is)=0.
          if (pnum(il,l,is) > ytiny .and. pmas(il,l,is) > ytiny &
              .and. abs(psi(il,l,is)-yna) > ytiny) then
            fgr=wetrc(il,l,is)/dryrc(is)
            alfgr(il,l,is)=log(fgr)
            tphis0(il,l,is)=tphis0(il,l,is)+alfgr(il,l,is)
            tphi0(il,l,is)=tphi0(il,l,is)+alfgr(il,l,is)
            fgr3i=1./fgr**3
            wetdn(il,l,is)=dnh2o*(1.-fgr3i)+drydn(il,l,is)*fgr3i
          end if
        end do
      end do
    end do
    !
    !       * integrate dry mass size distribution between smallest
    !       * available particle radius and dry cutoff size.
    !
    do isd=1,isn
      do kx=1,ispc
        do is=1,isec
          do l=1,leva
            do il=1,ilga
              fphis=tphis0(il,l,is)
              fphisd=phis0(il,l,is)
              !
              !         * this assumes that the air in the instrument inlet is
              !         * not preheated and particles are collected at specified
              !         * relative humdity. filter samples are subject to
              !         * to gravitmetric analysis at the same relative humdiity
              !         * (i.e. aerosol water content is included).
              !
              if (tphic(isd) >= fphis) then
                fphie=min(tphis0(il,l,is)+dphi0(il,l,is), &
                       tphic(isd))
                fdphi=max(fphie-fphis,0.)
                if (abs(psi(il,l,is)-yna) > ytiny) then
                  fmom3(il,l,kx,isd)=fmom3(il,l,kx,isd) &
                      +ycnst*wetdn(il,l,is)*pn0(il,l,is) &
                      *sdintb(tphi0(il,l,is),psi(il,l,is), &
                              rmom3,fphis,fdphi)*pfrc(il,l,is,kx)
                end if
              end if
              !
              !         * this assumes that the air in the instrument inlet is
              !         * preheated so that only dry particles are sampled.
              !
              if (tphic(isd) >= fphisd) then
                fphied=min(phis0(il,l,is)+dphi0(il,l,is), &
                       tphic(isd))
                fdphid=max(fphied-fphisd,0.)
                if (abs(psi(il,l,is)-yna) > ytiny) then
                  fmom3d(il,l,kx,isd)=fmom3d(il,l,kx,isd) &
                      +ycnst*drydn(il,l,is)*pn0(il,l,is) &
                      *sdintb(phi0(il,l,is),psi(il,l,is), &
                              rmom3,fphisd,fdphid)*pfrc(il,l,is,kx)
                end if
              end if
              !
              !         * this assumes that the air in the instrument inlet is
              !         * not preheated and particles are collected at specified
              !         * relative humdity. filter samples are subject to
              !         * to gravitmetric analysis in a dry laboratory.
              !
              !          if ( tphic(isd) >= fphis) then
              !            tcutd=tphic(isd)-alfgr(il,l,is)
              !            fphied=min(phis0(il,l,is)+dphi0(il,l,is), &
              !                       tcutd)
              !            fdphid=max(fphied-fphisd,0.)
              !            if ( abs(psi(il,l,is)-yna) > ytiny) then
              !              fmom3(il,l,kx,isd)=fmom3(il,l,kx,isd) &
              !                      +ycnst*drydn(il,l,is)*pn0(il,l,is) &
              !                      *sdintb(phi0(il,l,is),psi(il,l,is), &
              !                              rmom3,fphisd,fdphid)*pfrc(il,l,is,kx)
              !            end if
              !          end if
            end do
          end do
        end do
      end do
    end do
    deallocate(tphis0)
    deallocate(tphi0)
    deallocate(wetdn)
    deallocate(alfgr)
  end if
  !
  !     * convert from mass mixing ratio to concentration.
  !
  do is=1,isn
    do kx=1,ispc
      fmass(:,:,kx,is)=fmom3(:,:,kx,is)*rhoa
      fmassd(:,:,kx,is)=fmom3d(:,:,kx,is)*rhoa
    end do
  end do
  !
end subroutine intsdpm
