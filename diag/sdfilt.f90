!>    \file
!>    \brief Diagnose and filter size distributions for mass and number
!>           using bin scheme
!!
!!    @authors K. von Salzen
!
!-----------------------------------------------------------------------
subroutine sdfilt(fnumo,fmaso,pen0,pephi0,pepsi,peddn,pedphi0, &
                        pephis0,pewetrc,penum,pemas,pin0,piphi0,pipsi, &
                        piddn,pidphi0,piphis0,pifrc,piwetrc,pinum, &
                        pimas,ilga,leva)
  !
  use sdparm, only : aextf,aintf,dnh2o,isaext,isaint,kext,kint, &
                         pedryrc,pidryrc,r0,sextf,ycnst,yna,ytiny
  use sdcode, only : sdintb, sdintb0
  use compar, only : isdnum
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,amplitude) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,width), ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: peddn !< (Dry) density for externally mixed aerosol particles
  !< \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: penum !< Aerosol number concentration for externally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pemas !< Aerosol (dry) mass concentration for externally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pedphi0 !< Dry particle radius in the centres of the size
  !< sections for externally mixed aerosol \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pephis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for externally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaext) :: pewetrc !< Total/wet paricle radius \f$[m]\f$, ext. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$, amplitude) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$, width), int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for internally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaint) :: pidphi0 !< Dry particle radius in the centres of the size
  !< sections for internally mixed aerosol \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: piddn !< (Dry) density for internally mixed aerosol particles
  !< \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pinum !< Aerosol number concentration for internally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type
  !< for internally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaint) :: piwetrc !< Total/wet paricle radius \f$[m\f$, int. mixture
  real, intent(out), dimension(ilga,leva,isdnum,kext+1) :: fnumo !< Filtered number size distribution
  real, intent(out), dimension(ilga,leva,isdnum,kint+kext+2) :: fmaso !< Filtered mass size distribution
  !
  !     internal work variables
  !
  real, dimension(ilga,leva,isdnum) :: fimom0 !<
  real, dimension(ilga,leva,isdnum) :: fimom3 !<
  real, dimension(ilga,leva,isdnum) :: femom0 !<
  real, dimension(ilga,leva,isdnum) :: femom3 !<
  real, dimension(ilga,leva,isdnum) :: fphis0 !<
  real, dimension(ilga,leva,isdnum) :: fdphi0 !<
  real, dimension(ilga,leva,isdnum) :: finumi !<
  real, dimension(ilga,leva,isdnum,kext) :: fenumi !<
  real, dimension(ilga,leva,isdnum,kint+1) :: fimasi !<
  real, dimension(ilga,leva,isdnum,kext+1) :: femasi !<
  integer :: il !<
  integer :: ik !<
  integer :: ikf !<
  integer :: is !<
  integer :: isc !<
  integer :: isd !<
  integer :: ist !<
  integer :: isx !<
  integer :: kx !<
  integer :: l !<
  integer :: nsub !<
  integer :: nfilt !<
  real :: frac !<
  real :: fgr !<
  real :: fphis !<
  real :: fdphi0s !<
  real :: fph1 !<
  real :: fph2 !<
  real :: fphis1 !<
  real :: fphie1 !<
  real :: fdphi1 !<
  real :: rmom3 !<
  !
  !-----------------------------------------------------------------------
  !     * particle sizes corresponding to section boundaries.
  !
  call diagsdw(fphis,fdphi0s)
  fdphi0=fdphi0s
  fphis0(:,:,1)=fphis
  do is=2,isdnum
    fphis0(:,:,is)=fphis0(:,:,1)+real(is-1)*fdphi0(:,:,is-1)
  end do
  !
  !-----------------------------------------------------------------------
  !     * integrate concentrations over sub-sections for internally
  !     * mixed aerosol. this assumes that the diagnostic size
  !     * distribution has smaller section widths than the original pla
  !     * size disrtibution.
  !
  rmom3=3.
  fimom0=0.
  fimom3=0.
  finumi=0.
  fimasi=0.
  if (isaint > 0) then
    do is=1,isaint
      do isd=1,isdnum
        do l=1,leva
          do il=1,ilga
            fph1=piphis0(il,l,is)
            fph2=piphis0(il,l,is)+pidphi0(il,l,is)
            if (    (fphis0(il,l,isd) >= fph1 &
                .and. fphis0(il,l,isd) <= fph2) &
                .or. ( (fphis0(il,l,isd)+fdphi0(il,l,isd)) >= fph1 &
                .and. (fphis0(il,l,isd)+fdphi0(il,l,isd)) <= fph2) ) then
              fphis1=max(fphis0(il,l,isd),fph1)
              fphie1=min(fphis0(il,l,isd)+fdphi0(il,l,isd),fph2)
              fdphi1=max(fphie1-fphis1,0.)
              fimom0(il,l,isd)=fimom0(il,l,isd)+pin0(il,l,is) &
                 *sdintb0(piphi0(il,l,is),pipsi(il,l,is),fphis1,fdphi1)
              fimom3(il,l,isd)=fimom3(il,l,isd)+ycnst*piddn(il,l,is) &
                            *pin0(il,l,is)*sdintb(piphi0(il,l,is), &
                                    pipsi(il,l,is),rmom3,fphis1,fdphi1)
              !
              !           * aerosol number.
              !
              finumi(il,l,isd)=fimom0(il,l,isd)
              !
              !           * mass for each species.
              !
              do ik=1,kint
                fimasi(il,l,isd,ik)=fimom3(il,l,isd)*pifrc(il,l,is,ik)
              end do
              !
              !           * aerosol water.
              !
              if (pinum(il,l,is) > ytiny .and. pimas(il,l,is) > ytiny &
                  .and. abs(pipsi(il,l,is)-yna) > ytiny) then
                fgr=piwetrc(il,l,is)/pidryrc(is)
                fimasi(il,l,isd,kint+1)=fimasi(il,l,isd,kint+1) &
                                     +(fimom3(il,l,isd)/piddn(il,l,is)) &
                                               *dnh2o*max(fgr**3-1.,0.)
              end if
            end if
          end do
        end do
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * integrate concentrations over sub-sections for externally
  !     * mixed aerosol.
  !
  femom0=0.
  femom3=0.
  fenumi=0.
  femasi=0.
  if (isaext > 0) then
    do is=1,isaext
      do isd=1,isdnum
        do l=1,leva
          do il=1,ilga
            fph1=pephis0(il,l,is)
            fph2=pephis0(il,l,is)+pedphi0(il,l,is)
            if (    (fphis0(il,l,isd) >= fph1 &
                .and. fphis0(il,l,isd) <= fph2) &
                .or. ( (fphis0(il,l,isd)+fdphi0(il,l,isd)) >= fph1 &
                .and. (fphis0(il,l,isd)+fdphi0(il,l,isd)) <= fph2) ) then
              fphis1=max(fphis0(il,l,isd),fph1)
              fphie1=min(fphis0(il,l,isd)+fdphi0(il,l,isd),fph2)
              fdphi1=max(fphie1-fphis1,0.)
              femom0(il,l,isd)=femom0(il,l,isd)+pen0(il,l,is) &
                 *sdintb0(pephi0(il,l,is),pepsi(il,l,is),fphis1,fdphi1)
              femom3(il,l,isd)=femom3(il,l,isd)+ycnst*peddn(il,l,is) &
                            *pen0(il,l,is)*sdintb(pephi0(il,l,is), &
                                    pepsi(il,l,is),rmom3,fphis1,fdphi1)
              !
              !           * aerosol number.
              !
              kx=sextf%isaer(is)%ityp
              fenumi(il,l,isd,kx)=femom0(il,l,isd)
              !
              !           * mass for each species.
              !
              femasi(il,l,isd,kx)=femom3(il,l,isd)
              !
              !           * aerosol water.
              !
              if (penum(il,l,is) > ytiny .and. pemas(il,l,is) > ytiny &
                  .and. abs(pepsi(il,l,is)-yna) > ytiny) then
                fgr=pewetrc(il,l,is)/pedryrc(is)
                femasi(il,l,isd,kext+1)=femasi(il,l,isd,kext+1) &
                                     +(femom3(il,l,isd)/peddn(il,l,is)) &
                                               *dnh2o*max(fgr**3-1.,0.)
              end if
            end if
          end do
        end do
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * strength of filter. no filter, if zero. the particle size
  !     * range of the filter is always smaller than the smallest
  !     * section width.
  !
  if (isaint > 0) then
    nsub=int(aintf%dpstar/fdphi0s)
  end if
  if (isaext > 0) then
    do kx=1,kext
      nsub=min(nsub,int(aextf%tp(kx)%dpstar/fdphi0s))
    end do
  end if
  !      nfilt=int(0.5*(real(nsub)-1.))
  nfilt=1
  !
  !     * filter the numer size distribution. the number of sub-sections
  !     * over the which the filter is applied is 2*nfilt+1.
  !
  frac=1./real(2*nfilt+1)
  do ik=1,kext
    do isx=1,isdnum
      fnumo(:,:,isx,ik)=fenumi(:,:,isx,ik)
      do isc=1,nfilt
        ist=isx+isc
        if (ist <= isdnum) then
          fnumo(:,:,isx,ik)=fnumo(:,:,isx,ik)+fenumi(:,:,ist,ik)
        end if
      end do
      do isc=1,nfilt
        ist=isx-isc
        if (ist >= 1) then
          fnumo(:,:,isx,ik)=fnumo(:,:,isx,ik)+fenumi(:,:,ist,ik)
        end if
      end do
      fnumo(:,:,isx,ik)=fnumo(:,:,isx,ik)*frac
    end do
  end do
  ik=kext+1
  do isx=1,isdnum
    fnumo(:,:,isx,ik)=finumi(:,:,isx)
    do isc=1,nfilt
      ist=isx+isc
      if (ist <= isdnum) then
        fnumo(:,:,isx,ik)=fnumo(:,:,isx,ik)+finumi(:,:,ist)
      end if
    end do
    do isc=1,nfilt
      ist=isx-isc
      if (ist >= 1) then
        fnumo(:,:,isx,ik)=fnumo(:,:,isx,ik)+finumi(:,:,ist)
      end if
    end do
    fnumo(:,:,isx,ik)=fnumo(:,:,isx,ik)*frac
  end do
  !
  !     * filter the mass size distributions.
  !
  do ik=1,kext+1
    do isx=1,isdnum
      fmaso(:,:,isx,ik)=femasi(:,:,isx,ik)
      do isc=1,nfilt
        ist=isx+isc
        if (ist <= isdnum) then
          fmaso(:,:,isx,ik)=fmaso(:,:,isx,ik)+femasi(:,:,ist,ik)
        end if
      end do
      do isc=1,nfilt
        ist=isx-isc
        if (ist >= 1) then
          fmaso(:,:,isx,ik)=fmaso(:,:,isx,ik)+femasi(:,:,ist,ik)
        end if
      end do
      fmaso(:,:,isx,ik)=fmaso(:,:,isx,ik)*frac
    end do
  end do
  do ik=1,kint+1
    do isx=1,isdnum
      ikf=kext+ik+1
      fmaso(:,:,isx,ikf)=fimasi(:,:,isx,ik)
      do isc=1,nfilt
        ist=isx+isc
        if (ist <= isdnum) then
          fmaso(:,:,isx,ikf)=fmaso(:,:,isx,ikf)+fimasi(:,:,ist,ik)
        end if
      end do
      do isc=1,nfilt
        ist=isx-isc
        if (ist >= 1) then
          fmaso(:,:,isx,ikf)=fmaso(:,:,isx,ikf)+fimasi(:,:,ist,ik)
        end if
      end do
      fmaso(:,:,isx,ikf)=fmaso(:,:,isx,ikf)*frac
    end do
  end do
  !
end subroutine sdfilt
