!> \file
!> \brief Sea salt generation
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine ssgen (pedndt,pedmdt,pidfdt,pidndt,pidmdt,pemas, &
                        penum,pimas,pinum,pedphi0,pephis0,pidphi0, &
                        piphis0,pifrc,piddn,zspd,focn, &
                        sicn,dt,sicn_crt,ilga,leva)
  !
  use sdparm, only : aextf,aintf,iexss,iinss,isaext,isaint,isextss,isintss, &
                         kext,kextss,kint,kintss,yna,ytiny
  !
  implicit none
  !
  real, intent(in),  dimension(ilga,leva,isaext) :: pedphi0 !< Section size parameter for externally mixed aerosol
  real, intent(in),  dimension(ilga,leva,isaext) :: pephis0 !< Dry particle size (\f$ln(R_p /R_0)\f$) at the boundaries of
  !< the size sections for externally mixed aerosol
  real, intent(in),  dimension(ilga,leva,isaint) :: pidphi0 !< Section size parameter for internally mixed
  !< aerosol
  real, intent(in),  dimension(ilga,leva,isaint) :: piphis0 !< Dry particle size (\f$ln(R_p /R_0)\f$) at the boundaries of
  !< the size sections for internally mixed aerosol
  real, intent(in),  dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in),  dimension(ilga,leva,isaint) :: pinum !< Aerosol number concentration for internally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(in),  dimension(ilga,leva,isaint) :: piddn !< (Dry) density for internally mixed aerosol particles
  !< \f$[kg/m^3]\f$
  real, intent(in),  dimension(ilga,leva,isaext) :: pemas !< Aerosol (dry) mass concentration for externally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in),  dimension(ilga,leva,isaext) :: penum !< Aerosol number concentration for externally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(in),  dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type
  !< for internally mixed aerosol
  real, intent(in),  dimension(ilga) :: zspd !< Surface wind speed \f$[m/s]\f$
  real, intent(in),  dimension(ilga) :: focn !< Fraction of ocean
  real, intent(in),  dimension(ilga) :: sicn !< Fraction of sea ice
  real, intent(in) :: dt !< Model time step \f$[s]\f$
  real, intent(in) :: sicn_crt !< Critical sea ice fraction
  real, intent(out), dimension(ilga,leva,isaext) :: pedndt !< Number tendency, external mixture \f$[1/kg/s]\f$
  real, intent(out), dimension(ilga,leva,isaext) :: pedmdt !< Mass tendency, external mixture \f$[kg/kg/s]\f$
  real, intent(out), dimension(ilga,leva,isaint) :: pidndt !< Number tendency, internal mixture \f$[1/kg/s]\f$
  real, intent(out), dimension(ilga,leva,isaint) :: pidmdt !< Mass tendency, internal mixture \f$[kg/kg/s]\f$
  real, intent(out), dimension(ilga,leva,isaint,kint) :: pidfdt !< Aerosol species mass fraction tendency internal
  !< mixture \f$[1/s]\f$
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  !
  real, allocatable, dimension(:,:) :: aephi0 !<
  real, allocatable, dimension(:,:) :: aen0 !<
  real, allocatable, dimension(:,:) :: aepsi !<
  real, allocatable, dimension(:,:) :: aiphi0 !<
  real, allocatable, dimension(:,:) :: ain0 !<
  real, allocatable, dimension(:,:) :: aipsi !<
  real, allocatable, dimension(:,:,:) :: bephi0 !<
  real, allocatable, dimension(:,:,:) :: ben0 !<
  real, allocatable, dimension(:,:,:) :: bepsi !<
  real, allocatable, dimension(:,:,:) :: beddn !<
  real, allocatable, dimension(:,:,:) :: bedphi0 !<
  real, allocatable, dimension(:,:,:) :: bephis0 !<
  real, allocatable, dimension(:,:,:) :: benum !<
  real, allocatable, dimension(:,:,:) :: bemas !<
  real, allocatable, dimension(:,:,:) :: biphi0 !<
  real, allocatable, dimension(:,:,:) :: bin0 !<
  real, allocatable, dimension(:,:,:) :: bipsi !<
  real, allocatable, dimension(:,:,:) :: biddn !<
  real, allocatable, dimension(:,:,:) :: bidphi0 !<
  real, allocatable, dimension(:,:,:) :: biphis0 !<
  real, allocatable, dimension(:,:,:) :: binum !<
  real, allocatable, dimension(:,:,:) :: bimas !<
  real, allocatable, dimension(:,:) :: cimast !<
  real, allocatable, dimension(:,:) :: cinumt !<
  real, allocatable, dimension(:,:,:) :: cimaf !<
  real, allocatable, dimension(:) :: fwat !<
  !
  !     internal work variables
  !
  integer :: il !<
  integer :: is !<
  !
  !-----------------------------------------------------------------------
  !
  !     * allocation of arrays and initialization.
  !
  allocate (fwat (ilga))
  if (isextss > 0) then
    allocate (aephi0 (ilga,isextss))
    allocate (aen0   (ilga,isextss))
    allocate (aepsi  (ilga,isextss))
    allocate (bephi0 (ilga,1,isextss))
    allocate (ben0   (ilga,1,isextss))
    allocate (bepsi  (ilga,1,isextss))
    allocate (beddn  (ilga,1,isextss))
    allocate (bedphi0(ilga,1,isextss))
    allocate (bephis0(ilga,1,isextss))
    allocate (benum  (ilga,1,isextss))
    allocate (bemas  (ilga,1,isextss))
  end if
  if (isintss > 0) then
    allocate (aiphi0 (ilga,isintss))
    allocate (ain0   (ilga,isintss))
    allocate (aipsi  (ilga,isintss))
    allocate (biphi0 (ilga,1,isintss))
    allocate (bin0   (ilga,1,isintss))
    allocate (bipsi  (ilga,1,isintss))
    allocate (biddn  (ilga,1,isintss))
    allocate (bidphi0(ilga,1,isintss))
    allocate (biphis0(ilga,1,isintss))
    allocate (binum  (ilga,1,isintss))
    allocate (bimas  (ilga,1,isintss))
    allocate (cimast (ilga,isintss))
    allocate (cinumt (ilga,isintss))
  end if
  if (isaint > 0) then
    allocate (cimaf  (ilga,isaint,kint))
  end if
  if (kext > 0) then
    pedndt=0.
    pedmdt=0.
  end if
  if (kint > 0) then
    pidndt=0.
    pidmdt=0.
    pidfdt=0.
  end if
  !
  !     sea salt equilibrium size distributions
  !
  fwat=0.
  do il=1,ilga
    if (sicn(il) < sicn_crt) then
      fwat(il)=focn(il)*(1.-sicn(il))
    end if
  end do
  if (kextss > 0) then
    call ssfcc(aen0,aephi0,aepsi,zspd,fwat,ilga,isextss)
  end if
  if (kintss > 0) then
    call ssfcc(ain0,aiphi0,aipsi,zspd,fwat,ilga,isintss)
  end if
  !
  !     convert results
  !
  if (kextss > 0) then
    ben0   (:,1,:)=aen0 (:,:)
    bephi0 (:,1,:)=aephi0(:,:)
    bepsi  (:,1,:)=aepsi (:,:)
    do is=1,isextss
      beddn  (:,1,is)=aextf%tp(kextss)%dens
      bedphi0(:,1,is)=pedphi0(:,leva,iexss(is))
      bephis0(:,1,is)=pephis0(:,leva,iexss(is))
    end do
  end if
  if (kintss > 0) then
    bin0   (:,1,:)=ain0 (:,:)
    biphi0 (:,1,:)=aiphi0(:,:)
    bipsi  (:,1,:)=aipsi (:,:)
    do is=1,isintss
      biddn  (:,1,is)=aintf%tp(kintss)%dens
      bidphi0(:,1,is)=pidphi0(:,leva,iinss(is))
      biphis0(:,1,is)=piphis0(:,leva,iinss(is))
    end do
  end if
  !
  !     mass and number concentrations
  !
  if (kextss > 0) then
    call pla2nm(benum,bemas,ben0,bephi0,bepsi,bephis0,bedphi0, &
                    beddn,ilga,1,isextss)
  end if
  if (kintss > 0) then
    call pla2nm(binum,bimas,bin0,biphi0,bipsi,biphis0,bidphi0, &
                    biddn,ilga,1,isintss)
  end if
  !
  !     mass and number tendencies for externally mixed aerosol.
  !
  do is=1,isextss
    do il=1,ilga
      if (abs(benum(il,1,is)-yna) > ytiny &
          .and. abs(bemas(il,1,is)-yna) > ytiny) then
        pedndt(il,leva,iexss(is))=fwat(il)*(benum(il,1,is) &
                                    -penum(il,leva,iexss(is)))/dt
        pedmdt(il,leva,iexss(is))=fwat(il)*(bemas(il,1,is) &
                                    -pemas(il,leva,iexss(is)))/dt
      end if
    end do
  end do
  !
  !     total aerosol mass and number for internally mixed types of
  !     aerosol after extraction of sea salt for initial aerosol
  !     size distributions.\n
  !     the implicit assumption for the change in aerosol number is
  !     that the aerosol number for each species is proportional to
  !     the volume fraction of the species.
  !
  if (kintss > 0) then
    do is=1,isintss
      cimast(:,is)=pimas(:,leva,iinss(is))
      cinumt(:,is)=pinum(:,leva,iinss(is))
    end do
    do is=1,isintss
      do il=1,ilga
        if (abs(binum(il,1,is)-yna) > ytiny &
            .and. abs(bimas(il,1,is)-yna) > ytiny) then
          cimast(il,is)=cimast(il,is) &
              -pifrc(il,leva,iinss(is),kintss)*pimas(il,leva,iinss(is))
          cinumt(il,is)=cinumt(il,is) &
             -pinum(il,leva,iinss(is))*(pifrc(il,leva,iinss(is),kintss) &
                       *piddn(il,leva,iinss(is))/aintf%tp(kintss)%dens)
          cimast(il,is)=max(cimast(il,is),0.)
          cinumt(il,is)=max(cinumt(il,is),0.)
        end if
      end do
    end do
  end if
  !
  !     get total aerosol mass and number after addition of equilibrium
  !     sea salt aerosol concentrations.
  !
  if (kintss > 0) then
    do is=1,isintss
      do il=1,ilga
        if (abs(binum(il,1,is)-yna) > ytiny &
            .and. abs(bimas(il,1,is)-yna) > ytiny) then
          cimast(il,is)=cimast(il,is)+bimas(il,1,is)
          cinumt(il,is)=cinumt(il,is)+binum(il,1,is)
        end if
      end do
    end do
  end if
  !
  !
  !     adjust total concentrations after emission of sea salt to
  !     account for lack of emissions from non-ocean fraction of grid cell.
  !
  if (kintss > 0) then
    do is=1,isintss
      cimast(:,is)=(1.-fwat(:))*pimas(:,leva,iinss(is)) + &
                       fwat(:)*cimast(:,is)
      cinumt(:,is)=(1.-fwat(:))*pinum(:,leva,iinss(is)) + &
                       fwat(:)*cinumt(:,is)
    end do
  end if
  !
  !     get new sea salt mass fraction for internally mixed types of
  !     aerosol.
  !
  if (kint > 0) then
    cimaf=pifrc(:,leva,:,:)
  end if
  if (kintss > 0) then
    do is=1,isintss
      do il=1,ilga
        if (cimast(il,is) > ytiny &
            .and. abs(binum(il,1,is)-yna) > ytiny &
            .and. abs(bimas(il,1,is)-yna) > ytiny) then
          cimaf(il,iinss(is),:)=cimaf(il,iinss(is),:) &
                                *pimas(il,leva,iinss(is))/cimast(il,is)
          cimaf(il,iinss(is),kintss)= &
                                        (1.-fwat(il))*cimaf(il,iinss(is),kintss) &
                                        +fwat(il)*bimas(il,1,is)/cimast(il,is)
        else
          cimaf(il,iinss(is),:)=pifrc(il,leva,iinss(is),:)
        end if
      end do
    end do
  end if
  !
  !     get mass, number, and mass fraction tendencies for internally
  !     mixed types of aerosol.
  !
  if (kint > 0) then
    pidfdt(:,leva,:,:)=(cimaf-pifrc(:,leva,:,:))/dt
  end if
  if (kintss > 0) then
    do is=1,isintss
      pidndt(:,leva,iinss(is))=(cinumt(:,is) &
                                   -pinum(:,leva,iinss(is)))/dt
      pidmdt(:,leva,iinss(is))=(cimast(:,is) &
                                   -pimas(:,leva,iinss(is)))/dt
    end do
  end if
  !
  !     * deallocation of work arrays.
  !
  deallocate (fwat)
  if (isextss > 0) then
    deallocate (aephi0)
    deallocate (aen0)
    deallocate (aepsi)
    deallocate (bephi0)
    deallocate (ben0)
    deallocate (bepsi)
    deallocate (beddn)
    deallocate (bedphi0)
    deallocate (bephis0)
    deallocate (benum)
    deallocate (bemas)
  end if
  if (isintss > 0) then
    deallocate (aiphi0)
    deallocate (ain0)
    deallocate (aipsi)
    deallocate (biphi0)
    deallocate (bin0)
    deallocate (bipsi)
    deallocate (biddn)
    deallocate (bidphi0)
    deallocate (biphis0)
    deallocate (binum)
    deallocate (bimas)
    deallocate (cimast)
    deallocate (cinumt)
  end if
  if (isaint > 0) then
    deallocate (cimaf)
  end if
  !
end subroutine ssgen
!> \file
!! \subsection ssec_details Details
!! PLA parameters are determined for sea salt equilibrium size distributions.
!! These are converted to mass and number concentrations followed by calculation
!! of mass and number tendencies for externally mixed aerosol.
!! \n
!! \n
!! Total aerosol mass and number for internally mixed types of
!! aerosol are determined after extraction of sea salt for initial aerosol
!! size distributions. The implicit assumption for the change
!! in aerosol number is that the aerosol number for each species
!! is proportional to the volume fraction of the species.
!! \n
!! \n
!! Total aerosol mass and number after addition of equilibrium
!! sea salt aerosol concentrations are determined.
!! \n
!! \n
!! Total concentrations are adjusted after emission of sea salt to
!! account for lack of emissions from non-ocean fraction of grid cell.
!! \n
!! \n
!! New sea salt mass fraction for internally mixed types of
!! aerosol is determined
!! \n
!! \n
!! Mass, number, and mass fraction tendencies are determined for internally
!! mixed types of aerosol.
