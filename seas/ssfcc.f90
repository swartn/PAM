!> \file
!> \brief Calculates sea salt concentrations in the first model layer
!!
!! @authors M. Lazare, K. von Salzen, and X.Y. Ma
!
!-----------------------------------------------------------------------
subroutine ssfcc (pn0,phi0,psi,zspd,fwat,ilga,isec)
  !
  use sdparm, only : yna
  !
  implicit none
  !
  real, intent(in),  dimension(ilga) :: zspd !< Surface wind speed \f$[m/sec]\f$
  real, intent(in),  dimension(ilga) :: fwat !< Fraction water
  real, intent(out), dimension(ilga,isec) :: pn0 !< First PLA size distribution parameter (\f$n_{0,i}\f$, amplitude)
  real, intent(out), dimension(ilga,isec) :: phi0 !< Third PLA size distribution parameter (\f$\phi_{0,i}\f$, width)
  real, intent(out), dimension(ilga,isec) :: psi !< Second PLA size distribution parameter (\f$\psi_{i}\f$, mode size)
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: isec !< Number of size sections
  !
  !     internal work variables
  !
  integer :: il !<
  !
  !==================================================================
  !     physical (adjustable) parameters
  !
  real, parameter :: tuness=12. !< This is the adjustable parameter for computing sea salt
  !! concentration in the first model layer
  !! Its minimum/default/maximum is (?/12/?).
  !
  !==================================================================
  !
  !     * initialization of pla parameters.
  !
  pn0 =0.
  phi0=yna
  psi =yna
  !
  !    wind-speed dependent calculation of pla parameters
  !    based on the parameterization by lewis and schwartz.
  !    a growth factor of 2.1859 is assumed to convert the particle size
  !    at 80% to 0% relative humidity.
  !
  do il=1,ilga
    if (fwat(il)>0.) then
      pn0 (il,:)=tuness*30400.*zspd(il)**2
      !          phi0(il,:)=-0.3085e+01
      phi0(il,:)=-0.2679e+01
      !          phi0(il,:)=-0.2391e+01
      !          phi0(il,:)=-0.1986e+01
      psi (il,:)=0.4716e+00
    end if
  end do
  !
end subroutine ssfcc
!
!> \file
!! \subsection ssec_details Details
!! This routine performs a wind-speed dependent calculation of PLA parameters based
!! on the parameterization by Lewis and Schwartz. A growth factor of 2.1859 is assumed to convert the particle size
!! at 80% to 0% relative humidity.
