!>    \file
!>    \brief Arrays for model input/output of basic aerosol properties.
!!
!!    @author K. von Salzen
!
!-----------------------------------------------------------------------
module sdinfo
  !
  implicit none
  !
  integer, parameter :: imaxr=10 !<
  type spec
    real :: num !<
    real :: sig !<
    real :: modr !<
    real :: mfr !<
  end type spec
  type smode
    type(spec), dimension(imaxr) :: tp
    real :: num !<
    real :: sig !<
    real :: modr !<
  end type smode
  type sdinft
    type(smode), dimension(imaxr) :: mode
  end type sdinft
  type sdinfbt
    integer :: npnt !<
    integer :: kextt !<
    integer :: kintt !<
    integer :: imodee !<
    integer :: imodei !<
  end type sdinfbt
  type (sdinft) :: sdint,sdext
  type (sdinfbt) :: sd
  !
  real, allocatable, dimension(:,:) :: cinum !<
  real, allocatable, dimension(:,:) :: cisig !<
  real, allocatable, dimension(:,:) :: cimodr !<
  real, allocatable, dimension(:,:,:) :: cenum !<
  real, allocatable, dimension(:,:,:) :: cesig !<
  real, allocatable, dimension(:,:,:) :: cemodr !<
  real, allocatable, dimension(:,:,:) :: cimfr !<
  !
end module sdinfo
