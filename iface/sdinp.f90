!> \file
!> \brief Size distribution data
!!
!! @author Knut von Salzen
!
!-----------------------------------------------------------------------
subroutine sdinp(npnt,kextt,kintt,imodee,imodei)
  !
  use sdinfo, only : cemodr,cenum,cesig,cimfr,cimodr,cinum,cisig,sd,sdext,sdint
  !
  implicit none
  !
  integer, parameter :: nulst=77 !<
  integer, intent(out) :: npnt !< Total number of size distributions
  integer, intent(out) :: kextt !< Number of externally mixed species
  integer, intent(out) :: kintt !< Number of internally mixed species
  integer, intent(out) :: imodee !< Number of externally mixed log-normal modes
  integer, intent(out) :: imodei !< Number of internally mixed log-normal modes
  integer :: im !<
  integer :: kx !<
  namelist /sdinfb/ sd
  namelist /sdinf/ sdint,sdext
  !
  open(nulst,file='SDPAR')
  read(nulst,nml=sdinfb,end=1001)
  rewind(nulst)
  read(nulst,nml=sdinf,end=1001)
  !
  npnt=sd%npnt
  kextt=sd%kextt
  kintt=sd%kintt
  imodee=sd%imodee
  imodei=sd%imodei
  !
  if (kextt > 0) then
    allocate(cenum (npnt,imodee,kextt))
    allocate(cesig (npnt,imodee,kextt))
    allocate(cemodr(npnt,imodee,kextt))
    do im=1,imodee
      do kx=1,kextt
        cenum (:,im,kx)=sdext%mode(im)%tp(kx)%num
        cesig (:,im,kx)=sdext%mode(im)%tp(kx)%sig
        cemodr(:,im,kx)=sdext%mode(im)%tp(kx)%modr
      end do
    end do
  end if
  if (kintt > 0) then
    allocate(cinum (npnt,imodei))
    allocate(cisig (npnt,imodei))
    allocate(cimodr(npnt,imodei))
    allocate(cimfr (npnt,imodei,kintt))
    do im=1,imodei
      cinum (:,im)=sdint%mode(im)%num
      cisig (:,im)=sdint%mode(im)%sig
      cimodr(:,im)=sdint%mode(im)%modr
      do kx=1,kintt
        cimfr(:,im,kx)=sdint%mode(im)%tp(kx)%mfr
      end do
    end do
  end if
  !
  close(nulst)
  !
  return
  1001 call xit('SDINP',-1)
  !
end subroutine sdinp
