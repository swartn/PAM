!>    \file
!>    \brief Initial aerosol information
!
!-----------------------------------------------------------------------
module compar
  !
  implicit none
  !
  !     * basic parameters.
  !
  integer, parameter :: kextt   =  4 !< Number of externally mixed species
  integer, parameter :: kintt   =  3 !< Number of internally mixed species
  integer, parameter :: isaintt =  3 !< Internally mixed section index
  integer, parameter :: nrmfld  =  2 !< Maximum number of fields for averaging of results
  integer, parameter :: iavgprd = 24 !< Number of time steps in averaging time period
  integer, parameter :: iupdatp = 12 !< Number of time steps between updates of averaged results
  integer, parameter :: icfrq   = 12 !< Number of time steps between calls of activation code
  !
  !     * extra output.
  !
#if defined(GM_PAM)
integer, parameter :: isdnum  = 30 !<
#else
integer, parameter :: isdnum  = 22 !<
#endif

#if defined(GM_PAM)
integer, parameter :: isdiag  = 30 !<
#else
integer, parameter :: isdiag  = 22 !<
#endif

  logical :: kxtra1 !<
  logical :: kxtra2 !<
  !
  !     * extra output for mineral dust.
  !
#if defined(GM_PAM)
integer, parameter :: isdust  = 96 !<
#else
integer, parameter :: isdust  = 22 !<
#endif
  !
end module compar
