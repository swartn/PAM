!>    \file
!>    \brief Generates aerosol size distribution information for initialization
!>           of PLA calculations based on specified lognormal size disribution
!>           information as input
!
!-----------------------------------------------------------------------
subroutine geninit
  !
  use sdinfo, only : cemodr,cenum,cesig,cimfr,cimodr,cinum,cisig
  use sdparm, only : aextf,aintf,isaext,isaint,kext,kint,kpext,kpint, &
                         pedphis,peismax,peismin,pephiss, &
                         pidphis,piismax,piismin,piphiss, &
                         r0,sextf,ycnst,ylarge,yna,ypi,ysmall
  !
  implicit none
  !
  !     * pla parameters
  !
  real, allocatable, dimension(:,:,:) :: peddnt !<
  real, allocatable, dimension(:,:,:) :: piddnt !<
  real, allocatable, dimension(:,:,:) :: pen0t !<
  real, allocatable, dimension(:,:,:) :: pephi0t !<
  real, allocatable, dimension(:,:,:) :: pepsit !<
  real, allocatable, dimension(:,:,:) :: penumt !<
  real, allocatable, dimension(:,:,:) :: pemast !<
  real, allocatable, dimension(:,:,:) :: pin0t !<
  real, allocatable, dimension(:,:,:) :: piphi0t !<
  real, allocatable, dimension(:,:,:) :: pipsit !<
  real, allocatable, dimension(:,:,:) :: pinumt !<
  real, allocatable, dimension(:,:,:) :: pimast !<
  real, allocatable, dimension(:,:,:,:) :: pimasst !<
  real, allocatable, dimension(:,:,:,:) :: pifrct !<
  real, allocatable, dimension(:,:,:) :: pedphi0 !<
  real, allocatable, dimension(:,:,:) :: pephis0 !<
  real, allocatable, dimension(:,:,:) :: pidphi0 !<
  real, allocatable, dimension(:,:,:) :: piphis0 !<
  real, allocatable, dimension(:,:,:) :: pedphi0t !<
  real, allocatable, dimension(:,:,:) :: pephis0t !<
  real, allocatable, dimension(:,:,:) :: pidphi0t !<
  real, allocatable, dimension(:,:,:) :: piphis0t !<
  real, allocatable, dimension(:,:,:) :: pen0 !<
  real, allocatable, dimension(:,:,:) :: pephi0 !<
  real, allocatable, dimension(:,:,:) :: pepsi !<
  real, allocatable, dimension(:,:,:) :: penum !<
  real, allocatable, dimension(:,:,:) :: pemas !<
  real, allocatable, dimension(:,:,:) :: peddn !<
  real, allocatable, dimension(:,:,:) :: pin0 !<
  real, allocatable, dimension(:,:,:) :: piphi0 !<
  real, allocatable, dimension(:,:,:) :: pipsi !<
  real, allocatable, dimension(:,:,:) :: pinum !<
  real, allocatable, dimension(:,:,:) :: pimas !<
  real, allocatable, dimension(:,:,:) :: piddn !<
  real, allocatable, dimension(:,:,:,:) :: pifrc !<
  real, allocatable, dimension(:,:) :: cornt !<
  real, allocatable, dimension(:,:) :: cormt !<
  real, allocatable, dimension(:,:) :: resnt !<
  real, allocatable, dimension(:,:) :: resmt !<
  real, allocatable, dimension(:,:) :: penumr !<
  real, allocatable, dimension(:,:) :: pemasr !<
  real, allocatable, dimension(:,:) :: pinumr !<
  real, allocatable, dimension(:,:) :: pimasr !<
  real, allocatable, dimension(:,:) :: penumd !<
  real, allocatable, dimension(:,:) :: pemasd !<
  real, allocatable, dimension(:,:) :: pinumd !<
  real, allocatable, dimension(:,:) :: pimasd !<
  real, allocatable, dimension(:) :: tmpsdn !<
  character(len=2) :: cfnx !<
  character(len=2) :: cfkx !<
  character(len=2) :: cfis !<
  !
  integer, parameter :: isdbin=300 !<
  integer, parameter :: isdbinm=isdbin-1 ! number of diagnostic bins
  real, dimension(isdbin) :: fphis0 !<
  real, dimension(isdbin) :: fdphi0 !<
  integer :: nuout1 !<
  integer :: nuout2 !<
  integer :: nuout3 !<
  integer :: nuout4 !<
  integer :: npnt !<
  integer :: kextt !<
  integer :: kintt !<
  integer :: imodee !<
  integer :: imodei !<
  integer :: im !<
  integer :: is !<
  integer :: isd !<
  integer :: kx !<
  integer :: np !<
  integer :: nx !<
  real :: rmsee !<
  real :: rmsei !<
  real :: fph !<
  real :: fph1 !<
  real :: fph2 !<
  real :: fphie !<
  real :: fphit !<
  real :: fphis !<
  real :: fdphi0s !<
  !
  !     * input/output file unit numbers.
  !
  data nuout1 /10/
  data nuout2 /11/
  data nuout3 /12/
  data nuout4 /13/
  !
  !-----------------------------------------------------------------------
  !     * size distribution data.
  !
  call sdinp(npnt,kextt,kintt,imodee,imodei)
  !
  !    check input information.
  !
  if (kintt /= kint .or. kextt /= kext) then
    call xit('MAIN',-1)
  end if
  !
  !     * allocate memory.
  !
  if (isaext > 0) then
    allocate(pephis0(npnt,imodee,isaext))
    allocate(pedphi0(npnt,imodee,isaext))
    allocate(peddn  (npnt,imodee,isaext))
    allocate(pen0   (npnt,imodee,isaext))
    allocate(pephi0 (npnt,imodee,isaext))
    allocate(pepsi  (npnt,imodee,isaext))
    allocate(penum  (npnt,imodee,isaext))
    allocate(pemas  (npnt,imodee,isaext))
    allocate(pephis0t(npnt,1,isaext))
    allocate(pedphi0t(npnt,1,isaext))
    allocate(pen0t  (npnt,1,isaext))
    allocate(pephi0t(npnt,1,isaext))
    allocate(pepsit (npnt,1,isaext))
    allocate(penumt (npnt,1,isaext))
    allocate(pemast (npnt,1,isaext))
    allocate(peddnt (npnt,1,isaext))
    allocate(penumr (npnt,isdbinm))
    allocate(pemasr (npnt,isdbinm))
    allocate(penumd (npnt,isdbinm))
    allocate(pemasd (npnt,isdbinm))
  end if
  if (isaint > 0) then
    allocate(piphis0(npnt,imodei,isaint))
    allocate(pidphi0(npnt,imodei,isaint))
    allocate(piddn  (npnt,imodei,isaint))
    allocate(pin0   (npnt,imodei,isaint))
    allocate(piphi0 (npnt,imodei,isaint))
    allocate(pipsi  (npnt,imodei,isaint))
    allocate(pinum  (npnt,imodei,isaint))
    allocate(pimas  (npnt,imodei,isaint))
    allocate(pifrc  (npnt,imodei,isaint,kint))
    allocate(piphis0t(npnt,1,isaint))
    allocate(pidphi0t(npnt,1,isaint))
    allocate(pin0t  (npnt,1,isaint))
    allocate(piphi0t(npnt,1,isaint))
    allocate(pipsit (npnt,1,isaint))
    allocate(pinumt (npnt,1,isaint))
    allocate(pimast (npnt,1,isaint))
    allocate(pimasst(npnt,1,isaint,kint))
    allocate(pifrct (npnt,1,isaint,kint))
    allocate(piddnt (npnt,1,isaint))
    allocate(pinumr (npnt,isdbinm))
    allocate(pimasr (npnt,isdbinm))
    allocate(pinumd (npnt,isdbinm))
    allocate(pimasd (npnt,isdbinm))
  end if
  allocate(tmpsdn(npnt))
  !
  !-----------------------------------------------------------------------
  !     aerosol size information.
  !
  do is=1,isaext
    pedphi0(:,:,is)=pedphis(is)
    pephis0(:,:,is)=pephiss(is)
    pedphi0t(:,:,is)=pedphis(is)
    pephis0t(:,:,is)=pephiss(is)
  end do
  do is=1,isaint
    pidphi0(:,:,is)=pidphis(is)
    piphis0(:,:,is)=piphiss(is)
    pidphi0t(:,:,is)=pidphis(is)
    piphis0t(:,:,is)=piphiss(is)
  end do
  !
  !     dry particle densities for externally mixed aerosol types.
  !
  do is=1,isaext
    kx=sextf%isaer(is)%ityp
    peddn  (:,:,is)=aextf%tp(kx)%dens
  end do
  !
  !     initialize dry particle densities for internally mixed
  !     aerosol types. the densities are later calculated as a
  !     function of aerosol mass.
  !
  do is=1,isaint
    piddn  (:,:,is)=yna
  end do
  !
  !-----------------------------------------------------------------------
  !     pla size distribution parameters.
  !
  if (kext > 0) then
    do is=1,isaext
      kx=sextf%isaer(is)%ityp
      pen0(:,:,is)=cenum(:,:,kx)*1.e+06 &
                      /(sqrt(2.*ypi)*log(cesig(:,:,kx)))
      pephi0(:,:,is)=log((cemodr(:,:,kx)*1.e-06)/r0)
      pepsi(:,:,is)=1./(2.*log(cesig(:,:,kx))**2)
    end do
  end if
  if (kint > 0) then
    do kx=1,kint
      do is=1,isaint
        pin0(:,:,is)=cinum(:,:)*1.e+06 &
                      /(sqrt(2.*ypi)*log(cisig(:,:)))
        piphi0(:,:,is)=log((cimodr(:,:)*1.e-06)/r0)
        pipsi(:,:,is)=1./(2.*log(cisig(:,:))**2)
        pifrc(:,:,is,kx)=cimfr(:,:,kx)
      end do
    end do
  end if
  !
  !     write pla parameters for each mode.
  !
  open(nuout1,file='OUT1')
  write(nuout1,'(A5)') '&INIT'
  write(nuout1,'(1X)')
  write(nuout1,'(A8)') 'SDIMOD=1'
  write(nuout1,'(1X)')
  if (kext > 0) then
    nx=0
    do np=1,npnt
      do im=1,imodee
        nx=nx+1
        if (nx < 10) then
          cfnx='I1'
        else if (nx < 100) then
          cfnx='I2'
        else
          cfnx='I3'
        end if
        do kx=1,kext
          do is=1,aextf%tp(kx)%isec
            if (kx < 10) then
              cfkx='I1'
            else if (kx < 100) then
              cfkx='I2'
            else
              cfkx='I3'
            end if
            if (is < 10) then
              cfis='I1'
            else if (is < 100) then
              cfis='I2'
            else
              cfis='I3'
            end if
            write(nuout1, &
               '(A5,'//cfnx//',A5,'//cfkx//',A6,'//cfis//',A2,E15.9)') &
               'BEXT(',nx,')%TP(',kx,')%PN0(',is,')=',pen0(np,im,is)
            write(nuout1, &
               '(A5,'//cfnx//',A5,'//cfkx//',A8,'//cfis//',A2,E15.9)') &
               'BEXT(',nx,')%TP(',kx,')%PPHI0(',is,')=',pephi0(np,im,is)
            write(nuout1, &
               '(A5,'//cfnx//',A5,'//cfkx//',A7,'//cfis//',A2,E15.9)') &
               'BEXT(',nx,')%TP(',kx,')%PPSI(',is,')=',pepsi(np,im,is)
            write(nuout1,'(1X)')
          end do
        end do
      end do
    end do
  end if
  if (kint > 0) then
    nx=0
    do np=1,npnt
      do im=1,imodei
        nx=nx+1
        if (nx < 10) then
          cfnx='I1'
        else if (nx < 100) then
          cfnx='I2'
        else
          cfnx='I3'
        end if
        do is=1,isaint
          if (is < 10) then
            cfis='I1'
          else if (is < 100) then
            cfis='I2'
          else
            cfis='I3'
          end if
          write(nuout1, &
                '(A5,'//cfnx//',A6,'//cfis//',A2,E15.9)') &
                 'BINT(',nx,')%PN0(',is,')=',pin0(np,im,is)
          write(nuout1, &
                '(A5,'//cfnx//',A8,'//cfis//',A2,E15.9)') &
                 'BINT(',nx,')%PPHI0(',is,')=',piphi0(np,im,is)
          write(nuout1, &
                '(A5,'//cfnx//',A7,'//cfis//',A2,E15.9)') &
                 'BINT(',nx,')%PPSI(',is,')=',pipsi(np,im,is)
          do kx=1,kint
            if (kx < 10) then
              cfkx='I1'
            else if (kx < 100) then
              cfkx='I2'
            else
              cfkx='I3'
            end if
            write(nuout1, &
              '(A5,'//cfnx//',A5,'//cfkx//',A7,'//cfis//',A2,E15.9)') &
              'BINT(',nx,')%TP(',kx,')%CONC(',is,')=',pifrc(np,im,is,kx)
          end do
          write(nuout1,'(1X)')
        end do
      end do
    end do
  end if
  write(nuout1,'(A1)') '/'
  close(nuout1)
  !
  !-----------------------------------------------------------------------
  !     get dry particle density (internally mixed aerosol).
  !
  if (isaint > 0) call sddens(piddn,pifrc,npnt,imodei)
  !
  !     convert pla parameters to mass and number.
  !
  if (isaext > 0) &
      call pla2nm(penum,pemas,pen0,pephi0,pepsi,pephis0,pedphi0, &
      peddn,npnt,imodee,isaext)
  if (isaint > 0) &
      call pla2nm(pinum,pimas,pin0,piphi0,pipsi,piphis0,pidphi0, &
      piddn,npnt,imodei,isaint)
  !
  !     add size distributions from each mode.
  !
  if (kext > 0) then
    penumt=0.
    pemast=0.
    do im=1,imodee
      penumt(:,1,:)=penumt(:,1,:)+penum(:,im,:)
      pemast(:,1,:)=pemast(:,1,:)+pemas(:,im,:)
    end do
    print*,'TOTAL EXT. NUMBER = ',sum(penumt(1,1,:))/1.e+06
    print*,'TOTAL EXT. MASS   = ',sum(pemast(1,1,:))*1.e+09
  end if
  do is=1,isaext
    kx=sextf%isaer(is)%ityp
    peddnt(:,1,is)=aextf%tp(kx)%dens
  end do
  if (kint > 0) then
    pinumt=0.
    pimast=0.
    pimasst=0.
    do im=1,imodei
      pinumt(:,1,:)=pinumt(:,1,:)+pinum(:,im,:)
      pimast(:,1,:)=pimast(:,1,:)+pimas(:,im,:)
      do kx=1,kint
        pimasst(:,1,:,kx)=pimasst(:,1,:,kx) &
                           +pifrc(:,im,:,kx)*pimas(:,im,:)
      end do
    end do
    print*,'TOTAL INT. NUMBER = ',sum(pinumt(1,1,:))/1.e+06
    print*,'TOTAL INT. MASS   = ',sum(pimast(1,1,:))*1.e+09
    do kx=1,kint
      where (pimast(:,1,:) > ysmall)
        pifrct(:,1,:,kx)=pimasst(:,1,:,kx)/pimast(:,1,:)
      else where
        pifrct(:,1,:,kx)=0.
      end where
    end do
  end if
  print*,' '
  if (isaint > 0) call sddens(piddnt,pifrct,npnt,1)
  !
  !     write mass and number size distributions for all modes.
  !
  open(nuout2,file='OUT2')
  write(nuout2,'(A5)') '&INIT'
  write(nuout2,'(1X)')
  write(nuout2,'(A8)') 'SDIMOD=2'
  write(nuout2,'(1X)')
  if (kext > 0) then
    do np=1,npnt
      if (np < 10) then
        cfnx='I1'
      else if (np < 100) then
        cfnx='I2'
      else
        cfnx='I3'
      end if
      do kx=1,kext
        do is=1,aextf%tp(kx)%isec
          if (kx < 10) then
            cfkx='I1'
          else if (kx < 100) then
            cfkx='I2'
          else
            cfkx='I3'
          end if
          if (is < 10) then
            cfis='I1'
          else if (is < 100) then
            cfis='I2'
          else
            cfis='I3'
          end if
          write(nuout2, &
               '(A5,'//cfnx//',A5,'//cfkx//',A6,'//cfis//',A2,E15.9)') &
               'BEXT(',np,')%TP(',kx,')%NUM(',is,')=',penumt(np,1,is)
          write(nuout2, &
               '(A5,'//cfnx//',A5,'//cfkx//',A6,'//cfis//',A2,E15.9)') &
               'BEXT(',np,')%TP(',kx,')%MAS(',is,')=',pemast(np,1,is)
          write(nuout2,'(1X)')
        end do
      end do
    end do
  end if
  if (kint > 0) then
    do np=1,npnt
      if (np < 10) then
        cfnx='I1'
      else if (np < 100) then
        cfnx='I2'
      else
        cfnx='I3'
      end if
      do is=1,isaint
        if (is < 10) then
          cfis='I1'
        else if (is < 100) then
          cfis='I2'
        else
          cfis='I3'
        end if
        write(nuout2, &
               '(A5,'//cfnx//',A6,'//cfis//',A2,E15.9)') &
               'BINT(',np,')%NUM(',is,')=',pinumt(np,1,is)
        do kx=1,kint
          if (kx < 10) then
            cfkx='I1'
          else if (kx < 100) then
            cfkx='I2'
          else
            cfkx='I3'
          end if
          write(nuout2, &
               '(A5,'//cfnx//',A5,'//cfkx//',A6,'//cfis//',A2,E15.9)') &
               'BINT(',np,')%TP(',kx,')%MAS(',is,')=', &
                  pimast(np,1,is)*pifrct(np,1,is,kx)
        end do
        write(nuout2,'(1X)')
      end do
    end do
  end if
  write(nuout2,'(A1)') '/'
  close(nuout2)
  !
  !-----------------------------------------------------------------------
  !     make aerosol number and mass adjustments.
  !
  allocate(resmt(npnt,1))
  allocate(resnt(npnt,1))
  allocate(cormt(npnt,1))
  allocate(cornt(npnt,1))
  if (isaext > 0) then
    call cornmi(resmt,resnt,cormt,cornt,penumt,pemast,peddnt, &
                    peismin,peismax,pephiss,pedphis,npnt,1,isaext)
  end if
  if (isaint > 0) then
    call cornmi(resmt,resnt,cormt,cornt,pinumt,pimast,piddnt, &
                    piismin,piismax,piphiss,pidphis,npnt,1,isaint)
  end if
  !
  !-----------------------------------------------------------------------
  !     convert input number and mass mixing ratios to pla
  !     parameters.
  !
  if (isaext > 0) then
    call nm2pla(pen0t,pephi0t,pepsit,resnt,resmt,penumt,pemast, &
                    peddnt,pephiss,pedphis,pephis0t,pedphi0t, &
                    npnt,1,isaext,kpext)
  end if
  if (isaint > 0) then
    call nm2pla(pin0t,piphi0t,pipsit,resnt,resmt,pinumt,pimast, &
                    piddnt,piphiss,pidphis,piphis0t,pidphi0t, &
                    npnt,1,isaint,kpint)
  end if
  !
  !     write pla parameters for all modes.
  !
  open(nuout3,file='OUT3')
  write(nuout3,'(A5)') '&INIT'
  write(nuout3,'(1X)')
  write(nuout3,'(A8)') 'SDIMOD=1'
  write(nuout3,'(1X)')
  if (kext > 0) then
    do np=1,npnt
      if (np < 10) then
        cfnx='I1'
      else if (np < 100) then
        cfnx='I2'
      else
        cfnx='I3'
      end if
      do kx=1,kext
        do is=1,aextf%tp(kx)%isec
          if (kx < 10) then
            cfkx='I1'
          else if (kx < 100) then
            cfkx='I2'
          else
            cfkx='I3'
          end if
          if (is < 10) then
            cfis='I1'
          else if (is < 100) then
            cfis='I2'
          else
            cfis='I3'
          end if
          write(nuout3, &
               '(A5,'//cfnx//',A5,'//cfkx//',A6,'//cfis//',A2,E15.9)') &
               'BEXT(',np,')%TP(',kx,')%PN0(',is,')=',pen0t(np,1,is)
          write(nuout3, &
               '(A5,'//cfnx//',A5,'//cfkx//',A8,'//cfis//',A2,E15.9)') &
               'BEXT(',np,')%TP(',kx,')%PPHI0(',is,')=',pephi0t(np,1,is)
          write(nuout3, &
               '(A5,'//cfnx//',A5,'//cfkx//',A7,'//cfis//',A2,E15.9)') &
               'BEXT(',np,')%TP(',kx,')%PPSI(',is,')=',pepsit(np,1,is)
          write(nuout3,'(1X)')
        end do
      end do
    end do
  end if
  if (kint > 0) then
    do np=1,npnt
      if (np < 10) then
        cfnx='I1'
      else if (np < 100) then
        cfnx='I2'
      else
        cfnx='I3'
      end if
      do is=1,isaint
        if (is < 10) then
          cfis='I1'
        else if (is < 100) then
          cfis='I2'
        else
          cfis='I3'
        end if
        write(nuout3, &
                '(A5,'//cfnx//',A6,'//cfis//',A2,E15.9)') &
                 'BINT(',np,')%PN0(',is,')=',pin0t(np,1,is)
        write(nuout3, &
                '(A5,'//cfnx//',A8,'//cfis//',A2,E15.9)') &
                 'BINT(',np,')%PPHI0(',is,')=',piphi0t(np,1,is)
        write(nuout3, &
                '(A5,'//cfnx//',A7,'//cfis//',A2,E15.9)') &
                 'BINT(',np,')%PPSI(',is,')=',pipsit(np,1,is)
        do kx=1,kint
          if (kx < 10) then
            cfkx='I1'
          else if (kx < 100) then
            cfkx='I2'
          else
            cfkx='I3'
          end if
          write(nuout3, &
              '(A5,'//cfnx//',A5,'//cfkx//',A7,'//cfis//',A2,E15.9)') &
              'BINT(',np,')%TP(',kx,')%CONC(',is,')=',pifrct(np,1,is,kx)
        end do
        write(nuout3,'(1X)')
      end do
    end do
  end if
  write(nuout3,'(A1)') '/'
  close(nuout3)
  !
  !-----------------------------------------------------------------------
  !     get dry particle density (internally mixed aerosol).
  !
  if (isaint > 0) call sddens(piddnt,pifrct,npnt,1)
  !
  !     convert pla parameters to mass and number.
  !
  if (isaext > 0) &
      call pla2nm(penumt,pemast,pen0t,pephi0t,pepsit,pephis0t, &
      pedphi0t,peddnt,npnt,1,isaext)
  if (isaint > 0) &
      call pla2nm(pinumt,pimast,pin0t,piphi0t,pipsit,piphis0t, &
      pidphi0t,piddnt,npnt,1,isaint)
  !
  !     write mass and number size distributions for all modes.
  !
  open(nuout4,file='OUT4')
  write(nuout4,'(A5)') '&INIT'
  write(nuout4,'(1X)')
  write(nuout4,'(A8)') 'SDIMOD=2'
  write(nuout4,'(1X)')
  if (kext > 0) then
    do np=1,npnt
      if (np < 10) then
        cfnx='I1'
      else if (np < 100) then
        cfnx='I2'
      else
        cfnx='I3'
      end if
      do kx=1,kext
        do is=1,aextf%tp(kx)%isec
          if (kx < 10) then
            cfkx='I1'
          else if (kx < 100) then
            cfkx='I2'
          else
            cfkx='I3'
          end if
          if (is < 10) then
            cfis='I1'
          else if (is < 100) then
            cfis='I2'
          else
            cfis='I3'
          end if
          write(nuout4, &
               '(A5,'//cfnx//',A5,'//cfkx//',A6,'//cfis//',A2,E15.9)') &
               'BEXT(',np,')%TP(',kx,')%NUM(',is,')=',penumt(np,1,is)
          write(nuout4, &
               '(A5,'//cfnx//',A5,'//cfkx//',A6,'//cfis//',A2,E15.9)') &
               'BEXT(',np,')%TP(',kx,')%MAS(',is,')=',pemast(np,1,is)
          write(nuout4,'(1X)')
        end do
      end do
    end do
  end if
  if (kint > 0) then
    do np=1,npnt
      if (np < 10) then
        cfnx='I1'
      else if (np < 100) then
        cfnx='I2'
      else
        cfnx='I3'
      end if
      do is=1,isaint
        if (is < 10) then
          cfis='I1'
        else if (is < 100) then
          cfis='I2'
        else
          cfis='I3'
        end if
        write(nuout4, &
               '(A5,'//cfnx//',A6,'//cfis//',A2,E15.9)') &
               'BINT(',np,')%NUM(',is,')=',pinumt(np,1,is)
        do kx=1,kint
          if (kx < 10) then
            cfkx='I1'
          else if (kx < 100) then
            cfkx='I2'
          else
            cfkx='I3'
          end if
          write(nuout4, &
               '(A5,'//cfnx//',A5,'//cfkx//',A6,'//cfis//',A2,E15.9)') &
               'BINT(',np,')%TP(',kx,')%MAS(',is,')=', &
                  pimast(np,1,is)*pifrct(np,1,is,kx)
        end do
        write(nuout4,'(1X)')
      end do
    end do
  end if
  write(nuout4,'(A1)') '/'
  close(nuout4)
  !
  !-----------------------------------------------------------------------
  !     width of diagnostic sections. the size range covered
  !     by the diagnostic size distribution spans the range from
  !     the smallest to the largest simulated particle size for
  !     internally and externally mixed aerosol.
  !
  fphis= ylarge
  fphie=-ylarge
  if (isaint > 0) then
    fphis=log(aintf%radb/r0)
    fphie=fphis+isaint*aintf%dpstar
  end if
  if (isaext > 0) then
    do kx=1,kext
      fphit=log(aextf%tp(kx)%radb/r0)
      fphis=min(fphit,fphis)
      fphie=max(fphit+real(aextf%tp(kx)%isec)*aextf%tp(kx)%dpstar, &
                                                                 fphie)
    end do
  end if
  fdphi0s=(fphie-fphis)/real(isdbin)
  fdphi0=fdphi0s
  !
  !     get particle sizes corresponding to section boundaries.
  !
  fphis0(1)=fphis
  do is=2,isdbin
    fphis0(is)=fphis0(1)+real(is-1)*fdphi0(is-1)
  end do
  !
  !     get diagnostic size distribution for input data.
  !
  if (kext > 0) then
    penumr=0.
    pemasr=0.
    do im=1,imodee
      do is=1,isaext
        do isd=1,isdbin-1
          fph1=pephiss(is)
          fph2=pephiss(is)+pedphis(is)
          fph=0.5*(fphis0(isd)+fphis0(isd+1))
          if (fph >= fph1 .and. fph <= fph2) then
            tmpsdn=pen0(:,im,is)*exp(-pepsi(:,im,is)*(fph &
                                                  -pephi0(:,im,is))**2)
            penumr(:,isd)=penumr(:,isd)+tmpsdn
            pemasr(:,isd)=pemasr(:,isd) &
                         +ycnst*peddn(:,im,is)*tmpsdn*(r0*exp(fph))**3
          end if
        end do
      end do
    end do
  end if
  if (kint > 0) then
    pinumr=0.
    pimasr=0.
    do im=1,imodei
      do is=1,isaint
        do isd=1,isdbin-1
          fph1=piphiss(is)
          fph2=piphiss(is)+pidphis(is)
          fph=0.5*(fphis0(isd)+fphis0(isd+1))
          if (fph >= fph1 .and. fph <= fph2) then
            tmpsdn=pin0(:,im,is)*exp(-pipsi(:,im,is)*(fph &
                                                  -piphi0(:,im,is))**2)
            pinumr(:,isd)=pinumr(:,isd)+tmpsdn
            pimasr(:,isd)=pimasr(:,isd) &
                         +ycnst*piddn(:,im,is)*tmpsdn*(r0*exp(fph))**3
          end if
        end do
      end do
    end do
  end if
  !
  !     get diagnostic size distribution for model data.
  !
  if (kext > 0) then
    penumd=0.
    pemasd=0.
    do is=1,isaext
      do isd=1,isdbin-1
        fph1=pephiss(is)
        fph2=pephiss(is)+pedphis(is)
        fph=0.5*(fphis0(isd)+fphis0(isd+1))
        if (fph >= fph1 .and. fph <= fph2 &
            .and. abs(pepsit(1,1,is)-yna) > ysmall) then
          tmpsdn=pen0t(:,1,is)*exp(-pepsit(:,1,is)*(fph &
                                                  -pephi0t(:,1,is))**2)
          penumd(:,isd)=penumd(:,isd)+tmpsdn
          pemasd(:,isd)=pemasd(:,isd) &
                         +ycnst*peddnt(:,1,is)*tmpsdn*(r0*exp(fph))**3
        end if
      end do
    end do
  end if
  if (kint > 0) then
    pinumd=0.
    pimasd=0.
    do is=1,isaint
      do isd=1,isdbin-1
        fph1=piphiss(is)
        fph2=piphiss(is)+pidphis(is)
        fph=0.5*(fphis0(isd)+fphis0(isd+1))
        if (fph >= fph1 .and. fph <= fph2 &
            .and. abs(pipsit(1,1,is)-yna) > ysmall) then
          tmpsdn=pin0t(:,1,is)*exp(-pipsit(:,1,is)*(fph &
                                                  -piphi0t(:,1,is))**2)
          pinumd(:,isd)=pinumd(:,isd)+tmpsdn
          pimasd(:,isd)=pimasd(:,isd) &
                         +ycnst*piddnt(:,1,is)*tmpsdn*(r0*exp(fph))**3
        end if
      end do
    end do
  end if
  !
  !     get rms error.
  !
  if (kext > 0) then
    rmsee=0.
    do isd=1,isdbinm
      rmsee=rmsee+(penumr(1,isd)-penumd(1,isd))**2
    end do
    rmsee=sqrt(rmsee/real(isdbinm))/1.e+06
    print*,'RMS ERR. FOR EXT. NUMBER = ',rmsee
    rmsee=0.
    do isd=1,isdbinm
      rmsee=rmsee+(pemasr(1,isd)-pemasd(1,isd))**2
    end do
    rmsee=sqrt(rmsee/real(isdbinm))*1.e+09
    print*,'RMS ERR. FOR EXT. MASS   = ',rmsee
  end if
  if (kint > 0) then
    rmsei=0.
    do isd=1,isdbinm
      rmsei=rmsei+(pinumr(1,isd)-pinumd(1,isd))**2
    end do
    rmsei=sqrt(rmsei/real(isdbinm))/1.e+06
    print*,'RMS ERR. FOR INT. NUMBER = ',rmsei
    rmsei=0.
    do isd=1,isdbinm
      rmsei=rmsei+(pimasr(1,isd)-pimasd(1,isd))**2
    end do
    rmsei=sqrt(rmsei/real(isdbinm))*1.e+09
    print*,'RMS ERR. FOR INT. MASS   = ',rmsei
  end if
  !
end subroutine geninit
