!>    \file
!>    \brief Aerosol concentration
!!
!!    @authors K. von Salzen
!
!-----------------------------------------------------------------------
subroutine trconc (coacnc,cbccnc,cascnc,cmdcnc,csscnc,xrow,rhoa, &
                         iaind,ntrac,ntracp,il1,il2,ilg,ilev,ilevp1,msg)
  !
  use sdparm, only : iexbc,iexmd,iexoc,iexso4,iexss, &
                         iinbc,iinmd,iinoc,iinso4,iinss, &
                         isextbc,isextmd,isextoc,isextso4,isextss, &
                         isintbc,isintmd,isintoc,isintso4,isintss, &
                         kintbc,kintmd,kintoc,kintso4,kintss,sextf,sintf
  !
  implicit none
  !
  integer, intent(in), dimension(ntracp) :: iaind !< Aerosol tracer index array
  real, intent(in), dimension(ilg,ilev) :: rhoa !< Air density \f$[kg/m^3]\f$
  real, intent(in), dimension(ilg,ilevp1,ntrac) :: xrow !< Grid-cell mean tracer mass mixing ratio \f$[kg/kg]\f$
  real, intent(out), dimension(ilg,ilev) :: coacnc !< Mass concentration of organic aerosol in air \f$[kg/m^3]\f$
  real, intent(out), dimension(ilg,ilev) :: cbccnc !< Mass concentration of black carbon in air \f$[kg/m^3]\f$
  real, intent(out), dimension(ilg,ilev) :: cascnc !< Mass concentration of ammonium sulphate in air \f$[kg/m^3]\f$
  real, intent(out), dimension(ilg,ilev) :: cmdcnc !< Mass concentration of mineral dust in air \f$[kg/m^3]\f$
  real, intent(out), dimension(ilg,ilev) :: csscnc !< Mass concentration of sea salt in air \f$[kg/m^3]\f$
  integer, intent(in) :: il1 !< Start index of first horizontal grid cell
  integer, intent(in) :: il2 !< Index of last horizontal grid cell
  integer, intent(in) :: ilg !< Horizontal (lat/lon) grid cell index
  integer, intent(in) :: ilev !< Number of vertical grid cells
  integer, intent(in) :: ilevp1 !< Number of vertical grid cells
  integer, intent(in) :: msg !< Array dimension for PLA aerosol calculations
  integer, intent(in) :: ntrac !< Number of tracers
  integer, intent(in) :: ntracp !< Number of aerosol tracers
  !
  !     internal work variables
  !
  integer :: is !<
  integer :: il !<
  integer :: l !<
  integer :: ist !<
  integer :: kx !<
  integer :: ioff !<
  integer :: isx !<
  integer :: imlex !<
  integer :: imlin !<

  !
  !     * initialization.
  !
  ioff=ilevp1-ilev
  coacnc=0.
  cbccnc=0.
  cascnc=0.
  cmdcnc=0.
  csscnc=0.
  !
  !     organic carbon concentration.
  !
  if (isextoc > 0) then
    do isx=1,isextoc
      is=iexoc(isx)
      imlex=iaind(sextf%isaer(is)%ism)
      do il=il1,il2
        do l=msg+1,ilev
          coacnc(il,l)=coacnc(il,l)+xrow(il,l+ioff,imlex)
        end do
      end do
    end do
  end if
  if (isintoc > 0) then
    do isx=1,isintoc
      is=iinoc(isx)
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintoc) then
          imlin=iaind(sintf%isaer(is)%ism(ist))
          do il=il1,il2
            do l=msg+1,ilev
              coacnc(il,l)=coacnc(il,l)+xrow(il,l+ioff,imlin)
            end do
          end do
        end if
      end do
    end do
  end if
  !
  !     black carbon concentration.
  !
  if (isextbc > 0) then
    do isx=1,isextbc
      is=iexbc(isx)
      imlex=iaind(sextf%isaer(is)%ism)
      do il=il1,il2
        do l=msg+1,ilev
          cbccnc(il,l)=cbccnc(il,l)+xrow(il,l+ioff,imlex)
        end do
      end do
    end do
  end if
  if (isintbc > 0) then
    do isx=1,isintbc
      is=iinbc(isx)
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintbc) then
          imlin=iaind(sintf%isaer(is)%ism(ist))
          do il=il1,il2
            do l=msg+1,ilev
              cbccnc(il,l)=cbccnc(il,l)+xrow(il,l+ioff,imlin)
            end do
          end do
        end if
      end do
    end do
  end if
  !
  !     sulphate concentration.
  !
  if (isextso4 > 0) then
    do isx=1,isextso4
      is=iexso4(isx)
      imlex=iaind(sextf%isaer(is)%ism)
      do il=il1,il2
        do l=msg+1,ilev
          cascnc(il,l)=cascnc(il,l)+xrow(il,l+ioff,imlex)
        end do
      end do
    end do
  end if
  if (isintso4 > 0) then
    do isx=1,isintso4
      is=iinso4(isx)
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintso4) then
          imlin=iaind(sintf%isaer(is)%ism(ist))
          do il=il1,il2
            do l=msg+1,ilev
              cascnc(il,l)=cascnc(il,l)+xrow(il,l+ioff,imlin)
            end do
          end do
        end if
      end do
    end do
  end if
  !
  !     mineral dust concentration.
  !
  if (isextmd > 0) then
    do isx=1,isextmd
      is=iexmd(isx)
      imlex=iaind(sextf%isaer(is)%ism)
      do il=il1,il2
        do l=msg+1,ilev
          cmdcnc(il,l)=cmdcnc(il,l)+xrow(il,l+ioff,imlex)
        end do
      end do
    end do
  end if
  if (isintmd > 0) then
    do isx=1,isintmd
      is=iinmd(isx)
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintmd) then
          imlin=iaind(sintf%isaer(is)%ism(ist))
          do il=il1,il2
            do l=msg+1,ilev
              cmdcnc(il,l)=cmdcnc(il,l)+xrow(il,l+ioff,imlin)
            end do
          end do
        end if
      end do
    end do
  end if
  !
  !     sea salt concentration.
  !
  if (isextss > 0) then
    do isx=1,isextss
      is=iexss(isx)
      imlex=iaind(sextf%isaer(is)%ism)
      do il=il1,il2
        do l=msg+1,ilev
          csscnc(il,l)=csscnc(il,l)+xrow(il,l+ioff,imlex)
        end do
      end do
    end do
  end if
  if (isintss > 0) then
    do isx=1,isintss
      is=iinss(isx)
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintss) then
          imlin=iaind(sintf%isaer(is)%ism(ist))
          do il=il1,il2
            do l=msg+1,ilev
              csscnc(il,l)=csscnc(il,l)+xrow(il,l+ioff,imlin)
            end do
          end do
        end if
      end do
    end do
  end if
  !
  !     convert units from mass mixing ratios to concentrations.
  !
  coacnc=coacnc*rhoa
  cbccnc=cbccnc*rhoa
  cascnc=cascnc*rhoa
  cmdcnc=cmdcnc*rhoa
  csscnc=csscnc*rhoa
  !
end subroutine trconc
