!> \file
!> \brief Aerosol number concentration and burden
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine trnum (cssn,cmdn,cinn,vssn,vmdn,vinn,xrow,rhoa, &
                        iaind,dshj,pressg,grav,ntrac,ntracp, &
                        il1,il2,ilg,ilev,ilevp1,msg)
  !
  use sdparm, only : iexmd,iexss,infin,insin,isaint,isextmd,isextss,sextf
  !
  implicit none
  !
  integer, intent(in), dimension(ntracp) :: iaind !< Aerosol tracer index array
  real, intent(in), dimension(ilg) :: pressg !< Surface pressure
  real, intent(in), dimension(ilg,ilev) :: dshj !< Vertical depth of the grid cells (hybrid variable)
  real, intent(in), dimension(ilg,ilev) :: rhoa !< Air density \f$[kg/m^3]\f$
  real, intent(in), dimension(ilg,ilevp1,ntrac) :: xrow !< Grid-cell mean tracer mass mixing ratio \f$[kg/kg]\f$
  real, intent(out), dimension(ilg,ilev) :: cmdn !< Number concentration of mineral dust aerosol in air \f$[cm^{-3}]\f$
  real, intent(out), dimension(ilg,ilev) :: cssn !< Number concentration of sea salt aerosol in air \f$[cm^{-3}]\f$
  real, intent(out), dimension(ilg,ilev) :: cinn !< Number concentration of internally mixed aerosol in air \f$[cm^{-3}]\f$
  real, intent(out), dimension(ilg) :: vmdn !< Number load mineral dust \f$[m^{-2}]\f$
  real, intent(out), dimension(ilg) :: vssn !< Number load sea salt \f$[m^{-2}]\f$
  real, intent(out), dimension(ilg) :: vinn !< Number load internally mixed aerosol \f$[m^{-2}]\f$
  real, intent(in) :: grav !<  Gravtiational acceleration \f$[m/s^2]\f$
  integer, intent(in) :: il1 !< Start index of first horizontal grid cell
  integer, intent(in) :: il2 !< Index of last horizontal grid cell
  integer, intent(in) :: ilg !< Horizontal (lat/lon) grid cell index
  integer, intent(in) :: ilev !< Number of vertical grid cells
  integer, intent(in) :: ilevp1 !< Number of vertical grid cells
  integer, intent(in) :: msg !< Array dimension for PLA aerosol calculations
  integer, intent(in) :: ntrac !< Number of tracers
  integer, intent(in) :: ntracp !< Number of aerosol tracers
  !
  !     internal work variables
  !
  integer :: is !<
  integer :: il !<
  integer :: l !<
  integer :: ioff !<
  integer :: isx !<
  integer :: inlex !<
  integer :: inlins !<
  integer :: inlinf !<
  !
  !     * initialization.
  !
  ioff=ilevp1-ilev
  cmdn=0.
  cssn=0.
  cinn=0.
  vmdn=0.
  vssn=0.
  vinn=0.
  !
  !     externally mixed sea salt number concentration and burden.
  !
  if (isextss > 0) then
    do isx=1,isextss
      is=iexss(isx)
      inlex=iaind(sextf%isaer(is)%isn)
      do il=il1,il2
        do l=msg+1,ilev
          cssn(il,l)=cssn(il,l)+xrow(il,l+ioff,inlex)
          vssn(il)=vssn(il)+xrow(il,l+ioff,inlex) &
                             *pressg(il)*dshj(il,l)/grav
        end do
      end do
    end do
  end if
  !
  !     externally mixed mineral dust number concentration and burden.
  !
  if (isextmd > 0) then
    do isx=1,isextmd
      is=iexmd(isx)
      inlex=iaind(sextf%isaer(is)%isn)
      do il=il1,il2
        do l=msg+1,ilev
          cmdn(il,l)=cmdn(il,l)+xrow(il,l+ioff,inlex)
          vmdn(il)=vmdn(il)+xrow(il,l+ioff,inlex) &
                             *pressg(il)*dshj(il,l)/grav
        end do
      end do
    end do
  end if
  !
  !     internally mixed aerosol number concentration and burden.
  !
  if (isaint > 0) then
    inlins=iaind(insin)
    inlinf=iaind(infin)
    do is=inlins,inlinf
      do il=il1,il2
        do l=msg+1,ilev
          cinn(il,l)=cinn(il,l)+xrow(il,l+ioff,is)
          vinn(il)=vinn(il)+xrow(il,l+ioff,is) &
                                 *pressg(il)*dshj(il,l)/grav
        end do
      end do
    end do
  end if
  !
  !     convert units from mass mixing ratios to concentrations per cm3.
  !
  cmdn=cmdn*rhoa*1.e-06
  cssn=cssn*rhoa*1.e-06
  cinn=cinn*rhoa*1.e-06
  !
end subroutine trnum
