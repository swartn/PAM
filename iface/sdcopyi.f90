!> \file
!> \brief Copy size distribution information from input into corresponding
!>        PLA arrays.
!!
!!  @author Knut von Salzen
!
!-----------------------------------------------------------------------
subroutine sdcopyi(pinum,pimas,pin0,piphi0,pipsi,pifrc, &
                         indh,indv,nsampl,ns0,ilga,leva)
  !
  use sdi
  use sdparm
  !
  implicit none
  !
  real, intent(out), dimension(ilga,leva,isaint) :: pinum !< Aerosol number concentration for internally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(out), dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(out), dimension(ilga,leva,isaint) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,
  !< amplitude) int. mixture
  real, intent(out), dimension(ilga,leva,isaint) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,
  !< width), int. mixture
  real, intent(out), dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$,
  !< mode size) int. mixture
  real, intent(out), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type
  !< for internally mixed aerosol
  integer, intent(in), dimension(ns0:nsampl) :: indh !<
  integer, intent(in), dimension(ns0:nsampl) :: indv !<
  integer, intent(in)  :: ns0 !< First index to be selected
  integer, intent(in)  :: nsampl !< Number of points in the file
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, allocatable, dimension(:) :: frc !< Fraction
  !
  !      * internal work fields.
  !
  integer :: nx !<
  integer :: il !<
  integer :: l !<
  integer :: is !<
  integer :: isi !<
  integer :: kx !<
  !
  !-----------------------------------------------------------------------
  !     initialize work and io arrays and allocate memory.
  !
  if (isaint > 0) then
    allocate(frc(aintf%isec))
    pifrc=0.
  end if
  !
  !-----------------------------------------------------------------------
  !     copy results for externally mixed aerosol into corresponding
  !     pla arrays.
  !
  if (isaint > 0) then
    pin0  =yna
    piphi0=yna
    pipsi =yna
    pinum =yna
    pimas =yna
  end if
  do nx=ns0,nsampl
    il=indh(nx)
    l =indv(nx)
    if (l /= ina) then
      if (sdimod == 1) then
        if (isaint > 0) then
          do isi=1,isaint
            is=sintf%isaer(isi)%isi
            pin0  (il,l,isi)=cint(nx)%pn0  (is)
            piphi0(il,l,isi)=cint(nx)%pphi0(is)
            pipsi (il,l,isi)=cint(nx)%ppsi (is)
          end do
        end if
      else if (sdimod == 2) then
        if (isaint > 0) then
          do isi=1,isaint
            is=sintf%isaer(isi)%isi
            pinum(il,l,isi)=cint(nx)%num(is)
          end do
          pimas(il,l,:)=0.
          do kx=1,kint
            do is=aintf%tp(kx)%iscb,aintf%tp(kx)%isce
              pimas(il,l,is)=pimas(il,l,is)+cint(nx)%tp(kx)%mas(is)
            end do
          end do
        end if
      end if
    else
      if (sdimod == 1) then
        if (isaint > 0) then
          do isi=1,isaint
            is=sintf%isaer(isi)%isi
            pin0  (il,:,isi)=cint(nx)%pn0  (is)
            piphi0(il,:,isi)=cint(nx)%pphi0(is)
            pipsi (il,:,isi)=cint(nx)%ppsi (is)
          end do
        end if
      else if (sdimod == 2) then
        if (isaint > 0) then
          do isi=1,isaint
            is=sintf%isaer(isi)%isi
            pinum(il,:,isi)=cint(nx)%num(is)
          end do
          pimas(il,:,:)=0.
          do kx=1,kint
            do is=aintf%tp(kx)%iscb,aintf%tp(kx)%isce
              pimas(il,:,is)=pimas(il,:,is)+cint(nx)%tp(kx)%mas(is)
            end do
          end do
        end if
      end if
    end if
  end do
  !
  !-----------------------------------------------------------------------
  !     mass fraction of each externally mixed aerosol type relative
  !     to the total mass of the externally mixed aerosol.
  !
  do nx=ns0,nsampl
    il=indh(nx)
    l =indv(nx)
    if (l /= ina) then
      if (sdimod == 1) then
        do kx=1,kint
          do is=aintf%tp(kx)%iscb,aintf%tp(kx)%isce
            pifrc(il,l,is,kx)=cint(nx)%tp(kx)%conc(is)
          end do
        end do
      else if (sdimod == 2) then
        do kx=1,kint
          do is=aintf%tp(kx)%iscb,aintf%tp(kx)%isce
            pifrc(il,l,is,kx)=cint(nx)%tp(kx)%mas(is)
          end do
        end do
      end if
      if (isaint > 0) then
        frc=sum(pifrc(il,l,:,:),dim=2)
        do kx=1,kint
          do is=aintf%tp(kx)%iscb,aintf%tp(kx)%isce
            if (frc(is) > 0. ) then
              pifrc(il,l,is,kx)=pifrc(il,l,is,kx)/frc(is)
            else
              pifrc(il,l,is,kx)=0.
            end if
          end do
        end do
      end if
    else
      if (sdimod == 1) then
        do kx=1,kint
          do is=aintf%tp(kx)%iscb,aintf%tp(kx)%isce
            pifrc(il,:,is,kx)=cint(nx)%tp(kx)%conc(is)
          end do
        end do
      else if (sdimod == 2) then
        do kx=1,kint
          do is=aintf%tp(kx)%iscb,aintf%tp(kx)%isce
            pifrc(il,:,is,kx)=cint(nx)%tp(kx)%mas(is)
          end do
        end do
      end if
      if (isaint > 0) then
        frc=sum(pifrc(il,leva,:,:),dim=2)
        do kx=1,kint
          do is=aintf%tp(kx)%iscb,aintf%tp(kx)%isce
            if (frc(is) > 0. ) then
              pifrc(il,:,is,kx)=pifrc(il,:,is,kx)/frc(is)
            else
              pifrc(il,:,is,kx)=0.
            end if
          end do
        end do
      end if
    end if
  end do
  !
  !-----------------------------------------------------------------------
  !     * deallocate work array.
  !
  if (isaint > 0) then
    deallocate(frc)
  end if
  !
end subroutine sdcopyi
