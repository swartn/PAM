!> \file
!> \brief Initialization of parameters for droplet growth calculations
!>       based on tabulated data. Also initializes diagnostic parameters
!>       for subroutines that calculate droplet growth.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine cnconf(nuact)
  !
  use cnevdt,  only : bs1n,bs1p,bs2n,bs2p,bs3p, &
                          ib1n,ib1p,ib2n,ib2p,ib3p, &
                          ir1n,ir1p,ir2n,ir2p,ir3p, &
                          tp1n,tp1p,tp2n,tp2p,tp3p, &
                          xp1n,xp1p,xp2n,xp2p,xp3p, &
                          bsc,ird,rat,yndef
  use cnparm,  only : bscp,idef,ina,irdp,ireg,ratp,spp,ylarge,yna
  use cnparmt, only : rgpno
  !
  implicit none
  !
  integer, intent(in) :: nuact !< Input file number
  !
  !     internal work variables
  !
  integer, dimension(ireg) :: ibtm !<
  integer, dimension(ireg) :: ibtt !<
  integer, dimension(3,ird) :: ipl !<
  integer, dimension(3,ird) :: ipu !<
  character(len=15) :: cform !<
  integer :: ilin !<
  integer :: irat !<
  integer :: ib !<
  integer :: ir !<
  integer :: ibts !<
  integer :: irg !<
  integer :: irts !<
  integer :: istatus !<
  real :: dudx !<
  real :: adst !<
  real :: adstt !<
  !
  !-----------------------------------------------------------------------
  !     * mathematical constants.
  !
  ylarge=0.1*huge(1.)
  ina=nint(yna)
  !
  !     * read in tabulated data.
  !
  if (nuact>0) then

    do ilin=1,4
      read(nuact,'(1X)')
    end do
    if (ird < 100) then
      write(cform,'(A1,I2,A12)') '(',ird,'(1X,E12.6)) '
    else
      write(cform,'(A1,I3,A11)') '(',ird,'(1X,E12.6))'
    end if
    !
    !     * chemical parameter ratios and supersaturation parameter.
    !
    read(nuact,cform) (rat(irat),irat=1,ird)
    read(nuact,cform) (bsc(irat),irat=1,ird)
    !
    !     * data for positive supersaturation regmie, region 1.
    !
    do ib=1,ib1p
      read(nuact,cform) (bs1p(ib,irat),irat=1,ird)
    end do
    do ib=1,ib1p
      do ir=1,ir1p
        read(nuact,cform) (xp1p(ir,ib,irat),irat=1,ird)
      end do
    end do
    do ib=1,ib1p
      do ir=1,ir1p
        read(nuact,cform) (tp1p(ir,ib,irat),irat=1,ird)
      end do
    end do
    !
    !     * data for positive supersaturation regmie, region 2.
    !
    do ib=1,ib2p
      read(nuact,cform) (bs2p(ib,irat),irat=1,ird)
    end do
    do ib=1,ib2p
      do ir=1,ir2p
        read(nuact,cform) (xp2p(ir,ib,irat),irat=1,ird)
      end do
    end do
    do ib=1,ib2p
      do ir=1,ir2p
        read(nuact,cform) (tp2p(ir,ib,irat),irat=1,ird)
      end do
    end do
    !
    !     * data for positive supersaturation regmie, region 3.
    !
    do ib=1,ib3p
      read(nuact,cform) (bs3p(ib,irat),irat=1,ird)
    end do
    do ib=1,ib3p
      do ir=1,ir3p
        read(nuact,cform) (xp3p(ir,ib,irat),irat=1,ird)
      end do
    end do
    do ib=1,ib3p
      do ir=1,ir3p
        read(nuact,cform) (tp3p(ir,ib,irat),irat=1,ird)
      end do
    end do
    !
    !     * data for negative supersaturation regmie, region 1.
    !
    do ib=1,ib1n
      read(nuact,cform) (bs1n(ib,irat),irat=1,ird)
    end do
    do ib=1,ib1n
      do ir=1,ir1n
        read(nuact,cform) (xp1n(ir,ib,irat),irat=1,ird)
      end do
    end do
    do ib=1,ib1n
      do ir=1,ir1n
        read(nuact,cform) (tp1n(ir,ib,irat),irat=1,ird)
      end do
    end do
    !
    !     * data for negative supersaturation regmie, region 2.
    !
    do ib=1,ib2n
      read(nuact,cform) (bs2n(ib,irat),irat=1,ird)
    end do
    do ib=1,ib2n
      do ir=1,ir2n
        read(nuact,cform) (xp2n(ir,ib,irat),irat=1,ird)
      end do
    end do
    do ib=1,ib2n
      do ir=1,ir2n
        read(nuact,cform) (tp2n(ir,ib,irat),irat=1,ird)
      end do
    end do

  end if

#ifdef GM_PAM
      ! broadcast read variables to all processes
      call rpn_comm_bcast(rat ,       ird      , "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(bsc ,       ird      , "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(bs1p, (ib1p*ird)     , "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(xp1p, (ir1p*ib1p*ird), "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(tp1p, (ir1p*ib1p*ird), "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(bs2p, (ib2p*ird)     , "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(xp2p, (ir2p*ib2p*ird), "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(tp2p, (ir2p*ib2p*ird), "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(bs3p, (ib3p*ird)     , "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(xp3p, (ir3p*ib3p*ird), "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(tp3p, (ir3p*ib3p*ird), "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(bs1n, (ib1n*ird)     , "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(xp1n, (ir1n*ib1n*ird), "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(tp1n, (ir1n*ib1n*ird), "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(bs2n, (ib2n*ird)     , "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(xp2n, (ir2n*ib2n*ird), "MPI_REAL", 0, "GRID", istatus)
      call rpn_comm_bcast(tp2n, (ir2n*ib2n*ird), "MPI_REAL", 0, "GRID", istatus)
#endif
  !
  !-----------------------------------------------------------------------
  !     * allocate arrays and copy tabulated data into new arrays.
  !
  irdp=ird
  if (allocated(ratp) ) deallocate(ratp)
  allocate (ratp(irdp))
  ratp=rat
  if (allocated(bscp) ) deallocate(bscp)
  allocate (bscp(irdp))
  bscp=bsc
  if (allocated(rgpno) ) deallocate(rgpno)
  allocate (rgpno(ireg))
  rgpno(1)%ibt=ib1p+1
  rgpno(2)%ibt=ib2p
  rgpno(3)%ibt=ib3p+1
  rgpno(4)%ibt=ib1n
  rgpno(5)%ibt=ib2n
  rgpno(1)%irt=ir1p
  rgpno(2)%irt=ir2p
  rgpno(3)%irt=ir3p
  rgpno(4)%irt=ir1n
  rgpno(5)%irt=ir2n
  do irg=1,ireg
    if (allocated(rgpno(irg)%bs) ) deallocate(rgpno(irg)%bs)
    allocate (rgpno(irg)%bs(rgpno(irg)%ibt,ird))
    if (allocated(rgpno(irg)%xp) ) deallocate(rgpno(irg)%xp)
    allocate (rgpno(irg)%xp(rgpno(irg)%irt,rgpno(irg)%ibt,ird))
    if (allocated(rgpno(irg)%tp) ) deallocate(rgpno(irg)%tp)
    allocate (rgpno(irg)%tp(rgpno(irg)%irt,rgpno(irg)%ibt,ird))
  end do
  rgpno(2)%bs=bs2p
  rgpno(4)%bs=bs1n
  rgpno(5)%bs=bs2n
  rgpno(2)%xp=xp2p
  rgpno(4)%xp=xp1n
  rgpno(5)%xp=xp2n
  rgpno(2)%tp=tp2p
  rgpno(4)%tp=tp1n
  rgpno(5)%tp=tp2n
  do irat=1,ird
    do ib=1,rgpno(1)%ibt-1
      rgpno(1)%bs(ib+1,irat)=bs1p(ib,irat)
      do ir=1,rgpno(1)%irt
        rgpno(1)%xp(ir,ib+1,irat)=xp1p(ir,ib,irat)
        rgpno(1)%tp(ir,ib+1,irat)=tp1p(ir,ib,irat)
      end do
    end do
    do ib=1,rgpno(3)%ibt-1
      rgpno(3)%bs(ib+1,irat)=bs3p(ib,irat)
      do ir=1,rgpno(3)%irt
        rgpno(3)%xp(ir,ib+1,irat)=xp3p(ir,ib,irat)
        rgpno(3)%tp(ir,ib+1,irat)=tp3p(ir,ib,irat)
      end do
    end do
  end do
  do irg=1,ireg
    ibtt(irg)=rgpno(irg)%ibt
    ibtm(irg)=ibtt(irg)-1
  end do
  !
  !     * add a row for analytical solution for the limiting
  !     * case with supersaturation >> particle saturation. the addition
  !     * is only done for growth regimes 1 and 3.
  !
  do irat=1,ird
    !
    !       * determine range of results for adjacent row according to
    !       * the tabulated data.
    !
    ipl(1,irat)=idef
    do ir=1,rgpno(1)%irt
      if (nint(rgpno(1)%tp(ir,2,irat)-yndef) /= 0 &
          .and. ipl(1,irat) == idef) then
        ipl(1,irat)=ir
      end if
    end do
    ipu(1,irat)=idef
    do ir=rgpno(1)%irt,1,-1
      if (nint(rgpno(1)%tp(ir,2,irat)-yndef) /= 0 &
          .and. ipu(1,irat) == idef) then
        ipu(1,irat)=ir
      end if
    end do
    ipl(3,irat)=idef
    do ir=1,rgpno(3)%irt
      if (nint(rgpno(3)%tp(ir,2,irat)-yndef) /= 0 &
          .and. ipl(3,irat) == idef) then
        ipl(3,irat)=ir
      end if
    end do
    ipu(3,irat)=idef
    do ir=rgpno(3)%irt,1,-1
      if (nint(rgpno(3)%tp(ir,2,irat)-yndef) /= 0 &
          .and. ipu(3,irat) == idef) then
        ipu(3,irat)=ir
      end if
    end do
  end do
  dudx=1.
  do irg=1,3,2
    do irat=1,ird
      !
      !       * bs term (normalized b-term in koehler equation).
      !
      rgpno(irg)%bs(1,irat)=0.
      !
      !       * normalized particle sizes.
      !
      do ir=1,rgpno(irg)%irt
        rgpno(irg)%xp(ir,1,irat)=rgpno(irg)%xp(ir,2,irat)
      end do
      !
      !       * initialize normalized growth times.
      !
      rgpno(irg)%tp(1:rgpno(irg)%irt,1,irat)=yndef
      !
      !       * calculate normalized growth times.
      !
      if (ipl(irg,irat) /= idef .and. ipu(irg,irat) /= idef) then
        rgpno(irg)%tp(ipl(irg,irat),1,irat)=0.
        do ir=ipl(irg,irat)+1,ipu(irg,irat)
          rgpno(irg)%tp(ir,1,irat)=rgpno(irg)%tp(ir-1,1,irat) &
                                       +dudx*(rgpno(irg)%xp(ir,1,irat) &
                                          -rgpno(irg)%xp(ir-1,1,irat))
        end do
      end if
    end do
  end do
  !
  !     * add an extra point to represent the critical size in the data for
  !     * growth region 1. this is necessary because region 1 overlaps with
  !     * region 3 for supersaturations that are greater than the critical
  !     * supersaturation. therefore, the interpolation between points has a
  !     * discontinuity at the critical supersaturation (from input data).
  !
  if (allocated(spp) ) deallocate(spp)
  allocate(spp(ird))
  spp%xp=yndef
  spp%tp=yndef
  spp%bs=yndef
  do irat=1,ird
    spp(irat)%xp=3.*ratp(irat)
    spp(irat)%bs=bscp(irat)
    ibts=idef
    !
    !       * find matching bs-value in table for critical bs-value.
    !
    do ib=1,rgpno(1)%ibt-1
      if (spp(irat)%bs>=rgpno(1)%bs(ib,irat) ) then
        ibts=ib
      end if
    end do
    !
    !       * there is no discontinuity if the critical point is outside
    !       * the domain of the input data.
    !
    if (spp(irat)%bs < rgpno(1)%bs(1,irat) ) ibts=idef
    !
    !       * now find the xp and tp values for that point. first, xp
    !       * is taken to be that point that is the closest to the critical
    !       * point. tp is simply the value of the scaled time at that
    !       * point.
    !
    if (ibts /= idef) then
      irts=idef
      adst=ylarge
      do ir=1,rgpno(1)%irt
        adstt=abs(spp(irat)%xp-rgpno(1)%xp(ir,ibts,irat))
        if (adstt < adst) then
          adst=adstt
          irts=ir
        end if
      end do
      !
      !         * there is no discontinuity if the critical point is outside
      !         * the domain of the input data or if there is no subsequent
      !         * point with larger scaled particle sizes for the same bs
      !         * value.
      !
      if (spp(irat)%xp < rgpno(1)%xp(1           ,ibts,irat) &
          .or. spp(irat)%xp > rgpno(1)%xp(rgpno(1)%irt,ibts,irat) &
          .or. irts == rgpno(1)%irt) then
        ibts=idef
        irts=idef
      end if
      if (irts /= idef) then
        spp(irat)%tp=rgpno(1)%tp(irts,ibts,irat)
      end if
    end if
    spp(irat)%ibs=ibts
  end do
  !
  !     * set boundaries for growth regimes.
  !
  call cnbnds (ibtm,ird,yndef)
  !
  !     * interpolation parameters for particle sizes and growth times.
  !
  call cnintp (ibtt,ibtm,ird,yndef)
  !
  !     * deallocation of work arrays.
  !
  deallocate (rgpno)
  !
end subroutine cnconf
!> \file
!! \subsection ssec_details Details
!! This subroutine needs to be called after scconf.
