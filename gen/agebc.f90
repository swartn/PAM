!> \file
!> \brief Aging of externally mixed, hydrophobic black carbon.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine agebc(pedndt,pedmdt,pidndt,pidmdt,pidfdt,peddn,pen0, &
                       pephi0,pepsi,pifrc,pimas,ataus,dt,ilga,leva)
  !
  use sdparm, only : iexbc,iexoc,iinbc,iinoc,isaext,isaint,isextbc, &
                         isextoc,isintbc,isintoc,kext,kextbc,kextoc, &
                         kint,kintbc,kintoc,pedphis,pephiss,pidphis,piphiss,ycnst,yna,ytiny
  use sdcode, only : sdintb, sdintb0
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in) :: dt !< Model time step \f$[s]\f$
  real, intent(out), dimension(ilga,leva,isaext) :: pedndt !< Number tendency \f$[kg/kg/sec]\f$, external mixture
  real, intent(out), dimension(ilga,leva,isaext) :: pedmdt !< Mass tendency \f$[kg/kg/sec]\f$, external mixture
  real, intent(out), dimension(ilga,leva,isaint) :: pidndt !< Number tendency \f$[kg/kg/sec]\f$, internal mixture
  real, intent(out), dimension(ilga,leva,isaint) :: pidmdt !< Mass tendency \f$[kg/kg/sec]\f$, internal mixture
  real, intent(out), dimension(ilga,leva,isaint,kint) :: pidfdt !< Aerosol species mass fraction tendency \f$[1/sec]\f$, internal mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,amplitude) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,width), ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: peddn !< Density of dry aerosol particle \f$[kg/m^3]\f$, external mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol species mass fraction
  real, intent(in), dimension(ilga) :: ataus !< Aging time scale of hydrophobic black carbon \f$[s]\f$
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:,:) :: cimast !<
  real, allocatable, dimension(:,:,:) :: cim0bc !<
  real, allocatable, dimension(:,:,:) :: cim3bc !<
  real, allocatable, dimension(:,:,:) :: cem0bc !<
  real, allocatable, dimension(:,:,:) :: cem3bc !<
  real, allocatable, dimension(:,:,:) :: cim0oc !<
  real, allocatable, dimension(:,:,:) :: cim3oc !<
  real, allocatable, dimension(:,:,:) :: cem0oc !<
  real, allocatable, dimension(:,:,:) :: cem3oc !<
  real, allocatable, dimension(:,:,:,:) :: cimaf !<
  real, dimension(ilga) :: afrc !<
  real, dimension(ilga) :: term !<
  integer :: is !<
  integer :: isd !<
  integer :: isdx !<
  integer :: isx !<
  integer :: il !<
  integer :: l !<
  real :: dmom0 !<
  real :: dmom3 !<
  real :: fdphi1 !<
  real :: fph1 !<
  real :: fph2 !<
  real :: fphis1 !<
  real :: fphie1 !<
  real :: fph1d !<
  real :: fph2d !<
  real :: rmom3 !<
  !
  !-----------------------------------------------------------------------
  !     * memory allocation.
  !
  if (kint > 0) then
    allocate(cimast(ilga,leva,isaint))
    allocate(cimaf (ilga,leva,isaint,kint))
  end if
  if (isintbc > 0) then
    allocate(cim0bc(ilga,leva,isintbc))
    allocate(cim3bc(ilga,leva,isintbc))
  end if
  if (isintoc > 0) then
    allocate(cim0oc(ilga,leva,isintoc))
    allocate(cim3oc(ilga,leva,isintoc))
  end if
  if (isextbc > 0) then
    allocate(cem0bc(ilga,leva,isextbc))
    allocate(cem3bc(ilga,leva,isextbc))
  end if
  if (isextoc > 0) then
    allocate(cem0oc(ilga,leva,isextoc))
    allocate(cem3oc(ilga,leva,isextoc))
  end if
  !
  !-----------------------------------------------------------------------
  !     * fraction of aerosol converted by aging per time step.
  !
  term=-dt/ataus
  afrc=exp(term)
  afrc=max(1.-afrc,ytiny)
  !
  !-----------------------------------------------------------------------
  !
  !     * integrate number and mass concentrations for externally
  !     * bc mixed aerosol over size sections of internally mixed
  !     * bc aerosol.
  !
  rmom3=3.
  if (kextbc > 0 .and. kintbc > 0) then
    cim0bc=0.
    cim3bc=0.
    cem0bc=0.
    cem3bc=0.
    do is=1,isextbc
      isx=iexbc(is)
      do isd=1,isintbc
        isdx=iinbc(isd)
        do l=1,leva
          do il=1,ilga
            fph1=pephiss(isx)
            fph2=pephiss(isx)+pedphis(isx)
            fph1d=piphiss(isdx)
            fph2d=piphiss(isdx)+pidphis(isdx)
            if (   ((fph1d >= fph1 .and. fph1d <= fph2) &
                .or. (fph2d >= fph1 .and. fph2d <= fph2)) &
                .and.  abs(pepsi(il,l,isx)-yna) > ytiny &
                .and. pen0(il,l,isx) > ytiny) then
              fphis1=max(fph1d,fph1)
              fphie1=min(fph2d,fph2)
              fdphi1=max(fphie1-fphis1,0.)
              if (fdphi1 > ytiny) then
                dmom0=pen0(il,l,isx)*afrc(il) &
                                      *sdintb0(pephi0(il,l,isx), &
                                         pepsi(il,l,isx),fphis1,fdphi1)
                cim0bc(il,l,isd)=cim0bc(il,l,isd)+dmom0
                cem0bc(il,l,is)=cem0bc(il,l,is)+dmom0
                dmom3=ycnst*peddn(il,l,isx)*afrc(il) &
                               *pen0(il,l,isx)*sdintb(pephi0(il,l,isx), &
                                   pepsi(il,l,isx),rmom3,fphis1,fdphi1)
                cim3bc(il,l,isd)=cim3bc(il,l,isd)+dmom3
                cem3bc(il,l,is)=cem3bc(il,l,is)+dmom3
              end if
            end if
          end do
        end do
      end do
    end do
  end if
  if (kextoc > 0 .and. kintoc > 0) then
    cim0oc=0.
    cim3oc=0.
    cem0oc=0.
    cem3oc=0.
    do is=1,isextoc
      isx=iexoc(is)
      do isd=1,isintoc
        isdx=iinoc(isd)
        do l=1,leva
          do il=1,ilga
            fph1=pephiss(isx)
            fph2=pephiss(isx)+pedphis(isx)
            fph1d=piphiss(isdx)
            fph2d=piphiss(isdx)+pidphis(isdx)
            if (   ((fph1d >= fph1 .and. fph1d <= fph2) &
                .or. (fph2d >= fph1 .and. fph2d <= fph2)) &
                .and.  abs(pepsi(il,l,isx)-yna) > ytiny &
                .and. pen0(il,l,isx) > ytiny) then
              fphis1=max(fph1d,fph1)
              fphie1=min(fph2d,fph2)
              fdphi1=max(fphie1-fphis1,0.)
              if (fdphi1 > ytiny) then
                dmom0=pen0(il,l,isx)*afrc(il) &
                                      *sdintb0(pephi0(il,l,isx), &
                                         pepsi(il,l,isx),fphis1,fdphi1)
                cim0oc(il,l,isd)=cim0oc(il,l,isd)+dmom0
                cem0oc(il,l,is)=cem0oc(il,l,is)+dmom0
                dmom3=ycnst*peddn(il,l,isx)*afrc(il) &
                               *pen0(il,l,isx)*sdintb(pephi0(il,l,isx), &
                                   pepsi(il,l,isx),rmom3,fphis1,fdphi1)
                cim3oc(il,l,isd)=cim3oc(il,l,isd)+dmom3
                cem3oc(il,l,is)=cem3oc(il,l,is)+dmom3
              end if
            end if
          end do
        end do
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * mass and number tendencies for externally mixed aerosol.
  !
  if (kext > 0) then
    pedndt=0.
    pedmdt=0.
  end if
  if (kextbc > 0) then
    do is=1,isextbc
      where (cem0bc(:,:,is) > ytiny .and. cem3bc(:,:,is) > ytiny)
        pedndt(:,:,iexbc(is))=pedndt(:,:,iexbc(is)) &
                                 -cem0bc(:,:,is)/dt
        pedmdt(:,:,iexbc(is))=pedmdt(:,:,iexbc(is)) &
                                 -cem3bc(:,:,is)/dt
      end where
    end do
  end if
  if (kextoc > 0) then
    do is=1,isextoc
      where (cem0oc(:,:,is) > ytiny .and. cem3oc(:,:,is) > ytiny)
        pedndt(:,:,iexoc(is))=pedndt(:,:,iexoc(is)) &
                                 -cem0oc(:,:,is)/dt
        pedmdt(:,:,iexoc(is))=pedmdt(:,:,iexoc(is)) &
                                 -cem3oc(:,:,is)/dt
      end where
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * total aerosol mass after accounting for changes from aging
  !     * for internally mixed types of aerosol.
  !
  if (kint > 0) then
    cimast=pimas
  end if
  if (kintbc > 0) then
    do is=1,isintbc
      where (cim0bc(:,:,is) > ytiny .and. cim3bc(:,:,is) > ytiny)
        cimast(:,:,iinbc(is))=cimast(:,:,iinbc(is))+cim3bc(:,:,is)
      end where
    end do
  end if
  if (kintoc > 0) then
    do is=1,isintoc
      where (cim0oc(:,:,is) > ytiny .and. cim3oc(:,:,is) > ytiny)
        cimast(:,:,iinoc(is))=cimast(:,:,iinoc(is))+cim3oc(:,:,is)
      end where
    end do
  end if
  !
  !     * new mass fractions for internally mixed types of aerosol.
  !
  if (kint > 0) then
    do is=1,isaint
      do l=1,leva
        do il=1,ilga
          if (cimast(il,l,is) > ytiny) then
            cimaf(il,l,is,:)=pifrc(il,l,is,:)*pimas(il,l,is) &
                            /cimast(il,l,is)
          else
            cimaf(il,l,is,:)=pifrc(il,l,is,:)
          end if
        end do
      end do
    end do
  end if
  if (kintbc > 0) then
    do is=1,isintbc
      do l=1,leva
        do il=1,ilga
          if (cimast(il,l,iinbc(is)) > ytiny &
              .and. cim0bc(il,l,is) > ytiny &
              .and. cim3bc(il,l,is) > ytiny) then
            cimaf(il,l,iinbc(is),kintbc)=cimaf(il,l,iinbc(is),kintbc) &
                               +cim3bc(il,l,is)/cimast(il,l,iinbc(is))
          end if
        end do
      end do
    end do
  end if
  if (kintoc > 0) then
    do is=1,isintoc
      do l=1,leva
        do il=1,ilga
          if (cimast(il,l,iinoc(is)) > ytiny &
              .and. cim0oc(il,l,is) > ytiny &
              .and. cim3oc(il,l,is) > ytiny) then
            cimaf(il,l,iinoc(is),kintoc)=cimaf(il,l,iinoc(is),kintoc) &
                               +cim3oc(il,l,is)/cimast(il,l,iinoc(is))
          end if
        end do
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * mass, number, and mass fraction tendencies for internally
  !     * mixed types of aerosol.
  !
  if (kint > 0) then
    pidndt=0.
    pidmdt=0.
    pidfdt=(cimaf-pifrc)/dt
  end if
  if (kintbc > 0) then
    do is=1,isintbc
      where (cim0bc(:,:,is) > ytiny .and. cim3bc(:,:,is) > ytiny)
        pidndt(:,:,iinbc(is))=pidndt(:,:,iinbc(is)) &
                                 +cim0bc(:,:,is)/dt
        pidmdt(:,:,iinbc(is))=pidmdt(:,:,iinbc(is)) &
                                 +cim3bc(:,:,is)/dt
      end where
    end do
  end if
  if (kintoc > 0) then
    do is=1,isintoc
      where (cim0oc(:,:,is) > ytiny .and. cim3oc(:,:,is) > ytiny)
        pidndt(:,:,iinoc(is))=pidndt(:,:,iinoc(is)) &
                                 +cim0oc(:,:,is)/dt
        pidmdt(:,:,iinoc(is))=pidmdt(:,:,iinoc(is)) &
                                 +cim3oc(:,:,is)/dt
      end where
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * memory deallocation.
  !
  if (kint > 0) then
    deallocate(cimast)
    deallocate(cimaf)
  end if
  if (isintbc > 0) then
    deallocate(cim0bc)
    deallocate(cim3bc)
  end if
  if (isintoc > 0) then
    deallocate(cim0oc)
    deallocate(cim3oc)
  end if
  if (isextbc > 0) then
    deallocate(cem0bc)
    deallocate(cem3bc)
  end if
  if (isextoc > 0) then
    deallocate(cem0oc)
    deallocate(cem3oc)
  end if
  !
end subroutine agebc
