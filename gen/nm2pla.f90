!> \file
!> \brief Transformation of number and mass to PLA-parameters. Subroutine
!>       cornm should be called before this subroutine.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine nm2pla (pn0,phi0,psi,resn,resm,bnum,bmass, &
                         drydn,phiss,dphis,phis0,dphi0, &
                         ilga,leva,isec,kpt)
  !
  use sdparm, only : idrbm,ilarge,isatr,kpext,pext,pint,ptrant,r0,ycnst,ylarge,ylarges,yna,ytiny
  use sdcode, only : sdint, sdint0
  use fpdef,  only : r8
  !
  implicit none
  !
  logical, intent(in) :: kpt !< flag to distinguish between externally and internally mixed types of aerosol
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isec !< Number of separate aerosol tracers
  real, intent(in), dimension(isec) :: phiss !< Dry particle size \f$(ln(Rp/R0))\f$ at the boundaries of the size sections
  real, intent(in), dimension(isec) :: dphis !< Section width
  real, intent(in), dimension(ilga,leva,isec) :: bnum !< Aerosol number concentration \f$[1/kg]\f$
  real, intent(in), dimension(ilga,leva,isec) :: bmass !< Aerosol number concentration \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isec) :: drydn !< Density of dry aerosol particle \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isec) :: phis0 !< Dry particle size ln(Rp /R0) at the boundaries of the size sections
  real, intent(in), dimension(ilga,leva,isec) :: dphi0 !< Section width
  real, intent(out), dimension(ilga,leva) :: resm !< Mass residuum from numerical truncation in growth calculations
  real, intent(out), dimension(ilga,leva) :: resn !< Number residuum from numerical truncation in growth calculations
  real, intent(out), dimension(ilga,leva,isec) :: pn0 !< First PLA size distribution parameter (\f$n_{0,i}\f$, amplitude)
  real, intent(out), dimension(ilga,leva,isec) :: psi !< Second PLA size distribution parameter (\f$\psi_{i}\f$, mode size)
  real, intent(out), dimension(ilga,leva,isec) :: phi0 !< Third PLA size distribution parameter (\f$\phi_{0,i}\f$, width)
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:,:) :: pn0n !<
  real, allocatable, dimension(:,:,:) :: pn0m !<
  real, allocatable, dimension(:,:,:) :: sdfn !<
  real, allocatable, dimension(:,:,:) :: sdfm !<
  real, allocatable, dimension(:,:,:) :: rat0s !<
  real, allocatable, dimension(:,:,:) :: arg !<
  real, allocatable, dimension(:,:,:) :: phihat !<
  real, allocatable, dimension(:,:,:) :: atmp !<
  real, allocatable, dimension(:,:,:) :: amass !<
  real, allocatable, dimension(:,:) :: totn !<
  real, allocatable, dimension(:,:) :: totm !<
  type(ptrant), allocatable, dimension(:) :: ptrans
  !
  real, parameter :: onethird=1./3. !<
  real, parameter :: yscal=1.e+09 !<
  real, parameter :: rweigh=1. ! weighting factor
  integer, parameter :: itmax=8 ! 5         ! number of iterations for increased accuracy
  integer, parameter :: iconsm=2 ! determines if numerical truncation will
  ! primarily affect mass or number results
  logical, parameter :: kiter=.false. ! iterations for increased accuracy
  integer, parameter :: idef=-1 !<
  integer :: i !<
  integer :: il !<
  integer :: is !<
  integer :: l !<
  integer :: idrb !<
  integer :: ir !<
  integer :: iok !<
  integer :: it !<
  real :: r0p !<
  real :: acnst !<
  real :: dratb !<
  real :: rat0 !<
  real :: dratx !<
  real :: weigf !<
  real :: philx !<
  real :: rmom !<
  real :: phirx !<
  real :: aterm2 !<
  real :: frcm !<
  real :: frcn !<
  real(r8) :: rmom3 !<
  real(r8) :: trm1 !<
  real(r8) :: trm2 !<
  real(r8) :: sqrtpsi !<
  real(r8) :: dphi !<
  real(r8) :: arg4(4) !<
  real(r8) :: aierf(4) !<
  real(r8) :: aderf(4) !<
  real(r8) :: aterm1 !<
  real(r8) :: adfun !<
  real(r8) :: afun !<
  real(r8) :: nrinc !<
  real(r8) :: pid = 3.141592653589793238_r8 !<
  real(r8) :: sqrtpid = 1.7724538509055160273_8 !<
  real(r8) :: nreps = 1.d-8 !<
  !
  !-----------------------------------------------------------------------
  !     * allocate work arrays.
  !
  allocate(pn0n  (ilga,leva,isec))
  allocate(pn0m  (ilga,leva,isec))
  allocate(sdfn  (ilga,leva,isec))
  allocate(sdfm  (ilga,leva,isec))
  allocate(rat0s (ilga,leva,isec))
  allocate(arg   (ilga,leva,isec))
  allocate(phihat(ilga,leva,isec))
  allocate(atmp  (ilga,leva,isec))
  allocate(amass (ilga,leva,isec))
  allocate(ptrans(isatr))
  do is=1,isatr
    allocate(ptrans(is)%flg  (idrbm))
    allocate(ptrans(is)%psib (idrbm))
    allocate(ptrans(is)%phib (idrbm))
    allocate(ptrans(is)%dphib(idrbm))
  end do
  !
  !-----------------------------------------------------------------------
  !     * initializations.
  !
  pn0 =0.
  pn0m=0.
  pn0n=0.
  psi =yna
  phi0=yna
  r0p=r0**3
  acnst=ycnst*r0p*yscal
  !
  !     * copy parameters specific to internally resp. externally
  !     * mixed types of aerosol from permanent to temporary memory.
  !
  if (kpt .eqv. kpext) then
    do is=1,isatr
      ptrans(is)%idrb       =pext(is)%idrb
      ptrans(is)%flg  (:)   =pext(is)%flg  (:)
      ptrans(is)%psib (:)%vl=pext(is)%psib (:)%vl
      ptrans(is)%psib (:)%vr=pext(is)%psib (:)%vr
      ptrans(is)%phib (:)%vl=pext(is)%phib (:)%vl
      ptrans(is)%phib (:)%vr=pext(is)%phib (:)%vr
      ptrans(is)%dphib(:)%vl=pext(is)%dphib(:)%vl
      ptrans(is)%dphib(:)%vr=pext(is)%dphib(:)%vr
    end do
  else
    do is=1,isatr
      ptrans(is)%idrb       =pint(is)%idrb
      ptrans(is)%flg  (:)   =pint(is)%flg  (:)
      ptrans(is)%psib (:)%vl=pint(is)%psib (:)%vl
      ptrans(is)%psib (:)%vr=pint(is)%psib (:)%vr
      ptrans(is)%phib (:)%vl=pint(is)%phib (:)%vl
      ptrans(is)%phib (:)%vr=pint(is)%phib (:)%vr
      ptrans(is)%dphib(:)%vl=pint(is)%dphib(:)%vl
      ptrans(is)%dphib(:)%vr=pint(is)%dphib(:)%vr
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * values of psi and phi0.
  !
  arg=1.
  atmp=acnst*drydn*bnum
  amass=bmass*yscal
  where (bnum > ytiny .and. bmass > ytiny &
      .and. 2+abs(exponent(amass) - exponent(atmp)) &
      < maxexponent(amass) .and. atmp/=0. )
    arg=amass/atmp
  end where
  phihat=onethird*log(arg)
  do is=1,isec
    idrb=ptrans(is)%idrb
    dratb=1./real(idrb)
    do l=1,leva
      do il=1,ilga
        if (bnum(il,l,is) > ytiny .and. bmass(il,l,is) > ytiny) then
          !
          !           * size ratio from mass and number in each section.
          !
          rat0 = (phihat(il,l,is) - phiss(is)) / dphis(is)
          rat0s(il,l,is)=max(0.,min(1.,rat0))
          ir=min(nint(rat0*idrb+0.5),idrb)
          !
          !           * check if size ratio is valid. abort if this is not the case
          !           * subroutine cornm will prevent this from happening if
          !           * called before this subroutine.
          !
          if (rat0>1 .or. rat0<0) then
            print '("RAT0 error in NM2PLA:",3i4,4es16.8)', IS, L, IL,  RAT0, PHIHAT(IL,L,IS), PHISS(IS), DPHIS(IS)
            call xit ('NM2PLA',-1)
          end if
          if (ptrans(is)%flg(ir) ) then
            !
            !             * use tabulated results to calculate phi-parameter from
            !             * linear interpolation and to set psi-parameter.
            !
            psi(il,l,is)=ptrans(is)%psib(ir)%vl
            dratx=rat0-real(ir-1)*dratb
            weigf=1.-dratx/dratb
            philx = ptrans(is)%phib(ir)%vl + dratx*ptrans(is)%dphib(ir)%vl
            phirx = ptrans(is)%phib(ir)%vr - (dratb-dratx)*ptrans(is)%dphib(ir)%vr
            phi0(il,l,is)=phiss(is)+weigf*philx+(1.-weigf)*phirx
          else
            !
            !             * size distribution is a delta-function
            !
            psi (il,l,is) = ylarge
            phi0(il,l,is) = phihat(il,l,is)
          end if
        end if
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * deallocate work arrays.
  !
  deallocate(arg)
  deallocate(phihat)
  deallocate(ptrans)
  !
  !-----------------------------------------------------------------------
  !     * iterations to increase accuracy.
  !
  if (kiter) then
    rmom=3.0
    trm1=0.50_r8*dble(rmom)
    trm2=0.25_r8*dble(rmom)**2._r8
    do is=1,isec
      do l=1,leva
        do il=1,ilga
          if (psi(il,l,is) < ylarges .and. abs(psi(il,l,is)-yna) > ytiny) then
            sqrtpsi=sqrt(abs(dble(psi(il,l,is))))
            dphi=dble(phi0(il,l,is))-dble(phiss(is))
            iok=1
            do it=1,itmax
              arg4(3)= sqrtpsi*(dble(dphis(is))-dble(dphi))
              arg4(4)=-sqrtpsi*dphi
              arg4(1)=arg4(3)-trm1/sqrtpsi
              arg4(2)=arg4(4)-trm1/sqrtpsi
              aierf(1:4) = erf(arg4(1:4))
              !              aerf1 = erf(arg1)               ! proc-depend
              !              aerf2 = erf(arg2)               ! proc-depend
              !              aerf3 = erf(arg3)               ! proc-depend
              !              aerf4 = erf(arg4)               ! proc-depend
              !
              !             * critical function.
              !
              arg4(1)=3._r8*(dble(rat0s(il,l,is))*dble(dphis(is))-dphi)-9._r8/(4._r8*dble(psi(il,l,is)))
              if (arg4(1)>100._r8) then
                PRINT '("WARNING: large exp arg in nm2pla: ", 4i3, 7es10.2)', is,l,il,it, RAT0S(IL,L,IS), DPHIS(IS), &
                          dphi, psi(il,l,is), arg4(1), phi0(il,l,is), phiss(is)
                exit
              end if
              aterm1 = exp(arg4(1))                ! proc-depend
              if (abs(aierf(3)-aierf(4)) > ytiny) then
                aterm2=(aierf(1)-aierf(2))/(aierf(3)-aierf(4))
                afun=aterm1-aterm2
                !
                !               * derivative.
                !
                arg4(1:4)=-arg4(1:4)**2._r8
                aderf(1:4)=-2._r8*exp(arg4(1:4))*sqrtpsi/sqrtpid
                !                arg1=-arg1**2._r8
                !                arg2=-arg2**2._r8
                !                arg3=-arg3**2._r8
                !                arg4=-arg4**2._r8
                !                aderf1=-2._r8*exp(arg1)*sqrtpsi/sqrtpid             ! proc-depend
                !                aderf2=-2._r8*exp(arg2)*sqrtpsi/sqrtpid             ! proc-depend
                !                aderf3=-2._r8*exp(arg3)*sqrtpsi/sqrtpid             ! proc-depend
                !                aderf4=-2._r8*exp(arg4)*sqrtpsi/sqrtpid             ! proc-depend
                adfun =-3._r8*aterm1-( (aderf(1)-aderf(2))*(aierf(3)-aierf(4)) &
                                      -(aderf(3)-aderf(4))*(aierf(1)-aierf(2)) ) &
                                     / (aierf(3)-aierf(4))**2._r8
                !
                !               * update dphi
                !
                nrinc = afun / adfun
                dphi = dphi - nrinc
                if (abs(nrinc) < nreps) exit
              else
                iok=idef
              end if
            end do   ! itmax
            if (iok /= idef) phi0(il,l,is) = phiss(is) + real(dphi)
          end if
        end do ! il
      end do ! l
    end do ! is
  end if   ! kiter
  !
  !-----------------------------------------------------------------------
  !     * deallocate work array.
  !
  deallocate(rat0s)
  !
  !-----------------------------------------------------------------------
  !     * calculate remaining third parameter of gaussian distribution.
  !     * the calculation of the phi-parameter from tabulated results
  !     * will lead to inaccuracies for this parameter.
  !
  sdfn=sdint0(phi0,psi,phis0,dphi0,ilga,leva,isec)
  rmom=3.
  sdfm=drydn*ycnst*sdint(phi0,psi,rmom,phis0,dphi0,ilga,leva,isec)
  where (2+abs(exponent(bnum) - exponent(sdfn)) &
      < maxexponent(bnum) .and. sdfn/=0. .and. &
      2+abs(exponent(bmass) - exponent(sdfm)) &
      < maxexponent(bmass) .and. sdfm/=0. )
    pn0n=bnum /sdfn
    pn0m=bmass/sdfm
  end where
  if (iconsm==1) then
    !
    !       * truncation errors are assumed to only affect the mass
    !       * size distribution.
    !
    pn0=pn0n
    resn=0.
    resm=sum(pn0*sdfm-bmass,dim=3)
  else if (iconsm==2) then
    !
    !       * truncation errors are assumed to only affect the number
    !       * size distribution.
    !
    pn0=pn0m
    resn=sum(pn0*sdfn-bnum ,dim=3)
    resm=0.
  else if (iconsm==3) then
    !
    !       * reduce the impact of inaccuracies on total mass and number
    !       * by using a weighted average of the parameters that would
    !       * result for mass and number only.
    !
    allocate(totn(ilga,leva))
    allocate(totm(ilga,leva))
    totm=sum(bmass,dim=3)
    totn=sum(bnum ,dim=3)
    do is=1,isec
      do l=1,leva
        do il=1,ilga
          if (bnum(il,l,is) > ytiny .and. bmass(il,l,is) > ytiny) then
            frcm=rweigh*bmass(il,l,is)/totm(il,l)
            frcn=bnum(il,l,is)/totn(il,l)
            weigf=frcn/(frcn+frcm)
            pn0(il,l,is)=weigf*pn0n(il,l,is)+(1.-weigf)*pn0m(il,l,is)
          end if
        end do
      end do
    end do
    resn=sum(pn0*sdfn-bnum ,dim=3)
    resm=sum(pn0*sdfm-bmass,dim=3)
    deallocate(totn)
    deallocate(totm)
  end if
  !
  !-----------------------------------------------------------------------
  !     * deallocate work arrays.
  !
  deallocate(pn0n)
  deallocate(pn0m)
  deallocate(sdfn)
  deallocate(sdfm)
  deallocate(atmp)
  deallocate(amass)
  !
end subroutine nm2pla
!> \file
!! \subsection ssec_details Details
!! primary input/output variables (argument list):
!! |in/out | name   | details                                                  |
!! |:------|:-------|:---------------------------------------------------------|
!! | i     | bmass  | aerosol mass concentration (kg/kg)|
!! | i     | bnum   | aerosol number concentration (1/kg)|
!! | i     | dphi0  | section width|
!! | i     | dphis  | section width|
!! | i     | drydn  | density of dry aerosol particle (kg/m3)|
!! | i     | ilga   | number of grid points in horizontal direction|
!! | i     | isec   | number of separate aerosol tracers|
!! | i     | kpt    | flag to distinguish between externally resp. internally mixed types of aerosol|
!! | i     | leva   | number of grid pointa in vertical direction|
!! | o     | phi0   | 3rd pla size distribution parameter (mode size)|
!! | i     | phis0  | lower dry particle size (=log(r/r0))|
!! | i     | phiss  | lower dry particle size (=log(r/r0))|
!! | o     | pn0    | 1st pla size distribution parameter (amplitude)|
!! | o     | psi    | 2nd pla size distribution parameter (width)|
!! | i     | ptrans(:)%flg(:)        | flag to indicate valid data in pla data table|
!! | i     | ptrans(:)%idrb          | number of points in pla data table|
!! | i     | ptrans(:)%dphib(:)%vl   | tabulated derivative of delta-phi at lower section boundary|
!! | i     | ptrans(:)%dphib(:)%vr   | tabulated derivative of delta-phi at upper section boundary|
!! | i     | ptrans(:)%phib(:)%vl    | tabulated results for delta-phi at lower section boundary|
!! | i     | ptrans(:)%phib(:)%vr    | tabulated results for delta-phi at upper section boundary|
!! | i     | ptrans(:)%psib(:)%vl    | tabulated results for psi at lower section boundary|
!! | o     | resm    | mass residuum from numerical truncation (kg/kg)|
!! | o     | resn    | number residuum from numerical truncation (kg/kg)|
!! \n\n
!!     secondary input/output variables (via modules and common blocks):
!! |in/out | name   | details                                                  |
!! |:------|:-------|:---------------------------------------------------------|
!! | i     | ilarge | integer equivalent to ylarge|
!! | i     | r0     | particle radius reference radius (1.e-06 m)|
!! | i     | ycnst  | 4*pi/3|
!! | i     | ylarge | large number|
!! | i     | yna    | default for undefined value|
!! | i     | ytiny  | smallest valid number|
