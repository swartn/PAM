!> \file
!> \brief Dummy routine replacing tracer information for atmospheric model.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------\
subroutine trinfo(ntracp,iaindt,itrwet,ntrac,mynode)
  !
  use trcind, only : iso2,ihpo,igs6,igsp,idms ! gas-phase tracer indices.
  !
  implicit none

  integer, intent(in) :: ntracp !< Number of aerosol tracers
  integer, intent(in) :: ntrac !< Number of tracers
  integer, intent(in) :: mynode !< Processor ID
  !
  !     * maximum number of aerosol tracers. maxaero should be
  !     * equal to the number of aerosol tracers that are specified
  !     * according to input file "PARAM".
  !
  integer, parameter :: maxaero = 24 !<
  !
  !     * number of other tracers in the model.
  !
  integer, parameter :: maxothr = 5 !<
  !
  character(len=4) :: chtmp !<
  character(len=4), dimension(ntrac) :: chmnam !<
  integer, dimension(ntrac), intent(out) :: iaindt !< Aerosol tracer index array
  integer, dimension(ntrac), intent(out) :: itrwet !< Flags to enable wet deposition
  integer :: i !<
  integer :: ntracpl !<
  !
  !-----------------------------------------------------------------------
  ! check if sufficient memory is available to accommodate tracers. increase ntrac, if necessary.
  if (ntrac < maxaero+maxothr) call xit('TRINFO',-1)
  !
  ! give warning in case the memory required for tracers is less than specified. change ntrac accordingly if this warning appears.
  if (ntrac > maxaero+maxothr) call wrn('TRINFO',-1)
  !
  ! ALSO MAKE SURE THAT TRACER NAMES FOR AEROSOL DON'T EXCEED 3 LETTERS.
  if (maxaero > 99) call xit('TRINFO',-2)
  !
  ! initialization of tracer names.
  do i=1,ntrac
    write(chmnam(i),'(A4)') '****'
  end do
  !
  !-----------------------------------------------------------------------
  ! specify tracer names for aerosol.
  do i=1,maxaero
    if (i >= 10) then
      write(chmnam(i),'(1X,A1,I2)') 'A',i
    else
      write(chmnam(i),'(1X,A2,I1)') 'A0',i
    end if
  end do

  ! specify tracer names for gases (needs to be consistent with parameter maxothr).
  if (maxothr /= 5) call xit('TRINFO',-3)
  chmnam(maxaero+1)=' SO2'
  chmnam(maxaero+2)=' DMS'
  chmnam(maxaero+3)=' HPO'
  chmnam(maxaero+4)=' GS6'
  chmnam(maxaero+5)=' GSP'

  ! summary of gas tracers.
  if (mynode == 0) then
    write(6,'(1X)')
    write(6,'(A9)')    '*** Gases'
    write(6,'(1X)')
    write(6,'(2X,A62)') 'Gas Index          Gas Type                             Tracer'
    write(6,'(3X,A4,14X,A14,24X,I2)') 'ISO2', 'SO2           ', maxaero+1
    write(6,'(3X,A4,14X,A14,24X,I2)') 'IDMS', 'DMS           ', maxaero+2
    write(6,'(3X,A4,14X,A14,24X,I2)') 'IHPO', 'H2O2          ', maxaero+3
    write(6,'(3X,A4,14X,A14,24X,I2)') 'IGS6', 'H2SO4         ', maxaero+4
    write(6,'(3X,A4,14X,A14,24X,I2)') 'IGSP', 'SOA PRECURSORS', maxaero+5
    write(6,'(1X)')
  end if

  !-----------------------------------------------------------------------
  ! specify corresponding tracer indices. make sure that indices are consistent
  ! with specification of common block and tracer names above !
  iso2=maxaero+1
  idms=maxaero+2
  ihpo=maxaero+3
  igs6=maxaero+4
  igsp=maxaero+5

  !-----------------------------------------------------------------------
  ! extract aerosol tracers for pla calculations. all tracers with names that start with an "A"
  ! AND ARE FOLLOWED BY A NUMBER ARE CONSIDERED AEROSOL TRACERS (I.E. FORMAT ' A##').
  if (mynode == 0) then
    write(6,'(1X)')
    write(6,'(A12)')    '*** Aerosols'
    write(6,'(1X)')
    write(6,'(2X,A62)') 'Aer Index          Aer Type                             Tracer'
  end if
  iaindt=-1
  ntracpl=0
  do i=1,ntrac
    itrwet(i)=0
    chtmp=chmnam(i)
    if (i <= maxaero) then
      if (mynode == 0) write(6,'(3X,A2,A2,14X,A4,34X,I2)') 'IA',chtmp(3:4),chtmp,i
    end if
    if (chtmp(2:2) == 'A' &
        .and. (chtmp(3:3) == '0' .or. chtmp(3:3) == '1' &
        .or. chtmp(3:3) == '2' .or. chtmp(3:3) == '3' &
        .or. chtmp(3:3) == '4' .or. chtmp(3:3) == '5' &
        .or. chtmp(3:3) == '6' .or. chtmp(3:3) == '7' &
        .or. chtmp(3:3) == '8' .or. chtmp(3:3) == '9') &
        .and. (chtmp(4:4) == '0' .or. chtmp(4:4) == '1' &
        .or. chtmp(4:4) == '2' .or. chtmp(4:4) == '3' &
        .or. chtmp(4:4) == '4' .or. chtmp(4:4) == '5' &
        .or. chtmp(4:4) == '6' .or. chtmp(4:4) == '7' &
        .or. chtmp(4:4) == '8' .or. chtmp(4:4) == '9') &
        ) then
      ntracpl=ntracpl+1
      if (ntracpl > ntrac) call xit('TRINFO',-4)
      iaindt(ntracpl)=i
    end if
  end do
  !
  !     * flag to enable wet deposition in subroutine xtchempam
  !
  itrwet(igs6)=1
  itrwet(igsp)=1
  !
end subroutine trinfo
