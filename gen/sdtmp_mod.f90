!> \file
!> \brief Temporary arrays for input of tabulated growth data.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
module sdtmp
  !
  implicit none
  !
  type tbltmp
    integer :: idr !<
    integer :: idp !<
    integer :: idrm !<
    integer :: idpm !<
    real :: dpstar0 !<
    real :: yndef !<
    real, allocatable, dimension(:,:) :: phip !<
    real, allocatable, dimension(:,:) :: dphip !<
    real, allocatable, dimension(:,:) :: phin !<
    real, allocatable, dimension(:,:) :: dphin !<
    real, allocatable, dimension(:) :: psip !<
    real, allocatable, dimension(:) :: psin !<
    real, allocatable, dimension(:) :: rat !<
  end type tbltmp
  type(tbltmp), allocatable, dimension(:) :: tblt
  !
end module sdtmp
