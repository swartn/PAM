!> \file
!> \brief Initialization of basic parameters for aerosol calculations.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine sdconf(nupar, nucoa, mynode)
  !
  use sdcode, only : aerf, erfi
  use sdparm, only : aextf,aextfg,aintf,aintfg,coagp,fidryr,fidryrb,fidryrc,fidryvb, &
                         fiphi,fiphib0,ibcs,icls,idrbm,iexbc,iexcl,iexmd,iexno3,iexoc, &
                         iexsac,iexso4,iexss,iinbc,iincl,iinmd,iinno3,iinoc,iinsac,iinso4, &
                         iinss,ilarge,imds,imfex,imsex,ina,infex,infin,ino3s,insex,insin,iocs, &
                         iro,iro0,isacs,isaert,isaext,isaextb,isaint,isaintb,isatr,isextbc, &
                         isextcl,isextmd,isextno3,isextoc,isextsac,isextso4,isextss,isfint, &
                         isfintb,isfintm,isftri,isftrim,isintbc,isintcl,isintmd,isintno3,isintoc, &
                         isintsac,isintso4,isintss,iso4s,isss,iswett,iswext,iswint,itbc,itcl,itmd, &
                         itno3,itoc,itr,itrm,itsac,itso4,itss,jgs6,jgsp,jhpo,jso2,kcoag,kext,kextbc, &
                         kextcl,kextmd,kextno3,kextoc,kextsac,kextso4,kextss,kint,kintbc,kintcl, &
                         kintmd,kintno3,kintoc,kintsac,kintso4,kintss,kwext,kwint,ngas,ntracs,ntract, &
                         pedphis,pedryr,pedryrb,pedryrc,peismax,peismin,peismnk,peismxk,pephiss,pext, &
                         pidphis,pidryr,pidryrb,pidryrc,piismax,piismin,pint,piphiss,r0,sextf,sintf, &
                         sqrtpi,ycnst,ylarge,ylarge8,ylarges,yna,ypi,ysecs,ytiny,ytiny8
  use sdtmp,  only : tblt
  use sdtblf, only : sdtbl0, sdtbl1, sdtbl2, sdtbl3
  use coadat, only : cker,cphi,cpre,ctem,ida,idb,idc,idd
  use fpdef,  only : kdbl,r4,r8
  !
  implicit none
  !
  integer, intent(in) :: nucoa !< File unit number
  integer, intent(in) :: nupar !< File unit number
  !
  integer :: mynode !< Processor ID
  !
  !     internal work variables
  !
  integer, allocatable, dimension(:) :: iok !<
  integer, allocatable, dimension(:) :: ioki !<
  integer, parameter :: ntab=4 ! number of tables for pla conversions
  real, parameter :: yprec=0.1_r4 !<
  real :: rea1 !<
  real(r8) :: ax !<
  real(r8) :: ay !<
  real(r8) :: ythr1 !<
  real(r8) :: ythr2 !<
  real(r8) :: ythr1t !<
  real(r8) :: ythr2t !<
  character(len=70) :: cname !<
  real(r8), parameter :: tstt2=1. !<
  real(r4), parameter :: tstt1=1. !<
  real, parameter :: tstt=1. !<
  character(len=1) :: cchck !<
  integer :: icnt !<
  integer :: is !<
  integer :: isc !<
  integer :: ik !<
  integer :: ip !<
  integer :: isi !<
  integer :: ist !<
  integer :: ism !<
  integer :: isx !<
  integer :: ind !<
  integer :: isect !<
  integer :: ismin !<
  integer :: ismax !<
  integer :: idpr !<
  integer :: idprx !<
  integer :: kxp !<
  integer :: kxold !<
  integer :: kx !<
  integer :: kxt !<
  integer :: ir !<
  integer :: i1 !<
  integer :: i2 !<
  integer :: i3 !<
  integer :: i4 !<
  integer :: it !<
  integer :: ite !<
  integer :: its !<
  integer :: itt !<
  integer :: itx !<
  integer :: nt !<
  integer :: issst !<
  integer :: iso4st !<
  integer :: ino3st !<
  integer :: iclst !<
  integer :: iocst !<
  integer :: ibcst !<
  integer :: imdst !<
  integer :: isacst !<
  integer :: iscs !<
  integer :: isce !<
  integer :: itri !<
  integer :: itrim !<
  integer :: istatus !<
  real :: adist !<
  real :: adtmp !<
  real :: psit !<
  real :: fgrwt !<
  real :: rdryb !<
  real :: rdrye !<
  real :: aincr !<
  real :: adst !<
  real :: adstt !<
  real :: rest !<
  real :: risfint !<
  real :: aphib !<
  real :: dpstart !<
  real :: aphie !<
  real :: dphic !<
  !
  integer, parameter :: numtpe=2 ! number of test particles
  ! for externally mixed aerosol
  integer, parameter :: numtpi=6 ! number of test particles
  ! for internally mixed aerosol
  integer, parameter :: idp=100 !<
  real, dimension(idp) :: rdry !<
  integer, allocatable, dimension(:) :: intp !<
  real, allocatable, dimension(:) :: fgrws !<
  real, allocatable, dimension(:) :: rdrys !<
  real, allocatable, dimension(:) :: rdrym !<
  real, allocatable, dimension(:,:) :: rdryme !<
  real, allocatable, dimension(:,:) :: rdrymo !<
  data  rdry(1:100) / &
      0.100000e-08,0.109750e-08,0.120450e-08,0.132194e-08,0.145083e-08, &
      0.159228e-08,0.174753e-08,0.191791e-08,0.210490e-08,0.231013e-08, &
      0.253537e-08,0.278256e-08,0.305386e-08,0.335160e-08,0.367838e-08, &
      0.403702e-08,0.443062e-08,0.486260e-08,0.533670e-08,0.585702e-08, &
      0.642807e-08,0.705480e-08,0.774264e-08,0.849753e-08,0.932602e-08, &
      0.102353e-07,0.112332e-07,0.123285e-07,0.135305e-07,0.148497e-07, &
      0.162975e-07,0.178865e-07,0.196304e-07,0.215444e-07,0.236449e-07, &
      0.259503e-07,0.284803e-07,0.312572e-07,0.343047e-07,0.376494e-07, &
      0.413201e-07,0.453487e-07,0.497702e-07,0.546228e-07,0.599484e-07, &
      0.657933e-07,0.722081e-07,0.792483e-07,0.869749e-07,0.954548e-07, &
      0.104762e-06,0.114976e-06,0.126186e-06,0.138489e-06,0.151991e-06, &
      0.166810e-06,0.183074e-06,0.200923e-06,0.220513e-06,0.242013e-06, &
      0.265609e-06,0.291505e-06,0.319927e-06,0.351119e-06,0.385353e-06, &
      0.422924e-06,0.464159e-06,0.509414e-06,0.559081e-06,0.613591e-06, &
      0.673415e-06,0.739072e-06,0.811130e-06,0.890216e-06,0.977010e-06, &
      0.107227e-05,0.117681e-05,0.129155e-05,0.141747e-05,0.155568e-05, &
      0.170735e-05,0.187382e-05,0.205651e-05,0.225702e-05,0.247708e-05, &
      0.271859e-05,0.298365e-05,0.327455e-05,0.359381e-05,0.394421e-05, &
      0.432876e-05,0.475081e-05,0.521401e-05,0.572237e-05,0.628030e-05, &
      0.689262e-05,0.756464e-05,0.830217e-05,0.911163e-05,0.100000e-04 &
      /
  real, dimension(idp) :: fgrw !<
  data  fgrw(1:100) / &
      0.979986e-02,0.108759e-01,0.120165e-01,0.132198e-01,0.144798e-01, &
      0.157902e-01,0.171394e-01,0.185155e-01,0.199008e-01,0.212778e-01, &
      0.226232e-01,0.239160e-01,0.251301e-01,0.262436e-01,0.272314e-01, &
      0.280761e-01,0.287576e-01,0.292659e-01,0.295900e-01,0.297283e-01, &
      0.296827e-01,0.294578e-01,0.290645e-01,0.285159e-01,0.278300e-01, &
      0.270215e-01,0.261123e-01,0.251190e-01,0.240619e-01,0.229575e-01, &
      0.218233e-01,0.206738e-01,0.195229e-01,0.183813e-01,0.172590e-01, &
      0.161643e-01,0.151054e-01,0.140829e-01,0.131066e-01,0.121742e-01, &
      0.112906e-01,0.104541e-01,0.966720e-02,0.892710e-02,0.823470e-02, &
      0.758752e-02,0.698431e-02,0.642395e-02,0.590274e-02,0.542033e-02, &
      0.497481e-02,0.456135e-02,0.418141e-02,0.382891e-02,0.350835e-02, &
      0.320883e-02,0.293619e-02,0.268525e-02,0.245569e-02,0.224390e-02, &
      0.205055e-02,0.187295e-02,0.171121e-02,0.156173e-02,0.142676e-02, &
      0.130112e-02,0.118763e-02,0.108393e-02,0.989789e-03,0.901607e-03, &
      0.823886e-03,0.750215e-03,0.685204e-03,0.624017e-03,0.569016e-03, &
      0.518852e-03,0.473861e-03,0.431008e-03,0.392879e-03,0.359698e-03, &
      0.326293e-03,0.298624e-03,0.271405e-03,0.246997e-03,0.225402e-03, &
      0.205156e-03,0.187835e-03,0.171638e-03,0.154992e-03,0.141607e-03, &
      0.129685e-03,0.117875e-03,0.107302e-03,0.983040e-04,0.884061e-04, &
      0.821075e-04,0.729969e-04,0.681604e-04,0.610745e-04,0.569128e-04 &
      /

  !
  !-----------------------------------------------------------------------
  !     * mathematical constants.
  !
  ilarge=huge(1)
  ylarge=1.e-01_r8*huge(rea1)
  ylarges=0.99_r8*ylarge
  yna=-99999._r8
  ina=nint(yna)
  ytiny=10._r8*tiny(rea1)
  r0=1.e-06_r8
  ypi=3.141592653589793_r8
  sqrtpi=1.772453850905516_r8
  ycnst=4._r8*ypi/3._r8
  ylarge8=1.e-09_r8*huge(tstt2)
  ytiny8=10._r8*tiny(tstt2)
  !
  !     * determine whether autopromotion to double precision
  !     * (8 byte real) is used and check for code compliancy.
  !     * here and in all other routines, flags "D ! PROC-DEPEND"
  !     * (for 4 byte real) or " ! PROC-DEPEND" (for 8 byte real)
  !     * indicate associated compiler-dependent programming
  !     * options. to select a different option, replace one flag
  !     * by the other. for example, autopromotion using the
  !     * xlf compiler necessitates the use of functions such as
  !     * erf() and exp() instead of their double-precision
  !     * equivalents derf() and dexp(). the latter may be
  !     * required when using the xlf compiler without
  !     * autopromotion.
  !
  if (digits(tstt)==digits(tstt2) ) then
    kdbl=.true.
  else if (digits(tstt)==digits(tstt1) ) then
    kdbl=.false.
  else
    call xit('SDCONF',-1)
  end if
  !      cchck=                                                          'd ! proc-depend
  !     1'
  !      cchck=                                                          ' ! proc-depend
  !     1'
  !      if ( .not.((kdbl.and.cchck==' ').or.(.not.kdbl.and.cchck=='d')) )
  !     1  call xit('sdconf',-2)
  !
  !-----------------------------------------------------------------------
  !     * read and save parameters for individual aerosol chemical types.
  !
  call sdread(nupar)
  !
  !-----------------------------------------------------------------------
  !     * determine number of externally mixed aerosol tracers.
  !
  isaext=0
  do kx=1,kext
    isaext=isaext+aextf%tp(kx)%isec
  end do
  isaextb=isaext+kext
  !
  !     * aerosol type and section indices for externally mixed
  !     * aerosol tracers.
  !
  if (isaext > 0) then
    if (allocated(sextf%isaer)) deallocate(sextf%isaer)
    allocate(sextf%isaer(isaext))
  end if
  isi=0
  do kx=1,kext
    do is=1,aextf%tp(kx)%isec
      isi=isi+1
      ism=isi
      sextf%isaer(isi)%ityp=kx
      sextf%isaer(isi)%isi=is
      sextf%isaer(isi)%ism=ism
    end do
  end do
  isaint=0
  if (kint > 0) then
    !
    !       * determine number of internally mixed aerosol tracers.
    !
    allocate(iok(aintf%isec))
    do ist=1,aintf%isec
      iok(ist)=0
      do kx=1,kint
        if (ist >= aintf%tp(kx)%iscb &
            .and. ist <= aintf%tp(kx)%isce) then
          iok(ist)=iok(ist)+1
        end if
      end do
      if (iok(ist) /= 0) then
        isaint=isaint+1
      else
        call xit('SDCONF',-3)
      end if
    end do
    !
    !       * aerosol type and section indices for internally mixed
    !       * aerosol tracers.
    !
    if (isaint > 0) then
      if (allocated(sintf%isaer))  deallocate(sintf%isaer)
      allocate(sintf%isaer(isaint))
      ism=isaext
      do is=1,aintf%isec
        sintf%isaer(is)%isi=is
        sintf%isaer(is)%itypt=iok(is)
        if (allocated(sintf%isaer(is)%ityp)) deallocate(sintf%isaer(is)%ityp)
        allocate(sintf%isaer(is)%ityp(iok(is)))
        if (allocated(sintf%isaer(is)%ism))  deallocate(sintf%isaer(is)%ism)
        allocate(sintf%isaer(is)%ism (iok(is)))
        kxt=0
        do kx=1,kint
          if (is >= aintf%tp(kx)%iscb &
              .and. is <= aintf%tp(kx)%isce) then
            kxt=kxt+1
            ism=ism+1
            sintf%isaer(is)%ityp(kxt)=kx
            sintf%isaer(is)%ism (kxt)=ism
          end if
        end do
      end do
    end if
    deallocate(iok)
    isaintb=isaint+1
  end if
  !
  !-----------------------------------------------------------------------
  !     * determine number of wettable, externally mixed aerosol tracers.
  !
  iswext=0
  kwext=0
  do kx=1,kext
    if (     (abs(aextf%tp(kx)%nuio-yna) > ytiny &
        .and. aextf%tp(kx)%nuio > ytiny) &
        .or. (abs(aextf%tp(kx)%kappa-yna) > ytiny &
        .and. aextf%tp(kx)%kappa > ytiny) ) then
      iswext=iswext+aextf%tp(kx)%isec
      kwext=kwext+1
    end if
  end do
  !
  !     * aerosol type and section indices for wettable, externally mixed
  !     * aerosol tracers.
  !
  if (kwext > 0) then
    if (allocated(sextf%iswet)) deallocate(sextf%iswet)
    allocate(sextf%iswet(iswext))
  end if
  isi=0
  ism=0
  do kx=1,kext
    if (     (abs(aextf%tp(kx)%nuio-yna) > ytiny &
        .and. aextf%tp(kx)%nuio  > ytiny) &
        .or. (abs(aextf%tp(kx)%kappa-yna) > ytiny &
        .and. aextf%tp(kx)%kappa > ytiny) ) then
      do is=1,aextf%tp(kx)%isec
        isi=isi+1
        ism=ism+1
        sextf%iswet(isi)%ityp=kx
        sextf%iswet(isi)%isi=is
        sextf%iswet(isi)%ism=ism
      end do
    else
      do is=1,aextf%tp(kx)%isec
        ism=ism+1
      end do
    end if
  end do
  iswint=0
  kwint=0
  if (kint > 0) then
    !
    !       * determine number of wettable, internally mixed aerosol tracers.
    !
    allocate(iok(aintf%isec))
    if (allocated(ioki) ) deallocate (ioki)
    allocate(ioki(aintf%isec))
    do ist=1,aintf%isec
      iok(ist)=0
      ioki(ist)=0
      do kx=1,kint
        if (ist >= aintf%tp(kx)%iscb &
            .and. ist <= aintf%tp(kx)%isce) then
          iok(ist)=iok(ist)+1
          if (     (abs(aintf%tp(kx)%nuio-yna) > ytiny &
              .and. aintf%tp(kx)%nuio  > ytiny) &
              .or. (abs(aintf%tp(kx)%kappa-yna) > ytiny &
              .and. aintf%tp(kx)%kappa > ytiny) ) then
            ioki(ist)=1
          end if
        end if
      end do
      if (ioki(ist) /= 0) then
        iswint=iswint+1
      end if
    end do
    if (iswint > 0) kwint=1
    !
    !       * aerosol type and tracer indices for wettable, internally mixed
    !       * aerosol tracers. an aerosol is considered wettable if at least
    !       * one chemical species in a section dissolves into a number of ions
    !       * that is greater than 0.
    !
    if (iswint > 0) then
      if (allocated(sintf%iswet)) deallocate(sintf%iswet)
      allocate(sintf%iswet(iswint))
      is=0
      ism=isaext
      do ist=1,aintf%isec
        if (ioki(ist) /= 0) then
          is=is+1
          sintf%iswet(is)%isi=ist
          sintf%iswet(is)%itypt=iok(ist)
          if (allocated(sintf%iswet(is)%ityp)) deallocate(sintf%iswet(is)%ityp)
          allocate(sintf%iswet(is)%ityp(iok(ist)))
          if (allocated(sintf%iswet(is)%ism)) deallocate(sintf%iswet(is)%ism)
          allocate(sintf%iswet(is)%ism (iok(ist)))
          kxt=0
          do kx=1,kint
            if (ist >= aintf%tp(kx)%iscb &
                .and. ist <= aintf%tp(kx)%isce) then
              ism=ism+1
              kxt=kxt+1
              sintf%iswet(is)%ityp(kxt)=kx
              sintf%iswet(is)%ism (kxt)=ism
            end if
          end do
        else
          do kx=1,kint
            if (ist >= aintf%tp(kx)%iscb &
                .and. ist <= aintf%tp(kx)%isce) then
              ism=ism+1
            end if
          end do
        end if
      end do
    end if
    deallocate(iok)
  end if
  !
  !-----------------------------------------------------------------------
  !     * calculate number of tracers for aerosol mass.
  !
  isaert=isaext
  do is=1,isaint
    isaert=isaert+sintf%isaer(is)%itypt
  end do
  iswett=iswext
  do is=1,iswint
    iswett=iswett+sintf%iswet(is)%itypt
  end do
  !
  !-----------------------------------------------------------------------
  !     * tracer index for aerosol number concentration for all aerosols.
  !     * aerosol number concentrations are saved after aerosol mass
  !     * concentrations.
  !
  isi=isaert
  do kx=1,kext
    do is=1,aextf%tp(kx)%isec
      isi=isi+1
      sextf%isaer(isi-isaert)%isn=isi
    end do
  end do
  do is=1,aintf%isec
    isi=isi+1
    sintf%isaer(is)%isn=isi
  end do
  !
  !     * tracer index for aerosol number concentration for wettable
  !     * aerosols.
  !
  isi=isaert
  do kx=1,kext
    do is=1,aextf%tp(kx)%isec
      isi=isi+1
      if (     (abs(aextf%tp(kx)%nuio-yna) > ytiny &
          .and. aextf%tp(kx)%nuio  > ytiny) &
          .or. (abs(aextf%tp(kx)%kappa-yna) > ytiny &
          .and. aextf%tp(kx)%kappa > ytiny) ) then
        sextf%iswet(isi-isaert)%isn=isi
      end if
    end do
  end do
  isx=0
  do is=1,aintf%isec
    isi=isi+1
    if (ioki(is) /= 0) then
      isx=isx+1
      sintf%iswet(isx)%isn=isi
    end if
  end do
  if (allocated(ioki) ) then
    deallocate(ioki)
  end if
  !
  !-----------------------------------------------------------------------
  !     * index ranges for main aerosol tracer array.
  !
  if (isaext > 0) then
    insex=sextf%isaer(1)%isn
    infex=sextf%isaer(isaext)%isn
    imsex=sextf%isaer(1)%ism
    imfex=sextf%isaer(isaext)%ism
  end if
  if (isaint > 0) then
    insin=sintf%isaer(1)%isn
    infin=sintf%isaer(isaint)%isn
  end if
  !
  !-----------------------------------------------------------------------
  !     * total number of aerosol tracers for pam.
  !
  ntracs=isaert+isaext+isaint
  !
  !     * tracer indices for gases.
  !
  jgs6=ntracs+1
  jgsp=ntracs+2
  jso2=ntracs+3
  jhpo=ntracs+4
  ngas=4
  !
  !     * total number of aerosol + gas tracers.
  !
  ntract=ntracs+ngas
  !
  !-----------------------------------------------------------------------
  !     * access and save tabulated data for pla transformations for
  !     * arbitrary number of different tables. new tables can be added
  !     * by creating and calling a corresponding subroutine. the total
  !     * number of tables (ntab) needs to be consistent with the number
  !     * of subroutine calls in this section. correct ntab, if necessary.
  !
  if (allocated(tblt) ) deallocate(tblt)
  allocate(tblt(ntab))
  ythr1t=yna
  ythr2t=yna
  icnt=0
  call sdtbl0(icnt,ythr1t,ythr2t,ntab)
  call sdtbl1(icnt,ythr1t,ythr2t,ntab)
  call sdtbl2(icnt,ythr1t,ythr2t,ntab)
  call sdtbl3(icnt,ythr1t,ythr2t,ntab)
  if (icnt /= ntab) call xit ('SDCONF',-4)
  !
  !     * determine accuracy to prevent floating point over/underflow
  !     * in calculations involving erf().
  !
  aincr=0.05
  ax=0.
  ythr1=yna
  do ind=1,1000
    ax=ax+aincr
    ay= &! proc-depend
            aerf(ax)
    if (abs(ay-1.) <= epsilon(ay) .and. abs(ythr1-yna) <= ytiny &
        ) then
      ythr1=ax-aincr
    end if
  end do
  !
  !     * determine accuracy to prevent floating point over/underflow
  !     * in calculations involving erfi().the first check prevents
  !     * floating point overflow in calculation of exp(arg**2) in erfi(),
  !     * the second floating point overflow in applications of erfi().
  !     * note that the first check is specific to the method that is used
  !     * to numerically determine erfi().
  !
  !      ax=0.
  !      ythr2=yna
  !      do ind=1,1000
  !        ax=ax+aincr
  !        if ( ax**2 >= 88.7228 .and. abs(ythr2-yna) <= ytiny) then
  !          ythr2=ax-aincr
  !        end if
  !        ay=erfi(ax)
  !        if ( abs(ay)**2 >= huge(ay) .and. abs(ythr2-yna) <= ytiny) then
  !          ythr2=ax-aincr
  !        end if
  !      end do
  !
  !      * use the following instead if erfi() is not used...
  !
  ythr2=ythr2t
  !
  !     * compare to maxinum accuracy in tables and exit if accuracy
  !     * of calculations in the current code is not sufficient.
  !
  if (abs(ythr1-ythr1t) > yprec .or. abs(ythr2-ythr2t) > yprec &
      ) then
    call xit('SDCONF',-5)
  end if
  !
  !-----------------------------------------------------------------------
  !     * find the table result that corresponds to (or matches closely) the
  !     * requested section widths.
  !
  do kx=1,kext
    if (abs(aextf%tp(kx)%dpst-yna) > ytiny) then
      adst=ylarge
      do nt=1,ntab
        adstt=abs(aextf%tp(kx)%dpst-tblt(nt)%dpstar0)
        if (adstt < adst) then
          adst=adstt
          aextf%tp(kx)%ntbl=nt
        end if
      end do
    else
      aextf%tp(kx)%ntbl=1
    end if
  end do
  if (kint > 0) then
    if (abs(aintf%dpst-yna) > ytiny) then
      adst=ylarge
      do nt=1,ntab
        adstt=abs(aintf%dpst-tblt(nt)%dpstar0)
        if (adstt < adst) then
          adst=adstt
          aintf%ntbl=nt
        end if
      end do
    else
      aintf%ntbl=1
    end if
  end if
  do kx=1,kext
    nt=aextf%tp(kx)%ntbl
    aextf%tp(kx)%idrb=tblt(nt)%idrm
    aextf%tp(kx)%dpstar=tblt(nt)%dpstar0
  end do
  if (kint /= 0) then
    nt=aintf%ntbl
    aintf%idrb=tblt(nt)%idrm
    aintf%dpstar=tblt(nt)%dpstar0
  end if
  !
  !     * check if section width is multiple of section witdth
  !     * corresponding to doubling of particle volume. this is
  !     * required for coagulation calculations at present. the following
  !     * can be removed if coagulation is not accounted for in the model.
  !
  if (kint /= 0 .and. kcoag) kcoag=.true.
  if (kcoag) then
    coagp%dpstar=log(2.)/3.
    if (kint /= 0) then
      coagp%isec=nint(aintf%dpstar/coagp%dpstar)
      rest=aintf%dpstar-coagp%isec*coagp%dpstar
      if (abs(rest) > ysecs*coagp%dpstar) then
        call xit('SDCONF',-6)
      end if
    end if
    isfint =isaint*coagp%isec
    isfintb=isfint+1
    isfintm=isfint-1
    risfint=real(isfint)
    isftri =nint(risfint*(risfint+1.)/2.)
    isftrim=nint(risfint*(risfint-1.)/2.)
    !
    !       * read for coagulation coefficient.
    !
    if (nucoa>0) then
      read(nucoa,'(4(1X,I3))') idb,idc,ida,idd
    end if

#ifdef GM_PAM
        ! broadcast read variables to all processes
        call rpn_comm_bcast(idb, 1, "MPI_INTEGER", 0, "GRID", istatus)
        call rpn_comm_bcast(idc, 1, "MPI_INTEGER", 0, "GRID", istatus)
        call rpn_comm_bcast(ida, 1, "MPI_INTEGER", 0, "GRID", istatus)
        call rpn_comm_bcast(idd, 1, "MPI_INTEGER", 0, "GRID", istatus)
#endif

    if (allocated(cker)) deallocate(cker)
    allocate(cker(idb,idc,ida,idd))
    if (allocated(cpre)) deallocate(cpre)
    allocate(cpre(idd))
    if (allocated(ctem)) deallocate(ctem)
    allocate(ctem(ida))
    if (allocated(cphi)) deallocate(cphi)
    allocate(cphi(idb))
    if (nucoa>0) then
      do i4=1,idd
        do i3=1,ida
          do i2=1,idc
            do i1=1,idb
              read(nucoa,'(1X,E14.6)') cker(i1,i2,i3,i4)
            end do
          end do
        end do
      end do
      do i4=1,idd
        read(nucoa,'(1X,E14.6)') cpre(i4)
      end do
      do i3=1,ida
        read(nucoa,'(1X,E14.6)') ctem(i3)
      end do
      do i1=1,idb
        read(nucoa,'(1X,E14.6)') cphi(i1)
      end do
    end if

#ifdef GM_PAM
        ! broadcast read arrays to all processes
        call rpn_comm_bcast(cker, idb*idc*ida*idd, "MPI_REAL", 0, "GRID", istatus)
        call rpn_comm_bcast(cpre, idd            , "MPI_REAL", 0, "GRID", istatus)
        call rpn_comm_bcast(ctem, ida            , "MPI_REAL", 0, "GRID", istatus)
        call rpn_comm_bcast(cphi, idb            , "MPI_REAL", 0, "GRID", istatus)
#endif

  else
    coagp%dpstar=yna
    coagp%isec=ina
  end if
  !
  !-----------------------------------------------------------------------
  !     * allocate and initialize work arrays for tabulated pla data.
  !
  do kx=1,kext
    if (allocated(aextf%tp(kx)%flg)) deallocate(aextf%tp(kx)%flg)
    allocate (aextf%tp(kx)%flg  (aextf%tp(kx)%isec,aextf%tp(kx)%idrb))
    if (allocated(aextf%tp(kx)%psib)) deallocate(aextf%tp(kx)%psib)
    allocate (aextf%tp(kx)%psib (aextf%tp(kx)%isec,aextf%tp(kx)%idrb))
    if (allocated(aextf%tp(kx)%phib)) deallocate(aextf%tp(kx)%phib)
    allocate (aextf%tp(kx)%phib (aextf%tp(kx)%isec,aextf%tp(kx)%idrb))
    if (allocated(aextf%tp(kx)%dphib)) deallocate(aextf%tp(kx)%dphib)
    allocate (aextf%tp(kx)%dphib(aextf%tp(kx)%isec,aextf%tp(kx)%idrb))
    do is=1,aextf%tp(kx)%isec
      do ir=1,aextf%tp(kx)%idrb
        aextf%tp(kx)%flg(is,ir)     =.false.
        aextf%tp(kx)%psib(is,ir)%vl =yna
        aextf%tp(kx)%psib(is,ir)%vr =yna
        aextf%tp(kx)%phib(is,ir)%vl =yna
        aextf%tp(kx)%phib(is,ir)%vr =yna
        aextf%tp(kx)%dphib(is,ir)%vl=yna
        aextf%tp(kx)%dphib(is,ir)%vr=yna
      end do
    end do
  end do
  if (kint /= 0) then
    if (allocated(aintf%flg)) deallocate(aintf%flg)
    allocate (aintf%flg  (aintf%isec,aintf%idrb))
    if (allocated(aintf%psib)) deallocate(aintf%psib)
    allocate (aintf%psib (aintf%isec,aintf%idrb))
    if (allocated(aintf%phib)) deallocate(aintf%phib)
    allocate (aintf%phib (aintf%isec,aintf%idrb))
    if (allocated(aintf%dphib)) deallocate(aintf%dphib)
    allocate (aintf%dphib(aintf%isec,aintf%idrb))
    do is=1,aintf%isec
      do ir=1,aintf%idrb
        aintf%flg(is,ir)     =.false.
        aintf%psib(is,ir)%vl =yna
        aintf%psib(is,ir)%vr =yna
        aintf%phib(is,ir)%vl =yna
        aintf%phib(is,ir)%vr =yna
        aintf%dphib(is,ir)%vl=yna
        aintf%dphib(is,ir)%vr=yna
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * sizes and boundaries of sections.
  !
  !     * allocate size arrays.
  !
  do kx=1,kext
    isect=aextf%tp(kx)%isec
    if (allocated(aextf%tp(kx)%phi)) deallocate(aextf%tp(kx)%phi)
    allocate(aextf%tp(kx)%phi(isect))
    if (allocated(aextf%tp(kx)%dryr)) deallocate(aextf%tp(kx)%dryr)
    allocate(aextf%tp(kx)%dryr(isect))
    if (allocated(aextf%tp(kx)%dryrc)) deallocate(aextf%tp(kx)%dryrc)
    allocate(aextf%tp(kx)%dryrc(isect))
    if (allocated(aextf%tp(kx)%iso)) deallocate(aextf%tp(kx)%iso)
    allocate(aextf%tp(kx)%iso(isect))
  end do
  if (kint > 0) then
    isect=aintf%isec
    if (allocated(aintf%phi))  deallocate(aintf%phi)
    allocate(aintf%phi(isect))
    if (allocated(aintf%dryr)) deallocate(aintf%dryr)
    allocate(aintf%dryr(isect))
    if (allocated(aintf%dryrc)) deallocate(aintf%dryrc)
    allocate(aintf%dryrc(isect))
    if (allocated(aintf%iso)) deallocate(aintf%iso)
    allocate(aintf%iso(isect))
  end if
  !
  !     * assign values.
  !
  isi=0
  do kx=1,kext
    aphib=log(aextf%tp(kx)%radb/r0)
    dpstart=aextf%tp(kx)%dpstar
    isect=aextf%tp(kx)%isec
    aphie=aphib+real(isect)*dpstart
    aextf%tp(kx)%rade=r0*exp(aphie)
    aextf%tp(kx)%phi(1)%vl=aphib
    aextf%tp(kx)%phi(1)%vr=aphib+dpstart
    do is=2,isect
      aextf%tp(kx)%phi(is)%vl=aextf%tp(kx)%phi(is-1)%vr
      aextf%tp(kx)%phi(is)%vr=aextf%tp(kx)%phi(is-1)%vr+dpstart
    end do
    do is=1,isect
      aextf%tp(kx)%dryr(is)%vl=r0*exp(aextf%tp(kx)%phi(is)%vl)
      aextf%tp(kx)%dryr(is)%vr=r0*exp(aextf%tp(kx)%phi(is)%vr)
      aextf%tp(kx)%dryrc(is)=r0*exp(aextf%tp(kx)%phi(is)%vl &
                                +.5*dpstart)
    end do
    do is=1,aextf%tp(kx)%isec
      isi=isi+1
      aextf%tp(kx)%iso(is)=isi
    end do
  end do
  if (kint > 0) then
    aphib=log(aintf%radb/r0)
    dpstart=aintf%dpstar
    isect=aintf%isec
    aphie=aphib+real(isect)*dpstart
    aintf%rade=r0*exp(aphie)
    aintf%phi(1)%vl=aphib
    aintf%phi(1)%vr=aphib+dpstart
    do is=2,isect
      aintf%phi(is)%vl=aintf%phi(is-1)%vr
      aintf%phi(is)%vr=aintf%phi(is-1)%vr+dpstart
    end do
    do is=1,isect
      aintf%dryr(is)%vl=r0*exp(aintf%phi(is)%vl)
      aintf%dryr(is)%vr=r0*exp(aintf%phi(is)%vr)
      aintf%dryrc(is)=r0*exp(aintf%phi(is)%vl+.5*dpstart)
    end do
    do is=1,aintf%isec
      aintf%iso(is)=is
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * allocate memory for pla section size parameter arrays.
  !
  if (isaext > 0) then
    if (allocated(pephiss)) deallocate(pephiss)
    allocate(pephiss(isaext))
    if (allocated(pedphis))  deallocate(pedphis)
    allocate(pedphis(isaext))
    if (allocated(pedryr))  deallocate(pedryr)
    allocate(pedryr (isaext))
    if (allocated(pedryrc)) deallocate(pedryrc)
    allocate(pedryrc(isaext))
    if (allocated(peismin)) deallocate(peismin)
    allocate(peismin(isaext))
    if (allocated(peismax)) deallocate(peismax)
    allocate(peismax(isaext))
    if (allocated(peismnk)) deallocate(peismnk)
    allocate(peismnk(kext))
    if (allocated(peismxk)) deallocate(peismxk)
    allocate(peismxk(kext))
  end if
  if (isaextb > 0) then
    if (allocated(pedryrb)) deallocate(pedryrb)
    allocate(pedryrb(isaextb))
  end if
  if (isaint > 0) then
    if (allocated(piphiss)) deallocate(piphiss)
    allocate(piphiss(isaint))
    if (allocated(pidphis)) deallocate(pidphis)
    allocate(pidphis(isaint))
    if (allocated(pidryr))  deallocate(pidryr)
    allocate(pidryr (isaint))
    if (allocated(pidryrc)) deallocate(pidryrc)
    allocate(pidryrc(isaint))
    if (allocated(piismin)) deallocate(piismin)
    allocate(piismin(isaint))
    if (allocated(piismax)) deallocate(piismax)
    allocate(piismax(isaint))
  end if
  if (isaintb > 0) then
    if (allocated(pidryrb)) deallocate(pidryrb)
    allocate(pidryrb(isaintb))
  end if
  !
  !-----------------------------------------------------------------------
  !     * boundaries and section widths for externally and internally
  !     * mixed aerosol types.
  !
  kxold=ina
  ismin=1
  do is=1,isaext
    isi=sextf%isaer(is)%isi
    kx=sextf%isaer(is)%ityp
    pedphis(is)=aextf%tp(kx)%dpstar
    pephiss(is)=aextf%tp(kx)%phi(isi)%vl
    pedryr(is)%vl=aextf%tp(kx)%dryr(isi)%vl
    pedryr(is)%vr=aextf%tp(kx)%dryr(isi)%vr
    pedryrc(is)=aextf%tp(kx)%dryrc(isi)
    !
    if (kx /= kxold .and. kxold /= ina) then
      ismax=is-1
      peismax(ismin:ismax)=ismax
      peismin(ismin:ismax)=ismin
      peismxk(kxold)=ismax
      peismnk(kxold)=ismin
      ismin=is
    end if
    kxold=kx
  end do
  if (isaext > 0) then
    kx=sextf%isaer(isaext)%ityp
    ismax=isaext
    peismax(ismin:ismax)=ismax
    peismin(ismin:ismax)=ismin
    peismxk(kx)=ismax
    peismnk(kx)=ismin
  end if
  do is=1,isaint
    isi=sintf%isaer(is)%isi
    pidphis(is)=aintf%dpstar
    piphiss(is)=aintf%phi(isi)%vl
    pidryr(is)%vl=aintf%dryr(isi)%vl
    pidryr(is)%vr=aintf%dryr(isi)%vr
    pidryrc(is)=aintf%dryrc(isi)
    piismin(is)=1
    piismax(is)=isaint
  end do
  !
  !     * dry particle sizes at section boundaries for externally
  !     * mixed aerosol types.
  !
  if (isaextb > 0) then
    isc=0
    isx=0
    do is=1,isaext-1
      kx=sextf%isaer(is)%ityp
      kxp=sextf%isaer(is+1)%ityp
      isc=isc+1
      isx=isx+1
      pedryrb(isc)=pedryr(isx)%vl
      if (kxp /= kx) then
        isc=isc+1
        pedryrb(isc)=pedryr(isx)%vr
      end if
    end do
    isc=isc+1
    isx=isx+1
    pedryrb(isc)=pedryr(isx)%vl
    isc=isc+1
    pedryrb(isc)=pedryr(isx)%vr
  end if
  !
  !     * dry particle sizes at section boundaries for internally
  !     * mixed aerosol type.
  !
  if (isaintb > 0) then
    isc=0
    isx=0
    do is=1,isaint-1
      isc=isc+1
      isx=isx+1
      pidryrb(isc)=pidryr(isx)%vl
    end do
    isc=isc+1
    isx=isx+1
    pidryrb(isc)=pidryr(isx)%vl
    isc=isc+1
    pidryrb(isc)=pidryr(isx)%vr
  end if
  !
  !-----------------------------------------------------------------------
  !     * search for nearest value of psi relative to the reference
  !     * (master-psi) from the input that is not associated with any
  !     * undefined value of phi for given ratio rat in the table. no
  !     * result is returned in case only undefined value of phi exists
  !     * in the table. in that case, a delta-function will be used for
  !     * the size distribution in other parts of the code.
  !
  do isi=1,isaext
    kx=sextf%isaer(isi)%ityp
    is=sextf%isaer(isi)%isi
    nt=aextf%tp(kx)%ntbl
    if (aextf%tp(kx)%psim(is) < ylarges) then
      if (aextf%tp(kx)%psim(is) > ytiny) then
        !
        !           * determine index of nearest point.
        !
        idpr=ina
        adist=ylarge
        do ip=1,tblt(nt)%idp
          adtmp=abs(tblt(nt)%psip(ip)-aextf%tp(kx)%psim(is))
          if (adtmp < adist) then
            adist=adtmp
            idpr=ip
          end if
        end do
        !
        !           * check whether the range of psi is sufficient.
        !
        if (idpr == ina) then
          call xit ('SDCONF',-7)
        end if
        if ( (idpr == 1 &
            .and. (tblt(nt)%psip(idpr)-aextf%tp(kx)%psim(is)) > ytiny) &
            .or. (idpr == tblt(nt)%idp &
            .and. (tblt(nt)%psip(idpr)-aextf%tp(kx)%psim(is)) <-ytiny) &
            ) then
          psit=tblt(nt)%psip(idpr)
          if (mynode==0) then
            call wrn('SDCONF',-2)
            write(6,'(A22,A12,1X,I3,1X,F5.2,1X,F5.2)') &
                  '  TYPE,IS,PSIM,PSIP = ',aextf%tp(kx)%name(1:12),is, &
                                           aextf%tp(kx)%psim(is),psit
          end if
        end if
      else
        !
        !           * determine index of nearest point.
        !
        idpr=ina
        adist=ylarge
        do ip=1,tblt(nt)%idp
          adtmp=abs(tblt(nt)%psin(ip)-aextf%tp(kx)%psim(is))
          if (adtmp < adist) then
            adist=adtmp
            idpr=ip
          end if
        end do
        !
        !           * check whether the range of psi is sufficient.
        !
        if (idpr == ina) then
          call xit ('SDCONF',-8)
        end if
        if ( (idpr == 1 &
            .and. (tblt(nt)%psin(idpr)-aextf%tp(kx)%psim(is)) < ytiny) &
            .or. (idpr == tblt(nt)%idp &
            .and. (tblt(nt)%psin(idpr)-aextf%tp(kx)%psim(is)) >-ytiny) &
            ) then
          psit=tblt(nt)%psin(idpr)
          if (mynode==0) then
            call wrn('SDCONF',-3)
            write(6,'(A22,A12,1X,I3,1X,F5.2,1X,F5.2)') &
                  '  TYPE,IS,PSIM,PSIN = ',aextf%tp(kx)%name(1:12),is, &
                                           aextf%tp(kx)%psim(is),psit
          end if
        end if
      end if
      !
      !         * first search in direction of psi > psim.
      !
      if (aextf%tp(kx)%psim(is) > ytiny) then
        do ir=1,tblt(nt)%idrm
          do idprx=idpr,tblt(nt)%idp
            if ( &
                nint(tblt(nt)%phip(idprx,ir)-tblt(nt)%yndef) /= 0 &
                .and. nint(tblt(nt)%phip(idprx,ir+1)-tblt(nt)%yndef) /= 0 &
                .and. .not.aextf%tp(kx)%flg(is,ir)                 ) then
              aextf%tp(kx)%psib(is,ir)%vl=tblt(nt)%psip(idprx)
              aextf%tp(kx)%psib(is,ir)%vr=tblt(nt)%psip(idprx)
              aextf%tp(kx)%phib(is,ir)%vl=tblt(nt)%phip(idprx,ir)
              aextf%tp(kx)%phib(is,ir)%vr=tblt(nt)%phip(idprx,ir+1)
              aextf%tp(kx)%dphib(is,ir)%vl= &
                                              tblt(nt)%dphip(idprx,ir)
              aextf%tp(kx)%dphib(is,ir)%vr= &
                                              tblt(nt)%dphip(idprx,ir+1)
              aextf%tp(kx)%flg(is,ir)=.true.
            end if
          end do
        end do
      else
        do ir=1,tblt(nt)%idrm
          do idprx=idpr,tblt(nt)%idp
            if ( &
                nint(tblt(nt)%phin(idprx,ir)-tblt(nt)%yndef) /= 0 &
                .and. nint(tblt(nt)%phin(idprx,ir+1)-tblt(nt)%yndef) /= 0 &
                .and. .not.aextf%tp(kx)%flg(is,ir)                 ) then
              aextf%tp(kx)%psib(is,ir)%vl=tblt(nt)%psin(idprx)
              aextf%tp(kx)%psib(is,ir)%vr=tblt(nt)%psin(idprx)
              aextf%tp(kx)%phib(is,ir)%vl=tblt(nt)%phin(idprx,ir)
              aextf%tp(kx)%phib(is,ir)%vr=tblt(nt)%phin(idprx,ir+1)
              aextf%tp(kx)%dphib(is,ir)%vl= &
                                              tblt(nt)%dphin(idprx,ir)
              aextf%tp(kx)%dphib(is,ir)%vr= &
                                              tblt(nt)%dphin(idprx,ir+1)
              aextf%tp(kx)%flg(is,ir)=.true.
            end if
          end do
        end do
      end if
      !
      !         * then search in direction of psi < psim, if first search
      !         * unsuccessful.
      !
      if (aextf%tp(kx)%psim(is) > ytiny) then
        do ir=1,tblt(nt)%idrm
          do idprx=idpr-1,1,-1
            if ( &
                nint(tblt(nt)%phip(idprx,ir)-tblt(nt)%yndef) /= 0 &
                .and. nint(tblt(nt)%phip(idprx,ir+1)-tblt(nt)%yndef) /= 0 &
                .and. .not.aextf%tp(kx)%flg(is,ir)                 ) then
              aextf%tp(kx)%psib(is,ir)%vl=tblt(nt)%psip(idprx)
              aextf%tp(kx)%psib(is,ir)%vr=tblt(nt)%psip(idprx)
              aextf%tp(kx)%phib(is,ir)%vl=tblt(nt)%phip(idprx,ir)
              aextf%tp(kx)%phib(is,ir)%vr=tblt(nt)%phip(idprx,ir+1)
              aextf%tp(kx)%dphib(is,ir)%vl= &
                                              tblt(nt)%dphip(idprx,ir)
              aextf%tp(kx)%dphib(is,ir)%vr= &
                                              tblt(nt)%dphip(idprx,ir+1)
              aextf%tp(kx)%flg(is,ir)=.true.
            end if
          end do
        end do
      else
        do ir=1,tblt(nt)%idrm
          do idprx=idpr-1,1,-1
            if ( &
                nint(tblt(nt)%phin(idprx,ir)-tblt(nt)%yndef) /= 0 &
                .and. nint(tblt(nt)%phin(idprx,ir+1)-tblt(nt)%yndef) /= 0 &
                .and. .not.aextf%tp(kx)%flg(is,ir)                 ) then
              aextf%tp(kx)%psib(is,ir)%vl=tblt(nt)%psin(idprx)
              aextf%tp(kx)%psib(is,ir)%vr=tblt(nt)%psin(idprx)
              aextf%tp(kx)%phib(is,ir)%vl=tblt(nt)%phin(idprx,ir)
              aextf%tp(kx)%phib(is,ir)%vr=tblt(nt)%phin(idprx,ir+1)
              aextf%tp(kx)%dphib(is,ir)%vl= &
                                              tblt(nt)%dphin(idprx,ir)
              aextf%tp(kx)%dphib(is,ir)%vr= &
                                              tblt(nt)%dphin(idprx,ir+1)
              aextf%tp(kx)%flg(is,ir)=.true.
            end if
          end do
        end do
      end if
    end if
  end do
  if (kint > 0) then
    nt=aintf%ntbl
    do isi=1,isaint
      is=sintf%isaer(isi)%isi
      if (aintf%psim(is) < ylarges) then
        if (aintf%psim(is) > ytiny) then
          !
          !             * determine index of nearest point.
          !
          idpr=ina
          adist=ylarge
          do ip=1,tblt(nt)%idp
            adtmp=abs(tblt(nt)%psip(ip)-aintf%psim(is))
            if (adtmp < adist) then
              adist=adtmp
              idpr=ip
            end if
          end do
          !
          !             * check whether the range of psi is sufficient.
          !
          if (idpr == ina) then
            call xit ('SDCONF',-9)
          end if
          if ( (idpr == 1 &
              .and. (tblt(nt)%psip(idpr)-aintf%psim(is)) >  ytiny) &
              .or. (idpr == tblt(nt)%idp &
              .and. (tblt(nt)%psip(idpr)-aintf%psim(is)) < -ytiny) &
              ) then
            psit=tblt(nt)%psip(idpr)
            if (mynode==0) then
              call wrn('SDCONF',-4)
              write(6,'(A22,A12,1X,I3,1X,F5.2,1X,F5.2)') &
                    '  TYPE,IS,PSIM,PSIP = ','INTERNAL MIX',is, &
                                             aintf%psim(is),psit
            end if
          end if
        else
          !
          !             * determine index of nearest point.
          !
          idpr=ina
          adist=ylarge
          do ip=1,tblt(nt)%idp
            adtmp=abs(tblt(nt)%psin(ip)-aintf%psim(is))
            if (adtmp < adist) then
              adist=adtmp
              idpr=ip
            end if
          end do
          !
          !             * check whether the range of psi is sufficient.
          !
          if (idpr == ina) then
            call xit ('SDCONF',-10)
          end if
          if ( (idpr == 1 &
              .and. (tblt(nt)%psin(idpr)-aintf%psim(is)) <  ytiny) &
              .or. (idpr == tblt(nt)%idp &
              .and. (tblt(nt)%psin(idpr)-aintf%psim(is)) > -ytiny) &
              ) then
            psit=tblt(nt)%psin(idpr)
            if (mynode==0) then
              call wrn('SDCONF',-5)
              write(6,'(A22,A12,1X,I3,1X,F5.2,1X,F5.2)') &
                    '  TYPE,IS,PSIM,PSIN = ','INTERNAL MIX' ,is, &
                                             aintf%psim(is),psit
            end if
          end if
        end if
        !
        !           * first search in direction of psi > psim.
        !
        if (aintf%psim(is) > ytiny) then

          do ir=1,tblt(nt)%idrm
            do idprx=idpr,tblt(nt)%idp
              if ( &
                  nint(tblt(nt)%phip(idprx,ir)-tblt(nt)%yndef) /= 0 &
                  .and. nint(tblt(nt)%phip(idprx,ir+1)-tblt(nt)%yndef) /= 0 &
                  .and. .not.aintf%flg(is,ir)                        ) then
                aintf%psib(is,ir)%vl=tblt(nt)%psip(idprx)
                aintf%psib(is,ir)%vr=tblt(nt)%psip(idprx)
                aintf%phib(is,ir)%vl=tblt(nt)%phip(idprx,ir)
                aintf%phib(is,ir)%vr=tblt(nt)%phip(idprx,ir+1)
                aintf%dphib(is,ir)%vl=tblt(nt)%dphip(idprx,ir)
                aintf%dphib(is,ir)%vr=tblt(nt)%dphip(idprx,ir+1)
                aintf%flg(is,ir)=.true.
              end if
            end do
          end do
        else
          do ir=1,tblt(nt)%idrm
            do idprx=idpr,tblt(nt)%idp
              if ( &
                  nint(tblt(nt)%phin(idprx,ir)-tblt(nt)%yndef) /= 0 &
                  .and. nint(tblt(nt)%phin(idprx,ir+1)-tblt(nt)%yndef) /= 0 &
                  .and. .not.aintf%flg(is,ir)                        ) then
                aintf%psib(is,ir)%vl=tblt(nt)%psin(idprx)
                aintf%psib(is,ir)%vr=tblt(nt)%psin(idprx)
                aintf%phib(is,ir)%vl=tblt(nt)%phin(idprx,ir)
                aintf%phib(is,ir)%vr=tblt(nt)%phin(idprx,ir+1)
                aintf%dphib(is,ir)%vl=tblt(nt)%dphin(idprx,ir)
                aintf%dphib(is,ir)%vr=tblt(nt)%dphin(idprx,ir+1)
                aintf%flg(is,ir)=.true.
              end if
            end do
          end do
        end if
        !
        !           * then search in direction of psi < psim, if first search
        !           * unsuccessful.
        !
        if (aintf%psim(is) > ytiny) then
          do ir=1,tblt(nt)%idrm
            do idprx=idpr-1,1,-1
              if ( &
                  nint(tblt(nt)%phip(idprx,ir)-tblt(nt)%yndef) /= 0 &
                  .and. nint(tblt(nt)%phip(idprx,ir+1)-tblt(nt)%yndef) /= 0 &
                  .and. .not.aintf%flg(is,ir)                        ) then
                aintf%psib(is,ir)%vl=tblt(nt)%psip(idprx)
                aintf%psib(is,ir)%vr=tblt(nt)%psip(idprx)
                aintf%phib(is,ir)%vl=tblt(nt)%phip(idprx,ir)
                aintf%phib(is,ir)%vr=tblt(nt)%phip(idprx,ir+1)
                aintf%dphib(is,ir)%vl=tblt(nt)%dphip(idprx,ir)
                aintf%dphib(is,ir)%vr=tblt(nt)%dphip(idprx,ir+1)
                aintf%flg(is,ir)=.true.
              end if
            end do
          end do
        else
          do ir=1,tblt(nt)%idrm
            do idprx=idpr-1,1,-1
              if ( &
                  nint(tblt(nt)%phin(idprx,ir)-tblt(nt)%yndef) /= 0 &
                  .and. nint(tblt(nt)%phin(idprx,ir+1)-tblt(nt)%yndef) /= 0 &
                  .and. .not.aintf%flg(is,ir)                        ) then
                aintf%psib(is,ir)%vl=tblt(nt)%psin(idprx)
                aintf%psib(is,ir)%vr=tblt(nt)%psin(idprx)
                aintf%phib(is,ir)%vl=tblt(nt)%phin(idprx,ir)
                aintf%phib(is,ir)%vr=tblt(nt)%phin(idprx,ir+1)
                aintf%dphib(is,ir)%vl=tblt(nt)%dphin(idprx,ir)
                aintf%dphib(is,ir)%vr=tblt(nt)%dphin(idprx,ir+1)
                aintf%flg(is,ir)=.true.
              end if
            end do
          end do
        end if
      end if
    end do
  end if
  !
  !     * allocate arrays for pla tranformation parameters and initialize.
  !
  idrbm=0
  if (isaext > 0) then
    do is=1,isaext
      kx=sextf%isaer(is)%ityp
      idrbm=max(idrbm,aextf%tp(kx)%idrb)
    end do
  end if
  if (isaint > 0) then
    do is=1,isaint
      idrbm=max(idrbm,aintf%idrb)
    end do
  end if
  isatr=max(isaext,isaint)
  if (isaext > 0) then
    if (allocated(pext)) deallocate(pext)
    allocate(pext(isatr))
    do is=1,isatr
      if (allocated(pext(is)%flg)) deallocate(pext(is)%flg)
      allocate(pext(is)%flg  (idrbm))
      if (allocated(pext(is)%psib)) deallocate(pext(is)%psib)
      allocate(pext(is)%psib (idrbm))
      if (allocated(pext(is)%phib)) deallocate(pext(is)%phib)
      allocate(pext(is)%phib (idrbm))
      if (allocated(pext(is)%dphib)) deallocate(pext(is)%dphib)
      allocate(pext(is)%dphib(idrbm))
    end do
    do is=1,isatr
      pext(is)%idrb    =ina
      pext(is)%flg  (:)=.false.
      pext(is)%psib (:)%vl=yna
      pext(is)%psib (:)%vr=yna
      pext(is)%phib (:)%vl=yna
      pext(is)%phib (:)%vr=yna
      pext(is)%dphib(:)%vl=yna
      pext(is)%dphib(:)%vr=yna
    end do
  end if
  if (isaint > 0) then
    if (allocated(pint)) deallocate(pint)
    allocate(pint(isatr))
    do is=1,isatr
      if (allocated(pint(is)%flg)) deallocate(pint(is)%flg)
      allocate(pint(is)%flg(idrbm))
      if (allocated(pint(is)%psib)) deallocate(pint(is)%psib)
      allocate(pint(is)%psib (idrbm))
      if (allocated(pint(is)%phib)) deallocate(pint(is)%phib)
      allocate(pint(is)%phib (idrbm))
      if (allocated(pint(is)%dphib)) deallocate(pint(is)%dphib)
      allocate(pint(is)%dphib(idrbm))
    end do
    do is=1,isatr
      pint(is)%idrb    =ina
      pint(is)%flg  (:)=.false.
      pint(is)%psib (:)%vl=yna
      pint(is)%psib (:)%vr=yna
      pint(is)%phib (:)%vl=yna
      pint(is)%phib (:)%vr=yna
      pint(is)%dphib(:)%vl=yna
      pint(is)%dphib(:)%vr=yna
    end do
  end if
  !
  !     * copy results for pla tranformation parameters.
  !
  if (isaext > 0) then
    do is=1,isaext
      kx=sextf%isaer(is)%ityp
      pext(is)%idrb=aextf%tp(kx)%idrb
    end do
    do is=1,isaext
      kx=sextf%isaer(is)%ityp
      isi=sextf%isaer(is)%isi
      do ir=1,pext(is)%idrb
        pext(is)%flg  (ir)=aextf%tp(kx)%flg  (isi,ir)
        pext(is)%psib (ir)=aextf%tp(kx)%psib (isi,ir)
        pext(is)%phib (ir)=aextf%tp(kx)%phib (isi,ir)
        pext(is)%dphib(ir)=aextf%tp(kx)%dphib(isi,ir)
      end do
    end do
  end if
  if (isaint > 0) then
    do is=1,isaint
      pint(is)%idrb=aintf%idrb
    end do
    do is=1,isaint
      isi=sintf%isaer(is)%isi
      do ir=1,pint(is)%idrb
        pint(is)%flg  (ir)=aintf%flg  (isi,ir)
        pint(is)%psib (ir)=aintf%psib (isi,ir)
        pint(is)%phib (ir)=aintf%phib (isi,ir)
        pint(is)%dphib(ir)=aintf%dphib(isi,ir)
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * ensemble of test particles for aerosol water content
  !     * calculations. a larger number of test particles will
  !     * be used at small particle sizes to increase the accuracy
  !     * for the kelvin effect.
  !
  if (kext > 0) then
    if (allocated(aextfg%sec)) deallocate(aextfg%sec)
    allocate (aextfg%sec(isaext))
    if (allocated(rdryme)) deallocate(rdryme)
    allocate (rdryme(numtpe,kext))
    do kx=1,kext
      !
      !         * select range of tabulated particle density data corresponding
      !         * to simulated particle size spectrum.
      !
      its=1
      do it=1,idp-1
        if (aextf%tp(kx)%radb > rdry(it) ) its=it+1
      end do
      ite=idp
      do it=idp,2,-1
        if (aextf%tp(kx)%rade < rdry(it) ) ite=it-1
      end do
      itt=ite-its+1
      if (itt < 1) call xit('SDCONF',-11)
      !
      !         * normalized particle density data.
      !
      fgrwt=0.
      do it=its,ite
        fgrwt=fgrwt+fgrw(it)
      end do
      allocate (fgrws(itt))
      allocate (rdrys(itt))
      itx=0
      do it=its,ite
        itx=itx+1
        fgrws(itx)=real(numtpe)*fgrw(it)/fgrwt
        rdrys(itx)=rdry(it)
      end do
      !
      !         * select test particles based on criterion of equal
      !         * particle densities.
      !
      rdryb=rdrys(1)
      itx=0
      fgrwt=fgrws(1)
      do it=2,itt-1
        fgrwt=fgrwt+fgrws(it)
        if (fgrwt >= 1. ) then
          rdrye=rdrys(it-1)
          itx=itx+1
          rdryme(itx,kx)=sqrt(rdryb*rdrye)
          rdryb=rdrys(it)
          fgrwt=fgrwt-1.
        end if
      end do
      rdryme(numtpe,kx)=sqrt(rdryb*rdrys(itt))
      deallocate (fgrws)
      deallocate (rdrys)
    end do
    allocate (intp(isaext))
    allocate (rdrymo(numtpe,isaext))
    intp=0
    do is=1,isaext
      isi=sextf%isaer(is)%isi
      kx=sextf%isaer(is)%ityp
      !
      !         * check whether there is at least one test particle
      !         * per pla size section.
      !
      do it=1,numtpe
        if (rdryme(it,kx) >= aextf%tp(kx)%dryr(isi)%vl &
            .and. rdryme(it,kx) <  aextf%tp(kx)%dryr(isi)%vr) then
          intp(is)=intp(is)+1
          rdrymo(intp(is),is)=rdryme(it,kx)
        end if
      end do
      !
      !         * if the method above does not produce any test
      !         * particles, add one particle per section.
      !
      if (intp(is) == 0) then
        intp(is)=intp(is)+1
        rdrymo(intp(is),is)=sqrt(aextf%tp(kx)%dryr(isi)%vl &
                                    *aextf%tp(kx)%dryr(isi)%vr)
      end if
      if (allocated(aextfg%sec(is)%radt)) deallocate(aextfg%sec(is)%radt)
      allocate (aextfg%sec(is)%radt(intp(is)))
      aextfg%sec(is)%ntstp=intp(is)
      do itx=1,intp(is)
        aextfg%sec(is)%radt(itx)=rdrymo(itx,is)
      end do
    end do
    !
    !       * deallocation of work arrays.
    !
    deallocate (rdryme)
    deallocate (intp)
    deallocate (rdrymo)
  end if
  if (kint > 0) then
    if (allocated(aintfg%sec)) deallocate(aintfg%sec)
    allocate(aintfg%sec(isaint))
    !
    !       * select range of tabulated particle density data corresponding
    !       * to simulated particle size spectrum.
    !
    its=1
    do it=1,idp-1
      if (aintf%radb > rdry(it) ) its=it+1
    end do
    ite=idp
    do it=idp,2,-1
      if (aintf%rade < rdry(it) ) ite=it-1
    end do
    itt=ite-its+1
    if (itt < 1) call xit('SDCONF',-12)
    !
    !       * normalized particle density data.
    !
    fgrwt=0.
    do it=its,ite
      fgrwt=fgrwt+fgrw(it)
    end do
    allocate (fgrws(itt))
    allocate (rdrys(itt))
    allocate (rdrym(numtpi))
    itx=0
    do it=its,ite
      itx=itx+1
      fgrws(itx)=real(numtpi)*fgrw(it)/fgrwt
      rdrys(itx)=rdry(it)
    end do
    !
    !       * select test particles based on criterion of equal
    !       * particle densities.
    !
    rdryb=rdrys(1)
    itx=0
    fgrwt=fgrws(1)
    do it=2,itt-1
      fgrwt=fgrwt+fgrws(it)
      if (fgrwt >= 1. ) then
        rdrye=rdrys(it-1)
        itx=itx+1
        rdrym(itx)=sqrt(rdryb*rdrye)
        rdryb=rdrys(it)
        fgrwt=fgrwt-1.
      end if
    end do
    rdrym(numtpi)=sqrt(rdryb*rdrys(itt))
    !
    !       * check whether there is at least one test particle
    !       * per pla size section.
    !
    allocate (intp(isaint))
    allocate (rdrymo(numtpi,isaint))
    intp=0
    do is=1,isaint
      isi=sintf%isaer(is)%isi
      do it=1,numtpi
        if (rdrym(it) >= aintf%dryr(isi)%vl &
            .and. rdrym(it) <  aintf%dryr(isi)%vr) then
          intp(is)=intp(is)+1
          rdrymo(intp(is),is)=rdrym(it)
        end if
      end do
      !
      !         * if the method above does not produce any test
      !         * particles, add one particle per section.
      !
      if (intp(is) == 0) then
        intp(is)=intp(is)+1
        rdrymo(intp(is),is)=sqrt(aintf%dryr(isi)%vl &
                                    *aintf%dryr(isi)%vr)
      end if
      if (allocated(aintfg%sec(is)%radt)) deallocate(aintfg%sec(is)%radt)
      allocate (aintfg%sec(is)%radt(intp(is)))
    end do
    do is=1,isaint
      aintfg%sec(is)%ntstp=intp(is)
      do itx=1,intp(is)
        aintfg%sec(is)%radt(itx)=rdrymo(itx,is)
      end do
    end do
    !
    !       * deallocation of work arrays.
    !
    deallocate (fgrws)
    deallocate (rdrys)
    deallocate (rdrym)
    deallocate (intp)
    deallocate (rdrymo)
  end if
  !
  !-----------------------------------------------------------------------
  !     * determine index for pre-determined aerosol types. the index for
  !     * sulphate aerosol will be used to identify the sulphate aerosol
  !     * in the gas-to-particle conversion calculations. indices will
  !     * also be used in the aerosol growth calculations.
  !
  do kx=1,kext
    cname=aextf%tp(kx)%name
    if (cname(1:7) == 'SEASALT'   ) then
      kextss =kx
    end if
    if (cname(1:9) == '(NH4)2SO4' ) then
      kextso4=kx
    end if
    if (cname(1:6) == 'NH4NO3'    ) then
      kextno3=kx
    end if
    if (cname(1:5) == 'NH4CL'     ) then
      kextcl=kx
    end if
    if (cname(1:7) == 'ORGCARB'   ) then
      kextoc =kx
    end if
    if (cname(1:7) == 'BLCCARB'   ) then
      kextbc =kx
    end if
    if (cname(1:7) == 'MINDUST'   ) then
      kextmd =kx
    end if
    if (cname(1:5) == 'H2SO4' ) then
      kextsac=kx
    end if
  end do
  do kx=1,kint
    cname=aintf%tp(kx)%name
    if (cname(1:7) == 'SEASALT'   ) then
      kintss =kx
    end if
    if (cname(1:9) == '(NH4)2SO4' ) then
      kintso4=kx
    end if
    if (cname(1:6) == 'NH4NO3'    ) then
      kintno3=kx
    end if
    if (cname(1:5) == 'NH4CL'     ) then
      kintcl=kx
    end if
    if (cname(1:7) == 'ORGCARB'   ) then
      kintoc =kx
    end if
    if (cname(1:7) == 'BLCCARB'   ) then
      kintbc =kx
    end if
    if (cname(1:7) == 'MINDUST'   ) then
      kintmd =kx
    end if
    if (cname(1:5) == 'H2SO4' ) then
      kintsac=kx
    end if
  end do
  !
  !-----------------------------------------------------------------------
  !     * number of sections for externally resp. internally mixed
  !     * sea salt aerosol.
  !
  isextss=0
  isintss=0
  if (kextss > 0) isextss=aextf%tp(kextss)%isec
  if (kintss > 0) isintss=aintf%tp(kintss)%isce &
      -aintf%tp(kintss)%iscb+1
  !
  !     * number of additional chemical species for interally mixed aerosol
  !     * within the particle size range of the sea salt aerosol. make sure
  !     * that the other internal aerosol species fully overlap with the
  !     * sea salt size distribution.
  !
  isss=ina
  if (kintss > 0) then
    is=aintf%tp(kintss)%iscb
    isss=sintf%isaer(is)%itypt
    do is=aintf%tp(kintss)%iscb+1,aintf%tp(kintss)%isce
      issst=sintf%isaer(is)%itypt
      if (issst /= isss) then
        call xit('SDCONF',-13)
      else
        do it=1,issst
          if (sintf%isaer(is)%ityp(it) &
              /= sintf%isaer(is-1)%ityp(it) ) then
            call xit('SDCONF',-14)
          end if
        end do
      end if
    end do
    isss=isss-1
  end if
  !
  !     * aerosol tracer index for additional chemical species.
  !
  if (kintss > 0) then
    if (allocated(itss)) deallocate(itss)
    allocate(itss(isss))
    is=aintf%tp(kintss)%iscb
    itt=0
    do it=1,isss+1
      kx=sintf%isaer(is)%ityp(it)
      if (kx /= kintss) then
        itt=itt+1
        itss(itt)=kx
      end if
    end do
  end if
  !
  !     * index of sea salt aerosol tracers in aerosol mass and number
  !     * arrays.
  !
  if (kextss > 0) then
    if (allocated(iexss)) deallocate(iexss)
    allocate(iexss(isextss))
    isc=0
    do is=1,isaext
      kx=sextf%isaer(is)%ityp
      if (kx == kextss) then
        isc=isc+1
        iexss(isc)=is
      end if
    end do
  end if
  if (kintss > 0) then
    if (allocated(iinss)) deallocate(iinss)
    allocate(iinss(isintss))
    isc=0
    do is=1,isaint
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintss) then
          isc=isc+1
          iinss(isc)=is
        end if
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * number of sections for externally resp. internally mixed
  !     * sulphate aerosol.
  !
  isextso4=0
  isintso4=0
  if (kextso4 > 0) isextso4=aextf%tp(kextso4)%isec
  if (kintso4 > 0) isintso4=aintf%tp(kintso4)%isce &
      -aintf%tp(kintso4)%iscb+1
  !
  !     * number of additional chemical species for interally mixed aerosol
  !     * within the particle size range of the sulphate aerosol. make sure
  !     * that the other internal aerosol species fully overlap with the
  !     * sulphate size distribution.
  !
  iso4s=ina
  if (kintso4 > 0) then
    is=aintf%tp(kintso4)%iscb
    iso4s=sintf%isaer(is)%itypt
    do is=aintf%tp(kintso4)%iscb+1,aintf%tp(kintso4)%isce
      iso4st=sintf%isaer(is)%itypt
      if (iso4st /= iso4s) then
        call xit('SDCONF',-15)
      else
        do it=1,iso4st
          if (sintf%isaer(is)%ityp(it) &
              /= sintf%isaer(is-1)%ityp(it) ) then
            call xit('SDCONF',-16)
          end if
        end do
      end if
    end do
    iso4s=iso4s-1
  end if
  !
  !     * aerosol tracer index for additional chemical species.
  !
  if (kintso4 > 0) then
    if (allocated(itso4)) deallocate(itso4)
    allocate(itso4(iso4s))
    is=aintf%tp(kintso4)%iscb
    itt=0
    do it=1,iso4s+1
      kx=sintf%isaer(is)%ityp(it)
      if (kx /= kintso4) then
        itt=itt+1
        itso4(itt)=kx
      end if
    end do
  end if
  !
  !     * index of sulphate aerosol tracers in aerosol mass and number
  !     * arrays.
  !
  if (kextso4 > 0) then
    if (allocated(iexso4)) deallocate(iexso4)
    allocate(iexso4(isextso4))
    isc=0
    do is=1,isaext
      kx=sextf%isaer(is)%ityp
      if (kx == kextso4) then
        isc=isc+1
        iexso4(isc)=is
      end if
    end do
  end if
  if (kintso4 > 0) then
    if (allocated(iinso4)) deallocate(iinso4)
    allocate(iinso4(isintso4))
    isc=0
    do is=1,isaint
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintso4) then
          isc=isc+1
          iinso4(isc)=is
        end if
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * number of sections for externally resp. internally mixed
  !     * nitrate aerosol.
  !
  isextno3=0
  isintno3=0
  if (kextno3 > 0) isextno3=aextf%tp(kextno3)%isec
  if (kintno3 > 0) isintno3=aintf%tp(kintno3)%isce &
      -aintf%tp(kintno3)%iscb+1
  !
  !     * number of additional chemical species for interally mixed aerosol
  !     * within the particle size range of the nitrate aerosol. make sure
  !     * that the other internal aerosol species fully overlap with the
  !     * nitrate size distribution.
  !
  ino3s=ina
  if (kintno3 > 0) then
    is=aintf%tp(kintno3)%iscb
    ino3s=sintf%isaer(is)%itypt
    do is=aintf%tp(kintno3)%iscb+1,aintf%tp(kintno3)%isce
      ino3st=sintf%isaer(is)%itypt
      if (ino3st /= ino3s) then
        call xit('SDCONF',-17)
      else
        do it=1,ino3st
          if (sintf%isaer(is)%ityp(it) &
              /= sintf%isaer(is-1)%ityp(it) ) then
            call xit('SDCONF',-18)
          end if
        end do
      end if
    end do
    ino3s=ino3s-1
  end if
  !
  !     * aerosol tracer index for additional chemical species.
  !
  if (kintno3 > 0) then
    if (allocated(itno3)) deallocate(itno3)
    allocate(itno3(ino3s))
    is=aintf%tp(kintno3)%iscb
    itt=0
    do it=1,ino3s+1
      kx=sintf%isaer(is)%ityp(it)
      if (kx /= kintno3) then
        itt=itt+1
        itno3(itt)=kx
      end if
    end do
  end if
  !
  !     * index of nitrate aerosol tracers in aerosol mass and number
  !     * arrays.
  !
  if (kextno3 > 0) then
    if (allocated(iexno3)) deallocate(iexno3)
    allocate(iexno3(isextno3))
    isc=0
    do is=1,isaext
      kx=sextf%isaer(is)%ityp
      if (kx == kextno3) then
        isc=isc+1
        iexno3(isc)=is
      end if
    end do
  end if
  if (kintno3 > 0) then
    if (allocated(iinno3)) deallocate(iinno3)
    allocate(iinno3(isintno3))
    isc=0
    do is=1,isaint
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintno3) then
          isc=isc+1
          iinno3(isc)=is
        end if
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * number of sections for externally resp. internally mixed
  !     * chloride aerosol.
  !
  isextcl=0
  isintcl=0
  if (kextcl > 0) isextcl=aextf%tp(kextcl)%isec
  if (kintcl > 0) isintcl=aintf%tp(kintcl)%isce &
      -aintf%tp(kintcl)%iscb+1
  !
  !     * number of additional chemical species for interally mixed aerosol
  !     * within the particle size range of the chloride aerosol. make sure
  !     * that the other internal aerosol species fully overlap with the
  !     * chloride size distribution.
  !
  icls=ina
  if (kintcl > 0) then
    is=aintf%tp(kintcl)%iscb
    icls=sintf%isaer(is)%itypt
    do is=aintf%tp(kintcl)%iscb+1,aintf%tp(kintcl)%isce
      iclst=sintf%isaer(is)%itypt
      if (iclst /= icls) then
        call xit('SDCONF',-19)
      else
        do it=1,iclst
          if (sintf%isaer(is)%ityp(it) &
              /= sintf%isaer(is-1)%ityp(it) ) then
            call xit('SDCONF',-20)
          end if
        end do
      end if
    end do
    icls=icls-1
  end if
  !
  !     * aerosol tracer index for additional chemical species.
  !
  if (kintcl > 0) then
    if (allocated(itcl)) deallocate(itcl)
    allocate(itcl(icls))
    is=aintf%tp(kintcl)%iscb
    itt=0
    do it=1,icls+1
      kx=sintf%isaer(is)%ityp(it)
      if (kx /= kintcl) then
        itt=itt+1
        itcl(itt)=kx
      end if
    end do
  end if
  !
  !     * index of chloride aerosol tracers in aerosol mass and number
  !     * arrays.
  !
  if (kextcl > 0) then
    if (allocated(iexcl)) deallocate(iexcl)
    allocate(iexcl(isextcl))
    isc=0
    do is=1,isaext
      kx=sextf%isaer(is)%ityp
      if (kx == kextcl) then
        isc=isc+1
        iexcl(isc)=is
      end if
    end do
  end if
  if (kintcl > 0) then
    if (allocated(iincl)) deallocate(iincl)
    allocate(iincl(isintcl))
    isc=0
    do is=1,isaint
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintcl) then
          isc=isc+1
          iincl(isc)=is
        end if
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * number of sections for externally resp. internally mixed
  !     * organic carbon aerosol.
  !
  isextoc=0
  isintoc=0
  if (kextoc > 0) isextoc=aextf%tp(kextoc)%isec
  if (kintoc > 0) isintoc=aintf%tp(kintoc)%isce &
      -aintf%tp(kintoc)%iscb+1
  !
  !     * number of additional chemical species for interally mixed aerosol
  !     * within the particle size range of the organic carbon aerosol. make sure
  !     * that the other internal aerosol species fully overlap with the
  !     * organic carbon size distribution.
  !
  iocs=ina
  if (kintoc > 0) then
    is=aintf%tp(kintoc)%iscb
    iocs=sintf%isaer(is)%itypt
    do is=aintf%tp(kintoc)%iscb+1,aintf%tp(kintoc)%isce
      iocst=sintf%isaer(is)%itypt
      if (iocst /= iocs) then
        call xit('SDCONF',-21)
      else
        do it=1,iocst
          if (sintf%isaer(is)%ityp(it) &
              /= sintf%isaer(is-1)%ityp(it) ) then
            call xit('SDCONF',-22)
          end if
        end do
      end if
    end do
    iocs=iocs-1
  end if
  !
  !     * aerosol tracer index for additional chemical species.
  !
  if (kintoc > 0) then
    if (allocated(itoc)) deallocate(itoc)
    allocate(itoc(iocs))
    is=aintf%tp(kintoc)%iscb
    itt=0
    do it=1,iocs+1
      kx=sintf%isaer(is)%ityp(it)
      if (kx /= kintoc) then
        itt=itt+1
        itoc(itt)=kx
      end if
    end do
  end if
  !
  !     * index of organic carbon aerosol tracers in aerosol mass and number
  !     * arrays.
  !
  if (kextoc > 0) then
    if (allocated(iexoc)) deallocate(iexoc)
    allocate(iexoc(isextoc))
    isc=0
    do is=1,isaext
      kx=sextf%isaer(is)%ityp
      if (kx == kextoc) then
        isc=isc+1
        iexoc(isc)=is
      end if
    end do
  end if
  if (kintoc > 0) then
    if (allocated(iinoc)) deallocate(iinoc)
    allocate(iinoc(isintoc))
    isc=0
    do is=1,isaint
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintoc) then
          isc=isc+1
          iinoc(isc)=is
        end if
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * number of sections for externally resp. internally mixed
  !     * black carbon aerosol.
  !
  isextbc=0
  isintbc=0
  if (kextbc > 0) isextbc=aextf%tp(kextbc)%isec
  if (kintbc > 0) isintbc=aintf%tp(kintbc)%isce &
      -aintf%tp(kintbc)%iscb+1
  !
  !     * number of additional chemical species for interally mixed aerosol
  !     * within the particle size range of the black carbon aerosol. make sure
  !     * that the other internal aerosol species fully overlap with the
  !     * black carbon size distribution.
  !
  ibcs=ina
  if (kintbc > 0) then
    is=aintf%tp(kintbc)%iscb
    ibcs=sintf%isaer(is)%itypt
    do is=aintf%tp(kintbc)%iscb+1,aintf%tp(kintbc)%isce
      ibcst=sintf%isaer(is)%itypt
      if (ibcst /= ibcs) then
        call xit('SDCONF',-23)
      else
        do it=1,ibcst
          if (sintf%isaer(is)%ityp(it) &
              /= sintf%isaer(is-1)%ityp(it) ) then
            call xit('SDCONF',-24)
          end if
        end do
      end if
    end do
    ibcs=ibcs-1
  end if
  !
  !     * aerosol tracer index for additional chemical species.
  !
  if (kintbc > 0) then
    if (allocated(itbc)) deallocate(itbc)
    allocate(itbc(ibcs))
    is=aintf%tp(kintbc)%iscb
    itt=0
    do it=1,ibcs+1
      kx=sintf%isaer(is)%ityp(it)
      if (kx /= kintbc) then
        itt=itt+1
        itbc(itt)=kx
      end if
    end do
  end if
  !
  !     * index of black carbon aerosol tracers in aerosol mass and number
  !     * arrays.
  !
  if (kextbc > 0) then
    if (allocated(iexbc)) deallocate(iexbc)
    allocate(iexbc(isextbc))
    isc=0
    do is=1,isaext
      kx=sextf%isaer(is)%ityp
      if (kx == kextbc) then
        isc=isc+1
        iexbc(isc)=is
      end if
    end do
  end if
  if (kintbc > 0) then
    if (allocated(iinbc)) deallocate(iinbc)
    allocate(iinbc(isintbc))
    isc=0
    do is=1,isaint
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintbc) then
          isc=isc+1
          iinbc(isc)=is
        end if
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * number of sections for externally resp. internally mixed
  !     * min. dust aerosol.
  !
  isextmd=0
  isintmd=0
  if (kextmd > 0) isextmd=aextf%tp(kextmd)%isec
  if (kintmd > 0) isintmd=aintf%tp(kintmd)%isce &
      -aintf%tp(kintmd)%iscb+1
  !
  !     * number of additional chemical species for interally mixed aerosol
  !     * within the particle size range of the min. dust aerosol. make sure
  !     * that the other internal aerosol species fully overlap with the
  !     * min. dust size distribution.
  !
  imds=ina
  if (kintmd > 0) then
    is=aintf%tp(kintmd)%iscb
    imds=sintf%isaer(is)%itypt
    do is=aintf%tp(kintmd)%iscb+1,aintf%tp(kintmd)%isce
      imdst=sintf%isaer(is)%itypt
      if (imdst /= imds) then
        call xit('SDCONF',-25)
      else
        do it=1,imdst
          if (sintf%isaer(is)%ityp(it) &
              /= sintf%isaer(is-1)%ityp(it) ) then
            call xit('SDCONF',-26)
          end if
        end do
      end if
    end do
    imds=imds-1
  end if
  !
  !     * aerosol tracer index for additional chemical species.
  !
  if (kintmd > 0) then
    if (allocated(itmd)) deallocate(itmd)
    allocate(itmd(imds))
    is=aintf%tp(kintmd)%iscb
    itt=0
    do it=1,imds+1
      kx=sintf%isaer(is)%ityp(it)
      if (kx /= kintmd) then
        itt=itt+1
        itmd(itt)=kx
      end if
    end do
  end if
  !
  !     * index of min. dust aerosol tracers in aerosol mass and number
  !     * arrays.
  !
  if (kextmd > 0) then
    if (allocated(iexmd)) deallocate(iexmd)
    allocate(iexmd(isextmd))
    isc=0
    do is=1,isaext
      kx=sextf%isaer(is)%ityp
      if (kx == kextmd) then
        isc=isc+1
        iexmd(isc)=is
      end if
    end do
  end if
  if (kintmd > 0) then
    if (allocated(iinmd)) deallocate(iinmd)
    allocate(iinmd(isintmd))
    isc=0
    do is=1,isaint
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintmd) then
          isc=isc+1
          iinmd(isc)=is
        end if
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * number of sections for externally resp. internally mixed
  !     * sulphuric acid aerosol.
  !
  isextsac=0
  isintsac=0
  if (kextsac > 0) isextsac=aextf%tp(kextsac)%isec
  if (kintsac > 0) isintsac=aintf%tp(kintsac)%isce &
      -aintf%tp(kintsac)%iscb+1
  !
  !     * number of additional chemical species for interally mixed aerosol
  !     * within the particle size range of the sulphate aerosol. make sure
  !     * that the other internal aerosol species fully overlap with the
  !     * sulphate size distribution.
  !
  isacs=ina
  if (kintsac > 0) then
    is=aintf%tp(kintsac)%iscb
    isacs=sintf%isaer(is)%itypt
    do is=aintf%tp(kintsac)%iscb+1,aintf%tp(kintsac)%isce
      isacst=sintf%isaer(is)%itypt
      if (isacst /= isacs) then
        call xit('SDCONF',-15)
      else
        do it=1,isacst
          if (sintf%isaer(is)%ityp(it) &
              /= sintf%isaer(is-1)%ityp(it) ) then
            call xit('SDCONF',-16)
          end if
        end do
      end if
    end do
    isacs=isacs-1
  end if
  !
  !     * aerosol tracer index for additional chemical species.
  !
  if (kintsac > 0) then
    if (allocated(itsac)) deallocate(itsac)
    allocate(itsac(isacs))
    is=aintf%tp(kintsac)%iscb
    itt=0
    do it=1,isacs+1
      kx=sintf%isaer(is)%ityp(it)
      if (kx /= kintsac) then
        itt=itt+1
        itsac(itt)=kx
      end if
    end do
  end if
  !
  !     * index of sulphate aerosol tracers in aerosol mass and number
  !     * arrays.
  !
  if (kextsac > 0) then
    if (allocated(iexsac)) deallocate(iexsac)
    allocate(iexsac(isextsac))
    isc=0
    do is=1,isaext
      kx=sextf%isaer(is)%ityp
      if (kx == kextsac) then
        isc=isc+1
        iexsac(isc)=is
      end if
    end do
  end if
  if (kintsac > 0) then
    if (allocated(iinsac)) deallocate(iinsac)
    allocate(iinsac(isintsac))
    isc=0
    do is=1,isaint
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintsac) then
          isc=isc+1
          iinsac(isc)=is
        end if
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * arrays for coagulation.
  !
  if (kcoag) then
    if (isfint > 0) then
      if (allocated(fiphi)) deallocate(fiphi)
      allocate(fiphi  (isfint))
      if (allocated(fidryr)) deallocate(fidryr)
      allocate(fidryr (isfint))
      if (allocated(fidryrc)) deallocate(fidryrc)
      allocate(fidryrc(isfint))
      if (allocated(iro0))  deallocate(iro0)
      allocate(iro0   (isfint))
      if (allocated(iro)) deallocate(iro)
      allocate(iro    (isfint,isfint))
      if (allocated(itr)) deallocate(itr)
      allocate(itr    (isfint,isfint))
      if (allocated(itrm)) deallocate(itrm)
      allocate(itrm   (isfint,isfint))
    end if
    if (isfintb > 0) then
      if (allocated(fidryrb)) deallocate(fidryrb)
      allocate(fidryrb(isfintb))
      if (allocated(fidryvb)) deallocate(fidryvb)
      allocate(fidryvb(isfintb))
      if (allocated(fiphib0)) deallocate(fiphib0)
      allocate(fiphib0(isfint,isfint))
    end if
    !
    !       * pla section index corresponding to indices for coagulation
    !       * size distributions.
    !
    isc=0
    do is=1,isaint
      do ist=1,coagp%isec
        isc=isc+1
        iro0(isc)=is
      end do
    end do
    do is=1,isaint
      isi=sintf%isaer(is)%isi
      iscs=(isi-1)*coagp%isec+1
      isce=isi*coagp%isec
      iro(iscs:isce,iscs:isfint)=isi
      iro(iscs:isfint,iscs:isce)=isi
    end do
    itr=ina
    itrm=ina
    itri=0
    itrim=0
    do is=1,isfint
      do ik=1,is-1
        itri=itri+1
        itrim=itrim+1
        itr (is,ik)=itri
        itrm(is,ik)=itrim
      end do
      ik=is
      itri=itri+1
      itr (is,ik)=itri
    end do
    !
    !       * calculate sizes for dry size distribution for internally mixed
    !       * aerosol types for coagulation calculations.
    !
    dphic=coagp%dpstar
    isc=0
    do is=1,isaint
      isi=sintf%isaer(is)%isi
      isc=isc+1
      fiphi(isc)%vl=aintf%phi(isi)%vl
      fiphi(isc)%vr=aintf%phi(isi)%vl+dphic
      do ist=2,coagp%isec
        isc=isc+1
        fiphi(isc)%vl=fiphi(isc-1)%vr
        fiphi(isc)%vr=fiphi(isc-1)%vr+dphic
      end do
    end do
    !
    !       * calculate radii for dry size distribution for internally mixed
    !       * aerosol types for coagulation calculations.
    !
    do isc=1,isfint
      fidryr(isc)%vl=r0*exp(fiphi(isc)%vl)
      fidryr(isc)%vr=r0*exp(fiphi(isc)%vr)
    end do
    do isc=1,isfint
      dphic=fiphi(isc)%vr-fiphi(isc)%vl
      fidryrc(isc)=r0*exp(fiphi(isc)%vl+.5*dphic)
    end do
    if (isfintb > 0) then
      isc=0
      isx=0
      do is=1,isaint-1
        do ist=1,coagp%isec
          isc=isc+1
          isx=isx+1
          fidryrb(isc)=fidryr(isx)%vl
        end do
      end do
      do ist=1,coagp%isec
        isc=isc+1
        isx=isx+1
        fidryrb(isc)=fidryr(isx)%vl
      end do
      isc=isc+1
      fidryrb(isc)=fidryr(isx)%vr
    end if
    fidryvb=fidryrb**3
    !
    !       * projected particle sizes for dry sizes. it is assumed
    !       * that the growth factors for the wet particle size are
    !       * identical for each section so that the following particle
    !       * sizes can easily converted into projected wet particle
    !       * sizes.
    !
    fiphib0=yna
    do is=1,isfint
      do ik=1,is
        fiphib0(is,ik)=fiphi(1)%vl+(1./3.)*log(real(2**(is)) &
                                   -exp(3.*(fiphi(ik)%vl-fiphi(1)%vl)))
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * summarize aerosol parameters.
  !
  if (mynode==0) call sdsum

end subroutine sdconf
