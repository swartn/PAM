!> \file
!> \brief Various terms in coagulation equation for aerosol mass.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine coagm(dmdts,ctm,kern,fifrcs,fiddn,fmom0,fmom3,fmom6, &
                       fmom0i,fmom0d,fmom3i,fmom3d,volgf,ilga,leva)
  !
  use sdparm, only : iro,isaint,isfint,isftri,isftrim,itr,itrm,ycnst
  use fpdef,  only : r8
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(out), dimension(ilga,leva,isfint,isaint) :: dmdts !< Mass tendency
  real(r8), intent(in), dimension(ilga,leva,isfint) :: fmom0 !< Moment of size distribution
  real(r8), intent(in), dimension(ilga,leva,isfint) :: fmom3 !< Moment of size distribution
  real(r8), intent(in), dimension(ilga,leva,isfint) :: fmom6 !< Moment of size distribution
  real, intent(in), dimension(ilga,leva,isfint) :: fifrcs !< Aerosol species mass fraction for coagulation calculations
  real, intent(in), dimension(ilga,leva,isfint) :: fiddn !< Density of dry aerosol particle \f$[kg/m^3]\f$, internal mixture
  !< for coagulation calculations
  real(r8), intent(in), dimension(ilga,leva,isftrim) :: fmom0i !< Coefficient for linear volume interpolation of
  !< partial moments between adjacent section boundaries
  real(r8), intent(in), dimension(ilga,leva,isftrim) :: fmom0d !< Coefficient for linear volume interpolation of
  !< partial moments between adjacent section boundaries
  real(r8), intent(in), dimension(ilga,leva,isftrim) :: fmom3i !< Coefficient for linear volume interpolation of
  !< partial moments between adjacent section boundaries
  real(r8), intent(in), dimension(ilga,leva,isftrim) :: fmom3d !< Coefficient for linear volume interpolation of
  !< partial moments between adjacent section boundaries
  real, intent(in), dimension(ilga,leva,isftri) :: kern !< Coagulation coefficient (kernel)
  real, intent(in), dimension(ilga,leva,isftri) :: ctm !<
  real, intent(in), dimension(ilga,leva) :: volgf !< Volume growth factor
  !
  !     internal work variables
  !
  real(r8), dimension(ilga,leva) :: dmdtt !<
  integer :: is !<
  integer :: ik !<
  integer :: itsm !<
  integer :: irx !<
  integer :: its !<
  integer :: ir1 !<
  integer :: ir2 !<
  !
  !-----------------------------------------------------------------------
  !
  dmdts=0.
  !
  !     * first and third term in mass tendency equation.
  !
  do is=2,isfint
    ir1=is-1
    ir2=ir1
    its=itr(ir1,ir2)
    dmdtt=fifrcs(:,:,ir1)*ctm(:,:,its)
    irx=iro(ir1,ir2)
    dmdts(:,:,is-1,irx)=dmdts(:,:,is-1,irx)-dmdtt
    dmdts(:,:,is  ,irx)=dmdts(:,:,is  ,irx)+dmdtt
  end do
  is=isfint+1
  ir1=is-1
  ir2=ir1
  its=itr(ir1,ir2)
  dmdtt=fifrcs(:,:,ir1)*ctm(:,:,its)
  irx=iro(ir1,ir2)
  dmdts(:,:,is-1,irx)=dmdts(:,:,is-1,irx)-dmdtt
  !
  !     * second and fourth term in mass tendency equation.
  !
  do is=3,isfint
    ir1=is-1
    do ik=1,is-2
      ir2=ik
      its=itr(ir1,ir2)
      itsm=itrm(ir1,ir2)
      dmdtt= fifrcs(:,:,is-1) &
                 *(fmom0(:,:,ik)*kern(:,:,its)*fmom3i(:,:,itsm) &
                 +fmom3(:,:,ik)*kern(:,:,its)*fmom3d(:,:,itsm)) &
                 +fifrcs(:,:,ik) &
                 *(fmom3(:,:,ik)*kern(:,:,its)*fmom0i(:,:,itsm) &
                 +fmom6(:,:,ik)*kern(:,:,its)*fmom0d(:,:,itsm))
      irx=iro(ir1,ir2)
      dmdts(:,:,is-1,irx)=dmdts(:,:,is-1,irx)-dmdtt
      dmdts(:,:,is  ,irx)=dmdts(:,:,is  ,irx)+dmdtt
    end do
  end do
  is=isfint+1
  ir1=is-1
  do ik=1,is-2
    ir2=ik
    its=itr(ir1,ir2)
    itsm=itrm(ir1,ir2)
    dmdtt= fifrcs(:,:,is-1) &
               *(fmom0(:,:,ik)*kern(:,:,its)*fmom3i(:,:,itsm) &
               +fmom3(:,:,ik)*kern(:,:,its)*fmom3d(:,:,itsm)) &
               +fifrcs(:,:,ik) &
               *(fmom3(:,:,ik)*kern(:,:,its)*fmom0i(:,:,itsm) &
               +fmom6(:,:,ik)*kern(:,:,its)*fmom0d(:,:,itsm))
    irx=iro(ir1,ir2)
    dmdts(:,:,is-1,irx)=dmdts(:,:,is-1,irx)-dmdtt
  end do
  !
  !     * fifth and sixth term in mass tendency equation.
  !
  is=1
  ir1=is
  do ik=1,is-1
    ir2=ik
    its=itr(ir1,ir2)
    dmdtt=-fifrcs(:,:,ir2)*ctm(:,:,its)
    irx=iro(ir1,ir2)
    dmdts(:,:,is,irx)=dmdts(:,:,is,irx)+dmdtt
  end do
  do irx=1,isaint
    do is=1,isfint
      dmdts(:,:,is,irx)=dmdts(:,:,is,irx) &
                                        *ycnst*fiddn(:,:,is)/volgf(:,:)
    end do
  end do
  !
end subroutine coagm
