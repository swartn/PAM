!> \file
!> \brief Mapping of the PLA size distribution onto a piecewise linear size
!>       distribution with doubling of volume between section boundaries
!>       for coagulation calculations.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine sdmap(fil1,fil2,fiddn,fifrc,fivolw,fivola,fiphic,volgf, &
                       piddn,pifrc,piwetrc,pimas,pipsi,ilga,leva)
  !
  use sdparm, only : coagp,fidryvb,fiphi,fiphib0, &
                         isaext,isaint,isfint,isfintb,isftri, &
                         kint,pidryrc,r0,yna,ytiny
  use fpdef,  only : r8
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: piwetrc !< Total/wet paricle radius \f$[m\f$, int. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: piddn !< Density of dry aerosol particle \f$[kg/m^3]\f$, internal mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) int. mixture
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol species mass fraction
  real(r8), intent(inout), dimension(ilga,leva,isfint) :: fil1 !< Particle size at lower boundary of section
  real(r8), intent(inout), dimension(ilga,leva,isfint) :: fil2 !< Particle size at lower boundary of section
  real, intent(out), dimension(ilga,leva) :: volgf !< Volume growth factor
  real, intent(out), dimension(ilga,leva,isfint) :: fiphic !< Section width
  real, intent(out), dimension(ilga,leva,isfint) :: fiddn !< Density of dry aerosol particle \f$[kg/m^3]\f$, internal mixture
  !< for coagulation calculations
  real, intent(out), dimension(ilga,leva,isfintb) :: fivolw !< Volume
  real, intent(out), dimension(ilga,leva,isfint,kint) :: fifrc !< Aerosol species mass fraction for coagulation calculations
  real, intent(out), dimension(ilga,leva,isftri) :: fivola !< Volume
  !
  !     internal work variables
  !
  real, dimension(ilga,leva,isftri) :: term !<
  real, dimension(ilga,leva,isftri) :: fiphia0 !<
  real, dimension(ilga,leva) :: totm !<
  real, dimension(ilga,leva) :: totx !<
  real, dimension(ilga,leva) :: grwthf !<
  real, dimension(ilga,leva) :: dphit !<
  logical, dimension(ilga,leva) :: kok !<
  integer :: is !<
  integer :: ist !<
  integer :: isc !<
  integer :: itri !<
  integer :: ik !<
  real :: r03 !<
  !
  !-----------------------------------------------------------------------
  !     * mass-weighted particle size growth factor. in the following,
  !     * it is applied to all size sections so that there are no
  !     * overlapping size sections for the wet size distribution. note
  !     * that the code currently does not permit different growth
  !     * factors for individual sections.
  !
  totm=0.
  totx=0.
  kok=.true.
  do is=1,isaint
    where (abs(pipsi(:,:,is)-yna) > ytiny)
      totx(:,:)=totx(:,:)+max(piwetrc(:,:,is)/pidryrc(is),1.) &
                                 *pimas(:,:,is)
      totm(:,:)=totm(:,:)+pimas(:,:,is)
    else where
      kok(:,:)=.false.
    end where
  end do
  grwthf=1.
  where (2+abs(exponent(totx) - exponent(totm)) &
      < maxexponent(totx) .and. totm/=0. .and. kok)
    grwthf=max(totx/totm,1.)
  end where
  !
  dphit=log(grwthf)
  volgf=grwthf**3.
  !
  !     * particle size at lower boundary of each section and section
  !     * width.
  !
  do is=1,isfintb
    fivolw(:,:,is)=fidryvb(is)*volgf(:,:)
  end do
  do is=1,isfint
    fil1  (:,:,is)=fil1(:,:,is)/volgf(:,:)
    fil2  (:,:,is)=fil2(:,:,is)/volgf(:,:)**2
    fiphic(:,:,is)=fiphi(is)%vl+dphit(:,:)+.5*coagp%dpstar
  end do
  !
  !     * copy pla parameters into corresponding arrays for coagulation
  !     * calculations.
  !
  isc=0
  do is=1,isaint
    do ist=1,coagp%isec
      isc=isc+1
      fiddn (:,:,isc)  =piddn (:,:,is)
      fifrc (:,:,isc,:)=pifrc (:,:,is,:)
    end do
  end do
  itri=0
  do is=1,isfint
    do ik=1,is
      itri=itri+1
      fiphia0(:,:,itri)=fiphib0(is,ik)+dphit(:,:)
    end do
  end do
  term=3.*fiphia0
  fivola=exp(term)
  r03=r0**3
  fivola=r03*fivola
  !
end subroutine sdmap
