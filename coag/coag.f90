!> \file
!> \brief Brownian Coagulation
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine coag(pidndt,pidmdt,pidfdt,pinum,pimas,pifrc,pin0, &
                      piphi0,pipsi,piddn,piwetrc,ta,pa,dt,ilga,leva)
  !
  use sdparm, only : coagp,fidryvb,fiphi, &
                         isaint,isfint,isfintb,isftri,isftrim,itr, &
                         kcoag,kint,ycnst,ylarge,ylarges,yna,ysec,ytiny
  use coadat, only : cker,cphi,cpre,ctem,ida,idb,idd
  use fpdef,  only : r8
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(out), dimension(ilga,leva,isaint) :: pidndt !< Particle number coagulation tendency \f$[kg/kg/sec]\f$, internal mixture
  real, intent(out), dimension(ilga,leva,isaint) :: pidmdt !< Particle mass coagulation tendency \f$[kg/kg/sec]\f$, internal mixture
  real, intent(out), dimension(ilga,leva,isaint,kint) :: pidfdt !< Aerosol species mass fraction tendency \f$[1/sec]\f$, internal mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$, amplitude) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$, width), int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pinum !< Aerosol number concentration for internally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: piwetrc !< Total/wet paricle radius \f$[m]\f$, int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piddn !< Density of dry aerosol particle \f$[kg/m^3]\f$, internal mixture
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol species mass fraction
  real, intent(in), dimension(ilga,leva) :: ta !< Air temperature \f$[K]\f$
  real, intent(in), dimension(ilga,leva) :: pa !< Air pressure \f$[Pa]\f$
  real, intent(in) :: dt !< Model time step \f$[s]\f$
  !
  !     internal work variables
  !
  integer, parameter :: iss=1 !<
  integer, parameter :: imom3=1. !<
  integer, parameter :: imom6=2. !<
  real, dimension(ilga,leva,isaint) :: pil1 !<
  real, dimension(ilga,leva,isaint) :: pil2 !<
  real, dimension(ilga,leva,isaint) :: timast !<
  real, dimension(ilga,leva,isaint) :: tinum !<
  real, dimension(ilga,leva,isaint,kint) :: timas !<
  real(r8), dimension(ilga,leva,isfint) :: fmom0 !<
  real(r8), dimension(ilga,leva,isfint) :: fmom3 !<
  real(r8), dimension(ilga,leva,isfint) :: fmom6 !<
  real, dimension(ilga,leva,isfint) :: fiddn !<
  real, dimension(ilga,leva,isfint) :: timatn !<
  real, dimension(ilga,leva,isfint) :: timat !<
  real, dimension(ilga,leva,isfint) :: fifrcs !<
  real, dimension(ilga,leva,isfint,kint) :: tima !<
  real, dimension(ilga,leva,isfint,kint) :: fifrc !<
  real, dimension(ilga,leva,isftri) :: kern !<
  real, dimension(ilga,leva,isftri) :: ctm !<
  real(r8), dimension(ilga,leva,iss) :: fil1s !<
  real(r8), dimension(ilga,leva,iss) :: fil2s !<
  real(r8), dimension(ilga,leva,iss) :: volls !<
  real(r8), dimension(ilga,leva,iss) :: volrs !<
  real(r8), dimension(ilga,leva,iss) :: sdtmp !<
  real, dimension(ilga,leva) :: volgf !<
  real, dimension(ilga,leva) :: dtc !<
  real, dimension(ilga,leva) :: dndtt !<
  real, dimension(ilga,leva) :: adiff !<
  real, dimension(ilga,leva) :: adt !<
  real, dimension(ilga,leva) :: temp !<
  real, dimension(ilga,leva) :: pres !<
  real, dimension(ilga,leva) :: ratm !<
  real, dimension(ilga,leva) :: totm1 !<
  real, dimension(ilga,leva) :: totm2 !<
  real(r8), allocatable, dimension(:,:,:) :: fmom0s !<
  real(r8), allocatable, dimension(:,:,:) :: fmom3s !<
  real(r8), allocatable, dimension(:,:,:) :: fmom0i !<
  real(r8), allocatable, dimension(:,:,:) :: fmom0d !<
  real(r8), allocatable, dimension(:,:,:) :: fmom3i !<
  real(r8), allocatable, dimension(:,:,:) :: fmom3d !<
  real(r8), allocatable, dimension(:,:,:) :: fil1 !<
  real(r8), allocatable, dimension(:,:,:) :: fil2 !<
  real(r8), allocatable, dimension(:,:,:) :: finsd !<
  real(r8), allocatable, dimension(:,:,:) :: fivoll !<
  real(r8), allocatable, dimension(:,:,:) :: fivolr !<
  real(r8), allocatable, dimension(:,:,:) :: par1n !<
  real(r8), allocatable, dimension(:,:,:) :: par2n !<
  real(r8), allocatable, dimension(:,:,:) :: par1m !<
  real(r8), allocatable, dimension(:,:,:) :: par2m !<
  real(r8), allocatable, dimension(:,:,:) :: piscl1 !<
  real(r8), allocatable, dimension(:,:,:) :: piscl2 !<
  real, allocatable, dimension(:,:,:) :: fiphic !<
  real, allocatable, dimension(:,:,:) :: term !<
  real, allocatable, dimension(:,:,:) :: term1 !<
  real, allocatable, dimension(:,:,:) :: fivola !<
  real, allocatable, dimension(:,:,:) :: fivolw !<
  real, allocatable, dimension(:,:,:,:) :: dndts !<
  real, allocatable, dimension(:,:,:,:) :: dndt !<
  real, allocatable, dimension(:,:,:,:) :: dmdts !<
  real, allocatable, dimension(:,:,:,:,:) :: dmdt !<
  logical, allocatable, dimension(:,:,:) :: pitst !<
  integer, dimension(ilga,leva) :: idt !<
  integer, dimension(ilga,leva) :: idp !<
  integer, dimension(ilga,leva,isfint) :: idr !<
  real(r8) :: at1 !<
  real(r8) :: at2 !<
  real(r8) :: at3 !<
  integer :: i !<
  integer :: ik !<
  integer :: il !<
  integer :: ir1 !<
  integer :: ir2 !<
  integer :: is !<
  integer :: isc !<
  integer :: ist !<
  integer :: it !<
  integer :: itt !<
  integer :: its !<
  integer :: itri !<
  integer :: itrim !<
  integer :: k !<
  integer :: l !<
  !
  real, parameter :: tune=1. !<
  integer, parameter :: imeth=2 !<
  !
  !-----------------------------------------------------------------------
  !     * initialization.
  !
  if (isaint > 0) then
    pidndt=0.
    pidmdt=0.
    pidfdt=0.
  end if
  !
  if (kcoag) then
    !
    !       * allocate work arrays.
    !
    allocate (finsd(ilga,leva,isfintb))
    allocate (term (ilga,leva,isfintb))
    allocate (term1(ilga,leva,isfintb))
    allocate (fil1   (ilga,leva,isfint))
    allocate (fil2   (ilga,leva,isfint))
    !
    !-----------------------------------------------------------------------
    !       * parameters for piecewise linear representation of size
    !       * distribution. the use of linear distributions leads to
    !       * more efficient calculations in the following compared
    !       * lognormal distributions despite somewhat reduced accuracy.
    !
    !       * number size distribution [d n/d r**3] for boundaries of
    !       * coagulation size sections from pla size distribution.
    !
    term=0.
    isc=0
    do is=1,isaint
      do ist=1,coagp%isec
        isc=isc+1
        where (abs(pipsi(:,:,is)-yna) > ytiny &
            .and. pipsi(:,:,is) < ylarges)
          term(:,:,isc)=-pipsi(:,:,is) &
                                     *(fiphi(isc)%vl-piphi0(:,:,is))**2
        end where
      end do
    end do
    is=isaint
    isc=isfintb
    where (abs(pipsi(:,:,is)-yna) > ytiny &
        .and. pipsi(:,:,is) < ylarges)
      term(:,:,isc)=-pipsi(:,:,is) &
                                   *(fiphi(isc-1)%vr-piphi0(:,:,is))**2
    end where
    term1=exp(term)
    finsd=0._r8
    isc=0
    do is=1,isaint
      do ist=1,coagp%isec
        isc=isc+1
        where (abs(pipsi(:,:,is)-yna) > ytiny &
            .and. pinum(:,:,is) > ytiny .and. pimas(:,:,is) > ytiny)
          finsd(:,:,isc)=pin0(:,:,is)*term1(:,:,isc) &
                                                  /(3._r8*fidryvb(isc))
        end where
      end do
    end do
    is=isaint
    isc=isfintb
    where (abs(pipsi(:,:,is)-yna) > ytiny &
        .and. pinum(:,:,is) > ytiny .and. pimas(:,:,is) > ytiny)
      finsd(:,:,isc)=pin0(:,:,is)*term1(:,:,isc) &
                                                  /(3._r8*fidryvb(isc))
    end where
    !
    !       * parameters for piece-wise linear representation of number
    !       * size distribution [d n/d r**3 = fil1 + fil2 * r**3] from
    !       * linear interpolation of number size distribution [finsd]
    !       * between section boundaries.
    !
    fil1=0._r8
    fil2=0._r8
    isc=0
    do is=1,isaint
      do ist=1,coagp%isec
        isc=isc+1
        where (abs(pipsi(:,:,is)-yna) > ytiny &
            .and. pinum(:,:,is) > ytiny .and. pimas(:,:,is) > ytiny)
          fil2(:,:,isc)=(finsd(:,:,isc+1)-finsd(:,:,isc)) &
                         /(fidryvb(isc+1)-fidryvb(isc))
          fil1(:,:,isc)=finsd(:,:,isc)-fil2(:,:,isc)*fidryvb(isc)
        end where
      end do
    end do
    !
    !       * deallocate and allocate work arrays.
    !
    deallocate (finsd)
    deallocate (term)
    deallocate (term1)
    allocate (par1n (ilga,leva,isaint))
    allocate (par2n (ilga,leva,isaint))
    allocate (par1m (ilga,leva,isaint))
    allocate (par2m (ilga,leva,isaint))
    allocate (piscl1(ilga,leva,isaint))
    allocate (piscl2(ilga,leva,isaint))
    allocate (pitst (ilga,leva,isaint))
    !
    !       * adjust parameters for individual pieces of linear distributions
    !       * so that the total number and mass is conserved within the
    !       * size ranges given by the original pla distributions. this is
    !       * accomplished by scaling the parameters fil1 and fil2 linearly.
    !       * par1n+par2n is the number and parm1+parm2 is the mass
    !       * coressponding to the linear fit.
    !
    par1n=0._r8
    par2n=0._r8
    par1m=0._r8
    par2m=0._r8
    isc=0
    do is=1,isaint
      do ist=1,coagp%isec
        isc=isc+1
        at1=               fidryvb(isc+1)   -fidryvb(isc)
        at2=.5_r8        *(fidryvb(isc+1)**2-fidryvb(isc)**2)
        at3=(1._r8/3._r8)*(fidryvb(isc+1)**3-fidryvb(isc)**3)
        par1n(:,:,is)=par1n(:,:,is)+at1*fil1(:,:,isc)
        par2n(:,:,is)=par2n(:,:,is)+at2*fil2(:,:,isc)
        par1m(:,:,is)=par1m(:,:,is)+at2*fil1(:,:,isc)
        par2m(:,:,is)=par2m(:,:,is)+at3*fil2(:,:,isc)
      end do
    end do
    if (imeth == 1) then
      pitst=.false.
      do is=1,isaint
        do l=1,leva
          do il=1,ilga
            if (pinum(il,l,is) > ytiny .and. pimas(il,l,is) > ytiny &
                ) then
              piscl2(il,l,is)=pimas(il,l,is)/(ycnst*piddn(il,l,is))
              if (2+abs(exponent(piscl2(il,l,is))-exponent(par1m(il,l,is))) &
                  < maxexponent(piscl2(il,l,is)) .and. par1m(il,l,is)/=0. ) then
                piscl2(il,l,is)=piscl2(il,l,is)/par1m(il,l,is)
                pitst(il,l,is)=.true.
              end if
              piscl1(il,l,is)=pinum(il,l,is)
              if (2+abs(exponent(piscl1(il,l,is))-exponent(par1n(il,l,is))) &
                  < maxexponent(piscl1(il,l,is)) .and. par1n(il,l,is)/=0. &
                  .and. pitst(il,l,is) ) then
                piscl1(il,l,is)=piscl1(il,l,is)/par1n(il,l,is)
              else
                pitst(il,l,is)=.false.
              end if
              if (pitst(il,l,is) ) piscl2(il,l,is) &
                  =piscl2(il,l,is)-piscl1(il,l,is)
              if (2+abs(exponent(par2m(il,l,is))-exponent(par1m(il,l,is))) &
                  < maxexponent(par2m(il,l,is)) .and. par1m(il,l,is)/=0. &
                  .and. 2+abs(exponent(par2n(il,l,is))-exponent(par1n(il,l,is))) &
                  < maxexponent(par2n(il,l,is)) .and. par1n(il,l,is)/=0. &
                  .and. pitst(il,l,is) ) then
                piscl1(il,l,is)=par2m(il,l,is)/par1m(il,l,is) &
                               -par2n(il,l,is)/par1n(il,l,is)
              else
                pitst(il,l,is)=.false.
              end if
              if (2+abs(exponent(piscl2(il,l,is))-exponent(piscl1(il,l,is))) &
                  < maxexponent(piscl2(il,l,is)) .and. piscl1(il,l,is)/=0. &
                  .and. pitst(il,l,is) ) then
                piscl2(il,l,is)=piscl2(il,l,is)/piscl1(il,l,is)
              else
                pitst(il,l,is)=.false.
              end if
              piscl1(il,l,is)=pinum(il,l,is) &
                             -par2n(il,l,is)*piscl2(il,l,is)
              if (2+abs(exponent(piscl1(il,l,is))-exponent(par1n(il,l,is))) &
                  < maxexponent(piscl1(il,l,is)) .and. par1n(il,l,is)/=0. &
                  .and. pitst(il,l,is) ) then
                piscl1(il,l,is)=piscl1(il,l,is)/par1n(il,l,is)
              else
                pitst(il,l,is)=.false.
              end if
            end if
          end do
        end do
      end do
      where ( .not.pitst)
        piscl1=1._r8
        piscl2=1._r8
      end where
    else
      where (2+abs(exponent(pinum)-exponent(par1n+par2n)) &
          < maxexponent(pinum) .and.(par1n+par2n)/=0. )
        piscl1=pinum/(par1n+par2n)
      else where
        piscl1=1._r8
      end where
      where (2+abs(exponent(pimas/(ycnst*piddn))-exponent(par1m+par2m)) &
          < maxexponent(pimas/(ycnst*piddn)) .and.(par1m+par2m)/=0. )
        piscl2=(pimas/(ycnst*piddn))/(par1m+par2m)
      else where
        piscl2=1._r8
      end where
      piscl1=.5*(piscl1+piscl2)
      piscl2=piscl1
    end if
    isc=0
    do is=1,isaint
      do ist=1,coagp%isec
        isc=isc+1
        fil1(:,:,isc)=fil1(:,:,isc)*piscl1(:,:,is)
        fil2(:,:,isc)=fil2(:,:,isc)*piscl2(:,:,is)
      end do
    end do
    !
    !       * deallocate and allocate work arrays.
    !
    deallocate (par1n)
    deallocate (par2n)
    deallocate (par1m)
    deallocate (par2m)
    deallocate (piscl1)
    deallocate (piscl2)
    deallocate (pitst)
    allocate (fiphic (ilga,leva,isfint))
    allocate (fmom0s (ilga,leva,isftri))
    allocate (fmom3s (ilga,leva,isftri))
    allocate (fivola (ilga,leva,isftri))
    allocate (fivolw (ilga,leva,isfintb))
    allocate (fivoll (ilga,leva,isfint))
    allocate (fivolr (ilga,leva,isfint))
    !
    !       * map pla size distribution onto appropriate size distribution
    !       * for coagulation calculations. in the following, the size
    !       * distribution for coagulation calculations is for a doubling
    !       * of particle volume between section boundaries and for wet
    !       * particle sizes.
    !
    call sdmap(fil1,fil2,fiddn,fifrc,fivolw,fivola,fiphic,volgf, &
                   piddn,pifrc,piwetrc,pimas,pipsi,ilga,leva)
    !
    !       * temperature, pressure, and particle size corresponding
    !       * to tabulated coagulation data.
    !
    adiff=ylarge
    do itt=1,ida
      adt=abs(ta-ctem(itt))
      do i=1,ilga
        do k=1,leva
          if (adt(i,k) < adiff(i,k) ) then
            idt(i,k)=itt
            adiff(i,k)=adt(i,k)
          end if
        end do
      end do
    end do
    adiff=ylarge
    do itt=1,idd
      adt=abs(pa-cpre(itt))
      do i=1,ilga
        do k=1,leva
          if (adt(i,k) < adiff(i,k) ) then
            idp(i,k)=itt
            adiff(i,k)=adt(i,k)
          end if
        end do
      end do
    end do
    do is=1,isfint
      adiff=ylarge
      do itt=1,idb
        adt=abs(fiphic(:,:,is)-cphi(itt))
        do i=1,ilga
          do k=1,leva
            if (adt(i,k) < adiff(i,k) ) then
              idr(i,k,is)=itt
              adiff(i,k)=adt(i,k)
            end if
          end do
        end do
      end do
    end do
    !
    !       * coagulation coefficient (kernel).
    !
    itri=0
    do is=1,isfint
      do ik=1,is
        itri=itri+1
        do l=1,leva
          do il=1,ilga
            kern(il,l,itri)=tune &
                 *cker(idr(il,l,is),idr(il,l,ik),idt(il,l),idp(il,l))
          end do
        end do
      end do
    end do
    !
    !       * moments of size distribution for complete sections.
    !
    fivoll=fivolw(:,:,1:isfint)
    fivolr=fivolw(:,:,2:isfintb)
    !       calculate the integral of fil1+fil2*vol
    fmom0=fil1*(fivolr-fivoll)+.5_r8*fil2*(fivolr**2-fivoll**2)
    !       calculate the integral of vol**imom*(fil1+fil2*vol)
    fmom3=(1._r8/(imom3+1))*fil1*(fivolr**(imom3+1)-fivoll**(imom3+1))+ &
              (1._r8/(imom3+2))*fil2*(fivolr**(imom3+2)-fivoll**(imom3+2))
    fmom6=(1._r8/(imom6+1))*fil1*(fivolr**(imom6+1)-fivoll**(imom6+1))+ &
              (1._r8/(imom6+2))*fil2*(fivolr**(imom6+2)-fivoll**(imom6+2))
    !
    !       * moments over parts of sections for calculation of main terms.
    !
    itri=0
    do is=1,isfint
      do ik=1,is
        itri=itri+1
        volrs(:,:,iss)=fivolw(:,:,is+1)
        volls(:,:,iss)=min(fivola(:,:,itri),fivolw(:,:,is+1))
        fil1s(:,:,iss)=fil1(:,:,is)
        fil2s(:,:,iss)=fil2(:,:,is)
        !         calculate the integral of fil1+fil2*vol
        sdtmp=fil1s*(volrs-volls)+.5_r8*fil2s*(volrs**2-volls**2)
        fmom0s(:,:,itri)=sdtmp(:,:,iss)
        !         calculate the integral of vol**imom*(fil1+fil2*vol)
        sdtmp=(1._r8/(imom3+1))*fil1s*(volrs**(imom3+1)-volls**(imom3+1))+ &
                (1._r8/(imom3+2))*fil2s*(volrs**(imom3+2)-volls**(imom3+2))
        fmom3s(:,:,itri)=sdtmp(:,:,iss)
      end do
    end do

    !
    !       * deallocate work arrays.
    !
    deallocate (fil1)
    deallocate (fil2)
    deallocate (fiphic)
    deallocate (fivoll)
    deallocate (fivolr)
    deallocate (fivola)
    allocate (fmom0i(ilga,leva,isftrim))
    allocate (fmom0d(ilga,leva,isftrim))
    allocate (fmom3i(ilga,leva,isftrim))
    allocate (fmom3d(ilga,leva,isftrim))
    !
    !       * coefficients for linear volume interpolation of partial moments
    !       * between adjacent section boundaries.
    !
    itri=0
    itrim=0
    do is=1,isfint
      do ik=1,is-1
        itri=itri+1
        itrim=itrim+1
        fmom0i(:,:,itrim)=fmom0s(:,:,itri)-fivolw(:,:,ik) &
                                     /(fivolw(:,:,ik+1)-fivolw(:,:,ik)) &
                            *(fmom0s(:,:,itri+1)-fmom0s(:,:,itri))
        fmom3i(:,:,itrim)=fmom3s(:,:,itri)-fivolw(:,:,ik) &
                                     /(fivolw(:,:,ik+1)-fivolw(:,:,ik)) &
                            *(fmom3s(:,:,itri+1)-fmom3s(:,:,itri))
        fmom0d(:,:,itrim)=(fmom0s(:,:,itri+1) &
                   -fmom0s(:,:,itri))/(fivolw(:,:,ik+1)-fivolw(:,:,ik))
        fmom3d(:,:,itrim)=(fmom3s(:,:,itri+1) &
                   -fmom3s(:,:,itri))/(fivolw(:,:,ik+1)-fivolw(:,:,ik))
      end do
      itri=itri+1
    end do
    !
    !       * deallocate work arrays.
    !
    deallocate (fmom0s)
    deallocate (fmom3s)
    deallocate (fivolw)
    allocate (dndts(ilga,leva,isfint,isaint))
    allocate (dndt (ilga,leva,isaint,isaint))
    !
    !-----------------------------------------------------------------------
    !       * number tendency equation.
    !
    call coagn(dndts,kern,fmom0,fmom3,fmom0i,fmom0d,ilga,leva)
    !
    !-----------------------------------------------------------------------
    !       * sum up tendencies to match pla sections.
    !
    dndt=0.
    isc=0
    do is=1,isaint
      do ist=1,coagp%isec
        isc=isc+1
        where (abs(dndts(:,:,isc,:)) > ytiny)
          dndt(:,:,is,:)=dndt(:,:,is,:)+dndts(:,:,isc,:)
        end where
      end do
    end do
    !
    !       * deallocate and allocate work arrays.
    !
    deallocate (dndts)
    allocate (dmdt (ilga,leva,isaint,isaint,kint))
    allocate (dmdts(ilga,leva,isfint,isaint))
    !
    !-----------------------------------------------------------------------
    !       * mass tendency equation. sum up tendencies to match
    !       * pla sections.
    !
    do ir1=1,isfint
      do ir2=1,ir1
        its=itr(ir1,ir2)
        ctm(:,:,its)=kern(:,:,its)*fmom0(:,:,ir1)*fmom3(:,:,ir2)
      end do
    end do
    dmdt=0.
    do it=1,kint
      fifrcs(:,:,:)=fifrc(:,:,:,it)
      call coagm(dmdts,ctm,kern,fifrcs,fiddn,fmom0,fmom3,fmom6, &
                     fmom0i,fmom0d,fmom3i,fmom3d,volgf,ilga,leva)
      isc=0
      do is=1,isaint
        do ist=1,coagp%isec
          isc=isc+1
          where (abs(dmdts(:,:,isc,:)) > ytiny)
            dmdt(:,:,is,:,it)=dmdt(:,:,is,:,it)+dmdts(:,:,isc,:)
          end where
        end do
      end do
    end do
    !
    !       * deallocate work arrays.
    !
    deallocate (dmdts)
    deallocate (fmom0i)
    deallocate (fmom0d)
    deallocate (fmom3i)
    deallocate (fmom3d)
    !
    !-----------------------------------------------------------------------
    !       * initial mass size distribution.
    !
    do it=1,kint
      timas(:,:,:,it)=pimas(:,:,:)*pifrc(:,:,:,it)
    end do
    !        do it=1,kint
    !          timas(:,:,:,it)=0.
    !          isc=0
    !          do is=1,isaint
    !          do ist=1,coagp%isec
    !            isc=isc+1
    !            where ( abs(pipsi(:,:,is)-yna) > ytiny)
    !              timas(:,:,is,it)=timas(:,:,is,it)+fifrc(:,:,isc,it)
    !     1                  *ycnst*fiddn(:,:,isc)*fmom3(:,:,isc)/volgf(:,:)
    !            end where
    !          end do
    !          end do
    !        end do
    !
    !-----------------------------------------------------------------------
    !       * adjust time step, if necessary. the adjustment is performed
    !       * for each individual section (outer loop) based on the gathered
    !       * tendencies for coagulation of particles in that section with
    !       * particles with equal or greater sizes to account for differences
    !       * in coagulation efficiencies depending on particle sizes.
    !       * generally, coagulation will be most efficient for the smallest
    !       * particles resulting in potentially necessary reductions in
    !       * time step to avoid negative concentrations from simple euler
    !       * forward integration. however, little or no adjustment may be
    !       * required for larger particles so that there may only be minor
    !       * adjustments to time steps for coagulation of the larger
    !       * particles.
    !
    tinum=pinum
    do isc=isaint,1,-1
      dtc=dt/ysec
      do is=1,isaint
        do l=1,leva
          do il=1,ilga
            if (2+abs(exponent(tinum(il,l,is))-exponent(dtc(il,l))) &
                < maxexponent(tinum(il,l,is)) .and. dtc(il,l)/= 0. &
                .and. 2+abs(exponent(tinum(il,l,is))-exponent(dndt(il,l,is,isc))) &
                < maxexponent(tinum(il,l,is)) .and. dndt(il,l,is,isc)/=0. ) then
              if ( -tinum(il,l,is)/dtc(il,l) > dndt(il,l,is,isc) ) then
                dtc(il,l)=-tinum(il,l,is)/dndt(il,l,is,isc)
              end if
              !              dtc(il,l)=max(min(dtc(il,l),
              !     1                     -tinum(il,l,is)/dndt(il,l,is,isc)),0.)
            end if
            do it=1,kint
              if (2+abs(exponent(timas(il,l,is,it))-exponent(dtc(il,l))) &
                  < maxexponent(timas(il,l,is,it)) .and. dtc(il,l)/=0. &
                  .and. 2+abs(exponent(timas(il,l,is,it))-exponent(dmdt(il,l,is,isc,it))) &
                  < maxexponent(timas(il,l,is,it)) .and. dmdt(il,l,is,isc,it)/=0. ) then
                if ( -timas(il,l,is,it)/dtc(il,l) &
                    > dmdt(il,l,is,isc,it) ) then
                  dtc(il,l)=-timas(il,l,is,it)/dmdt(il,l,is,isc,it)
                end if
                !                dtc(il,l)=max(min(dtc(il,l),
                !     1                     -timas(il,l,is,it)/dmdt(il,l,is,isc,it)),0.)
              end if
            end do
          end do
        end do
      end do
      dtc=max(min(dt,dtc*ysec),0.)
      do is=1,isaint
        where (abs(pipsi(:,:,is)-yna) > ytiny)
          dndt(:,:,is,isc)=dndt(:,:,is,isc)*dtc(:,:)/dt
          tinum(:,:,is)=tinum(:,:,is)+dndt(:,:,is,isc)*dt
        end where
        do it=1,kint
          where (abs(pipsi(:,:,is)-yna) > ytiny)
            dmdt(:,:,is,isc,it)=dmdt(:,:,is,isc,it)*dtc(:,:)/dt
            timas(:,:,is,it)=timas(:,:,is,it)+dmdt(:,:,is,isc,it)*dt
          end where
        end do
      end do
    end do
    !
    !       * reject negative solutions from numerical truncation.
    !
    tinum=max(tinum,0.)
    timas=max(timas,0.)
    !
    !       * mass fixer to compensate for truncation errors.
    !
    do it=1,kint
      totm1=sum(pimas(:,:,:)*pifrc(:,:,:,it),dim=3)
      totm2=sum(timas(:,:,:,it),dim=3)
      where (2+abs(exponent(totm1) - exponent(totm2)) &
          < maxexponent(totm1) .and. totm2/=0. )
        ratm=totm1/totm2
      else where
        ratm=1.
      end where
      do is=1,isaint
        timas(:,:,is,it)=timas(:,:,is,it)*ratm(:,:)
      end do
    end do
    !
    !       * deallocate work arrays.
    !
    deallocate (dndt)
    deallocate (dmdt)
    !
    !       * particle number coagulation tendecy from difference
    !       * of adjusted new and original particle number concentration.
    !
    where (abs(pipsi-yna) > ytiny)
      pidndt=(tinum-pinum)/dt
    end where
    !
    !       * particle mass coagulation tendecy from difference
    !       * of adjusted new and original particle mass concentration.
    !
    timast=0.
    do it=1,kint
      timast(:,:,:)=timast(:,:,:)+timas(:,:,:,it)
    end do
    do it=1,kint
      where (2+abs(exponent(timas(:,:,:,it))-exponent(timast(:,:,:))) &
          < maxexponent(timas(:,:,:,it)) .and. timast(:,:,:)/=0. &
          .and. abs(pipsi-yna) > ytiny)
        pidfdt(:,:,:,it)=(timas(:,:,:,it)/timast(:,:,:) &
                                                   -pifrc(:,:,:,it))/dt
      end where
    end do
    where (abs(pipsi-yna) > ytiny)
      pidmdt=(timast-pimas)/dt
    end where
  end if
  !
end subroutine coag
