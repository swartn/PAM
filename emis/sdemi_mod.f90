!> \file
!> \brief Parameters for primary aerosol emissions.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
module sdemi
  !
  implicit none
  !
  !     * emission parameter arrays.
  !
  type iniep
    real :: radms !<
    real :: sigms !<
    real :: cnveff !<
  end type iniep
  type(iniep) :: oce1,oce2,oce3,oce4
  type(iniep) :: bce1,bce2,bce3,bce4
  type(iniep) :: sue1,sue2,sue3,sue4
  type plaep
    real :: phi0 !<
    real :: psi !<
    real, allocatable, dimension(:) :: efmas !<
    real, allocatable, dimension(:) :: efnum !<
  end type plaep
  type(plaep) :: peoct1,peoct2,peoct3,peoct4
  type(plaep) :: pebct1,pebct2,pebct3,pebct4
  type(plaep) :: pesut1,pesut2,pesut3,pesut4
  type(plaep) :: pioct1,pioct2,pioct3,pioct4
  type(plaep) :: pibct1,pibct2,pibct3,pibct4
  type(plaep) :: pisut1,pisut2,pisut3,pisut4
  !
end module sdemi
