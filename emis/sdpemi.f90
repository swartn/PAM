!> \file
!> \brief Initialization of basic parameters for primary aerosol emissions.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine sdpemi
  !
  use sdcode, only : sdintb, sdintb0
  use sdparm, only : aextf,aintf,iexbc,iexoc,iexso4,iinbc,iinoc,iinso4, &
                         isextbc,isextoc,isextso4, &
                         isintbc,isintoc,isintso4, &
                         kextbc,kextoc,kextso4, &
                         kintbc,kintoc,kintso4, &
                         pedphis,pephiss,pidphis,piphiss,r0,ycnst,ytiny
  use sdemi,  only : bce1,bce2,bce3,bce4, &
                         oce1,oce2,oce3,oce4, &
                         pebct1,pebct2,pebct3,pebct4, &
                         peoct1,peoct2,peoct3,peoct4, &
                         pesut1,pesut2,pesut3,pesut4, &
                         pibct1,pibct2,pibct3,pibct4, &
                         pioct1,pioct2,pioct3,pioct4, &
                         pisut1,pisut2,pisut3,pisut4, &
                         sue1,sue2,sue3,sue4
  !
  implicit none
  !
  real :: rmom !<
  real :: ocddn !<
  real :: phis0 !<
  real :: dphi0 !<
  real :: bcddn !<
  real :: suddn !<
  real :: peocm1t !<
  real :: peocm2t !<
  real :: peocm3t !<
  real :: peocm4t !<
  real :: pebcm1t !<
  real :: pebcm2t !<
  real :: pebcm3t !<
  real :: pebcm4t !<
  real :: pesum1t !<
  real :: pesum2t !<
  real :: pesum3t !<
  real :: pesum4t !<
  real :: piocm1t !<
  real :: piocm2t !<
  real :: piocm3t !<
  real :: piocm4t !<
  real :: pibcm1t !<
  real :: pibcm2t !<
  real :: pibcm3t !<
  real :: pibcm4t !<
  real :: pisum1t !<
  real :: pisum2t !<
  real :: pisum3t !<
  real :: pisum4t !<
  integer :: is !<
  !
  !     * work arrays.
  !
  real, allocatable, dimension(:) :: peocm1 !<
  real, allocatable, dimension(:) :: peocn1 !<
  real, allocatable, dimension(:) :: peocm2 !<
  real, allocatable, dimension(:) :: peocn2 !<
  real, allocatable, dimension(:) :: peocm3 !<
  real, allocatable, dimension(:) :: peocn3 !<
  real, allocatable, dimension(:) :: peocm4 !<
  real, allocatable, dimension(:) :: peocn4 !<
  real, allocatable, dimension(:) :: pebcm1 !<
  real, allocatable, dimension(:) :: pebcn1 !<
  real, allocatable, dimension(:) :: pebcm2 !<
  real, allocatable, dimension(:) :: pebcn2 !<
  real, allocatable, dimension(:) :: pebcm3 !<
  real, allocatable, dimension(:) :: pebcn3 !<
  real, allocatable, dimension(:) :: pebcm4 !<
  real, allocatable, dimension(:) :: pebcn4 !<
  real, allocatable, dimension(:) :: pesum1 !<
  real, allocatable, dimension(:) :: pesun1 !<
  real, allocatable, dimension(:) :: pesum2 !<
  real, allocatable, dimension(:) :: pesun2 !<
  real, allocatable, dimension(:) :: pesum3 !<
  real, allocatable, dimension(:) :: pesun3 !<
  real, allocatable, dimension(:) :: pesum4 !<
  real, allocatable, dimension(:) :: pesun4 !<
  real, allocatable, dimension(:) :: piocm1 !<
  real, allocatable, dimension(:) :: piocn1 !<
  real, allocatable, dimension(:) :: piocm2 !<
  real, allocatable, dimension(:) :: piocn2 !<
  real, allocatable, dimension(:) :: piocm3 !<
  real, allocatable, dimension(:) :: piocn3 !<
  real, allocatable, dimension(:) :: piocm4 !<
  real, allocatable, dimension(:) :: piocn4 !<
  real, allocatable, dimension(:) :: pibcm1 !<
  real, allocatable, dimension(:) :: pibcn1 !<
  real, allocatable, dimension(:) :: pibcm2 !<
  real, allocatable, dimension(:) :: pibcn2 !<
  real, allocatable, dimension(:) :: pibcm3 !<
  real, allocatable, dimension(:) :: pibcn3 !<
  real, allocatable, dimension(:) :: pibcm4 !<
  real, allocatable, dimension(:) :: pibcn4 !<
  real, allocatable, dimension(:) :: pisum1 !<
  real, allocatable, dimension(:) :: pisun1 !<
  real, allocatable, dimension(:) :: pisum2 !<
  real, allocatable, dimension(:) :: pisun2 !<
  real, allocatable, dimension(:) :: pisum3 !<
  real, allocatable, dimension(:) :: pisun3 !<
  real, allocatable, dimension(:) :: pisum4 !<
  real, allocatable, dimension(:) :: pisun4 !<
  !
  !     * intrinsic functions.
  !
  ! zpsi(arg)=1./(2.*log(arg)**2)
  ! zphi(arg)=log(arg/r0)
  !
  !-----------------------------------------------------------------------
  !     size distribution parameters (mode radius, variance)
  !     for primary emissions of organic carbon. sources: dentener
  !     et al. (2006)\cite dentener2006, bond et al. (2013)\cite bond2013, kondo et al. (2011)\cite kondo2011,
  !     petzold et al. (1998)\cite petzold1998, shiraiwa et al. (2008)\cite shiraiwa2008, seinfeld\cite seinfeld1998.
  !
  !     natural and anthropogenic emissions in following order:
  !     1 - open vegetation and biofuel burning
  !     2 - fossil fuel burning
  !     3 - aircraft
  !     4 - shipping
  !
  oce1%radms=0.050e-06
  oce1%sigms=1.3
  oce2%radms=0.020e-06
  oce2%sigms=1.8
  oce3%radms=0.250e-06
  oce3%sigms=1.3
  oce4%radms=0.250e-06
  oce4%sigms=1.3
  !
  !     the same as above for black carbon.
  !
  bce1%radms=0.050e-06
  bce1%sigms=1.3
  bce2%radms=0.020e-06
  bce2%sigms=1.8
  bce3%radms=0.250e-06
  bce3%sigms=1.3
  bce4%radms=0.250e-06
  bce4%sigms=1.3
  !
  !     the same as above for sulphate.
  !
  sue1%radms=0.050e-06
  sue1%sigms=1.3
  sue2%radms=0.020e-06
  sue2%sigms=1.8
  sue3%radms=0.250e-06
  sue3%sigms=1.3
  sue4%radms=0.250e-06
  sue4%sigms=1.3
  !
  !     fraction of emissions that go into primary particle emissions
  !     vs. total (gas-phase + primary) emissions.
  !
  oce1%cnveff=1.
  oce2%cnveff=1.
  oce3%cnveff=1.
  oce4%cnveff=1.
  bce1%cnveff=1.
  bce2%cnveff=1.
  bce3%cnveff=1.
  bce4%cnveff=1.
  sue1%cnveff=0.025
  sue2%cnveff=0.025
  sue3%cnveff=0.025
  sue4%cnveff=0.025
  !
  !-----------------------------------------------------------------------
  !     * allocate work arrays and initialization of contstants.
  !
  if (isextoc > 0) then
    allocate(peocm1(isextoc))
    allocate(peocn1(isextoc))
    allocate(peocm2(isextoc))
    allocate(peocn2(isextoc))
    allocate(peocm3(isextoc))
    allocate(peocn3(isextoc))
    allocate(peocm4(isextoc))
    allocate(peocn4(isextoc))
  end if
  if (isextbc > 0) then
    allocate(pebcm1(isextbc))
    allocate(pebcn1(isextbc))
    allocate(pebcm2(isextbc))
    allocate(pebcn2(isextbc))
    allocate(pebcm3(isextbc))
    allocate(pebcn3(isextbc))
    allocate(pebcm4(isextbc))
    allocate(pebcn4(isextbc))
  end if
  if (isextso4 > 0) then
    allocate(pesum1(isextso4))
    allocate(pesun1(isextso4))
    allocate(pesum2(isextso4))
    allocate(pesun2(isextso4))
    allocate(pesum3(isextso4))
    allocate(pesun3(isextso4))
    allocate(pesum4(isextso4))
    allocate(pesun4(isextso4))
  end if
  if (isintoc > 0) then
    allocate(piocm1(isintoc))
    allocate(piocn1(isintoc))
    allocate(piocm2(isintoc))
    allocate(piocn2(isintoc))
    allocate(piocm3(isintoc))
    allocate(piocn3(isintoc))
    allocate(piocm4(isintoc))
    allocate(piocn4(isintoc))
  end if
  if (isintbc > 0) then
    allocate(pibcm1(isintbc))
    allocate(pibcn1(isintbc))
    allocate(pibcm2(isintbc))
    allocate(pibcn2(isintbc))
    allocate(pibcm3(isintbc))
    allocate(pibcn3(isintbc))
    allocate(pibcm4(isintbc))
    allocate(pibcn4(isintbc))
  end if
  if (isintso4 > 0) then
    allocate(pisum1(isintso4))
    allocate(pisun1(isintso4))
    allocate(pisum2(isintso4))
    allocate(pisun2(isintso4))
    allocate(pisum3(isintso4))
    allocate(pisun3(isintso4))
    allocate(pisum4(isintso4))
    allocate(pisun4(isintso4))
  end if
  rmom=3.
  !
  !-----------------------------------------------------------------------
  !     pla parameters for primary emissions of organic carbon.
  !
  if (kextoc > 0) then
    peoct1%phi0=zphi(oce1%radms)
    peoct1%psi =zpsi(oce1%sigms)
    peoct2%phi0=zphi(oce2%radms)
    peoct2%psi =zpsi(oce2%sigms)
    peoct3%phi0=zphi(oce3%radms)
    peoct3%psi =zpsi(oce3%sigms)
    peoct4%phi0=zphi(oce4%radms)
    peoct4%psi =zpsi(oce4%sigms)
  end if
  if (kintoc > 0) then
    pioct1%phi0=zphi(oce1%radms)
    pioct1%psi =zpsi(oce1%sigms)
    pioct2%phi0=zphi(oce2%radms)
    pioct2%psi =zpsi(oce2%sigms)
    pioct3%phi0=zphi(oce3%radms)
    pioct3%psi =zpsi(oce3%sigms)
    pioct4%phi0=zphi(oce4%radms)
    pioct4%psi =zpsi(oce4%sigms)
  end if
  !
  !     pla parameters for primary emissions of black carbon.
  !
  if (kextbc > 0) then
    pebct1%phi0=zphi(bce1%radms)
    pebct1%psi =zpsi(bce1%sigms)
    pebct2%phi0=zphi(bce2%radms)
    pebct2%psi =zpsi(bce2%sigms)
    pebct3%phi0=zphi(bce3%radms)
    pebct3%psi =zpsi(bce3%sigms)
    pebct4%phi0=zphi(bce4%radms)
    pebct4%psi =zpsi(bce4%sigms)
  end if
  if (kintbc > 0) then
    pibct1%phi0=zphi(bce1%radms)
    pibct1%psi =zpsi(bce1%sigms)
    pibct2%phi0=zphi(bce2%radms)
    pibct2%psi =zpsi(bce2%sigms)
    pibct3%phi0=zphi(bce3%radms)
    pibct3%psi =zpsi(bce3%sigms)
    pibct4%phi0=zphi(bce4%radms)
    pibct4%psi =zpsi(bce4%sigms)
  end if
  !
  !     pla parameters for primary emissions of sulphate.
  !
  if (kextso4 > 0) then
    pesut1%phi0=zphi(sue1%radms)
    pesut1%psi =zpsi(sue1%sigms)
    pesut2%phi0=zphi(sue2%radms)
    pesut2%psi =zpsi(sue2%sigms)
    pesut3%phi0=zphi(sue3%radms)
    pesut3%psi =zpsi(sue3%sigms)
    pesut4%phi0=zphi(sue4%radms)
    pesut4%psi =zpsi(sue4%sigms)
  end if
  if (kintso4 > 0) then
    pisut1%phi0=zphi(sue1%radms)
    pisut1%psi =zpsi(sue1%sigms)
    pisut2%phi0=zphi(sue2%radms)
    pisut2%psi =zpsi(sue2%sigms)
    pisut3%phi0=zphi(sue3%radms)
    pisut3%psi =zpsi(sue3%sigms)
    pisut4%phi0=zphi(sue4%radms)
    pisut4%psi =zpsi(sue4%sigms)
  end if
  !
  !-----------------------------------------------------------------------
  !     integrate over emission size distributions for natural
  !     and anthropogenic emissions of organic and black carbon.
  !     for externally mixed types of aerosol.
  !
  if (kextoc > 0) then
    ocddn=aextf%tp(kextoc)%dens
    peocm1t=0.
    peocm2t=0.
    peocm3t=0.
    peocm4t=0.
    do is=1,isextoc
      phis0=pephiss(iexoc(is))
      dphi0=pedphis(iexoc(is))
      peocn1(is)=sdintb0(peoct1%phi0,peoct1%psi,phis0,dphi0)
      peocm1(is)=ocddn*ycnst &
                   *sdintb(peoct1%phi0,peoct1%psi,rmom,phis0,dphi0)
      peocm1t=peocm1t+peocm1(is)
      peocn2(is)=sdintb0(peoct2%phi0,peoct2%psi,phis0,dphi0)
      peocm2(is)=ocddn*ycnst &
                   *sdintb(peoct2%phi0,peoct2%psi,rmom,phis0,dphi0)
      peocm2t=peocm2t+peocm2(is)
      peocn3(is)=sdintb0(peoct3%phi0,peoct3%psi,phis0,dphi0)
      peocm3(is)=ocddn*ycnst &
                   *sdintb(peoct3%phi0,peoct3%psi,rmom,phis0,dphi0)
      peocm3t=peocm3t+peocm3(is)
      peocn4(is)=sdintb0(peoct4%phi0,peoct4%psi,phis0,dphi0)
      peocm4(is)=ocddn*ycnst &
                   *sdintb(peoct4%phi0,peoct4%psi,rmom,phis0,dphi0)
      peocm4t=peocm4t+peocm4(is)
    end do
  end if
  if (kextbc > 0) then
    bcddn=aextf%tp(kextbc)%dens
    pebcm1t=0.
    pebcm2t=0.
    pebcm3t=0.
    pebcm4t=0.
    do is=1,isextbc
      phis0=pephiss(iexbc(is))
      dphi0=pedphis(iexbc(is))
      pebcn1(is)=sdintb0(pebct1%phi0,pebct1%psi,phis0,dphi0)
      pebcm1(is)=bcddn*ycnst &
                   *sdintb(pebct1%phi0,pebct1%psi,rmom,phis0,dphi0)
      pebcm1t=pebcm1t+pebcm1(is)
      pebcn2(is)=sdintb0(pebct2%phi0,pebct2%psi,phis0,dphi0)
      pebcm2(is)=bcddn*ycnst &
                   *sdintb(pebct2%phi0,pebct2%psi,rmom,phis0,dphi0)
      pebcm2t=pebcm2t+pebcm2(is)
      pebcn3(is)=sdintb0(pebct3%phi0,pebct3%psi,phis0,dphi0)
      pebcm3(is)=bcddn*ycnst &
                   *sdintb(pebct3%phi0,pebct3%psi,rmom,phis0,dphi0)
      pebcm3t=pebcm3t+pebcm3(is)
      pebcn4(is)=sdintb0(pebct4%phi0,pebct4%psi,phis0,dphi0)
      pebcm4(is)=bcddn*ycnst &
                   *sdintb(pebct4%phi0,pebct4%psi,rmom,phis0,dphi0)
      pebcm4t=pebcm4t+pebcm4(is)
    end do
  end if
  if (kextso4 > 0) then
    suddn=aextf%tp(kextso4)%dens
    pesum1t=0.
    pesum2t=0.
    pesum3t=0.
    pesum4t=0.
    do is=1,isextso4
      phis0=pephiss(iexso4(is))
      dphi0=pedphis(iexso4(is))
      pesun1(is)=sdintb0(pesut1%phi0,pesut1%psi,phis0,dphi0)
      pesum1(is)=suddn*ycnst &
                   *sdintb(pesut1%phi0,pesut1%psi,rmom,phis0,dphi0)
      pesum1t=pesum1t+pesum1(is)
      pesun2(is)=sdintb0(pesut2%phi0,pesut2%psi,phis0,dphi0)
      pesum2(is)=suddn*ycnst &
                   *sdintb(pesut2%phi0,pesut2%psi,rmom,phis0,dphi0)
      pesum2t=pesum2t+pesum2(is)
      pesun3(is)=sdintb0(pesut3%phi0,pesut3%psi,phis0,dphi0)
      pesum3(is)=suddn*ycnst &
                   *sdintb(pesut3%phi0,pesut3%psi,rmom,phis0,dphi0)
      pesum3t=pesum3t+pesum3(is)
      pesun4(is)=sdintb0(pesut4%phi0,pesut4%psi,phis0,dphi0)
      pesum4(is)=suddn*ycnst &
                   *sdintb(pesut4%phi0,pesut4%psi,rmom,phis0,dphi0)
      pesum4t=pesum4t+pesum4(is)
    end do
  end if
  !
  !      the same as above for internally mixed aerosol.
  !
  if (kintoc > 0) then
    ocddn=aintf%tp(kintoc)%dens
    piocm1t=0.
    piocm2t=0.
    piocm3t=0.
    piocm4t=0.
    do is=1,isintoc
      phis0=piphiss(iinoc(is))
      dphi0=pidphis(iinoc(is))
      piocn1(is)=sdintb0(pioct1%phi0,pioct1%psi,phis0,dphi0)
      piocm1(is)=ocddn*ycnst &
                   *sdintb(pioct1%phi0,pioct1%psi,rmom,phis0,dphi0)
      piocm1t=piocm1t+piocm1(is)
      piocn2(is)=sdintb0(pioct2%phi0,pioct2%psi,phis0,dphi0)
      piocm2(is)=ocddn*ycnst &
                   *sdintb(pioct2%phi0,pioct2%psi,rmom,phis0,dphi0)
      piocm2t=piocm2t+piocm2(is)
      piocn3(is)=sdintb0(pioct3%phi0,pioct3%psi,phis0,dphi0)
      piocm3(is)=ocddn*ycnst &
                   *sdintb(pioct3%phi0,pioct3%psi,rmom,phis0,dphi0)
      piocm3t=piocm3t+piocm3(is)
      piocn4(is)=sdintb0(pioct4%phi0,pioct4%psi,phis0,dphi0)
      piocm4(is)=ocddn*ycnst &
                   *sdintb(pioct4%phi0,pioct4%psi,rmom,phis0,dphi0)
      piocm4t=piocm4t+piocm4(is)
    end do
  end if
  if (kintbc > 0) then
    bcddn=aintf%tp(kintbc)%dens
    pibcm1t=0.
    pibcm2t=0.
    pibcm3t=0.
    pibcm4t=0.
    do is=1,isintbc
      phis0=piphiss(iinbc(is))
      dphi0=pidphis(iinbc(is))
      pibcn1(is)=sdintb0(pibct1%phi0,pibct1%psi,phis0,dphi0)
      pibcm1(is)=bcddn*ycnst &
                   *sdintb(pibct1%phi0,pibct1%psi,rmom,phis0,dphi0)
      pibcm1t=pibcm1t+pibcm1(is)
      pibcn2(is)=sdintb0(pibct2%phi0,pibct2%psi,phis0,dphi0)
      pibcm2(is)=bcddn*ycnst &
                   *sdintb(pibct2%phi0,pibct2%psi,rmom,phis0,dphi0)
      pibcm2t=pibcm2t+pibcm2(is)
      pibcn3(is)=sdintb0(pibct3%phi0,pibct3%psi,phis0,dphi0)
      pibcm3(is)=bcddn*ycnst &
                   *sdintb(pibct3%phi0,pibct3%psi,rmom,phis0,dphi0)
      pibcm3t=pibcm3t+pibcm3(is)
      pibcn4(is)=sdintb0(pibct4%phi0,pibct4%psi,phis0,dphi0)
      pibcm4(is)=bcddn*ycnst &
                   *sdintb(pibct4%phi0,pibct4%psi,rmom,phis0,dphi0)
      pibcm4t=pibcm4t+pibcm4(is)
    end do
  end if
  if (kintso4 > 0) then
    suddn=aintf%tp(kintso4)%dens
    pisum1t=0.
    pisum2t=0.
    pisum3t=0.
    pisum4t=0.
    do is=1,isintso4
      phis0=piphiss(iinso4(is))
      dphi0=pidphis(iinso4(is))
      pisun1(is)=sdintb0(pisut1%phi0,pisut1%psi,phis0,dphi0)
      pisum1(is)=suddn*ycnst &
                   *sdintb(pisut1%phi0,pisut1%psi,rmom,phis0,dphi0)
      pisum1t=pisum1t+pisum1(is)
      pisun2(is)=sdintb0(pisut2%phi0,pisut2%psi,phis0,dphi0)
      pisum2(is)=suddn*ycnst &
                   *sdintb(pisut2%phi0,pisut2%psi,rmom,phis0,dphi0)
      pisum2t=pisum2t+pisum2(is)
      pisun3(is)=sdintb0(pisut3%phi0,pisut3%psi,phis0,dphi0)
      pisum3(is)=suddn*ycnst &
                   *sdintb(pisut3%phi0,pisut3%psi,rmom,phis0,dphi0)
      pisum3t=pisum3t+pisum3(is)
      pisun4(is)=sdintb0(pisut4%phi0,pisut4%psi,phis0,dphi0)
      pisum4(is)=suddn*ycnst &
                   *sdintb(pisut4%phi0,pisut4%psi,rmom,phis0,dphi0)
      pisum4t=pisum4t+pisum4(is)
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     allocate emission mass fractions.
  !
  if (isextoc > 0) then
    if (allocated(peoct1%efmas)) deallocate(peoct1%efmas)
    allocate(peoct1%efmas(isextoc))
    if (allocated(peoct1%efnum)) deallocate(peoct1%efnum)
    allocate(peoct1%efnum(isextoc))
    if (allocated(peoct2%efmas)) deallocate(peoct2%efmas)
    allocate(peoct2%efmas(isextoc))
    if (allocated(peoct2%efnum)) deallocate(peoct2%efnum)
    allocate(peoct2%efnum(isextoc))
    if (allocated(peoct3%efmas)) deallocate(peoct3%efmas)
    allocate(peoct3%efmas(isextoc))
    if (allocated(peoct3%efnum)) deallocate(peoct3%efnum)
    allocate(peoct3%efnum(isextoc))
    if (allocated(peoct4%efmas)) deallocate(peoct4%efmas)
    allocate(peoct4%efmas(isextoc))
    if (allocated(peoct4%efnum)) deallocate(peoct4%efnum)
    allocate(peoct4%efnum(isextoc))
  end if
  if (isextbc > 0) then
    if (allocated(pebct1%efmas)) deallocate(pebct1%efmas)
    allocate(pebct1%efmas(isextbc))
    if (allocated(pebct1%efnum)) deallocate(pebct1%efnum)
    allocate(pebct1%efnum(isextbc))
    if (allocated(pebct2%efmas)) deallocate(pebct2%efmas)
    allocate(pebct2%efmas(isextbc))
    if (allocated(pebct2%efnum)) deallocate(pebct2%efnum)
    allocate(pebct2%efnum(isextbc))
    if (allocated(pebct3%efmas)) deallocate(pebct3%efmas)
    allocate(pebct3%efmas(isextbc))
    if (allocated(pebct3%efnum)) deallocate(pebct3%efnum)
    allocate(pebct3%efnum(isextbc))
    if (allocated(pebct4%efmas)) deallocate(pebct4%efmas)
    allocate(pebct4%efmas(isextbc))
    if (allocated(pebct4%efnum)) deallocate(pebct4%efnum)
    allocate(pebct4%efnum(isextbc))
  end if
  if (isextso4 > 0) then
    if (allocated(pesut1%efmas)) deallocate(pesut1%efmas)
    allocate(pesut1%efmas(isextso4))
    if (allocated(pesut1%efnum)) deallocate(pesut1%efnum)
    allocate(pesut1%efnum(isextso4))
    if (allocated(pesut2%efmas)) deallocate(pesut2%efmas)
    allocate(pesut2%efmas(isextso4))
    if (allocated(pesut2%efnum)) deallocate(pesut2%efnum)
    allocate(pesut2%efnum(isextso4))
    if (allocated(pesut3%efmas)) deallocate(pesut3%efmas)
    allocate(pesut3%efmas(isextso4))
    if (allocated(pesut3%efnum)) deallocate(pesut3%efnum)
    allocate(pesut3%efnum(isextso4))
    if (allocated(pesut4%efmas)) deallocate(pesut4%efmas)
    allocate(pesut4%efmas(isextso4))
    if (allocated(pesut4%efnum)) deallocate(pesut4%efnum)
    allocate(pesut4%efnum(isextso4))
  end if
  if (isintoc > 0) then
    if (allocated(pioct1%efmas)) deallocate(pioct1%efmas)
    allocate(pioct1%efmas(isintoc))
    if (allocated(pioct1%efnum)) deallocate(pioct1%efnum)
    allocate(pioct1%efnum(isintoc))
    if (allocated(pioct2%efmas)) deallocate(pioct2%efmas)
    allocate(pioct2%efmas(isintoc))
    if (allocated(pioct2%efnum)) deallocate(pioct2%efnum)
    allocate(pioct2%efnum(isintoc))
    if (allocated(pioct3%efmas)) deallocate(pioct3%efmas)
    allocate(pioct3%efmas(isintoc))
    if (allocated(pioct3%efnum)) deallocate(pioct3%efnum)
    allocate(pioct3%efnum(isintoc))
    if (allocated(pioct4%efmas)) deallocate(pioct4%efmas)
    allocate(pioct4%efmas(isintoc))
    if (allocated(pioct4%efnum)) deallocate(pioct4%efnum)
    allocate(pioct4%efnum(isintoc))
  end if
  if (isintbc > 0) then
    if (allocated(pibct1%efmas)) deallocate(pibct1%efmas)
    allocate(pibct1%efmas(isintbc))
    if (allocated(pibct1%efnum)) deallocate(pibct1%efnum)
    allocate(pibct1%efnum(isintbc))
    if (allocated(pibct2%efmas)) deallocate(pibct2%efmas)
    allocate(pibct2%efmas(isintbc))
    if (allocated(pibct2%efnum)) deallocate(pibct2%efnum)
    allocate(pibct2%efnum(isintbc))
    if (allocated(pibct3%efmas)) deallocate(pibct3%efmas)
    allocate(pibct3%efmas(isintbc))
    if (allocated(pibct3%efnum)) deallocate(pibct3%efnum)
    allocate(pibct3%efnum(isintbc))
    if (allocated(pibct4%efmas)) deallocate(pibct4%efmas)
    allocate(pibct4%efmas(isintbc))
    if (allocated(pibct4%efnum)) deallocate(pibct4%efnum)
    allocate(pibct4%efnum(isintbc))
  end if
  if (isintso4 > 0) then
    if (allocated(pisut1%efmas)) deallocate(pisut1%efmas)
    allocate(pisut1%efmas(isintso4))
    if (allocated(pisut1%efnum)) deallocate(pisut1%efnum)
    allocate(pisut1%efnum(isintso4))
    if (allocated(pisut2%efmas)) deallocate(pisut2%efmas)
    allocate(pisut2%efmas(isintso4))
    if (allocated(pisut2%efnum)) deallocate(pisut2%efnum)
    allocate(pisut2%efnum(isintso4))
    if (allocated(pisut3%efmas)) deallocate(pisut3%efmas)
    allocate(pisut3%efmas(isintso4))
    if (allocated(pisut3%efnum)) deallocate(pisut3%efnum)
    allocate(pisut3%efnum(isintso4))
    if (allocated(pisut4%efmas)) deallocate(pisut4%efmas)
    allocate(pisut4%efmas(isintso4))
    if (allocated(pisut4%efnum)) deallocate(pisut4%efnum)
    allocate(pisut4%efnum(isintso4))
  end if
  !
  !-----------------------------------------------------------------------
  !     determine emission mass and number fractions. multiplication
  !     with total mass concentration yields the mass resp. number
  !     concentration in each aerosol size section. first, for
  !     externally mixed types of aerosol.
  !
  if (isextoc > 0) then
    if (peocm1t > ytiny) then
      do is=1,isextoc
        peoct1%efmas(is)=peocm1(is)/peocm1t
        peoct1%efnum(is)=peocn1(is)/peocm1t
      end do
    else
      peoct1%efmas=0.
      peoct1%efnum=0.
    end if
    if (peocm2t > ytiny) then
      do is=1,isextoc
        peoct2%efmas(is)=peocm2(is)/peocm2t
        peoct2%efnum(is)=peocn2(is)/peocm2t
      end do
    else
      peoct2%efmas=0.
      peoct2%efnum=0.
    end if
    if (peocm3t > ytiny) then
      do is=1,isextoc
        peoct3%efmas(is)=peocm3(is)/peocm3t
        peoct3%efnum(is)=peocn3(is)/peocm3t
      end do
    else
      peoct3%efmas=0.
      peoct3%efnum=0.
    end if
    if (peocm4t > ytiny) then
      do is=1,isextoc
        peoct4%efmas(is)=peocm4(is)/peocm4t
        peoct4%efnum(is)=peocn4(is)/peocm4t
      end do
    else
      peoct4%efmas=0.
      peoct4%efnum=0.
    end if
  end if
  if (isextbc > 0) then
    if (pebcm1t > ytiny) then
      do is=1,isextbc
        pebct1%efmas(is)=pebcm1(is)/pebcm1t
        pebct1%efnum(is)=pebcn1(is)/pebcm1t
      end do
    else
      pebct1%efmas=0.
      pebct1%efnum=0.
    end if
    if (pebcm2t > ytiny) then
      do is=1,isextbc
        pebct2%efmas(is)=pebcm2(is)/pebcm2t
        pebct2%efnum(is)=pebcn2(is)/pebcm2t
      end do
    else
      pebct2%efmas=0.
      pebct2%efnum=0.
    end if
    if (pebcm3t > ytiny) then
      do is=1,isextbc
        pebct3%efmas(is)=pebcm3(is)/pebcm3t
        pebct3%efnum(is)=pebcn3(is)/pebcm3t
      end do
    else
      pebct3%efmas=0.
      pebct3%efnum=0.
    end if
    if (pebcm4t > ytiny) then
      do is=1,isextbc
        pebct4%efmas(is)=pebcm4(is)/pebcm4t
        pebct4%efnum(is)=pebcn4(is)/pebcm4t
      end do
    else
      pebct4%efmas=0.
      pebct4%efnum=0.
    end if
  end if
  if (isextso4 > 0) then
    if (pesum1t > ytiny) then
      do is=1,isextso4
        pesut1%efmas(is)=pesum1(is)/pesum1t
        pesut1%efnum(is)=pesun1(is)/pesum1t
      end do
    else
      pesut1%efmas=0.
      pesut1%efnum=0.
    end if
    if (pesum2t > ytiny) then
      do is=1,isextso4
        pesut2%efmas(is)=pesum2(is)/pesum2t
        pesut2%efnum(is)=pesun2(is)/pesum2t
      end do
    else
      pesut2%efmas=0.
      pesut2%efnum=0.
    end if
    if (pesum3t > ytiny) then
      do is=1,isextso4
        pesut3%efmas(is)=pesum3(is)/pesum3t
        pesut3%efnum(is)=pesun3(is)/pesum3t
      end do
    else
      pesut3%efmas=0.
      pesut3%efnum=0.
    end if
    if (pesum4t > ytiny) then
      do is=1,isextso4
        pesut4%efmas(is)=pesum4(is)/pesum4t
        pesut4%efnum(is)=pesun4(is)/pesum4t
      end do
    else
      pesut4%efmas=0.
      pesut4%efnum=0.
    end if
  end if
  !
  !     the same as above for internally mixed types of aerosol.
  !
  if (isintoc > 0) then
    if (piocm1t > ytiny) then
      do is=1,isintoc
        pioct1%efmas(is)=piocm1(is)/piocm1t
        pioct1%efnum(is)=piocn1(is)/piocm1t
      end do
    else
      pioct1%efmas=0.
      pioct1%efnum=0.
    end if
    if (piocm2t > ytiny) then
      do is=1,isintoc
        pioct2%efmas(is)=piocm2(is)/piocm2t
        pioct2%efnum(is)=piocn2(is)/piocm2t
      end do
    else
      pioct2%efmas=0.
      pioct2%efnum=0.
    end if
    if (piocm3t > ytiny) then
      do is=1,isintoc
        pioct3%efmas(is)=piocm3(is)/piocm3t
        pioct3%efnum(is)=piocn3(is)/piocm3t
      end do
    else
      pioct3%efmas=0.
      pioct3%efnum=0.
    end if
    if (piocm4t > ytiny) then
      do is=1,isintoc
        pioct4%efmas(is)=piocm4(is)/piocm4t
        pioct4%efnum(is)=piocn4(is)/piocm4t
      end do
    else
      pioct4%efmas=0.
      pioct4%efnum=0.
    end if
  end if
  if (isintbc > 0) then
    if (pibcm1t > ytiny) then
      do is=1,isintbc
        pibct1%efmas(is)=pibcm1(is)/pibcm1t
        pibct1%efnum(is)=pibcn1(is)/pibcm1t
      end do
    else
      pibct1%efmas=0.
      pibct1%efnum=0.
    end if
    if (pibcm2t > ytiny) then
      do is=1,isintbc
        pibct2%efmas(is)=pibcm2(is)/pibcm2t
        pibct2%efnum(is)=pibcn2(is)/pibcm2t
      end do
    else
      pibct2%efmas=0.
      pibct2%efnum=0.
    end if
    if (pibcm3t > ytiny) then
      do is=1,isintbc
        pibct3%efmas(is)=pibcm3(is)/pibcm3t
        pibct3%efnum(is)=pibcn3(is)/pibcm3t
      end do
    else
      pibct3%efmas=0.
      pibct3%efnum=0.
    end if
    if (pibcm4t > ytiny) then
      do is=1,isintbc
        pibct4%efmas(is)=pibcm4(is)/pibcm4t
        pibct4%efnum(is)=pibcn4(is)/pibcm4t
      end do
    else
      pibct4%efmas=0.
      pibct4%efnum=0.
    end if
  end if
  if (isintso4 > 0) then
    if (pisum1t > ytiny) then
      do is=1,isintso4
        pisut1%efmas(is)=pisum1(is)/pisum1t
        pisut1%efnum(is)=pisun1(is)/pisum1t
      end do
    else
      pisut1%efmas=0.
      pisut1%efnum=0.
    end if
    if (pisum2t > ytiny) then
      do is=1,isintso4
        pisut2%efmas(is)=pisum2(is)/pisum2t
        pisut2%efnum(is)=pisun2(is)/pisum2t
      end do
    else
      pisut2%efmas=0.
      pisut2%efnum=0.
    end if
    if (pisum3t > ytiny) then
      do is=1,isintso4
        pisut3%efmas(is)=pisum3(is)/pisum3t
        pisut3%efnum(is)=pisun3(is)/pisum3t
      end do
    else
      pisut3%efmas=0.
      pisut3%efnum=0.
    end if
    if (pisum4t > ytiny) then
      do is=1,isintso4
        pisut4%efmas(is)=pisum4(is)/pisum4t
        pisut4%efnum(is)=pisun4(is)/pisum4t
      end do
    else
      pisut4%efmas=0.
      pisut4%efnum=0.
    end if
  end if
  !
  !-----------------------------------------------------------------------
  !     * deallocate work arrays.
  !
  if (isextoc > 0) then
    deallocate(peocm1)
    deallocate(peocn1)
    deallocate(peocm2)
    deallocate(peocn2)
    deallocate(peocm3)
    deallocate(peocn3)
    deallocate(peocm4)
    deallocate(peocn4)
  end if
  if (isextbc > 0) then
    deallocate(pebcm1)
    deallocate(pebcn1)
    deallocate(pebcm2)
    deallocate(pebcn2)
    deallocate(pebcm3)
    deallocate(pebcn3)
    deallocate(pebcm4)
    deallocate(pebcn4)
  end if
  if (isextso4 > 0) then
    deallocate(pesum1)
    deallocate(pesun1)
    deallocate(pesum2)
    deallocate(pesun2)
    deallocate(pesum3)
    deallocate(pesun3)
    deallocate(pesum4)
    deallocate(pesun4)
  end if
  if (isintoc > 0) then
    deallocate(piocm1)
    deallocate(piocn1)
    deallocate(piocm2)
    deallocate(piocn2)
    deallocate(piocm3)
    deallocate(piocn3)
    deallocate(piocm4)
    deallocate(piocn4)
  end if
  if (isintbc > 0) then
    deallocate(pibcm1)
    deallocate(pibcn1)
    deallocate(pibcm2)
    deallocate(pibcn2)
    deallocate(pibcm3)
    deallocate(pibcn3)
    deallocate(pibcm4)
    deallocate(pibcn4)
  end if
  if (isintso4 > 0) then
    deallocate(pisum1)
    deallocate(pisun1)
    deallocate(pisum2)
    deallocate(pisun2)
    deallocate(pisum3)
    deallocate(pisun3)
    deallocate(pisum4)
    deallocate(pisun4)
  end if
  !
contains
  function zpsi(arg)
    real :: zpsi !<
    real :: arg !<
    zpsi=1./(2.*log(arg)**2)
  end function zpsi
  function zphi(arg)
    real :: zphi !<
    real :: arg !<
    zphi=log(arg/r0)
  end function zphi
  !
end subroutine sdpemi
