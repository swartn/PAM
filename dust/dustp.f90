!> \file
!> \brief Calculation of size-dependent dust flux.
!!
!! @authors K. von Salzen and M. Lazare
!
!-----------------------------------------------------------------------
subroutine dustp (pn0,phi0,psi,ilga,isec, &
                        flnd,gt,smfr,fn,zspd,ustard, &
                        suz0,slai,spot,st02,st03,st04,st06, &
                        st13,st14,st15,st16,st17, &
                        isvdust,defa,defc, &
                        fall,fa10,fa2,fa1, &
                        duwd,dust,duth,usmk, &
                        diagmas,diagnum,isdust)
  !
  use sdparm,  only : r0,yna,ypi
  use duparm1, only : cuscpla,nats,nclass
  !
  implicit none
  !
  integer, intent(in) :: ilga !<
  integer, intent(in) :: isec !<
  integer, intent(in) :: isvdust !<
  integer, intent(in) :: isdust !<
  real, intent(in),  dimension(ilga) :: flnd !< Fraction of land
  real, intent(in),  dimension(ilga) :: gt !< Surface temperature, \f$[K]\f$
  real, intent(in),  dimension(ilga) :: smfr !< Soil moisture fraction
  real, intent(in),  dimension(ilga) :: fn !< Fraction
  real, intent(in),  dimension(ilga) :: zspd !< Model surface horizontal wind speed \f$[m/sec]\f$
  real, intent(in),  dimension(ilga) :: ustard !< Friction velocity \f$[m/sec]\f$
  real, intent(in),  dimension(ilga) :: suz0 !< Aerodynamic roughness length for bare ground
  real, intent(in),  dimension(ilga) :: slai !<
  real, intent(in),  dimension(ilga) :: spot !< Compositional fraction of preferential soil type 1 (paleo lake bed).
  real, intent(in),  dimension(ilga) :: st02 !< Compositional fraction of soil type 2 (medium)
  real, intent(in),  dimension(ilga) :: st03 !< Compositional fraction of soil type 3 (fine)
  real, intent(in),  dimension(ilga) :: st04 !< Compositional fraction of soil type 4 (coarse medium)
  real, intent(in),  dimension(ilga) :: st06 !< Compositional fraction of soil type 6 (medium fine)
  real, intent(in),  dimension(ilga) :: st13 !< Compositional fraction of soil type 13 (Asian taklimakan)
  real, intent(in),  dimension(ilga) :: st14 !< Compositional fraction of soil type 14 (Asian loess)
  real, intent(in),  dimension(ilga) :: st15 !< Compositional fraction of soil type 15 (Asian gobi)
  real, intent(in),  dimension(ilga) :: st16 !< Compositional fraction of soil type 16 (Asian desert and sandland)
  real, intent(in),  dimension(ilga) :: st17 !< Compositional fraction of soil type 17 (Asian other mixture soil)
  real, intent(out), dimension(ilga,isec) :: pn0 !< First PLA size distribution parameter (\f$n_{0,i}\f$, amplitude)
  real, intent(out), dimension(ilga,isec) :: phi0 !< Third PLA size distribution parameter (\f$\phi_{0,i}\f$, width)
  real, intent(out), dimension(ilga,isec) :: psi !< Second PLA size distribution parameter (\f$\psi_{i}\f$, mode size)
  real, intent(out), dimension(ilga,isdust*2) :: diagmas !< Diagnostic calculation for model output of mass \f$[kg/m^2/sec]\f$
  real, intent(out), dimension(ilga,isdust*2) :: diagnum !< Diagnostic calculation for model output of number \f$[1/m^2/sec]\f$
  real, intent(out), dimension(ilga) :: defa !< Emission flux of mineral dust mass for accumulation mode \f$[kg/m^2/sec]\f$
  real, intent(out), dimension(ilga) :: defc !< Emission flux of mineral dust mass for coarse mode) \f$[kg/m2/sec]\f$
  real, intent(out), dimension(ilga) :: fall !< Fluxall array in mineral dust calculation
  real, intent(out), dimension(ilga) :: fa10 !< FA10 in mineral dust calculations
  real, intent(out), dimension(ilga) :: fa2 !< FA2 in mineral dust calculations
  real, intent(out), dimension(ilga) :: fa1 !< FA1 in mineral dust calculations
  real, intent(out), dimension(ilga) :: duwd !< Wind speed in mineral dust calculation \f$[m/sec]\f$
  real, intent(out), dimension(ilga) :: dust !< Friction velocity in mineral dust calculation \f$[m/sec]\f$
  real, intent(out), dimension(ilga) :: duth !< Friction velocity threshold in mineral dust calculation \f$[m/sec]\f$
  real, intent(out), dimension(ilga) :: usmk !< Time fraction of threshold exceedence in mineral dust calculation
  !
  real, dimension(nclass) :: uth !<
  real, dimension(nats,nclass) :: srel !<
  real, dimension(nats,nclass) :: srelv !<
  real, dimension(nats,nclass) :: su_srelv !<
  real, dimension(ilga,isec) :: tdflx !<
  real, dimension(ilga,isec) :: tdsize !<
  real, dimension(ilga,isec) :: tdvar !<
  real, dimension(ilga) :: tdmask !<
  !
  !
  integer :: il !<
  integer :: is !<
  !-----------------------------------------------------------------------
  !      dust emission from the surface
  !
  call duinit1(uth,srel,srelv,su_srelv,cuscpla)
  !
  call dufcc (flnd,gt,smfr,fn,zspd,ustard, &
                   suz0,slai,spot,st02,st03,st04,st06, &
                   st13,st14,st15,st16,st17, &
                   uth,srel,srelv,su_srelv, &
                   tdflx,tdsize,tdvar,tdmask,ilga,isec, &
                   isvdust,defa,defc, &
                   fall,fa10,fa2,fa1, &
                   duwd,dust,duth,usmk, &
                   diagmas,diagnum,isdust)
  !
  !-----------------------------------------------------------------------
  !     initialization of pla parameters.
  !
  pn0 =0.
  phi0=yna
  psi =yna
  !
  !     emission flux expressed in terms of pla parameters for
  !     log-normal size distribution.
  !
  do il=1,ilga
    if (tdmask(il)==1.) then
      do is=1,isec
        pn0 (il,is)=tdflx(il,is)* &
                      1./(sqrt(2.*ypi)*log(tdvar(il,is)))
        phi0(il,is)=log(tdsize(il,is)/r0)
        psi (il,is)=1./(2.*log(tdvar(il,is))**2)
      end do
    end if
  end do
  !
end subroutine dustp
