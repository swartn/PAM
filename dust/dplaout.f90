!> \file
!> \brief Output dust PLA parameters
!!
!! @author Knut von Salzen
!
!-----------------------------------------------------------------
subroutine dplaout (den0,dephi0,depsi,deddn, &
                          isdiag,ilga,desize,deflux,defnum)
  !
  use sdparm, only : aextf,ina,isextmd,kextmd,r0,ycnst,yna,ytiny
  use fpdef,  only : r8
  !
  implicit none
  !
  real(r8), parameter :: y2cm3=1.e-06_r8 !<
  real(r8), parameter :: y2ug=1.e+15_r8 !<
  integer, intent(in) :: isdiag !< Number of diagnostic sections
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  real, intent(in), dimension(ilga,1,isextmd) :: den0 !< First PLA parameter \f$(n_{0,i})\f$ for mineral dust
  real, intent(in), dimension(ilga,1,isextmd) :: dephi0 !< Second PLA parameter \f$(\phi_{0,i})\f$ for mineral dust
  real, intent(in), dimension(ilga,1,isextmd) :: depsi !< Third PLA parameter \f$(\psi_{i})\f$ for mineral dust
  real, intent(in), dimension(ilga,1,isextmd) :: deddn !< (Dry) density for mineral dust \f$[kg/m^3]\f$
  real, intent(out), dimension(ilga,isdiag) :: desize !< Mineral dust emissions size
  real, intent(out), dimension(ilga,isdiag) :: deflux !< Mineral dust emissions flux \f$[kg/m^2/sec]\f$
  real, intent(out), dimension(ilga,isdiag) :: defnum !< Mineral dust emissions number
  !
  !     * internal work variables
  !
  real, dimension(isdiag) :: ansd !<
  real, dimension(isdiag) :: amsd !<
  real, dimension(isdiag) :: aphip !<
  real(r8) :: atmp !<
  integer :: il !<
  integer :: is !<
  integer :: ip !<
  integer :: isec !<
  integer :: isp !<
  integer :: isi !<
  real :: radb !<
  real :: rade !<
  real :: ph0 !<
  real :: ph1 !<
  real :: dph !<
  real :: ph !<
  real :: rad !<
  !
  isec=isextmd
  radb=aextf%tp(kextmd)%radb
  rade=aextf%tp(kextmd)%rade
  ph0=log(radb/r0)
  ph1=log(rade/r0)
  dph=ph1-ph0
  do il=1,ilga
    do is=1,isextmd
      do ip=1,isdiag
        ph=ph0+real(ip-1)*dph/real(isdiag-1)
        rad=exp(ph)
        aphip(ip)=ph
        isp=ina
        if (aphip(ip) <= aextf%tp(kextmd)%phi(1)%vl) then
          isp=1
        else if (aphip(ip) > aextf%tp(kextmd)%phi(isec)%vr) then
          isp=isec
        else
          do isi=1,isec
            if (aphip(ip) >  aextf%tp(kextmd)%phi(isi)%vl &
                .and. aphip(ip) <= aextf%tp(kextmd)%phi(isi)%vr) then
              isp=isi
            end if
          end do
        end if
        if (isp /= ina) then
          if (den0(il,1,isp) > ytiny &
              .and. abs(depsi(il,1,isp)-yna) > ytiny) then
            atmp=-depsi(il,1,isp)*(aphip(ip)-dephi0(il,1,isp))**2
            atmp= &! proc-depend
                    exp(atmp)
            ansd(ip)=den0(il,1,isp)*y2cm3*atmp
            amsd(ip)=y2ug*ycnst*((r0*rad)**3)*deddn(il,1,isp)*ansd(ip)
          else
            ansd(ip)=0.
            amsd(ip)=0.
          end if
        else
          ansd(ip)=0.
          amsd(ip)=0.
        end if
      end do
      !
      do ip=1,isdiag
        !        radius [um]
        desize(il,ip)=exp(aphip(ip))
        !        dm/dlogr [ug/m3]
        deflux(il,ip)=amsd(ip)
        !        dn/dlogr [1/cm3]
        defnum(il,ip)=ansd(ip)
      end do
      !
    end do
  end do
  !
end subroutine dplaout
