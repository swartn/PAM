!> \file
!> \brief Initialize soil fields for dust emission
!!
!! @authors Y. Peng and M. Lazare
!
!-----------------------------------------------------------------------
subroutine duinit1(uth,srel,srelv,su_srelv,cuscal)
  !
  use duparm1, only : a_rnolds,b_rnolds,x_rnolds, &
                            dmax,dmin,dstep,d_thrsld, &
                            gravi,nats,nclass,nmode,pi,roa,rop,solspe
  use sdparm,  only : ytiny

  implicit none
  !
  !      * internal work fields.
  !
  real, dimension(nclass) :: uth !< Threshold friction velocity
  real, dimension(nats,nclass) :: srel !< Related surface
  real, dimension(nats,nclass) :: srelv !< Related surface
  real, dimension(nats,nclass) :: su_srelv !< Related surface
  !==================================================================
  ! physical (adjustable) parameters
  !
  real :: cuscal !< tuning parameter for dust (scale factor for wind stress threshold).
  !                      !< its minimum/default/maximum is (0.9/1.45/??).
  !==================================================================
  !
  !      * auxiliary variables
  !
  real, dimension(nclass) :: rsize !<
  real, dimension(nclass) :: dlastj !<
  real, dimension(nclass) :: su_class !<
  real, dimension(nclass) :: su_classv !<
  real, dimension(nats) :: utest !<
  real :: xku !<
  real :: xk !<
  real :: xl !<
  real :: xm !<
  real :: xn !<
  real :: xnv !<
  real :: stotal !<
  real :: stotalv !<
  real :: su !<
  real :: suv !<
  real :: su_loc !<
  real :: su_locv !<
  real :: bb !<
  real :: ccc !<
  real :: ddd !<
  real :: ee !<
  real :: ff !<
  real :: rdp !<
  !
  integer :: i !<
  integer :: j !<
  integer :: kk !<
  integer :: ns !<
  integer :: nd !<
  integer :: nsi !<
  integer :: np !<
  !
  !     * initialization
  !
  !      threshold friction velocity uth
  !      marticorena and bergametti 1995 equations (3) and (4) \cite marticorena1995
  !
  i=0
  !
  rdp=dmin
  do while (rdp <= dmax+1.e-5)
    i=i+1
    rsize(i)=rdp
    bb = a_rnolds * (rdp ** x_rnolds) + b_rnolds
    xku = sqrt(rop * gravi * rdp / roa)
    ccc = sqrt(1. + d_thrsld /(rdp ** 2.5))
    if (bb<10.) then
      ddd=sqrt(1.928 * (bb ** 0.092) - 1.)
      uth(i) = 0.129 * xku * ccc / ddd
    else
      ee = -0.0617 * (bb - 10.)
      ff = 1. -0.0858 * exp(ee)
      uth(i) = 0.12 * xku * ccc * ff
    end if
    rdp = rdp * exp(dstep)
  end do
  !
  !      scale the wind stress threshold
  !
  do i=1, nclass
    uth(i)=uth(i)*cuscal
  end do
  !
  !      loop over soil types ns
  !      calculate soil distribution and related surfaces
  !
  do ns=1,nats
    utest(ns)=0.
    !      ! initial surface values
    rdp = dmin
    kk = 0
    stotal = 0.
    stotalv = 0.
    do i=1,nclass
      su_class(i) = 0.
      su_classv(i) = 0.
    end do  ! i nclass
    !      ! surface calculation
    do while (rdp<=dmax+1.e-5)
      kk = kk + 1
      su = 0.
      suv = 0.
      !
      do i = 1, nmode
        nd  = ((i - 1) *3) + 1
        nsi = nd + 1
        np  = nd + 2
        if (solspe(nd,ns)==0.) then
          su_loc =0.
          su_locv=0.
        else
          xk = solspe(np,ns)/(sqrt(2.*pi)*log(solspe(nsi,ns)))
          xl = ((log(rdp)-log(solspe(nd,ns)))**2)/ &
                (2.*(log(solspe(nsi,ns)))**2)
          xl = min(xl,80.)
          xm = xk * exp(-xl)
          if (xm <= 10.*ytiny) xm=0.
          xn = rop*(2./3.)*(rdp/2.)
          xnv = 1.
          su_loc = xm*(dstep/xn)
          su_locv = xm*(dstep/xnv)
        end if
        su  = su  + su_loc
        suv = suv + su_locv
      end do
      !
      su_class(kk) = su
      su_classv(kk)= suv
      stotal = stotal + su
      stotalv= stotalv+suv
      dlastj(kk)=rdp*5000.
      rdp=rdp*exp(dstep)
    end do     ! rdp<=dmax
    !      ! related surface
    do j = 1,nclass
      if (stotal==0.) then
        srel(ns,j) = 0.
        srelv(ns,j)= 0.
      else
        srel(ns,j) = su_class(j) /stotal
        srelv(ns,j)= su_classv(j)/stotalv
        utest(ns) = utest(ns)+srelv(ns,j)
        su_srelv(ns,j) = utest(ns)
      end if
    end do    ! j nclass
    !
  end do     ! ns nats
  !
  !      ! loop over soil types ns ends
  !
end subroutine duinit1
