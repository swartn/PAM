!> \file
!> \brief Dunne's parameterization of nucleation.
!>
!! @author K. von Salzen
!!
!-----------------------------------------------------------------------
subroutine dunucl(gnucl,h2so4,sxp,adu,rkbn,rkbch,rktn, &
                        rktch,dualfa,prion,sclncl,acnd,dfso4,ticgr1,ticgr2, &
                        tidryr,yscale,ym3tocm3,ysdunne,pdiam,pmass,dt, &
                        ilga,leva)
  !
  use sdparm, only : avo,isintso4,wamsul,ypi,ytiny
  use fpdef,  only : r8
  !
  implicit none
  !
  real(r8), intent(out), dimension(ilga,leva) :: gnucl !< Nucleation coefficient
  real(r8), intent(out), dimension(ilga,leva) :: sxp !< Nucleation coefficient
  real(r8), intent(in), dimension(ilga,leva) :: h2so4 !< Sulphuric acid concentration
  real(r8), intent(in), dimension(ilga,leva) :: acnd !<
  real(r8), intent(in), dimension(ilga,leva) :: dfso4 !< Diffusivity of H2SO4
  real(r8), intent(in), dimension(ilga,leva) :: rkbn !< Coefficient in Dunne's parameterization
  real(r8), intent(in), dimension(ilga,leva) :: rkbch !< Coefficient in Dunne's parameterization
  real(r8), intent(in), dimension(ilga,leva) :: rktn !< Coefficient in Dunne's parameterization
  real(r8), intent(in), dimension(ilga,leva) :: rktch !< Coefficient in Dunne's parameterization
  real(r8), intent(in), dimension(ilga,leva) :: dualfa !< Ion-ion recombination rate (Brasseur and Chattel
  !< \cite Brasseur1983)
  real(r8), intent(in), dimension(ilga,leva) :: prion !< Ion production rate \f$[ion\:pairs/cm^3/sec]\f$)
  real(r8), intent(in), dimension(ilga,leva) :: sclncl !< Nucleation rate scaling parameter
  real, intent(in), dimension(20) :: adu !< Nucleation parameters
  real, dimension(ilga,leva,isintso4) :: ticgr1 !< Growth rate
  real, dimension(ilga,leva,isintso4) :: ticgr2 !< Growth rate
  real, dimension(isintso4+1) :: tidryr !< Temporary aerosol field for dry particle sizes for internally
  !< mixed aerosol type.
  real, intent(in) :: yscale !< Factor for conversion mol->fmol (1.e+15)
  real, intent(in) :: ym3tocm3 !< Factor for conversion m3->cm3 (1.e+15)
  real, intent(in) :: ysdunne !< Scaling factor (1.e-06)
  real, intent(in) :: pdiam !< Nucleation particle diameter
  real, intent(in) :: pmass !< Nucleation particle mass
  real, intent(in) :: dt !< Model time step \f$[s]\f$
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  !
  !     internal work variables
  !
  real(r8), allocatable, dimension(:,:) :: h2so4p !<
  real(r8), allocatable, dimension(:,:) :: conds !<
  real(r8), allocatable, dimension(:,:) :: grwthr !<
  real(r8), allocatable, dimension(:,:) :: eta !<
  real(r8), allocatable, dimension(:,:) :: arg !<
  real(r8), allocatable, dimension(:,:) :: cgr !<
  real(r8), allocatable, dimension(:,:) :: cgasc !<
  real(r8), allocatable, dimension(:,:) :: scalef !<
  real(r8), allocatable, dimension(:,:) :: aterm !<
  real(r8), allocatable, dimension(:,:) :: sabn !<
  real(r8), allocatable, dimension(:,:) :: sabi !<
  real(r8), allocatable, dimension(:,:) :: satn !<
  real(r8), allocatable, dimension(:,:) :: sati !<
  real(r8), allocatable, dimension(:,:) :: sabic !<
  real(r8), allocatable, dimension(:,:) :: xd !<
  real(r8), allocatable, dimension(:,:) :: cion !<
  real(r8), allocatable, dimension(:,:) :: j1p7bnr !<
  real(r8), allocatable, dimension(:,:) :: j1p7bchr !<
  real(r8), allocatable, dimension(:,:) :: j1p7tnr !<
  real(r8), allocatable, dimension(:,:) :: j1p7tchr !<
  real(r8), allocatable, dimension(:,:) :: akinlim !<
  real(r8), allocatable, dimension(:,:) :: j1p7r !<
  real, parameter :: ydref=1.7 !<
  real, parameter :: ftune=0.01 !<
  logical, parameter :: kerku=.true. !<
  real(r8) :: fact !<
  !
  !-----------------------------------------------------------------------
  !
  !     * allocate work arrays.
  !
  allocate(h2so4p  (ilga,leva))
  allocate(scalef  (ilga,leva))
  allocate(aterm   (ilga,leva))
  allocate(sabn    (ilga,leva))
  allocate(sabi    (ilga,leva))
  allocate(satn    (ilga,leva))
  allocate(sati    (ilga,leva))
  allocate(sabic   (ilga,leva))
  allocate(xd      (ilga,leva))
  allocate(cion    (ilga,leva))
  allocate(j1p7bnr (ilga,leva))
  allocate(j1p7bchr(ilga,leva))
  allocate(j1p7tnr (ilga,leva))
  allocate(j1p7tchr(ilga,leva))
  allocate(akinlim (ilga,leva))
  allocate(j1p7r   (ilga,leva))
  allocate(arg     (ilga,leva))
  if (kerku) then
    allocate(conds (ilga,leva))
    allocate(grwthr(ilga,leva))
    allocate(cgasc (ilga,leva))
    allocate(cgr   (ilga,leva))
    allocate(eta   (ilga,leva))
  end if
  !
  !     scale sulphuric acid concentration to convert from fmol/m3
  !     to 10e6*molecules/cm3
  !
  fact=(avo/(yscale*ym3tocm3))*ysdunne
  where (h2so4 > dble(ytiny)/fact)
    h2so4p=h2so4*fact
  else where
    h2so4p=0.
  end where
  !
  !     h2so4-dependent terms in parameterization. bn refers to the binary
  !     neutral rate, bi to the binary ion-induced rate, tn the ternary
  !     neutral rate, and ti the ternary ion-induced rate.
  !
  arg(:,:)=dble(adu( 1))
  sabn=h2so4p**arg
  arg(:,:)=dble(adu(14))
  sabi=h2so4p**arg
  arg(:,:)=dble(adu(10))
  satn=h2so4p**arg
  arg(:,:)=dble(adu(20))
  sati=h2so4p**arg
  sabic=rkbch
  !
  !     concentration of small negative ions (1/cm3). losses of ions due
  !     to sticking to particles and depletion via ion-induced nucleation.
  !     currently assumes steady state ion balance and that organic-h2so4
  !     nucleation is neutral.
  !
  xd=acnd+sabi*sabic+rktch*sati
  aterm=xd**2+4.*dualfa*prion
  arg=sqrt(aterm)
  arg=(arg-xd)/2.
  where (2+abs(exponent(arg) - exponent(dualfa)) &
      < maxexponent(arg) .and. dualfa/=0. )
    cion=arg/dualfa
  else where
    where (2+abs(exponent(prion) - exponent(xd)) &
        < maxexponent(prion) .and. xd/=0. )
      cion=prion/xd
    else where
      cion=prion*dt
    end where
  end where
  !
  !     nucleation rates for each process.
  !
  j1p7bnr =rkbn*sabn
  j1p7bchr=cion*sabi*sabic
  j1p7tnr =rktn*satn
  j1p7tchr=rktch*cion*sati
  !
  !     kinetic limit for ternary neutral nucleation, which can in
  !     principle be exceeded at very high nh3 concentrations.
  !
  where (h2so4p > ytiny)
    akinlim=(49.19*h2so4p**2.016)/(1.+3.126*h2so4p**(-3.594))
  else where
    akinlim=0.
  end where
  where (j1p7tnr > akinlim)
    j1p7tnr=akinlim
  end where
  where (j1p7tchr > akinlim)
    j1p7tchr=akinlim
  end where
  !
  !     sum of all nucleation rates (particles/cm3/sec).
  !
  j1p7r=j1p7bnr+j1p7bchr+j1p7tnr+j1p7tchr
  aterm=adu(1)*j1p7bnr+adu(14)*j1p7bchr+adu(10)*j1p7tnr+ &
            adu(20)*j1p7tchr
  sxp=aterm/max(j1p7r,ytiny)
  !
  !     h2so4 depletion rate in mol/m3/sec.
  !
  gnucl=-pmass*j1p7r*ym3tocm3/wamsul
  gnucl=sclncl*gnucl
  !
  !-----------------------------------------------------------------------
  !     parameterization to calculate nucleation rate at size of the
  !     smallest particles in the simulated size distribution, for a
  !     parameterized nucleation rate at a particle diameter of ydref
  !     (in nanometres).
  !
  if (kerku) then
    !
    !       condensation sink (1/m2).
    !
    conds=acnd/(4.*ypi*dfso4)
    !
    !       nuclei growth rate (nm/hour), from product of particle growth
    !       factor (m5/kg/sec) and condensable gas concentration (h2so4+2*nh3,
    !       kg/m3), divided by particle radius (m), and final unit scaling.
    !
    cgr=(ticgr1(:,:,1)+ticgr2(:,:,1)*tidryr(1))
    cgasc=h2so4*wamsul/yscale
    grwthr=cgr/tidryr(1)*cgasc
    grwthr=grwthr*1.e+09*3600.
    !
    !       eta parameter and nucleation rate scaling factor according to
    !       kerminen and kulmala (2002) \cite kerminen2002.
    !
    where (2+abs(exponent(0.23*conds) - exponent(grwthr) ) &
        < maxexponent(0.23*conds) .and. grwthr/=0. )
      eta=(0.23*conds)/grwthr
    else where
      eta=1.
    end where
    arg=(1._r8/pdiam - 1._r8/ydref)*eta
    arg=max(-10.,min(arg,10.))
    scalef=exp(arg)
  else
    scalef=0.001
  end if
  gnucl=gnucl*scalef
  !
  !     scale h2so4 depletion rate to convert from mol/m3/sec to fmol/m3/sec.
  !
  gnucl=gnucl*yscale*ftune
  !
  !     * deallocate work arrays.
  !
  deallocate(h2so4p)
  deallocate(scalef)
  deallocate(aterm)
  deallocate(sabn)
  deallocate(sabi)
  deallocate(satn)
  deallocate(sati)
  deallocate(sabic)
  deallocate(xd)
  deallocate(cion)
  deallocate(j1p7bnr)
  deallocate(j1p7bchr)
  deallocate(j1p7tnr)
  deallocate(j1p7tchr)
  deallocate(akinlim)
  deallocate(j1p7r)
  deallocate(arg)
  if (kerku) then
    deallocate(conds)
    deallocate(grwthr)
    deallocate(cgasc)
    deallocate(cgr)
    deallocate(eta)
  end if
  !
end subroutine dunucl
!
!> \file
!! \subsection ssec_details Details
!! It is assumed that sufficient ammonia
!! is available to form (NH4)2SO4 particles once the particles
!! are produced by nucleation. Although the particles intially
!! contain water they are injected as dry particles at the lower
!! boundary of the size spectrum.
!! \n
!! ETA parameter and nucleation rate scaling factor is according to
!! Kerminen and Kulmala (2002) \cite Kerminen2002
