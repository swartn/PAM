!> \file
!> \brief Gas-to-particle conversion for ammonium-sulphate aerosol.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
!
subroutine gtpcnv (dgdt,pedndt,pedmdt,pidndt,pidmdt,pidfdt, &
                         peddn,pemas,pen0,penum,pephi0,pepsi,pewetrc, &
                         pifrc,piddn,pimas,pin0,pinum,piphi0,pipsi, &
                         piwetrc,t,rh,rhoa,h2so4i,cnh3,sgpr,pblts, &
                         dt,ilga,leva,dersn,dersm,dirsn,dirsmn,dirsms, &
                         denncl,decncl,dinncl,dicncl,decnd,dicnd,ierr)
  !
  use sdparm, only : aextf,aintf,drydn0, &
                         iexso4,iinso4,isaext,isaint,isextso4,isintso4,iso4s,itso4, &
                         kextso4,kint,kintso4, &
                         pedphis,pephiss, &
                         pidphis,piphiss, &
                         r0,wamsul,wh2so4,wnh3, &
                         ylarge,ylarge8,yna,ypi,ysec,ysecs,ysmall,ytiny,ytiny8
  use fpdef,  only : r8
  !
  implicit none
  !
  real, intent(out), dimension(ilga,leva) :: dgdt !< Gas-phase sulphuric acid (H2SO4) tendency \f$[kg/kg/sec]\f$
  real, intent(out), dimension(ilga,leva,isaext) :: pedndt !< Number tendency \f$[1/kg/sec]\f$, ext. mixture
  real, intent(out), dimension(ilga,leva,isaext) :: pedmdt !< mass tendency \f$[kg/kg/sec]\f$, ext. mixture
  real, intent(out), dimension(ilga,leva,isaint) :: pidndt !< Number tendency \f$[1/kg/sec]\f$, int. mixture
  real, intent(out), dimension(ilga,leva,isaint) :: pidmdt !< Mass tendency \f$[kg/kg/sec]\f$, int. mixture
  real, intent(out), dimension(ilga,leva,isaint,kint) :: pidfdt !< Aerosol species mass fraction tendency
  !< \f$[1/sec]\f$, int. mixture
  real, intent(out), dimension(ilga,leva) :: denncl !< (NH4)2SO4 number nucleation rate \f$[1/kg/sec]\f$ ext. mixture
  real, intent(out), dimension(ilga,leva) :: decncl !< (NH4)2SO4 mass nucleation rate \f$[kg/kg/sec]\f$ ext. mixture
  real, intent(out), dimension(ilga,leva) :: dersn !< Number residuum from numerical truncation in growth
  !< calculations \f$[kg/kg]\f$, ext. mixture
  real, intent(out), dimension(ilga,leva) :: dersm !< Mass residuum from numerical truncation in growth
  !< calculations \f$[kg/kg]\f$ ext. mixture
  real, intent(out), dimension(ilga,leva) :: decnd !< (NH4)2SO4 condensation rate \f$[kg/kg/sec]\f$ ext. mixture
  real, intent(out), dimension(ilga,leva) :: dinncl !< (NH4)2SO4 number nucleation rate \f$[1/kg/sec]\f$ int. mixture
  real, intent(out), dimension(ilga,leva) :: dicncl !< (NH4)2SO4 mass nucleation rate \f$[kg/kg/sec]\f$ int. mixture
  real, intent(out), dimension(ilga,leva) :: dirsmn !< Mass residuum for non-(NH4)2SO4 aerosol species from num.
  !< truncation in growth calculations \f$[kg/kg]\f$ int. mixture
  real, intent(out), dimension(ilga,leva) :: dirsn !< Number residuum from numerical truncation in growth
  !< calculations \f$[kg/kg]\f$ int. mixture
  real, intent(out), dimension(ilga,leva) :: dirsms !< Mass residuum for (NH4)2SO4 aerosol from num. truncationin
  !< growth calculations \f$[kg/kg]\f$ int. mixture
  real, intent(out), dimension(ilga,leva) :: dicnd !< (NH4)2SO4 condensation rate \f$[kg/kg/sec]\f$ int. mixture
  integer, intent(in), dimension(ilga) :: pblts !< Offset index for vertical axis, indicating topmost point
  !< in boundary layer
  real, intent(in), dimension(ilga,leva) :: sgpr !< Gas-phase production rate of H2SO4 \f$[kg/kg/sec]\f$
  real, intent(in), dimension(ilga,leva) :: t !< Air temperature \f$ [K]\f$
  real, intent(in), dimension(ilga,leva) :: rh !< Relative Humidity \f$[dimensionless]\f$
  real, intent(in), dimension(ilga,leva) :: rhoa !< Air density \f$[kg/m3]\f$
  real, intent(in), dimension(ilga,leva) :: h2so4i !< Initial H2SO4 mixing ratio \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva) :: cnh3 !< Gas-phase ammonia (NH3) volume mixing ratio
  real, intent(in), dimension(ilga,leva,isaext) :: peddn !< (Dry) density for externally mixed aerosol particles
  !< \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pemas !< Aerosol (dry) mass concentration for externally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,
  !< amplitude) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: penum !< Aerosol number concentration for externally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,
  !< width), ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$,
  !< mode size) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pewetrc !< Total/wet paricle radius \f$[m]\f$, ext. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piddn !< (Dry) density for internally mixed aerosol particles
  !< \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,
  !< amplitude) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pinum !< Aerosol number concentration for internally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,
  !< width), int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$,
  !< mode size) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piwetrc !< Total/wet paricle radius \f$[m\f$, int. mixture
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type
  !< for internally mixed aerosol
  integer, intent(out) :: ierr !< Warning code index
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  !
  !     internal work variables
  !
  integer, parameter :: ida=40 !<
  integer, parameter :: idam=ida-1 !<
  real, dimension(ilga,leva,idam) :: acoef !<
  real, dimension(ilga,leva,idam) :: bcoef !<
  real, dimension(ida) :: ch2so4 !<
  real, dimension(ida) :: epsc !<
  real, allocatable, dimension(:,:,:) :: ch2so4f !<
  real, allocatable, dimension(:,:,:) :: epscf !<
  real, allocatable, dimension(:,:,:,:) :: timn !<
  real, allocatable, dimension(:,:,:,:) :: tifrcn !<
  real(r8), allocatable, dimension(:,:,:) :: tecndgp !<
  real(r8), allocatable, dimension(:,:,:) :: tect1 !<
  real(r8), allocatable, dimension(:,:,:) :: tect2 !<
  real, allocatable, dimension(:,:,:) :: tewetrc !<
  real, allocatable, dimension(:,:,:) :: teddn !<
  real, allocatable, dimension(:,:,:) :: tecgr1 !<
  real, allocatable, dimension(:,:,:) :: tecgr2 !<
  real, allocatable, dimension(:,:,:) :: ten0 !<
  real, allocatable, dimension(:,:,:) :: tephi0 !<
  real, allocatable, dimension(:,:,:) :: tepsi !<
  real, allocatable, dimension(:,:,:) :: tedphit !<
  real, allocatable, dimension(:,:,:) :: tephist !<
  real, allocatable, dimension(:,:,:) :: tecorf !<
  real, allocatable, dimension(:,:,:) :: tedccnd !<
  real, allocatable, dimension(:,:,:) :: tedxcnd !<
  real, allocatable, dimension(:,:,:) :: tent !<
  real, allocatable, dimension(:,:,:) :: tefrc !<
  real, allocatable, dimension(:,:,:) :: tedryrt !<
  real(r8), allocatable, dimension(:,:,:) :: tedp0l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp1l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp2l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp3l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp4l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp5l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp6l !<
  real(r8), allocatable, dimension(:,:,:) :: tedm1l !<
  real(r8), allocatable, dimension(:,:,:) :: tedm2l !<
  real(r8), allocatable, dimension(:,:,:) :: tedm3l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp0r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp1r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp2r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp3r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp4r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp5r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp6r !<
  real(r8), allocatable, dimension(:,:,:) :: tedm1r !<
  real(r8), allocatable, dimension(:,:,:) :: tedm2r !<
  real(r8), allocatable, dimension(:,:,:) :: tedm3r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp0s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp1s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp2s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp3s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp4s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp5s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp6s !<
  real(r8), allocatable, dimension(:,:,:) :: tedm1s !<
  real(r8), allocatable, dimension(:,:,:) :: tedm2s !<
  real(r8), allocatable, dimension(:,:,:) :: tedm3s !<
  real(r8), allocatable, dimension(:,:,:) :: ticndgp !<
  real(r8), allocatable, dimension(:,:,:) :: tict1 !<
  real(r8), allocatable, dimension(:,:,:) :: tict2 !<
  real, allocatable, dimension(:,:,:) :: tiwetrc !<
  real, allocatable, dimension(:,:,:) :: tiddn !<
  real, allocatable, dimension(:,:,:) :: ticgr1 !<
  real, allocatable, dimension(:,:,:) :: ticgr2 !<
  real, allocatable, dimension(:,:,:) :: tin0 !<
  real, allocatable, dimension(:,:,:) :: tiphi0 !<
  real, allocatable, dimension(:,:,:) :: tipsi !<
  real, allocatable, dimension(:,:,:) :: tidphit !<
  real, allocatable, dimension(:,:,:) :: tiphist !<
  real, allocatable, dimension(:,:,:) :: ticorf !<
  real, allocatable, dimension(:,:,:) :: tidccnd !<
  real, allocatable, dimension(:,:,:) :: tidxcnd !<
  real, allocatable, dimension(:,:,:) :: tint !<
  real, allocatable, dimension(:,:,:) :: tifrcs !<
  real, allocatable, dimension(:,:,:) :: tidryrt !<
  real(r8), allocatable, dimension(:,:,:) :: tidp0l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp1l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp2l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp3l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp4l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp5l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp6l !<
  real(r8), allocatable, dimension(:,:,:) :: tidm1l !<
  real(r8), allocatable, dimension(:,:,:) :: tidm2l !<
  real(r8), allocatable, dimension(:,:,:) :: tidm3l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp0r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp1r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp2r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp3r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp4r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp5r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp6r !<
  real(r8), allocatable, dimension(:,:,:) :: tidm1r !<
  real(r8), allocatable, dimension(:,:,:) :: tidm2r !<
  real(r8), allocatable, dimension(:,:,:) :: tidm3r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp0s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp1s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp2s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp3s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp4s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp5s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp6s !<
  real(r8), allocatable, dimension(:,:,:) :: tidm1s !<
  real(r8), allocatable, dimension(:,:,:) :: tidm2s !<
  real(r8), allocatable, dimension(:,:,:) :: tidm3s !<
  real(r8), allocatable, dimension(:,:,:) :: temt !<
  real(r8), allocatable, dimension(:,:,:) :: timst !<
  real(r8), allocatable, dimension(:,:,:,:) :: timnt !<
  real, allocatable, dimension(:,:,:) :: ten !<
  real, allocatable, dimension(:,:,:) :: tem !<
  real, allocatable, dimension(:,:,:) :: tin !<
  real, allocatable, dimension(:,:,:) :: tim !<
  real, allocatable, dimension(:,:,:) :: timt !<
  real, allocatable, dimension(:,:,:) :: tims !<
  real, allocatable, dimension(:,:,:) :: tirsmn !<
  real, allocatable, dimension(:,:,:) :: titm1 !<
  real, allocatable, dimension(:,:,:) :: titm2 !<
  real(r8), allocatable, dimension(:,:) :: anucl !<
  real(r8), allocatable, dimension(:,:) :: sxp !<
  real(r8), allocatable, dimension(:,:) :: dfso4 !<
  real(r8), allocatable, dimension(:,:) :: dbl1 !<
  real(r8), allocatable, dimension(:,:) :: dbl2 !<
  real(r8), allocatable, dimension(:,:) :: dbl3 !<
  real(r8), allocatable, dimension(:,:) :: gnucla !<
  real(r8), allocatable, dimension(:,:) :: gnuclb !<
  real(r8), allocatable, dimension(:,:) :: gnuclc !<
  real(r8), allocatable, dimension(:,:) :: gnucll !<
  real(r8), allocatable, dimension(:,:) :: gnuclh !<
  real(r8), allocatable, dimension(:,:) :: sxpt !<
  real(r8), allocatable, dimension(:,:) :: afuna !<
  real(r8), allocatable, dimension(:,:) :: afunb !<
  real(r8), allocatable, dimension(:,:) :: afunc !<
  real, allocatable, dimension(:,:) :: bnucl !<
  real, allocatable, dimension(:,:) :: plfn !<
  real, allocatable, dimension(:,:) :: cndm !<
  real, allocatable, dimension(:,:) :: terms !<
  real, allocatable, dimension(:,:) :: cndmx !<
  real, allocatable, dimension(:,:) :: effc !<
  real, allocatable, dimension(:,:) :: frp !<
  real, allocatable, dimension(:,:) :: plam !<
  real, allocatable, dimension(:,:) :: pertc !<
  real, allocatable, dimension(:,:) :: corfa !<
  real, allocatable, dimension(:,:) :: corfar !<
  real, allocatable, dimension(:,:) :: dtc !<
  real(r8), allocatable, dimension(:,:) :: acnd !<
  real(r8), allocatable, dimension(:,:) :: termx !<
  real(r8), allocatable, dimension(:,:) :: termy !<
  real(r8), allocatable, dimension(:,:) :: term1 !<
  real(r8), allocatable, dimension(:,:) :: term2 ! ,yscalf
  real(r8), allocatable, dimension(:,:) :: term3 !<
  real(r8), allocatable, dimension(:,:) :: term4 !<
  real(r8), allocatable, dimension(:,:) :: term5 !<
  real(r8), allocatable, dimension(:,:) :: sgprt !<
  real(r8), allocatable, dimension(:,:) :: h2so4t !<
  real(r8), allocatable, dimension(:,:) :: expt !<
  real(r8), allocatable, dimension(:,:) :: sadt !<
  real(r8), allocatable, dimension(:,:) :: ctmpx !<
  real(r8), allocatable, dimension(:,:) :: ctmpy !<
  real(r8), allocatable, dimension(:,:) :: ctmpmx !<
  real(r8), allocatable, dimension(:,:) :: ctmp !<
  real(r8), allocatable, dimension(:,:) :: fmulf !<
  real(r8), allocatable, dimension(:,:) :: h2so4a !<
  real(r8), allocatable, dimension(:,:) :: h2so4b !<
  real(r8), allocatable, dimension(:,:) :: h2so4c !<
  real(r8), allocatable, dimension(:,:) :: h2so4l !<
  real(r8), allocatable, dimension(:,:) :: h2so4h !<
  real(r8), allocatable, dimension(:,:) :: h2so4eq !<
  real(r8), allocatable, dimension(:,:) :: cnh3n !<
  real(r8), allocatable, dimension(:,:) :: cnh3ch !<
  real(r8), allocatable, dimension(:,:) :: rkbn !<
  real(r8), allocatable, dimension(:,:) :: rkbch !<
  real(r8), allocatable, dimension(:,:) :: rktn !<
  real(r8), allocatable, dimension(:,:) :: rktch !<
  real(r8), allocatable, dimension(:,:) :: dualfa !<
  real(r8), allocatable, dimension(:,:) :: prion !<
  real(r8), allocatable, dimension(:,:) :: sclncl !<
  real, allocatable, dimension(:,:) :: tecorfa !<
  real, allocatable, dimension(:,:) :: texncl !<
  real, allocatable, dimension(:,:) :: ticorfa !<
  real, allocatable, dimension(:,:) :: tixncl !<
  real, allocatable, dimension(:) :: tecorfs !<
  real, allocatable, dimension(:) :: tedryr !<
  real, allocatable, dimension(:) :: tedryrc !<
  real, allocatable, dimension(:) :: ticorfs !<
  real, allocatable, dimension(:) :: tidryr !<
  real, allocatable, dimension(:) :: tidryrc !<
  integer, allocatable, dimension(:,:,:) :: tei0l !<
  integer, allocatable, dimension(:,:,:) :: tei0r !<
  integer, allocatable, dimension(:,:,:) :: tii0l !<
  integer, allocatable, dimension(:,:,:) :: tii0r !<
  real(r8) :: ccoef !<
  real(r8) :: ccoefb !<
  real(r8) :: ctmpxa !<
  real(r8) :: ctmpxb !<
  real :: ym3tocm3 !<
  real :: ysdunne !<
  real, dimension(20) :: adu !<
  integer :: iter !<
  integer :: is !<
  integer :: it !<
  integer :: inds !<
  integer :: il !<
  integer :: l !<
  integer :: i !<
  integer :: k !<
  real :: weight !<
  real :: wrat !<
  real :: fmul !<
  real :: pmassex !<
  real :: pmassin !<
  real :: pmass !<
  real :: pdiam !<
  real :: ysclrf !<
  real :: dt !<
  real :: atmn !<
  real :: atmx !<
  real :: tecorfm !<
  real :: pdiamex !<
  real :: pdiamin !<
  real :: ticorfm !<
  real :: corfm !<
  real :: cfn !<
  real :: cfm !<
  real :: term6 !<
  !
  !     declare numerical constants.
  !
  real, parameter :: ywg=1. ! weighting for initial h2so4
  real, parameter :: yeps=0.1 ! threshold for convergence test
  real, parameter :: yred=0. ! step size for nucleation calls
  real, parameter :: yscale=1.e+15 ! factor for conversion mol->fmol
  integer, parameter :: itern=5 ! number of iterations for
  ! h2so4 budget balance
  integer, parameter :: imodc=1 !<
  integer, parameter :: imeth=3 !<
  logical, parameter :: kblncl=.false. !<
  !
  !-----------------------------------------------------------------------
  !
  ierr=0
  if (imeth == 3) then
    iter=1
  else
    iter=5
  end if
  !
  !     define weighting for nucleation of externally vs. internally mixed
  !     aerosol type.
  !
  if (kextso4 > 0 .and. kintso4 > 0) then
    weight=.5
  else if (kextso4 > 0) then
    weight=1.
  else
    weight=0.
  end if
  !
  !     * allocate memory for local fields.
  !
  if (isextso4 > 0) then
    allocate(tewetrc(ilga,leva,isextso4))
    allocate(ten    (ilga,leva,isextso4))
    allocate(tent   (ilga,leva,isextso4))
    allocate(tem    (ilga,leva,isextso4))
    allocate(temt   (ilga,leva,isextso4))
    allocate(teddn  (ilga,leva,isextso4))
    allocate(ten0   (ilga,leva,isextso4))
    allocate(tephi0 (ilga,leva,isextso4))
    allocate(tepsi  (ilga,leva,isextso4))
    allocate(tect1  (ilga,leva,isextso4))
    allocate(tect2  (ilga,leva,isextso4))
    allocate(tecgr1 (ilga,leva,isextso4))
    allocate(tecgr2 (ilga,leva,isextso4))
    allocate(tecndgp(ilga,leva,isextso4))
    allocate(tedccnd(ilga,leva,isextso4))
    allocate(tedxcnd(ilga,leva,isextso4))
    allocate(tedphit(ilga,leva,isextso4))
    allocate(tephist(ilga,leva,isextso4))
    allocate(tecorf (ilga,leva,isextso4))
    allocate(tei0l  (ilga,leva,isextso4))
    allocate(tei0r  (ilga,leva,isextso4))
    allocate(tedp0l (ilga,leva,isextso4))
    allocate(tedp1l (ilga,leva,isextso4))
    allocate(tedp2l (ilga,leva,isextso4))
    allocate(tedp3l (ilga,leva,isextso4))
    allocate(tedp4l (ilga,leva,isextso4))
    allocate(tedp5l (ilga,leva,isextso4))
    allocate(tedp6l (ilga,leva,isextso4))
    allocate(tedm1l (ilga,leva,isextso4))
    allocate(tedm2l (ilga,leva,isextso4))
    allocate(tedm3l (ilga,leva,isextso4))
    allocate(tedp0r (ilga,leva,isextso4))
    allocate(tedp1r (ilga,leva,isextso4))
    allocate(tedp2r (ilga,leva,isextso4))
    allocate(tedp3r (ilga,leva,isextso4))
    allocate(tedp4r (ilga,leva,isextso4))
    allocate(tedp5r (ilga,leva,isextso4))
    allocate(tedp6r (ilga,leva,isextso4))
    allocate(tedm1r (ilga,leva,isextso4))
    allocate(tedm2r (ilga,leva,isextso4))
    allocate(tedm3r (ilga,leva,isextso4))
    allocate(tedp0s (ilga,leva,isextso4))
    allocate(tedp1s (ilga,leva,isextso4))
    allocate(tedp2s (ilga,leva,isextso4))
    allocate(tedp3s (ilga,leva,isextso4))
    allocate(tedp4s (ilga,leva,isextso4))
    allocate(tedp5s (ilga,leva,isextso4))
    allocate(tedp6s (ilga,leva,isextso4))
    allocate(tedm1s (ilga,leva,isextso4))
    allocate(tedm2s (ilga,leva,isextso4))
    allocate(tedm3s (ilga,leva,isextso4))
    allocate(tefrc  (ilga,leva,isextso4))
    allocate(tedryrt(ilga,leva,isextso4+1))
    allocate(tecorfs(isextso4))
    allocate(tedryr (isextso4+1))
    allocate(tedryrc(isextso4))
  end if
  if (isintso4 > 0) then
    allocate(timn   (ilga,leva,isintso4,iso4s))
    allocate(timnt  (ilga,leva,isintso4,iso4s))
    allocate(tifrcn (ilga,leva,isintso4,iso4s))
    allocate(tims   (ilga,leva,isintso4))
    allocate(timst  (ilga,leva,isintso4))
    allocate(tiwetrc(ilga,leva,isintso4))
    allocate(tin    (ilga,leva,isintso4))
    allocate(tint   (ilga,leva,isintso4))
    allocate(tim    (ilga,leva,isintso4))
    allocate(timt   (ilga,leva,isintso4))
    allocate(tiddn  (ilga,leva,isintso4))
    allocate(tin0   (ilga,leva,isintso4))
    allocate(tiphi0 (ilga,leva,isintso4))
    allocate(tipsi  (ilga,leva,isintso4))
    allocate(tict1  (ilga,leva,isintso4))
    allocate(tict2  (ilga,leva,isintso4))
    allocate(ticgr1 (ilga,leva,isintso4))
    allocate(ticgr2 (ilga,leva,isintso4))
    allocate(ticndgp(ilga,leva,isintso4))
    allocate(tidccnd(ilga,leva,isintso4))
    allocate(tidxcnd(ilga,leva,isintso4))
    allocate(tidphit(ilga,leva,isintso4))
    allocate(tiphist(ilga,leva,isintso4))
    allocate(ticorf (ilga,leva,isintso4))
    allocate(tii0l  (ilga,leva,isintso4))
    allocate(tii0r  (ilga,leva,isintso4))
    allocate(tidp0l (ilga,leva,isintso4))
    allocate(tidp1l (ilga,leva,isintso4))
    allocate(tidp2l (ilga,leva,isintso4))
    allocate(tidp3l (ilga,leva,isintso4))
    allocate(tidp4l (ilga,leva,isintso4))
    allocate(tidp5l (ilga,leva,isintso4))
    allocate(tidp6l (ilga,leva,isintso4))
    allocate(tidm1l (ilga,leva,isintso4))
    allocate(tidm2l (ilga,leva,isintso4))
    allocate(tidm3l (ilga,leva,isintso4))
    allocate(tidp0r (ilga,leva,isintso4))
    allocate(tidp1r (ilga,leva,isintso4))
    allocate(tidp2r (ilga,leva,isintso4))
    allocate(tidp3r (ilga,leva,isintso4))
    allocate(tidp4r (ilga,leva,isintso4))
    allocate(tidp5r (ilga,leva,isintso4))
    allocate(tidp6r (ilga,leva,isintso4))
    allocate(tidm1r (ilga,leva,isintso4))
    allocate(tidm2r (ilga,leva,isintso4))
    allocate(tidm3r (ilga,leva,isintso4))
    allocate(tidp0s (ilga,leva,isintso4))
    allocate(tidp1s (ilga,leva,isintso4))
    allocate(tidp2s (ilga,leva,isintso4))
    allocate(tidp3s (ilga,leva,isintso4))
    allocate(tidp4s (ilga,leva,isintso4))
    allocate(tidp5s (ilga,leva,isintso4))
    allocate(tidp6s (ilga,leva,isintso4))
    allocate(tidm1s (ilga,leva,isintso4))
    allocate(tidm2s (ilga,leva,isintso4))
    allocate(tidm3s (ilga,leva,isintso4))
    allocate(tifrcs (ilga,leva,isintso4))
    allocate(tidryrt(ilga,leva,isintso4+1))
    allocate(tirsmn (ilga,leva,iso4s))
    allocate(titm1  (ilga,leva,iso4s))
    allocate(titm2  (ilga,leva,iso4s))
    allocate(ticorfs(isintso4))
    allocate(tidryr (isintso4+1))
    allocate(tidryrc(isintso4))
  end if
  allocate(tecorfa(ilga,leva))
  allocate(ticorfa(ilga,leva))
  allocate(texncl (ilga,leva))
  allocate(tixncl (ilga,leva))
  allocate(gnucla (ilga,leva))
  allocate(gnuclb (ilga,leva))
  allocate(gnuclc (ilga,leva))
  allocate(gnucll (ilga,leva))
  allocate(gnuclh (ilga,leva))
  allocate(afuna  (ilga,leva))
  allocate(afunb  (ilga,leva))
  allocate(afunc  (ilga,leva))
  allocate(anucl  (ilga,leva))
  allocate(bnucl  (ilga,leva))
  allocate(sxp    (ilga,leva))
  allocate(sxpt   (ilga,leva))
  allocate(ctmp   (ilga,leva))
  allocate(acnd   (ilga,leva))
  allocate(plfn   (ilga,leva))
  allocate(cndm   (ilga,leva))
  allocate(cndmx  (ilga,leva))
  allocate(effc   (ilga,leva))
  allocate(termx  (ilga,leva))
  allocate(termy  (ilga,leva))
  allocate(terms  (ilga,leva))
  allocate(term1  (ilga,leva))
  allocate(term2  (ilga,leva))
  allocate(term3  (ilga,leva))
  allocate(term4  (ilga,leva))
  allocate(term5  (ilga,leva))
  allocate(sgprt  (ilga,leva))
  allocate(h2so4t (ilga,leva))
  allocate(h2so4a (ilga,leva))
  allocate(h2so4b (ilga,leva))
  allocate(h2so4c (ilga,leva))
  allocate(h2so4l (ilga,leva))
  allocate(h2so4h (ilga,leva))
  allocate(h2so4eq (ilga,leva))
  allocate(dfso4  (ilga,leva))
  allocate(frp    (ilga,leva))
  allocate(plam   (ilga,leva))
  allocate(pertc  (ilga,leva))
  allocate(corfa  (ilga,leva))
  allocate(corfar (ilga,leva))
  allocate(dtc    (ilga,leva))
  allocate(expt   (ilga,leva))
  allocate(sadt   (ilga,leva))
  allocate(ctmpx  (ilga,leva))
  allocate(ctmpy  (ilga,leva))
  allocate(ctmpmx (ilga,leva))
  allocate(fmulf  (ilga,leva))
  !      allocate(yscalf (ilga,leva))
  allocate(ch2so4f(ilga,leva,ida))
  allocate(epscf  (ilga,leva,ida))
  allocate(dbl1   (ilga,leva))
  allocate(dbl2   (ilga,leva))
  allocate(dbl3   (ilga,leva))
  allocate(cnh3n  (ilga,leva))
  allocate(cnh3ch (ilga,leva))
  allocate(rkbn   (ilga,leva))
  allocate(rkbch  (ilga,leva))
  allocate(rktn   (ilga,leva))
  allocate(rktch  (ilga,leva))
  allocate(dualfa (ilga,leva))
  allocate(prion  (ilga,leva))
  allocate(sclncl (ilga,leva))
  !
  !     * initialization.
  !
  if (isaext > 0) then
    pedndt=0.
    pedmdt=0.
  end if
  if (isaint > 0) then
    pidndt=0.
    pidmdt=0.
    pidfdt=0.
  end if
  !
  !     temporary aerosol fields for sulphate aerosol for dry
  !     and wet particle sizes for externally mixed aerosol type.
  !
  if (kextso4 > 0) then
    do is=1,isextso4
      tewetrc(:,:,is)=pewetrc(:,:,iexso4(is))
      ten    (:,:,is)=penum  (:,:,iexso4(is))
      tem    (:,:,is)=pemas  (:,:,iexso4(is))
      teddn  (:,:,is)=peddn  (:,:,iexso4(is))
      ten0   (:,:,is)=pen0   (:,:,iexso4(is))
      tephi0 (:,:,is)=pephi0 (:,:,iexso4(is))
      tepsi  (:,:,is)=pepsi  (:,:,iexso4(is))
      tedphit(:,:,is)=pedphis(iexso4(is))
      tephist(:,:,is)=pephiss(iexso4(is))
      tedryrc(is)=aextf%tp(kextso4)%dryrc(is)
      tedryr(is)=aextf%tp(kextso4)%dryr(is)%vl
      tedryrt(:,:,is)=tedryr(is)
    end do
    tedryr(isextso4+1)=aextf%tp(kextso4)%dryr(isextso4)%vr
    tedryrt(:,:,isextso4+1)=tedryr(isextso4+1)
  end if
  !
  !     temporary aerosol fields for sulphate aerosol for dry
  !     and wet particle sizes for internally mixed aerosol type.
  !
  if (kintso4 > 0) then
    do is=1,isintso4
      tiwetrc(:,:,is)=piwetrc(:,:,iinso4(is))
      tin    (:,:,is)=pinum  (:,:,iinso4(is))
      tim    (:,:,is)=pimas  (:,:,iinso4(is))
      tims   (:,:,is)=pimas  (:,:,iinso4(is)) &
                         *pifrc  (:,:,iinso4(is),kintso4)
      tifrcs (:,:,is)=pifrc  (:,:,iinso4(is),kintso4)
      do it=1,iso4s
        timn(:,:,is,it)=pimas  (:,:,iinso4(is)) &
                           *pifrc  (:,:,iinso4(is),itso4(it))
        tifrcn(:,:,is,it)=pifrc(:,:,iinso4(is),itso4(it))
      end do
      tiddn  (:,:,is)=piddn  (:,:,iinso4(is))
      tin0   (:,:,is)=pin0   (:,:,iinso4(is))
      tiphi0 (:,:,is)=piphi0 (:,:,iinso4(is))
      tipsi  (:,:,is)=pipsi  (:,:,iinso4(is))
      tidphit(:,:,is)=pidphis(iinso4(is))
      tiphist(:,:,is)=piphiss(iinso4(is))
      tidryrc(is)=aintf%dryrc(iinso4(is))
      tidryr(is)=aintf%dryr(iinso4(is))%vl
      tidryrt(:,:,is)=tidryr(is)
    end do
    tidryr(isintso4+1)=aintf%dryr(aintf%tp(kintso4)%isce)%vr
    tidryrt(:,:,isintso4+1)=tidryr(isintso4+1)
  end if
  !
  !-----------------------------------------------------------------------
  !     diffusivity of h2so4, mean free path, and rate coefficients for
  !     condensation of h2so4 onto pre-existing aerosol (cndgp, in 1/s).
  !
  terms=sqrt(t)
  terms=terms*t
  dfso4 = 2.150e-09_r8*terms
  term2=1.62864e-25_r8/t
  term1=sqrt(term2)
  frp = 3.372999311e+11_r8*dfso4*term1
  wrat=wamsul/wh2so4
  acnd=0.
  if (isextso4 > 0) then
    call grwtrm(tecndgp,tecgr1,tecgr2,tect1,tect2,ten0,ten,tewetrc, &
                    tedryrc,tedryr,teddn,tephi0,tepsi,tephist,tedphit, &
                    frp,dfso4,wrat,rhoa,leva,ilga,isextso4)
    acnd=acnd+sum(tecndgp,dim=3)
    !        tecgr2=0.
  end if
  if (isintso4 > 0) then
    call grwtrm(ticndgp,ticgr1,ticgr2,tict1,tict2,tin0,tin,tiwetrc, &
                    tidryrc,tidryr,tiddn,tiphi0,tipsi,tiphist,tidphit, &
                    frp,dfso4,wrat,rhoa,leva,ilga,isintso4)
    acnd=acnd+sum(ticndgp,dim=3)
    !        ticgr2=0.
  end if
  !
  !-----------------------------------------------------------------------
  !     scale initial sulphuric acid production rate and concentration
  !     in order to obtain units fmol/m3/sec resp. fmol/m3.
  !
  fmul=wh2so4/yscale
  sgprt=sgpr*rhoa/fmul
  h2so4t = max(h2so4i*rhoa/fmul,0.)
  !
  !-----------------------------------------------------------------------
  !     density and molecular weight for nucleation particle. check if
  !     the specified molecular weight from the model input agrees
  !     with the internally assumed molecular weight for nucleation
  !     particles.
  !
  if (kextso4>0) then
    drydn0=aextf%tp(kextso4)%dens
    if (abs(aextf%tp(kextso4)%molw-yna) > ytiny &
        .and. abs(aextf%tp(kextso4)%molw/wamsul-1.) > ysecs) then
      call xit('GTPCNV',-1)
    end if
  else if (kintso4>0) then
    drydn0=aintf%tp(kintso4)%dens
    if (abs(aintf%tp(kintso4)%molw-yna) > ytiny &
        .and. abs(aintf%tp(kintso4)%molw/wamsul-1.) > ysecs) then
      call xit('GTPCNV',-2)
    end if
  end if
  !
  !-----------------------------------------------------------------------
  !     mass and diameter of nucleation particles.
  !
  pmassex=0.
  pmassin=0.
  pdiamex=0.
  pdiamin=0.
  if (kextso4 > 0) then
    pmassex=(4.*ypi/3.)*drydn0*tedryr(1)**3
    pdiamex=tedryr(1)**3
  end if
  if (kintso4 > 0) then
    pmassin=(4.*ypi/3.)*drydn0*tidryr(1)**3
    pdiamin=tidryr(1)**3
  end if
  pmass=weight*pmassex+(1.-weight)*pmassin
  pdiam=2.*1.e+09*(weight*pdiamex+(1.-weight)*pdiamin)**(1./3.)
  if (imeth==1) then
    !
    !       Kulmala's parameterization of nulceation.
    !
    call bhnucl(sxp,anucl,t,rh,pmass,yscale,ilga,leva)
  else if (imeth==2) then
    !
    !       Tabulated results for merikanto's parameterization of nulceation.
    !
    call tbnucl(acoef,bcoef,ch2so4,epsc,t,cnh3,pmass, &
                    ilga,leva,ida,idam)
    ysclrf=1.e+09
    acoef=acoef*yscale/ysclrf
    ch2so4=ch2so4*yscale/ysclrf
    epsc=epsc*yscale/ysclrf
  else if (imeth==3) then
    !
    !      Dunne's parameterization of nucleation at high h2so4 conc.
    !
    h2so4a=max(h2so4t,ytiny)
    call dunnepar(sclncl,adu,cnh3n,cnh3ch,rkbn,rkbch,rktn,rktch, &
                      dualfa,prion,t,rh,rhoa,h2so4a,cnh3,yscale, &
                      ym3tocm3,ysdunne,ilga,leva)
    ! do il=1,ilga
    !   do l=1,leva
    !     if (h2so4a(il,l)>1.0e10) print '("Large H2SO4A.1:",2i4,es9.2)', il,l,h2so4a(il,l)
    !   end do
    ! end do
    call dunucl(gnucla,h2so4a,sxpt,adu,rkbn,rkbch,rktn, &
                    rktch,dualfa,prion,sclncl,acnd,dfso4,ticgr1,ticgr2, &
                    tidryr,yscale,ym3tocm3,ysdunne,pdiam,pmass,dt, &
                    ilga,leva)
    ! do il=1,ilga
    !   do l=1,leva
    !     if (h2so4a(il,l)>1.0e10) print '("Large H2SO4A.2:",2i4,es9.2)', il,l,h2so4a(il,l)
    !   end do
    ! end do
    anucl=-gnucla/h2so4a**sxpt
    afuna=sgprt-acnd*h2so4a+gnucla
    !
    !       upper/lower bound on equilibrated h2so4 concentration based
    !       on h2so4 mass budget with linearized nucleation term.
    !
    where (h2so4a > ytiny)
      where (afuna <= 0. )
        ctmpy=sgprt
        ctmpx=acnd-gnucla/h2so4a
      else where
        ctmpy=sgprt-(sxpt-1.)*gnucla
        ctmpx=acnd-sxpt*gnucla/h2so4a
      end where
    else where
      ctmpy=sgprt
      ctmpx=acnd
    end where
    h2so4eq = h2so4a+(10.*dt)*sgprt
    where (2 + exponent(ctmpy) - exponent(ctmpx) < maxexponent(ctmpy) .and. ctmpx/=0. )
      h2so4l=min(ctmpy/ctmpx, h2so4eq)
    else where
      h2so4l=h2so4eq
    end where
    where (afuna <= 0. )
      h2so4b=max(min(h2so4l,ysec*h2so4a),yred*h2so4a)
    else where
      h2so4b=max(h2so4l,h2so4a/ysec)
    end where
    sxpt=real(int(sxpt))
    where (afuna > 0. .and. sxpt > ytiny &
        .and. 2+abs(exponent(sgprt) - exponent(anucl)) &
        < maxexponent(sgprt) .and. anucl/=0. )
      ctmp=(sgprt/anucl)**(1./sxpt)
      h2so4b=min(h2so4b,ctmp)
    end where
    h2so4b=max(h2so4b,ytiny)
    !
    !       Dunne's parameterization of nucleation at upper/lower H2SO4 conc.
    !
    call dunucl(gnuclb,h2so4b,sxpt,adu,rkbn,rkbch,rktn, &
                    rktch,dualfa,prion,sclncl,acnd,dfso4,ticgr1,ticgr2, &
                    tidryr,yscale,ym3tocm3,ysdunne,pdiam,pmass,dt, &
                    ilga,leva)
    afunb=sgprt-acnd*h2so4b+gnuclb
    !
    where (afuna*afunb <= 0. )
      h2so4c=.5*(h2so4a+h2so4b)
    else where (afunb > 0. )
      h2so4c=h2so4b+(h2so4b-h2so4a)
    else where (afuna < 0. )
      h2so4c=.5*h2so4a
    end where
    h2so4c=max(h2so4c,ytiny)
    atmn=exp(-10.)
    atmx=exp(10.)
    do inds=1,itern
      !
      !         Dunne's parameterization of nucleation at iteration points.
      !
      call dunucl(gnuclc,h2so4c,sxpt,adu,rkbn,rkbch,rktn, &
                      rktch,dualfa,prion,sclncl,acnd,dfso4,ticgr1,ticgr2, &
                      tidryr,yscale,ym3tocm3,ysdunne,pdiam,pmass,dt, &
                      ilga,leva)
      afunc=sgprt-acnd*h2so4c+gnuclc
      where (afuna*afunc <= 0. )
        h2so4b=h2so4c
        afunb=afunc
        gnuclb=gnuclc
      else where (afunb*afunc <= 0. )
        h2so4a=h2so4c
        afuna=afunc
        gnucla=gnuclc
      end where
      where (afuna*afunb <= 0. )
        h2so4c=.5*(h2so4a+h2so4b)
      else where (afunb > 0. )
        h2so4c=2.*h2so4b
      else where (afuna < 0. )
        h2so4c=.5*h2so4a
      end where
      h2so4c=max(h2so4c,ytiny)
    end do
    !
    !       fit power-law relationship to given h2so4 depletion rates,
    !        -anucl*h2so4**sxp
    !
    gnuclh=gnucla
    gnucll=gnuclb
    h2so4h=h2so4a
    h2so4l=h2so4b
    do i=1,ilga
      do k=1,leva
        if ( (2+abs(exponent(gnuclh(i,k)) - exponent(gnucll(i,k))) < maxexponent(gnuclh(i,k))) .and. (gnucll(i,k)>ytiny8) &
            .and. (2+abs(exponent(h2so4h(i,k)) - exponent(h2so4l(i,k))) < maxexponent(h2so4h(i,k))) .and. (h2so4l(i,k)>ytiny8) &
            .and. (2+abs(exponent(gnuclh(i,k)) - exponent(anucl (i,k))) < maxexponent(gnuclh(i,k))) .and. (h2so4h(i,k)>ytiny8)) &
            then
          if ( (gnuclh(i,k)/gnucll(i,k) > dble(atmn)) .and. (gnuclh(i,k)/gnucll(i,k)) < dble(atmx) .and. &
              (h2so4h(i,k)/h2so4l(i,k) > dble(atmn)) .and. (h2so4h(i,k)/h2so4l(i,k)) < dble(atmx) ) then
            sxp(i,k)   = dlog(gnuclh(i,k)/gnucll(i,k))/dlog(h2so4h(i,k)/h2so4l(i,k))
            anucl(i,k) = -gnuclh(i,k)/h2so4h(i,k)**sxp(i,k)
          else
            sxp  (i,k) = 1.
            anucl(i,k) = 0.
          end if
        else
          sxp  (i,k) = 1.
          anucl(i,k) = 0.
        end if
      end do
    end do
    !
    !        where ( 2+abs(exponent(gnuclh) - exponent(gnucll)) &
    !              < maxexponent(gnuclh) .and. gnucll/=0. &
    !              .and. 2+abs(exponent(h2so4h) - exponent(h2so4l)) &
    !              < maxexponent(h2so4h) .and. h2so4l/=0. &
    !              .and. 2+abs(exponent(gnuclh) - exponent(anucl)) &
    !              < maxexponent(gnuclh) .and. h2so4h>ytiny)
    !          where ( gnuclh/gnucll > atmn .and. gnuclh/gnucll < atmx &
    !                  .and. h2so4h/h2so4l > atmn .and. h2so4h/h2so4l < atmx)
    !            sxp=log(gnuclh/gnucll)/log(h2so4h/h2so4l)
    !            anucl=-gnuclh/h2so4h**sxp
    !          else where
    !            sxp=1.
    !            anucl=0.
    !          end where
    !        else where
    !          sxp=1.
    !          anucl=0.
    !        end where
    !
    !        initial guess of h2so4 concentration.
    !
    ctmp=h2so4c
  end if
  !
  !-----------------------------------------------------------------------
  !     solve nucleation/condensation equation for sulphuric acid.
  !     calculate steady-state solution of the gas-to-particle
  !     conversion equation using the newton-rhapson-method (h2so4 is
  !     in fmol/m3 in the iteration). the equation accounts for production
  !     of gaseous sulphuric acid (+sgpr) and losses due to condensation
  !     onto pre-existing aerosol (-acnd*h2so4) and nucleation of new
  !     particles (-ANUCL*H2SO4**SXP for Kulmala's parameterization
  !     and -(ANUCL+BNUCL*(H2SO4-H2SO4_REF)) for Merikanto's
  !     parameterization).
  !     production/loss rates are in fmol/m3/s. the steady-state
  !     solution (ctmp) is later on used to calculate the approximate
  !     time evolution of the h2so4 conc. according to the prognostic
  !     gas-to-particle conversion equation.
  !     initial guess of equilibrated h2so4 concentration without
  !     taking into account nucleation.
  !
  if (imeth==1) then
    where (2+abs(exponent(sgprt) - exponent(acnd)) &
        < maxexponent(sgprt) .and. acnd/=0. )
      ctmp=sgprt/acnd
    else where
      ctmp=h2so4t
    end where
  end if
  !
  !     initial guess of equilibrated h2so4 concentration from weighted
  !     mean of solutions for very efficient and inefficient condensation
  !     and nucleation.
  !
  if (imeth==1 .or. imeth==3) then
    dbl1=sgprt/ylarge8
    where (anucl > max(dbl1,ytiny8) )
      dbl1=sgprt/anucl
      dbl2=1./sxp
      dbl1=dbl1**dbl2
      ctmpx=dbl1
    else where
      ctmpx=h2so4t
    end where
    ctmpmx=min(ctmp,ctmpx)
    if (imeth==1) then
      ctmp=ywg*ctmpmx
    end if
  end if
  !
  !     option to rescale sulphuric acid concentrations to unity in order
  !     to obtain numerically more robust approach.
  !
  !      yscalf=yscale/min(max(1.e+03,ctmp),1.e+15)
  !      term1=yscalf/yscale
  term1=1.
  !
  fmulf=fmul/term1
  sgprt=sgprt*term1
  h2so4t=h2so4t*term1
  ctmp=ctmp*term1
  ctmpmx=ctmpmx*term1
  if (imeth==1 .or. imeth==3) then
    dbl2=term1
    dbl1=1.-sxp
    dbl3=dbl2**dbl1
    anucl=anucl*dbl3
  else if (imeth==2) then
    do inds=1,idam
      acoef(:,:,inds)=acoef(:,:,inds)*term1(:,:)
    end do
    do inds=1,ida
      ch2so4f(:,:,inds)=ch2so4(inds)*term1(:,:)
      epscf(:,:,inds)=epsc(inds)*term1(:,:)
    end do
  end if
  !
  !     iterations to find equilbrium. the initial value is returned if no
  !     condensation or nucleation. if IMETH==1, Kulmala's parameterization
  !     of nucleation is used. if imeth==2, tabulated nucleation rates
  !     (from merikanto and colleagues) are used.
  !
  if (imeth==1 .or. imeth==3) then
    do inds = 1,iter
      where (ctmp > ytiny8)
        dbl1=ctmp
        dbl2=sxp-1.
        dbl1=dbl1**dbl2
        term1=dbl1
      else where
        term1=0.
      end where
      term3=acnd*ctmp
      term4=anucl*ctmp*term1
      term2=sgprt-term3-term4
      termx=term3+sxp*term4
      termy=term3+sxp**2*term4
      where (2+abs(exponent(termy) - exponent(termx)) &
          < maxexponent(termy) .and. termx/=0. )
        expt=termy/termx
        dbl1=max(1.+expt*term2/termx,0.)
        dbl2=1./expt
        dbl1=dbl1**dbl2
        ctmp=ctmp*dbl1
        ctmp=min(ctmp,ctmpmx)
      end where
    end do
  else
    where (acnd < ytiny)
      ctmp=h2so4t
    else where
      ctmp=sgprt/acnd
    end where
    anucl=0.
    bnucl=0.
    term2=0.
    term3=0.
    term4=0.
    do inds=1,idam
      term1=acnd+bcoef(:,:,inds)
      ctmpx=sgprt-acoef(:,:,inds)+bcoef(:,:,inds)*ch2so4f(:,:,inds)
      where (2+abs(exponent(ctmpx) - exponent(term1)) &
          < maxexponent(ctmpx) .and. term1/=0. )
        ctmpx=ctmpx/term1
      else where
        ctmpx=0.
      end where
      term5=acoef(:,:,inds)
      where (abs(ctmpx-ch2so4f(:,:,inds)) > epscf(:,:,inds) )
        term5=term5+bcoef(:,:,inds)*(ctmpx-ch2so4f(:,:,inds))
      end where
      where (ctmpx >= ch2so4f(:,:,inds) &
          .and. ctmpx <= ch2so4f(:,:,inds+1) &
          .and. abs(term5) > ytiny)
        ctmp=ctmpx
        anucl=acoef(:,:,inds)-bcoef(:,:,inds)*ch2so4f(:,:,inds)
        bnucl=bcoef(:,:,inds)
        term3=acnd*ctmp
        term2=sgprt-term3-term5
        term4=term5
      end where
    end do
    term1=acnd
    ctmpx=sgprt-acoef(:,:,idam) &
                   -bcoef(:,:,idam)*(ch2so4f(:,:,ida)-ch2so4f(:,:,idam))
    where (2+abs(exponent(ctmpx) - exponent(term1)) &
        < maxexponent(ctmpx) .and. term1/=0. )
      ctmpx=ctmpx/term1
    else where
      ctmpx=0.
    end where
    term5=acoef(:,:,idam)
    where (ctmpx > ch2so4f(:,:,ida) )
      term5=term5 &
               +bcoef(:,:,idam)*(ch2so4f(:,:,ida)-ch2so4f(:,:,idam))
    end where
    where (ctmpx > ch2so4f(:,:,ida) .and. abs(term5) > ytiny)
      ctmp=ctmpx
      anucl=term5
      bnucl=0.
      term3=acnd*ctmp
      term2=sgprt-term3-term5
      term4=term5
    end where
  end if
  !
  !     boundary layer nucleation according to merikanto et al. (2009)
  !     \cite merikanto2009. the nucleation rates calculated above are replaced by the
  !     boundary layer nucleation rate (proportional to sulphuric
  !     acid concentration) if the boundary layer nucleation
  !     rate exceeds the rate calculated above (i.e. if sulphuric acid
  !     concentration is lower).
  !
  if (kblncl) then
    ccoefb=2.e-06_r8
    do il=1,ilga
      do l=1,leva
        if (l >= leva+pblts(il) ) then
          ccoef=ccoefb
        else
          ccoef=0.
        end if
        ctmpxa=sgprt(il,l)
        ctmpxb=acnd(il,l)+ccoef
        if (2+abs(exponent(ctmpxa) - exponent(ctmpxb)) &
            < maxexponent(ctmpxa) .and.ctmpxb/=0. ) then
          ctmpxa=ctmpxa/ctmpxb
        else
          ctmpxa=0.
        end if
        if (ctmpxa < ctmp(il,l) ) then
          ctmp(il,l)=ctmpxa
          if (imeth==1 .or. imeth==3) then
            sxp(il,l)=1.
            anucl(il,l)=ccoef
            bnucl(il,l)=0.
          else
            anucl(il,l)=0.
            bnucl(il,l)=ccoef
          end if
          term3(il,l)=acnd(il,l)*ctmpxa
          term2(il,l)=sgprt(il,l)-term3(il,l)-ccoef*ctmpxa
          term4(il,l)=ccoef*ctmpxa
        end if
      end do
    end do
  end if
  !
  !     limit equilibrium value to values which would be reached
  !     after 10 times steps at current gas-phase production rates
  !     and in the absence of condensation and nucleation.
  !
  ctmp=min(ctmp,h2so4t+(10.*dt)*sgprt)
  !
  !     relative budget error for iterative solution.
  !
  termx=0.
  term2=abs(term2)*dt
  termy=max(h2so4t,ctmp)
  where ( (term3 > ytiny8 .or. term4 > ytiny8) .and. &
      2+abs(exponent(term2) - exponent(termy)) &
      < maxexponent(term2) .and.termy/=0. ) termx=term2/termy
  if (maxval(termx) > yeps) ierr=-1
  !
  !-----------------------------------------------------------------------
  !     parameters for time-evolution of sulphuric acid and aerosol
  !     concentrations from relaxation of concentrations to equilibrium
  !     solution. calculations are based on linearization of
  !     gas-to-particle equation around equilibrium solution.
  !
  term1=ctmp
  term2=h2so4t
  effc=.5*(term1+term2)
  pertc=h2so4t-ctmp
  plam=acnd
  if (imeth==1 .or. imeth==3) then
    where (effc > ytiny)
      dbl1=effc
      dbl2=sxp-1.
      dbl1=dbl1**dbl2
      termy=dbl1
    else where
      termy=0.
    end where
    plam=plam+anucl*sxp*termy
  else
    plam=plam+bnucl
  end if
  dbl1=-plam*dt
  dbl2=exp(dbl1)
  expt=dbl2
  !
  !     time-integrated sulphuric acid concentration for condensation
  !     and nucleation calculations (sadt, kg/m3*sec).
  !
  term3=pertc*(expt-1.)
  where (2+abs(exponent(term3) - exponent(plam)) &
      < maxexponent(term3) .and. plam/=0. )
    plfn=term3/plam
    sadt=(ctmp*dt-plfn)*fmulf
  else where
    plfn=-pertc*dt
    sadt=(h2so4t*dt+.5*sgprt*dt**2)*fmulf
  end where
  sadt=max(sadt,0.)
  !
  !     growth factor from time-integration of r*dr/dt. units: m**2.
  !     ysec is the threshold for the adjustment to time step.
  !
  tecorfm=1./ysec
  tecorfa=1./ysec
  if (isextso4 > 0) then
    do is=1,isextso4
      tecgr1(:,:,is)=tecgr1(:,:,is)*sadt(:,:)
      tecgr2(:,:,is)=tecgr2(:,:,is)*sadt(:,:)
      term1=max(tecgr1(:,:,is)+tecgr2(:,:,is)*tedryr(is),ysmall)
      term2=max(tecgr1(:,:,is)+tecgr2(:,:,is)*tedryr(is+1),ysmall)
      tecorf(:,:,is)=ysec*min(tedryr(is)**2/term1(:,:), &
                                  tedryr(is+1)**2/term2(:,:))
    end do
    tecorf=min(1./ysec,tecorf)
    tecorfm=minval(tecorf)
    do l=1,leva
      do il=1,ilga
        tecorfs(:)=tecorf(il,l,:)
        tecorfa(il,l)=minval(tecorfs)
      end do
    end do
  end if
  ticorfm=1./ysec
  ticorfa=1./ysec
  if (isintso4 > 0) then
    do is=1,isintso4
      ticgr1(:,:,is)=ticgr1(:,:,is)*sadt(:,:)
      ticgr2(:,:,is)=ticgr2(:,:,is)*sadt(:,:)
      term1=max(ticgr1(:,:,is)+ticgr2(:,:,is)*tidryr(is),ysmall)
      term2=max(ticgr1(:,:,is)+ticgr2(:,:,is)*tidryr(is+1),ysmall)
      ticorf(:,:,is)=ysec*min(tidryr(is)**2/term1(:,:), &
                                  tidryr(is+1)**2/term2(:,:))
    end do
    ticorf=min(1./ysec,ticorf)
    ticorfm=minval(ticorf)
    do l=1,leva
      do il=1,ilga
        ticorfs(:)=ticorf(il,l,:)
        ticorfa(il,l)=minval(ticorfs)
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     if necessary, adjust time step to limit aerosol growth
  !     if growth rate exceeds critical threshold for treatment
  !     of condensation in subroutine pgrwth.
  !
  dtc=dt
  corfm=min(tecorfm,ticorfm)
  corfar=1.
  if (corfm < 1. ) then
    corfa=min(tecorfa,ticorfa)
    corfar=min(corfa,1.)
    sadt=sadt*corfar
    if (isextso4 > 0) then
      do is=1,isextso4
        tecgr1(:,:,is)=tecgr1(:,:,is)*corfar(:,:)
        tecgr2(:,:,is)=tecgr2(:,:,is)*corfar(:,:)
      end do
    end if
    if (isintso4 > 0) then
      do is=1,isintso4
        ticgr1(:,:,is)=ticgr1(:,:,is)*corfar(:,:)
        ticgr2(:,:,is)=ticgr2(:,:,is)*corfar(:,:)
      end do
    end if
    !
    !       * iterate to obtain reduced time step.
    !
    termx=(ctmp+pertc*expt)*fmulf
    where (termx < ytiny .and. corfa < 1. ) dtc=0.
    term2=0.
    do inds = 1,iter
      term3=pertc*(expt-1.)
      where (corfa < 1. .and. &
          2+abs(exponent(term3) - exponent(plam)) < maxexponent(term3) .and. plam /=0. .and. &
          2+abs(exponent(term2) - exponent(termx)) < maxexponent(term2) .and. termx/=0. )
        plfn=term3/plam
        term2=(ctmp*dtc-plfn)*fmulf-sadt
        dtc=min(max(dtc-term2/termx,0.),dt)
      end where
      dbl1=-plam*dtc
      dbl2=exp(dbl1)
      expt=dbl2
      termx=(ctmp+pertc*expt)*fmulf
    end do
    term3=abs(term2)
    where (2+abs(exponent(term3) - exponent(sadt)) < maxexponent(term3) .and. sadt/=0. )
      termx=term3/sadt
    else where
      termx=0.
    end where
    if (maxval(termx) > yeps) ierr=-2
    term3=pertc*(expt-1.)
    where (2+abs(exponent(term3) - exponent(plam)) < maxexponent(term3) .and. plam/=0. )
      plfn=term3/plam
    else where
      plfn=-pertc*dtc
    end where
  end if
  if (minval(dtc) < dt) ierr=-3
  !
  !-----------------------------------------------------------------------
  !     change in sulphuric acid and aerosol mass from nucleation.
  !     it is assumed that the nucleation aerosol consists of ammonium
  !     sulphate (make sure that this is consistent with nucleation particle
  !     density). dicncl is in kg/kg.
  !
  if (imeth==1 .or. imeth==3) then
    where (ctmp > ytiny)
      dbl1=ctmp
      dbl2=sxp
      dbl1=dbl1**dbl2
      term2=dbl1
    else where
      term2=0.
    end where
    term1=min(-fmulf*anucl*(term2*dtc-sxp*termy*plfn)/rhoa,0.)
  else
    term1=min(-(fmulf*anucl*dtc+bnucl*sadt)/rhoa,0.)
  end if
  where (sadt <= ytiny) term1=0.
  decncl=weight*term1
  dicncl=(1.-weight)*term1
  texncl=decncl*2.*wnh3/wh2so4
  tixncl=dicncl*2.*wnh3/wh2so4
  !
  !     change in aerosol number from nucleation. nncl in 1/kg.
  !
  denncl=0.
  dinncl=0.
  if (isextso4 > 0) denncl=-(decncl+texncl)/pmassex
  if (isintso4 > 0) dinncl=-(dicncl+tixncl)/pmassin
  !
  !-----------------------------------------------------------------------
  !     change in sulphuric acid concentration from condensation.
  !
  if (isextso4 > 0) then
    do is=1,isextso4
      tedccnd(:,:,is)=-tecndgp(:,:,is)*sadt(:,:)/rhoa(:,:)
    end do
    tedxcnd=tedccnd*2.*wnh3/wh2so4
  end if
  if (isintso4 > 0) then
    do is=1,isintso4
      tidccnd(:,:,is)=-ticndgp(:,:,is)*sadt(:,:)/rhoa(:,:)
    end do
    tidxcnd=tidccnd*2.*wnh3/wh2so4
  end if
  !
  !     diagnose condensation rates.
  !
  decnd=0.
  dicnd=0.
  cndm=0.
  if (isextso4 > 0) then
    termx=sum(tedccnd,dim=3)
    cndm=cndm+termx
    decnd=decnd-termx/dt
  end if
  if (isintso4 > 0) then
    termx=sum(tidccnd,dim=3)
    cndm=cndm+termx
    dicnd=dicnd-termx/dt
  end if
  !
  !     adjust condensation rates to make sure that truncation errors
  !     in calculations of condensation rates do not lead to any
  !     negative h2so4 concentrations.
  !
  cndmx=-h2so4i-sgpr*dt
  termx=1.
  where (cndm < cndmx .and. 2+abs(exponent(cndmx) - exponent(cndm)) &
      < maxexponent(cndmx) .and. cndm/=0. )
    termx=cndmx/cndm
    cndm=cndmx
    decnd=decnd*termx
    decnd=-decnd*wamsul/wh2so4
    dicnd=dicnd*termx
    dicnd=-dicnd*wamsul/wh2so4
  end where
  if (isextso4 > 0) then
    do is=1,isextso4
      tedccnd(:,:,is)=tedccnd(:,:,is)*termx(:,:)
      tedxcnd(:,:,is)=tedxcnd(:,:,is)*termx(:,:)
    end do
  end if
  if (isintso4 > 0) then
    do is=1,isintso4
      tidccnd(:,:,is)=tidccnd(:,:,is)*termx(:,:)
      tidxcnd(:,:,is)=tidxcnd(:,:,is)*termx(:,:)
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     particle growth due to condensation/evaporation. the following
  !     calculations provide the updated size distributions for
  !     particle number, sulphur aerosol mass, and mass of other
  !     aerosol species after the condensation step.
  !
  dersn=0.
  dersm=0.
  if (isextso4 > 0) then
    call pgrpar (tedp0l,tedp1l,tedp2l,tedp3l,tedp4l,tedp5l,tedp6l, &
                     tedm1l,tedm2l,tedm3l,tedp0r,tedp1r,tedp2r,tedp3r, &
                     tedp4r,tedp5r,tedp6r,tedm1r,tedm2r,tedm3r,tedp0s, &
                     tedp1s,tedp2s,tedp3s,tedp4s,tedp5s,tedp6s,tedm1s, &
                     tedm2s,tedm3s,tei0l,tei0r,tecgr1,tecgr2,ten0, &
                     tedryrt,tephist,tedphit,tephi0,tepsi,ilga,leva, &
                     isextso4,ierr,imodc)
    call pgrwth0(tent,dersn,tei0l,tei0r,tedp0l,tedp0r,tedp0s, &
                     ten0,ilga,leva,isextso4)
    tefrc(:,:,:)=1.
    call pgrwths(temt,dersm,tefrc,tei0l,tei0r, &
                     tedp0l,tedp1l,tedp2l,tedp3l,tedm1l,tedm2l,tedm3l, &
                     tedp0r,tedp1r,tedp2r,tedp3r,tedm1r,tedm2r,tedm3r, &
                     tedp0s,tedp1s,tedp2s,tedp3s,tedm1s,tedm2s,tedm3s, &
                     tecgr1,tecgr2,ten0,teddn,ilga,leva,isextso4)
  end if
  dirsn=0.
  dirsms=0.
  dirsmn=0.
  if (isintso4 > 0) then
    call pgrpar (tidp0l,tidp1l,tidp2l,tidp3l,tidp4l,tidp5l,tidp6l, &
                     tidm1l,tidm2l,tidm3l,tidp0r,tidp1r,tidp2r,tidp3r, &
                     tidp4r,tidp5r,tidp6r,tidm1r,tidm2r,tidm3r,tidp0s, &
                     tidp1s,tidp2s,tidp3s,tidp4s,tidp5s,tidp6s,tidm1s, &
                     tidm2s,tidm3s,tii0l,tii0r,ticgr1,ticgr2,tin0, &
                     tidryrt,tiphist,tidphit,tiphi0,tipsi,ilga,leva, &
                     isintso4,ierr,imodc)
    call pgrwth0(tint,dirsn,tii0l,tii0r,tidp0l,tidp0r,tidp0s, &
                     tin0,ilga,leva,isintso4)
    call pgrwths(timst,dirsms,tifrcs,tii0l,tii0r, &
                     tidp0l,tidp1l,tidp2l,tidp3l,tidm1l,tidm2l,tidm3l, &
                     tidp0r,tidp1r,tidp2r,tidp3r,tidm1r,tidm2r,tidm3r, &
                     tidp0s,tidp1s,tidp2s,tidp3s,tidm1s,tidm2s,tidm3s, &
                     ticgr1,ticgr2,tin0,tiddn,ilga,leva,isintso4)
    if (iso4s > 0) then
      tirsmn=0.
      call pgrwthn(timnt,tirsmn,tifrcn,tii0l,tii0r,tidp3l,tidp3r, &
                       tidp3s,tin0,tiddn,ilga,leva,iso4s,isintso4)
      dirsmn=sum(tirsmn,dim=3)
    end if
  end if
  !
  !-----------------------------------------------------------------------
  !     number and mass fixers. this corrects for any truncation
  !     errors that may have occurred previously. the approach is
  !     to scale the total number and mass concentrations so that the
  !     final total mass will be consistent with the corresponding
  !     physico-chemical production and loss rates.
  !
  term1=0.
  term2=0.
  term3=0.
  term4=0.
  if (isextso4 > 0) then
    tent=max(tent,0.)
    temt=max(temt,0._r8)
    term1=term1+sum(temt,dim=3)+dersm
    term2=term2+sum(tent,dim=3)+dersn
    term3=term3+sum(tem ,dim=3)
    term4=term4+sum(ten ,dim=3)
  end if
  if (isintso4 > 0) then
    tint=max(tint,0.)
    timst=max(timst,0._r8)
    term1=term1+sum(timst,dim=3)+dirsms
    term2=term2+sum(tint ,dim=3)+dirsn
    term3=term3+sum(tims ,dim=3)
    term4=term4+sum(tin  ,dim=3)
    if (iso4s > 0) then
      titm1=sum(timnt,dim=3)+tirsmn
      titm2=sum(timn ,dim=3)
    end if
  end if
  !
  do l=1,leva
    do il=1,ilga
      !
      !       correct aerosol number concentration.
      !
      if (2+abs(exponent(term4(il,l)) - exponent(term2(il,l))) &
          < maxexponent(term4(il,l)) .and.term2(il,l)/=0. ) then
        cfn=term4(il,l)/term2(il,l)
        if (isextso4 > 0) tent(il,l,:)=tent(il,l,:)*cfn
        if (isintso4 > 0) tint(il,l,:)=tint(il,l,:)*cfn
      end if
      !
      !       correct sulphur aerosol mass concentration.
      !
      term6=term3(il,l)-cndm(il,l)*wamsul/wh2so4
      if (2+abs(exponent(term6) - exponent(term1(il,l))) &
          < maxexponent(term6) .and. term1(il,l)/=0. ) then
        cfm=term6/term1(il,l)
        if (isextso4 > 0) temt (il,l,:)=temt (il,l,:)*cfm
        if (isintso4 > 0) timst(il,l,:)=timst(il,l,:)*cfm
      end if
    end do
  end do
  !
  !     correct non-sulphate aerosol mass concentration.
  !
  if (isintso4 > 0) then
    do it=1,iso4s
      do l=1,leva
        do il=1,ilga
          if (2+abs(exponent(titm2(il,l,it)) - exponent(titm1(il,l,it))) &
              < maxexponent(titm2(il,l,it)) .and. titm1(il,l,it)/=0. ) then
            cfm=titm2(il,l,it)/titm1(il,l,it)
            timnt(il,l,:,it)=timnt(il,l,:,it)*cfm
          end if
        end do
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     total aerosol mass for internally mixed aerosol species.
  !
  if (kintso4 > 0) then
    timt=timst
    do it=1,iso4s
      timt(:,:,:)=timt(:,:,:)+timnt(:,:,:,it)
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     sulphuric acid tendency.
  !
  term2=decncl+dicncl
  termx=h2so4i+sgpr*dt+cndm+term2
  !
  !     in case the new h2so4 concentration is negative, try to
  !     adjust the nucleation rate so that the result is nil.
  !
  term1=1.
  term3=-h2so4i-sgpr*dt-cndm
  where (termx < -ytiny .and. &
      2+abs(exponent(term3) - exponent(term2)) < maxexponent(term3) &
      .and.term2/=0. )
    termx=0.
    term1=term3/term2
    decncl=term1*decncl
    texncl=term1*texncl
    denncl=term1*denncl
    dicncl=term1*dicncl
    tixncl=term1*tixncl
    dinncl=term1*dinncl
  end where
  dgdt=(termx-h2so4i)/dt
  !
  !-----------------------------------------------------------------------
  !     change for smallest particles due to nucleation.
  !
  is=1
  if (isextso4 > 0) then
    tent (:,:,is)=tent (:,:,is)+denncl
    decncl=decncl+texncl
    temt (:,:,is)=temt (:,:,is)-decncl
    denncl= denncl/dt
    decncl=-decncl/dt
  end if
  if (isintso4 > 0) then
    tint (:,:,is)=tint (:,:,is)+dinncl
    dicncl=dicncl+tixncl
    timst(:,:,is)=timst(:,:,is)-dicncl
    timt (:,:,is)=timt (:,:,is)-dicncl
    dinncl= dinncl/dt
    dicncl=-dicncl/dt
  end if
  !
  !-----------------------------------------------------------------------
  !     tracer tendencies.
  !
  if (kextso4 > 0) then
    do is=1,isextso4
      pedndt(:,:,iexso4(is))=(tent(:,:,is)-ten(:,:,is))/dt
      pedmdt(:,:,iexso4(is))=(temt(:,:,is)-tem(:,:,is))/dt
    end do
  end if
  if (kintso4 > 0) then
    do is=1,isintso4
      pidndt(:,:,iinso4(is))=(tint(:,:,is)-tin(:,:,is))/dt
      pidmdt(:,:,iinso4(is))=(timt(:,:,is)-tim(:,:,is))/dt
      where (timt(:,:,is) > ytiny)
        pidfdt(:,:,iinso4(is),kintso4)= &
                                            (timst(:,:,is)/timt(:,:,is)-tifrcs(:,:,is))/dt
      else where
        pidfdt(:,:,iinso4(is),kintso4)=0.
      end where
      do it=1,iso4s
        where (timt(:,:,is) > ytiny)
          pidfdt(:,:,iinso4(is),itso4(it))= &
                                                (timnt(:,:,is,it)/timt(:,:,is)-tifrcn(:,:,is,it))/dt
        else where
          pidfdt(:,:,iinso4(is),itso4(it))=0.
        end where
      end do
    end do
  end if
  !
  !     * deallocation.
  !
  if (isextso4 > 0) then
    deallocate(tewetrc)
    deallocate(ten)
    deallocate(tent)
    deallocate(tem)
    deallocate(temt)
    deallocate(teddn)
    deallocate(ten0)
    deallocate(tephi0)
    deallocate(tepsi)
    deallocate(tect1)
    deallocate(tect2)
    deallocate(tecgr1)
    deallocate(tecgr2)
    deallocate(tecndgp)
    deallocate(tedccnd)
    deallocate(tedxcnd)
    deallocate(tedphit)
    deallocate(tephist)
    deallocate(tecorf)
    deallocate(tei0l)
    deallocate(tei0r)
    deallocate(tedp0l)
    deallocate(tedp1l)
    deallocate(tedp2l)
    deallocate(tedp3l)
    deallocate(tedp4l)
    deallocate(tedp5l)
    deallocate(tedp6l)
    deallocate(tedm1l)
    deallocate(tedm2l)
    deallocate(tedm3l)
    deallocate(tedp0r)
    deallocate(tedp1r)
    deallocate(tedp2r)
    deallocate(tedp3r)
    deallocate(tedp4r)
    deallocate(tedp5r)
    deallocate(tedp6r)
    deallocate(tedm1r)
    deallocate(tedm2r)
    deallocate(tedm3r)
    deallocate(tedp0s)
    deallocate(tedp1s)
    deallocate(tedp2s)
    deallocate(tedp3s)
    deallocate(tedp4s)
    deallocate(tedp5s)
    deallocate(tedp6s)
    deallocate(tedm1s)
    deallocate(tedm2s)
    deallocate(tedm3s)
    deallocate(tefrc)
    deallocate(tedryrt)
    deallocate(tecorfs)
    deallocate(tedryr)
    deallocate(tedryrc)
  end if
  if (isintso4 > 0) then
    deallocate(timn)
    deallocate(timnt)
    deallocate(tifrcn)
    deallocate(tims)
    deallocate(timst)
    deallocate(tiwetrc)
    deallocate(tin)
    deallocate(tint)
    deallocate(tim)
    deallocate(timt)
    deallocate(tiddn)
    deallocate(tin0)
    deallocate(tiphi0)
    deallocate(tipsi)
    deallocate(tict1)
    deallocate(tict2)
    deallocate(ticgr1)
    deallocate(ticgr2)
    deallocate(ticndgp)
    deallocate(tidccnd)
    deallocate(tidxcnd)
    deallocate(tidphit)
    deallocate(tiphist)
    deallocate(ticorf)
    deallocate(tii0l)
    deallocate(tii0r)
    deallocate(tidp0l)
    deallocate(tidp1l)
    deallocate(tidp2l)
    deallocate(tidp3l)
    deallocate(tidp4l)
    deallocate(tidp5l)
    deallocate(tidp6l)
    deallocate(tidm1l)
    deallocate(tidm2l)
    deallocate(tidm3l)
    deallocate(tidp0r)
    deallocate(tidp1r)
    deallocate(tidp2r)
    deallocate(tidp3r)
    deallocate(tidp4r)
    deallocate(tidp5r)
    deallocate(tidp6r)
    deallocate(tidm1r)
    deallocate(tidm2r)
    deallocate(tidm3r)
    deallocate(tidp0s)
    deallocate(tidp1s)
    deallocate(tidp2s)
    deallocate(tidp3s)
    deallocate(tidp4s)
    deallocate(tidp5s)
    deallocate(tidp6s)
    deallocate(tidm1s)
    deallocate(tidm2s)
    deallocate(tidm3s)
    deallocate(tifrcs)
    deallocate(tidryrt)
    deallocate(tirsmn)
    deallocate(titm1)
    deallocate(titm2)
    deallocate(ticorfs)
    deallocate(tidryr)
    deallocate(tidryrc)
  end if
  deallocate(tecorfa)
  deallocate(ticorfa)
  deallocate(texncl)
  deallocate(tixncl)
  deallocate(gnucla)
  deallocate(gnuclb)
  deallocate(gnuclc)
  deallocate(gnucll)
  deallocate(gnuclh)
  deallocate(afuna)
  deallocate(afunb)
  deallocate(afunc)
  deallocate(anucl)
  deallocate(bnucl)
  deallocate(sxp)
  deallocate(sxpt)
  deallocate(ctmp)
  deallocate(acnd)
  deallocate(plfn)
  deallocate(cndm)
  deallocate(cndmx)
  deallocate(effc)
  deallocate(termx)
  deallocate(termy)
  deallocate(terms)
  deallocate(term1)
  deallocate(term2)
  deallocate(term3)
  deallocate(term4)
  deallocate(term5)
  deallocate(sgprt)
  deallocate(h2so4t)
  deallocate(h2so4a)
  deallocate(h2so4b)
  deallocate(h2so4c)
  deallocate(h2so4l)
  deallocate(h2so4h)
  deallocate(h2so4eq)
  deallocate(dfso4)
  deallocate(frp)
  deallocate(plam)
  deallocate(pertc)
  deallocate(corfa)
  deallocate(corfar)
  deallocate(dtc)
  deallocate(expt)
  deallocate(sadt)
  deallocate(ctmpx)
  deallocate(ctmpy)
  deallocate(ctmpmx)
  deallocate(fmulf)
  !      deallocate(yscalf)
  deallocate(ch2so4f)
  deallocate(epscf)
  deallocate(dbl1)
  deallocate(dbl2)
  deallocate(dbl3)
  deallocate(cnh3n)
  deallocate(cnh3ch)
  deallocate(rkbn)
  deallocate(rkbch)
  deallocate(rktn)
  deallocate(rktch)
  deallocate(dualfa)
  deallocate(prion)
  deallocate(sclncl)
  !
end subroutine gtpcnv
!
!> \file
!! \subsection ssec_details Details
!! \n
!! It is critical that the dry particle densities (PEDDN,
!! PIDDN) and mass fractions for internally mixed aerosol (PIFRC)
!! are consistent with the aerosol mass (PEMAS, PIMAS) and number
!! (PENUM, PINUM) concentrations. Make sure these are calculated
!! before this subroutine.
!!
!! PRIMARY INPUT/OUTPUT VARIABLES (ARGUMENT LIST):
!! |In/Out | Name   | Details                                                  |
!! |:------|:-------|:---------------------------------------------------------|
!! | I     | CNH3   |  GAS-PHASE AMMONIA (NH3) VOLUME MIXING RATIO|
!! | O     | DGDT   |  GAS-PHASE SULPHURIC ACID (H2SO4) TENDENCY (KG/KG/SEC)|
!! | O     | DECNCL |  (NH4)2SO4 MASS NUCLEATION RATE (KG/KG/SEC), EXT. MIXTURE|
!! | O     | DECND  |  (NH4)2SO4 CONDENSATION RATE (KG/KG/SEC), EXT. MIXTURE|
!! | O     | DENNCL |  (NH4)2SO4 NUMBER NUCLEATION RATE (1/KG/SEC), EXT. MIXTURE|
!! | O     | DERSM  |  MASS RESIDUUM FROM NUMERICAL TRUNCATION IN GROWTH CALCULATIONS (KG/KG), EXT. MIXTURE|
!! | O     | DERSN  |  NUMBER RESIDUUM FROM NUMERICAL TRUNCATION IN GROWTH CALCULATIONS (KG/KG), EXT. MIXTURE|
!! | O     | DICNCL |  (NH4)2SO4 MASS NUCLEATION RATE (KG/KG/SEC), INT. MIXTURE|
!! | O     | DICND  |  (NH4)2SO4 CONDENSATION RATE (KG/KG/SEC), INT. MIXTURE|
!! | O     | DINNCL |  (NH4)2SO4 NUMBER NUCLEATION RATE (1/KG/SEC), INT. MIXTURE|
!! | O     | DIRSMN |  MASS RESIDUUM FOR NON-(NH4)2SO4 AEROSOL SPECIES FROM NUM. TRUNCATION IN GROWTH CALCULATIONS (KG/KG), INT. MIXTURE|
!! | O     | DIRSMS |  MASS RESIDUUM FOR (NH4)2SO4 AEROSOL FROM NUM. TRUNCATION IN GROWTH CALCULATIONS (KG/KG), INT. MIXTURE|
!! | O     | DIRSN  |  NUMBER RESIDUUM FROM NUMERICAL TRUNCATION IN GROWTH CALCULATIONS (KG/KG), INT. MIXTURE|
!! | I     | DT     |  TIME STEP (SEC)|
!! | I     | H2SO4I |  INITIAL H2SO4 MIXING RATIO (KG/KG)|
!! | O     | IERR   |  WARNING CODE INDEX|
!! | I     | ILGA   |  NUMBER OF GRID POINTS IN HORIZONTAL DIRECTION|
!! | I     | LEVA   |  NUMBER OF GRID POINTA IN VERTICAL DIRECTION|
!! | I     | PBLTS  |  OFFSET INDEX FOR VERTICAL AXIS, INDICATING TOPMOST POINT IN BOUNDARY LAYER|
!! | I     | PEDDN  |  DENSITY OF DRY AEROSOL PARTICLE (KG/M3), EXT. MIXTURE|
!! | O     | PEDMDT |  MASS TENDENCY (KG/KG/SEC), EXT. MIXTURE|
!! | O     | PEDNDT |  NUMBER TENDENCY (1/KG/SEC), EXT. MIXTURE|
!! | I     | PEMAS  |  AEROSOL MASS CONCENTRATION (KG/KG), EXT. MIXTURE|
!! | I     | PEN0   |  1ST PLA SIZE DISTRIBUTION PARAMETER (AMPLITUDE), EXT. MIXTURE|
!! | I     | PENUM  |  AEROSOL NUMBER CONCENTRATION (1/KG), EXT. MIXTURE|
!! | I     | PEPHI0 |  3RD PLA SIZE DISTRIBUTION PARAMETER (MODE SIZE), EXT. MIXTURE|
!! | I     | PEPSI  |  2ND PLA SIZE DISTRIBUTION PARAMETER (WIDTH), EXT. MIXTURE|
!! | I     | PEWETRC|  TOTAL/WET PARICLE RADIUS (M), EXT. MIXTURE|
!! | I     | PIDDN  |  DENSITY OF DRY AEROSOL PARTICLE (KG/M3), INT. MIXTURE|
!! | O     | PIDFDT |  AEROSOL SPECIES MASS FRACTION TENDENCY (1/SEC), INT. MIXTURE|
!! | O     | PIDMDT |  MASS TENDENCY (KG/KG/SEC), INT. MIXTURE|
!! | O     | PIDNDT |  NUMBER TENDENCY (1/KG/SEC), INT. MIXTURE|
!! | I     | PIFRC  |  AEROSOL SPECIES MASS FRACTION|
!! | I     | PIMAS  |  AEROSOL MASS CONCENTRATION (KG/KG), INT. MIXTURE|
!! | I     | PIN0   |  1ST PLA SIZE DISTRIBUTION PARAMETER (AMPLITUDE), INT. MIXTURE|
!! | I     | PINUM  |  AEROSOL NUMBER CONCENTRATION (1/KG), INT. MIXTURE|
!! | I     | PIPHI0 |  3RD PLA SIZE DISTRIBUTION PARAMETER (MODE SIZE), INT. MIXTURE|
!! | I     | PIPSI  |  2ND PLA SIZE DISTRIBUTION PARAMETER (WIDTH), INT. MIXTURE|
!! | I     | PIWETRC|  TOTAL/WET PARICLE RADIUS (M), INT. MIXTURE|
!! | I     | RH     |  RELATIVE HUMIDITY (DIMENSIONLESS)|
!! | I     | RHOA   |  AIR DENSITY (KG/M3)|
!! | I     | SGPR   |  GAS-PHASE PRODUCTION RATE OF H2SO4 (KG/KG/SEC)|
!! | I     | T      |  AIR TEMPERATURE (K)|
!! SECONDARY INPUT/OUTPUT VARIABLES (VIA MODULES AND COMMON BLOCKS):
!! |In/Out | Name                  | Details                                                  |
!! |:------|:----------------------|:---------------------------------------------------------|
!! | I     | AEXTF%TP(:)%DRYRC(:)  | DRY PARTICLE RADIUS IN SECTION CENTRE (M), EXT. MIXTURE|
!! | I     | AEXTF%TP(:)%DRYR(:)%VL| LOWER DRY PARTICLE RADIUS FOR EACH SECTION (M), EXT. MIXTURE|
!! | I     | AEXTF%TP(:)%DRYR(:)%VR| UPPER DRY PARTICLE RADIUS FOR EACH SECTION (M), EXT. MIXTURE|
!! | I     | AINTF%DRYRC(:)        | DRY PARTICLE RADIUS IN SECTION CENTRE (M), INT. MIXTURE|
!! | I     | AINTF%DRYR(:)%VL      | LOWER DRY PARTICLE RADIUS FOR EACH SECTION (M), INT. MIXTURE|
!! | I     | AINTF%DRYR(:)%VR      | UPPER DRY PARTICLE RADIUS FOR EACH SECTION (M), INT. MIXTURE|
!! | I     | IEXSO4                | AEROSOL TRACER INDEX FOR SULPHATE, EXT. MIXTURE|
!! | I     | IINSO4                | AEROSOL TRACER INDEX FOR SULPHATE, INT. MIXTURE|
!! | I     | ISAEXT                | NUMBER OF AEROSOL TRACERS, EXT. MIXTURE|
!! | I     | ISAINT                | NUMBER OF AEROSOL TRACERS, INT. MIXTURE|
!! | I     | ISEXTSO4              | NUMBER OF SECTIONS FOR (NH4)2SO4 AEROSOL, EXT. MIXTURE|
!! | I     | ISINTSO4              | NUMBER OF SECTIONS FOR (NH4)2SO4 AEROSOL, INT. MIXTURE|
!! | I     | ISO4S                 | NUMBER OF NON-(NH4)2SO4 AEROSOL SPECIES, INT. MIXTURE|
!! | I     | KEXTSO4               | SPECIES INDEX FOR (NH4)2SO4 AEROSOL, EXT. MIXTURE|
!! | I     | KINT                  | NUMBER OF INTERNALLY MIXED AEROSOL TYPES, INT. MIXTURE|
!! | I     | KINTSO4               | SPECIES INDEX FOR (NH4)2SO4 AEROSOL, INT. MIXTURE|
!! | I     | PEDPHIS               | SECTION WIDTH, EXT. MIXTURE|
!! | I     | PEPHISS               | LOWER DRY PARTICLE SIZE (=LOG(R/R0)), EXT. MIXTURE|
!! | I     | PIDPHIS               | SECTION WIDTH, INT. MIXTURE|
!! | I     | PIPHISS               | LOWER DRY PARTICLE SIZE (=LOG(R/R0)), INT. MIXTURE|
!! | I     | WH2SO4                | MOLECULAR WEIGHT OF H2SO4 (KG/MOL)|
!! | I     | WNH3                  | MOLECULAR WEIGHT OF NH4 (KG/MOL)|
!! | I     | YLARGE                | LARGE NUMBER|
!! | I     | YSEC                  | SECURITY PARAMETER FOR ADJUSTMENT OF TIMESTEP|
!! | I     | YSMALL                | SMALL NUMBER|
!! | I     | YTINY                 | SMALLEST VALID NUMBER|
