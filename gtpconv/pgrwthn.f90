!> \file
!> \brief Particle growth from condensation of aerosol mass for passive
!>       aerosol species without change in species mass.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine pgrwthn (tmas,resm,frc,i0l,i0r,dp3l,dp3r, &
                          dp3s,pn0,drydn,ilga,leva,itrs,isec)
  !
  use sdparm, only : ycnst,ytiny
  use fpdef,  only : r8
  !
  implicit none
  !
  real(r8), intent(out), dimension(ilga,leva,isec,itrs) :: tmas !< Mass size distributionMass size distribution
  real, intent(inout), dimension(ilga,leva,itrs) :: resm !< Mass residuum from numerical truncation in growth calculations
  real, intent(in), dimension(ilga,leva,isec,itrs) :: frc !< Fraction
  real, intent(in), dimension(ilga,leva,isec) :: pn0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$)
  real, intent(in), dimension(ilga,leva,isec) :: drydn !< Density of dry aerosol particle \f$[kg/m^3]\f$
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp3l !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp3r !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp3s !<
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isec !< Number of separate aerosol tracers
  integer, intent(in), dimension(ilga,leva,isec) :: i0l !< Left-hand side part of the modified section
  integer, intent(in), dimension(ilga,leva,isec) :: i0r !< Right-hand side part of the modified section
  !
  !     internal work variables
  !
  integer :: il !<
  integer :: is !<
  integer :: isi !<
  integer :: it !<
  integer :: itrs !<
  integer :: l !<
  real :: c0 !<
  !-----------------------------------------------------------------------
  !     * initialization.
  !
  tmas=0.
  !
  !-----------------------------------------------------------------------
  !     calculate number and mass in each section.
  !
  do it=1,itrs
    do isi=1,isec
      do l=1,leva
        do il=1,ilga
          !
          !        calculate contribution in each section from the left-hand
          !        side part of the modified section.
          !
          is=i0l(il,l,isi)
          if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
              ) then
            !
            !          mass size distribution from
            !          m(phi(t))=f*(r(t=0))**3*n(phi(t=0))
            !
            c0=pn0(il,l,isi)*drydn(il,l,isi)*frc(il,l,isi,it)*ycnst
            tmas(il,l,is,it)=tmas(il,l,is,it)+c0*dp3l(il,l,isi)
          end if
          !
          !        calculate contribution in each section from the right-hand
          !        side part of the modified section.
          !
          is=i0r(il,l,isi)
          if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
              .and. i0l(il,l,isi) /= is) then
            !
            !          mass size distribution from
            !          m(phi(t))=f*(r(t=0))**3*n(phi(t=0))
            !
            c0=pn0(il,l,isi)*drydn(il,l,isi)*frc(il,l,isi,it)*ycnst
            tmas(il,l,is,it)=tmas(il,l,is,it)+c0*dp3r(il,l,isi)
          end if
        end do
      end do
    end do
    !
    !-----------------------------------------------------------------------
    !       residuum due to particles growing to sizes larger than
    !       the spectrum cutoff.
    !
    do isi=1,isec
      do l=1,leva
        do il=1,ilga
          if ( (i0r(il,l,isi) == (isec+1) .or. i0l(il,l,isi) == 0) &
              .and. pn0(il,l,isi) > ytiny) then
            !
            !          mass size distribution from
            !          m(phi(t))=f*(r(t=0))**3*n(phi(t=0))
            !
            c0=pn0(il,l,isi)*drydn(il,l,isi)*frc(il,l,isi,it)*ycnst
            resm(il,l,it)=resm(il,l,it)+c0*dp3s(il,l,isi)
          end if
        end do
      end do
    end do
  end do
  !
end subroutine pgrwthn
!> \file
!! \subsection ssec_details Details
!! It is assumed that sections initially have the same size and that
!! growth leads to a an equal or smaller width of each section.
!! the subroutine treats growth in particle size space based on
!! a Lagrangian approach.
