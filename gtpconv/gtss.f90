!> \file
!> \brief Gas-to-particle conversion for sea salt.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine gtss (dso4dt,dsoadt,peddn,pen0,penum,pephi0,pepsi, &
                        pewetrc,t,rhoa,h2so4i,soapi,sgpr,ogpr,dt, &
                        ilga,leva,cnds,cndo)
  !
  use sdparm, only : aextf,iexss,isaext,isextss,kextss, &
                         pedphis,pephiss,r0,wamsul,wh2so4,ylarge,ysec,ysmall,ytiny
  use fpdef,  only : r8
  !
  implicit none
  !
  real, intent(out), dimension(ilga,leva) :: dso4dt !< Gas-phase sulphuric acid (H2SO4) tendency \f$[kg/kg/sec]\f$
  real, intent(out), dimension(ilga,leva) :: dsoadt !< Gas-phase soa aerosol prrecursor (SOAP) tendency \f$[kg/kg/sec]\f$
  real, intent(out), dimension(ilga,leva) :: cndo !< SOA precursor gas condensation rate \f$[kg/kg/sec]\f$
  real, intent(out), dimension(ilga,leva) :: cnds !< (NH4)2SO4 condensation rate \f$[kg/kg/sec]\f$
  real, intent(in), dimension(ilga,leva) :: sgpr !< Gas-phase production rate of H2SO4 \f$[kg/kg/sec]\fi$
  real, intent(in), dimension(ilga,leva) :: ogpr !< Gas-phase production rate of SOA precursor gases \f$[kg/kg/sec]\f$
  real, intent(in), dimension(ilga,leva) :: t !< Air temperature \f$ [K]\f$
  real, intent(in), dimension(ilga,leva) :: rhoa !< Air density \f$[kg/m3]\f$
  real, intent(in), dimension(ilga,leva) :: h2so4i !< Initial H2SO4 mixing ratio \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva) :: soapi !< Initial SOA precursor mixing ratio \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: peddn !< (Dry) density for externally mixed aerosol particles
  !< \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,
  !< amplitude) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: penum !< Aerosol number concentration for externally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,
  !< width), ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$,
  !< mode size) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pewetrc !< Total/wet paricle radius \f$[m]\f$, ext. mixture
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  !
  !     internal work variables
  !
  real(r8), allocatable, dimension(:,:,:) :: tecndgp !<
  real(r8), allocatable, dimension(:,:,:) :: tect1 !<
  real(r8), allocatable, dimension(:,:,:) :: tect2 !<
  real, allocatable, dimension(:,:,:) :: tewetrc !<
  real, allocatable, dimension(:,:,:) :: teddn !<
  real, allocatable, dimension(:,:,:) :: tecgr1 !<
  real, allocatable, dimension(:,:,:) :: tecgr2 !<
  real, allocatable, dimension(:,:,:) :: ten0 !<
  real, allocatable, dimension(:,:,:) :: tephi0 !<
  real, allocatable, dimension(:,:,:) :: tepsi !<
  real, allocatable, dimension(:,:,:) :: tedphit !<
  real, allocatable, dimension(:,:,:) :: tephist !<
  real, allocatable, dimension(:,:,:) :: tedccs !<
  real, allocatable, dimension(:,:,:) :: tedcco !<
  real, allocatable, dimension(:,:,:) :: ten !<
  real(r8), allocatable, dimension(:,:) :: dfso4 !<
  real(r8), allocatable, dimension(:,:) :: dbl1 !<
  real(r8), allocatable, dimension(:,:) :: dbl2 !<
  real(r8), allocatable, dimension(:,:) :: acnd !<
  real, allocatable, dimension(:,:) :: plfn !<
  real, allocatable, dimension(:,:) :: cndms !<
  real, allocatable, dimension(:,:) :: cndmsx !<
  real, allocatable, dimension(:,:) :: termx !<
  real, allocatable, dimension(:,:) :: term1 !<
  real, allocatable, dimension(:,:) :: term2 !<
  real, allocatable, dimension(:,:) :: frp !<
  real, allocatable, dimension(:,:) :: plam !<
  real, allocatable, dimension(:,:) :: pertc !<
  real, allocatable, dimension(:,:) :: expt !<
  real, allocatable, dimension(:,:) :: sadts !<
  real, allocatable, dimension(:,:) :: sadto !<
  real, allocatable, dimension(:,:) :: ctmps !<
  real, allocatable, dimension(:,:) :: ctmpo !<
  real, allocatable, dimension(:,:) :: cndmo !<
  real, allocatable, dimension(:,:) :: cndmox !<
  real, allocatable, dimension(:) :: tedryr !<
  real, allocatable, dimension(:) :: tedryrc !<
  real :: dt !<
  real :: wrat !<
  integer :: is !<
  !
  !-----------------------------------------------------------------------
  !     * allocate memory for local fields.
  !
  if (isextss > 0) then
    allocate(tewetrc(ilga,leva,isextss))
    allocate(ten    (ilga,leva,isextss))
    allocate(teddn  (ilga,leva,isextss))
    allocate(ten0   (ilga,leva,isextss))
    allocate(tephi0 (ilga,leva,isextss))
    allocate(tepsi  (ilga,leva,isextss))
    allocate(tect1  (ilga,leva,isextss))
    allocate(tect2  (ilga,leva,isextss))
    allocate(tecgr1 (ilga,leva,isextss))
    allocate(tecgr2 (ilga,leva,isextss))
    allocate(tecndgp(ilga,leva,isextss))
    allocate(tedcco (ilga,leva,isextss))
    allocate(tedccs (ilga,leva,isextss))
    allocate(tedphit(ilga,leva,isextss))
    allocate(tephist(ilga,leva,isextss))
    allocate(tedryr (isextss+1))
    allocate(tedryrc(isextss))
  end if
  allocate(ctmpo  (ilga,leva))
  allocate(ctmps  (ilga,leva))
  allocate(acnd   (ilga,leva))
  allocate(plfn   (ilga,leva))
  allocate(cndms  (ilga,leva))
  allocate(cndmsx (ilga,leva))
  allocate(cndmo  (ilga,leva))
  allocate(cndmox (ilga,leva))
  allocate(termx  (ilga,leva))
  allocate(term1  (ilga,leva))
  allocate(term2  (ilga,leva))
  allocate(dfso4  (ilga,leva))
  allocate(frp    (ilga,leva))
  allocate(plam   (ilga,leva))
  allocate(pertc  (ilga,leva))
  allocate(expt   (ilga,leva))
  allocate(sadto  (ilga,leva))
  allocate(sadts  (ilga,leva))
  allocate(dbl1   (ilga,leva))
  allocate(dbl2   (ilga,leva))
  !
  !     temporary aerosol fields for sea salt aerosol for dry
  !     and wet particle sizes for externally mixed aerosol type.
  !
  if (kextss > 0) then
    do is=1,isextss
      tewetrc(:,:,is)=pewetrc(:,:,iexss(is))
      ten    (:,:,is)=penum  (:,:,iexss(is))
      teddn  (:,:,is)=peddn  (:,:,iexss(is))
      ten0   (:,:,is)=pen0   (:,:,iexss(is))
      tephi0 (:,:,is)=pephi0 (:,:,iexss(is))
      tepsi  (:,:,is)=pepsi  (:,:,iexss(is))
      tedphit(:,:,is)=pedphis(iexss(is))
      tephist(:,:,is)=pephiss(iexss(is))
      tedryrc(is)=aextf%tp(kextss)%dryrc(is)
      tedryr(is)=aextf%tp(kextss)%dryr(is)%vl
    end do
    tedryr(isextss+1)=aextf%tp(kextss)%dryr(isextss)%vr
  end if
  !
  !-----------------------------------------------------------------------
  !     diffusivity of h2so4, mean free path, and rate coefficients
  !     for condensation of gases onto pre-existing aerosol
  !     (cndgp, in 1/s).
  !
  term1=sqrt(t)
  term1=term1*t
  dfso4 = 2.150e-09_r8*term1
  dbl1=1.62864e-25/t
  dbl2=sqrt(dbl1)
  frp = 3.372999311e+11_r8*dfso4*dbl2
  wrat=wamsul/wh2so4
  acnd=0.
  if (isextss > 0) then
    call grwtrm(tecndgp,tecgr1,tecgr2,tect1,tect2,ten0,ten,tewetrc, &
                    tedryrc,tedryr,teddn,tephi0,tepsi,tephist,tedphit, &
                    frp,dfso4,wrat,rhoa,leva,ilga,isextss)
    acnd=acnd+sum(tecndgp,dim=3)
  end if
  !
  !-----------------------------------------------------------------------
  !     parameters for time-evolution of gas concentrations
  !     to equilibrium solution (ctmp).
  !
  ctmps=sgpr/max(acnd,ytiny)
  ctmpo=ogpr/max(acnd,ytiny)
  plam=acnd
  dbl1=-plam*dt
  dbl2=exp(dbl1)
  expt=dbl2
  !
  !     time-integrated h2so4 concentration (sadts).
  !
  pertc=h2so4i-ctmps
  term2=pertc*(expt-1.)
  where (2+abs(exponent(term2) - exponent(plam)) &
      < maxexponent(term2) .and. plam/=0. )
    plfn=term2/plam
    sadts=ctmps*dt-plfn
  else where
    sadts=h2so4i*dt+.5*sgpr*dt**2
  end where
  sadts=max(sadts,0.)
  !
  !     time-integrated soa concentration (sadto).
  !
  pertc=soapi-ctmpo
  term2=pertc*(expt-1.)
  where (2+abs(exponent(term2) - exponent(plam)) &
      < maxexponent(term2) .and. plam/=0. )
    plfn=term2/plam
    sadto=ctmpo*dt-plfn
  else where
    sadto=soapi*dt+.5*ogpr*dt**2
  end where
  sadto=max(sadto,0.)
  !
  !-----------------------------------------------------------------------
  !     change in gas concentration from condensation.
  !
  if (isextss > 0) then
    do is=1,isextss
      tedccs(:,:,is)=-tecndgp(:,:,is)*sadts(:,:)/rhoa(:,:)
      tedcco(:,:,is)=-tecndgp(:,:,is)*sadto(:,:)/rhoa(:,:)
    end do
  end if
  !
  !     diagnose condensation rates.
  !
  cnds=0.
  cndms=0.
  if (isextss > 0) then
    termx=sum(tedccs,dim=3)
    cndms=cndms+termx
    cnds=cnds-termx/dt
  end if
  cndo=0.
  cndmo=0.
  if (isextss > 0) then
    termx=sum(tedcco,dim=3)
    cndmo=cndmo+termx
    cndo=cndo-termx/dt
  end if
  !
  !     adjust condensation rates to make sure that truncation errors
  !     in calculations of condensation rates do not lead to any
  !     negative gas concentrations.
  !
  cndmsx=-h2so4i-sgpr*dt
  where (cndms < cndmsx .and. 2+abs(exponent(cndmsx) - exponent(cndms)) &
      < maxexponent(cndmsx) .and. cndms/=0. )
    termx=cndmsx/cndms
    cndms=cndmsx
    cnds=cnds*termx
  end where
  cndmox=-soapi-ogpr*dt
  where (cndmo < cndmox .and. 2+abs(exponent(cndmox) - exponent(cndmo)) &
      < maxexponent(cndmox) .and. cndmo/=0. )
    termx=cndmox/cndmo
    cndmo=cndmox
    cndo=cndo*termx
  end where
  !
  !-----------------------------------------------------------------------
  !     gas tendencies.
  !
  dso4dt=(sgpr*dt+cndms)/dt
  dsoadt=(ogpr*dt+cndmo)/dt
  !
  !     * deallocation.
  !
  if (isextss > 0) then
    deallocate(tewetrc)
    deallocate(ten)
    deallocate(teddn)
    deallocate(ten0)
    deallocate(tephi0)
    deallocate(tepsi)
    deallocate(tect1)
    deallocate(tect2)
    deallocate(tecgr1)
    deallocate(tecgr2)
    deallocate(tecndgp)
    deallocate(tedcco)
    deallocate(tedccs)
    deallocate(tedphit)
    deallocate(tephist)
    deallocate(tedryr)
    deallocate(tedryrc)
  end if
  deallocate(ctmpo)
  deallocate(ctmps)
  deallocate(acnd)
  deallocate(plfn)
  deallocate(cndms)
  deallocate(cndmsx)
  deallocate(cndmo)
  deallocate(cndmox)
  deallocate(termx)
  deallocate(term1)
  deallocate(term2)
  deallocate(dfso4)
  deallocate(frp)
  deallocate(plam)
  deallocate(pertc)
  deallocate(expt)
  deallocate(sadto)
  deallocate(sadts)
  deallocate(dbl1)
  deallocate(dbl2)
  !
end subroutine gtss
!
!> \file
!! \subsection ssec_details Details
!! PRIMARY INPUT/OUTPUT VARIABLES (ARGUMENT LIST):
!! |In/Out | Name   | Details                                                  |
!! |:------|:-------|:---------------------------------------------------------|
!! | O     | DSO4DT |  GAS-PHASE SULPHURIC ACID (H2SO4) TENDENCY (KG/KG/SEC)|
!! | O     | DSOADT |  GAS-PHASE SOA AEROSOL PRRECURSOR (SOAP) TENDENCY (KG/KG/SEC)|
!! | O     | CNDO   |  SOA PRECURSOR GAS CONDENSATION RATE (KG/KG/SEC)|
!! | O     | CNDS   |  (NH4)2SO4 CONDENSATION RATE (KG/KG/SEC)|
!! | I     | DT     |  TIME STEP (SEC)|
!! | I     | H2SO4I |  INITIAL H2SO4 MIXING RATIO (KG/KG)|
!! | I     | ILGA   |  NUMBER OF GRID POINTS IN HORIZONTAL DIRECTION|
!! | I     | LEVA   |  NUMBER OF GRID POINTA IN VERTICAL DIRECTION|
!! | I     | OGPR   |  GAS-PHASE PRODUCTION RATE OF SOA PRECURSOR GASES (KG/KG/SEC)|
!! | I     | PEDDN  |  DENSITY OF DRY AEROSOL PARTICLE (KG/M3), EXT. MIXTURE|
!! | I     | PEN0   |  1ST PLA SIZE DISTRIBUTION PARAMETER (AMPLITUDE), EXT. MIXTURE|
!! | I     | PENUM  |  AEROSOL NUMBER CONCENTRATION (1/KG), EXT. MIXTURE|
!! | I     | PEPHI0 |  3RD PLA SIZE DISTRIBUTION PARAMETER (MODE SIZE), EXT. MIXTURE|
!! | I     | PEPSI  |  2ND PLA SIZE DISTRIBUTION PARAMETER (WIDTH), EXT. MIXTURE|
!! | I     | PEWETRC|  TOTAL/WET PARICLE RADIUS (M), EXT. MIXTURE|
!! | I     | RHOA   |  AIR DENSITY (KG/M3)|
!! | I     | SGPR   |  GAS-PHASE PRODUCTION RATE OF H2SO4 (KG/KG/SEC)|
!! | I     | SOAPI  |  INITIAL SOA PRECURSOR GAS MIXING RATIO (KG/KG)|
!! | I     | T      |  AIR TEMPERATURE (K)|
!! SECONDARY INPUT/OUTPUT VARIABLES (VIA MODULES AND COMMON BLOCKS):
!! |In/Out | Name                  | Details                                                  |
!! |:------|:----------------------|:---------------------------------------------------------|
!! | I     | AEXTF%TP(:)%DRYRC(:)  | DRY PARTICLE RADIUS IN SECTION CENTRE (M), EXT. MIXTURE|
!! | I     | AEXTF%TP(:)%DRYR(:)%VL| LOWER DRY PARTICLE RADIUS FOR EACH SECTION (M), EXT. MIXTURE|
!! | I     | AEXTF%TP(:)%DRYR(:)%VR| UPPER DRY PARTICLE RADIUS FOR EACH SECTION (M), EXT. MIXTURE|
!! | I     | IEXSS                 | AEROSOL TRACER INDEX FOR SEA SALT EXT. MIXTURE|
!! | I     | ISAEXT                | NUMBER OF AEROSOL TRACERS, EXT. MIXTURE|
!! | I     | ISEXTSS               | NUMBER OF SECTIONS FOR SEA SALT AEROSOL, EXT. MIXTURE|
!! | I     | KEXTSS                | SPECIES INDEX FOR SEA SALT AEROSOL, EXT. MIXTURE|
!! | I     | PEDPHIS               | SECTION WIDTH, EXT. MIXTURE|
!! | I     | PEPHISS               | LOWER DRY PARTICLE SIZE (=LOG(R/R0)), EXT. MIXTURE|
!! | I     | YLARGE                | LARGE NUMBER|
!! | I     | YSEC                  | SECURITY PARAMETER FOR ADJUSTMENT OF TIMESTEP|
!! | I     | YSMALL                | SMALL NUMBER|
!! | I     | YTINY                 | SMALLEST VALID NUMBER|
